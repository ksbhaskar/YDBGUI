<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#"  onclick="app.link('stats/index')">Statistics</a>

## See it in action: how to run the demo report

---

In order to show how statistics collection works we created a demo report that you can load and run in a matter of seconds.

If you have the GUI installed on a working system (with database traffic) you simply need to start collecting data.

If you have the GUI installed on a dummy system (like Docker) you can run our stress program (`do go^%ydbguiStatsGenerator`) to emulate traffic.

### Step-by-step instruction on how to acquire and display data:

- Start the Statistics  <img src="md/stats/img/demo-open-stats.png" width="200">
- Load the Demo report <img src="md/stats/img/demo-load-report.png" width="300">
- Start acquiring data samples by clicking on the `record` button (the left-hand highlighted button) and stop the acquisition with the `stop` button (the right-hand highlighted button)
  <img src="md/stats/img/demo-overview.png" width="500">

### Instructions to use our internal stress program

In order to run the stress program you need to enter the M direct mode:

`$ydb_dist/yottadb -dir`

At the prompt, type in:

`do go^%ydbguiStatsGenerator`

The following prompt will appear:

````js
Random Stats Generator 1.0.1
How many processes you want to run (press ^ to quit) ? 3
Enter the lower rate in ms.: 1000
Enter the higher rate in ms.: 2000

Press R to run, ^ to quit: r

````

The following parameters are requested:

- The number of processes you want to run:
  - how many processes will be used for the stress
- The lower rate and higher rate in ms.:
  - this is the repeat rate range.
  - A random number between the two values will be used to `hang` the process once a burst have been generated
  - While hanging, the process will hold a lock, which will be released when the hang is over.
  - A suggested value for basic testing can be 1000 and 5000 (one and 5 seconds).
  - In order to generate more lock failures, specify a high number of processes (20 to 100) and a short range (low: 250, high: 750)

When you run the program you will see the following:

````
Random Stats Generator 1.0.1
How many processes you want to run (press ^ to quit) ? 5
Enter the lower rate in ms.: 1000
Enter the higher rate in ms.: 3000

Press R to run, ^ to quit: r

Starting...
Process: 2459 started...
Process: 2461 started...
Process: 2463 started...
Process: 2465 started...
Process: 2467 started...

Started

Press Q to quit:
````

To terminate all the processes, simply type Q(uit):

````
Press Q to quit: q
Process 2459 terminated with return code: 0
Process 2461 terminated with return code: 0
Process 2463 terminated with return code: 0
Process 2465 terminated with return code: 0
Process 2467 terminated with return code: 0

Random Stats Generator 1.0.1
How many processes you want to run(press ^ to quit ) ?
````

If you want to exit to the M command line, type ^C
If you want to go back to the shell, type ^<ENTER>
