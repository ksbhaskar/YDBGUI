<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## <a href="#" onclick="app.link('stats/spark-chart/index')">Report definition</a> / Graph

---

<img src="md/stats/spark-chart/img/graph.png" width="600">

The Graph dialog allows you to change the graph properties.

A real-time preview (filled with random data) will be updated on each setting changes, allowing you to preview your changes.

<img src="md/stats/spark-chart/img/graph-preview-chart.png" width="400">

Individual series appearance and Y-axis can be configured on the left pane.

<img src="md/stats/spark-chart/img/graph-series-list.png" width="250">

It will give you access to the <a href="#" onclick="app.link('stats/spark-chart/graph-series')">Graph series dialog</a>.

---

#### Global settings

The global settings can be changed using the <img src="md/stats/spark-chart/img/graph-global-settings-btn.png" width="150"> button.

These settings include global colors and fonts.

---

#### Shadow

The shadow can be changed using the <img src="md/stats/spark-chart/img/graph-shadow-btn.png" width="150"> button.

---

#### Title

The title can be changed using the <img src="md/stats/spark-chart/img/graph-title-btn.png" width="150"> button.

The title text gets automatically filled with the region, but can be overridden.

---

#### Sub title

The sub title can be changed using the <img src="md/stats/spark-chart/img/graph-sub-title-btn.png" width="150"> button.

The sub title text gets automatically filled with the processes, but can be overridden.

---

#### Grid

The grid can be changed using the <img src="md/stats/spark-chart/img/graph-grid-btn.png" width="150"> button.

---

#### Legend

The legend can be changed using the <img src="md/stats/spark-chart/img/graph-legend-btn.png" width="150"> button.

---

#### Timescale

The timescale can be changed using the <img src="md/stats/spark-chart/img/graph-timescale-btn.png" width="150"> button.

---

#### (Common) Y axis

The y-axis can be changed using the <img src="md/stats/spark-chart/img/graph-y-axis-btn.png" width="150"> button.

This setting has effect only for shared y-axis. Individual y-axis can be configured in the <a href="#" onclick="app.link('stats/spark-chart/graph-series')">Graph series dialog</a>.

---

#### Toolbar

The toolbar can be changed using the <img src="md/stats/spark-chart/img/graph-toolbar-btn.png" width="300"> button.

---

#### Tooltips

The tooltips can be changed using the <img src="md/stats/spark-chart/img/graph-tooltips-btn.png" width="300"> button.
