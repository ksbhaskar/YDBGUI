<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('stats/index')">Statistics</a>

## <a href="#" onclick="app.link('stats/spark-chart/index')">Report definition</a> / Mapping dialog

---

<img src="md/stats/spark-chart/img/mapping-dialog.png" width="600">

In the mapping dialog you choose which sample data you want to display in this report section.

For each sample, you can choose between the following statistical data:

- **Abs** (absolute): the actual counter value from the YottaDB Statistics Database...
- **Δ** (delta): the difference between the current absolute sample value and the previous one
- **Δ Min** (delta minimum): the minimum delta (across the all sampled data)
- **Δ Max** (delta maximum): the maximum delta (across the all sampled data)
- **Δ Avg** (delta average): the average delta value (across the all sampled data)

There is no limit in the number of selected fields, but you have to keep in mind that too many fields on a graph may make the graph unreadable.

To quickly select/deselect all fields for a specific statistical value, you can check/uncheck the header checkbox:

<img src="md/stats/spark-chart/img/mapping-header.png" width="400">

> When changing an existing mapping selection, the items you uncheck will be removed from the target (graph or table)
> automatically.
