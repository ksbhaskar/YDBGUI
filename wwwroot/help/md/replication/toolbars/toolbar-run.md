<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

# <a href="#" onclick="app.link('replication/toolbars/toolbars')">Toolbars</a>

## Run toolbar

---

The Design toolbar is used to modify the appearance of the interactive chart.

The following actions can be taken:

- Switch to the Design toolbar
- Load
- Save
- Refresh time

---

## <img src="md/replication/toolbars/img/run-switch.png" width="50"> Switch

This icon switches to the Design toolbar.

---

## <img src="md/replication/toolbars/img/run-load.png" width="50"> Load

This icon opens the Load dialog.

---

## <img src="md/replication/toolbars/img/run-save.png" width="50"> Save

This icon opens the Save dialog.

---

## <img src="md/replication/toolbars/img/run-refresh-on.png" width="250"> Refresh

This drop down sets the refresh time of the health / backlog status.

When on, the green LED will flash indicating sample collection activity.

When off, the green LED will be disabled.

<img src="md/replication/toolbars/img/run-refresh-off.png" width="150">

---
