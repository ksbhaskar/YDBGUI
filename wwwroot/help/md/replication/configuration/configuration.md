<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## Configuration

---

In order to view the Replication Topology; you need to first configure the browser to let it know where the servers are, what protocol the servers use and, optionally, the path to an SSL certificate.

> Before proceeding, it is important that all the <a href="#" onclick="app.link('replication/pre-requisites')">pre-requisites</a> are satisfied and configured.

Once you are sure that all the YottaDB Web Servers are installed, configured and tested, you can proceed with the configuration.

---

### Before you start

Before starting configuring, it is handy to have all the required information ready to be filled.

> Here you can find an overview of all the settings: <a href="#overview-of-all-the-replication-settings">Overview of all the Replication settings</a>

---

### Start by locating the settings in the Preferences dialog

You can edit the Topology configuration by opening the menu option:

`Database Administration > Preferences`

and selecting `Replication`

<img src="md/replication/configuration/img/prefs-replication-node.png" width="150"> 

---

### Configuring the servers

The servers can be configured here:

<img src="md/replication/configuration/img/prefs-server-config.png" width="750"> 

The following fields are available:

- Instance name
- Host
- GUI Port
- GUI Statistics port
- Protocol
- Certificate Chain location
- Verify certificates

`Instance name`

This entry must be filled with the Instance name specified in the .repl file.

`host`

This entry must be filled with the path of the web server (with no protocol and no port).

Both IP numbers and host names are valid.

`GUI port`

The port number assigned to the YottaDB-Web-Server.

`GUI Statistics port` (optional)

The port number assigned to the YottaDB-Web-Server statistics. This is currently not used, but ready for future extensions of the Statistics module.

`Protocol`

A choice between `http` and `https`.

`Certificate Chain location` (optional, used only in https)

If you use `https` as transport protocol, you must save the .pem certificate (or a certificate chain) in the parent server to a location where the M-Web-Server has read access. Specify the absolute path of the location here.

`Verify certificates` (optional, used only in https)

If you use `https` as transport protocol and, you can disable certificate verification.

> You must enter ALL the servers in your replication, or they won't be discovered...

<br>

---

### Practical server configuration example

Using the layout proposed in the <a href="#" onclick="app.link('replication/pre-requisites')">pre-requisites</a>, we can configure the servers as following:

<img src="md/replication/configuration/img/layout-config.png" width="450"> 

- **Server 0**
  - `Instance name`: melbourne
  - `Host`: 33.0.0.100
  - `GUI Port`: 8089
  - `Protocol`: http
- **Server 1**
  - `Instance name`: chicago
  - `Host`: 168.0.0.25
  - `GUI Port`: 8089
  - `Protocol`: http
- **Server 2**
  - `Instance name`: tampa1
  - `Host`: 27.2.0.18
  - `GUI Port`: 8089
  - `Protocol`: http
- **Server 3**
  - `Instance name`: tampa2
  - `Host`: 27.2.0.19
  - `GUI Port`: 9500
  - `Protocol`: http

<br>

---

### Additional servers parameters

Additional server parameters can be configured in the `Replication/Discovery service/Timeouts` node.

Two parameters are available:

- `Connection timeout`: the time (in milliseconds) that the Discovery Service will wait until it will declare the instance "DEAD"
- `Response timeout`: the time (in milliseconds) that the Discovery Service will wait until it gets a response from the server.

> The `Response timeout` should be set to a minimum of the "maximum number of nodes in a branch + 1" MULTIPLIED by the "Connection timeout"
> If the `Connection Timeout` is set to 500 milliseconds and you have a maximum of 4 servers in a branch, you should set this value to not less than:
> (4 + 1) * 500 ms = 2500 ms
> The reason is that if the software crawls into the 4 nodes and they all timeout, it won't return to the caller before 2 seconds.
> By setting a value to 2 seconds or less, you will get a timeout even if the machine is alive (only its children are dead), giving you a false negative.

> The default values are valid for most of the cases, but connection timeout could be increased for low-latency lines.

<br>

--- 

### Configuring the dashboard

The dashboard has an (optional) backlog indicator that will display, on hover, the current backlog values from the receiver (if available) and all of the sources.

The option `Dashboard/Refresh backlog` allows you to set the refresh time (in seconds). If you choose for **off**, the entire Backlog pill won't be displayed.

The option `Dashboard/Process range` allows you to choose the behaviour of the backlog coloring:

- Selecting `No` it will display the backlog always in dark color
- Selecting `Yes` it will display the following:
  - if the current backlog sample is LOWER than the previous one, the pill background will have the GREEN color and the popup will display the backlog value in dark color
  - If the current backlog sample is EQUAL or HIGHER than the previous sample, the pill background will have the AMBER color and the popup will display the backlog value in red color

<br>

---

### Configuring the Topology

The topology parameters controls:

- The refresh time of the backlog and health status
- The default appearance of the initial Topology interactive chart and the undo buffer size

##### Topology/Default layout orientation

The `Default layout orientation` defines how the interactive chart gets drawn.

Two options are available:

- Top to Bottom
- Left to right

##### Topology/Default refresh value

The `Default refresh value` defines how often the interactive chart gets updated with Health and Backlog information.

The following options are available:

- off (this is a static chart)
- 2 seconds (the default value)
- 5 seconds
- 10 seconds
- 20 seconds
- 30 seconds
- 1 minute
- 5 minutes
- 15 minutes

##### Topology/Undo buffer size

The `Undo buffer size` sets the maximum number of entries in the Undo buffer.

##### Topology/Links/Default connector

The `Default connector` specifies the default connector to be used to link the instances.

The following options are available:

- Straight (default)
- Curve
- Smooth

> You may need to experiment which value best suits your configuration.

##### Topology/Links/Default router

The `Default router` specifies the default router to be used to link the instances.

The following options are available:

- Normal (default)
- Manhattan
- Metro
- Orthogonal

> You will need to experiment which value best suits your configuration.

##### Topology/Backlog/Process range

The `Process range` specifies how the backlog gets colored.

- Selecting `No` will display the backlog always in dark color
- Selecting `Yes` will display the following:
  - if the current backlog sample is LOWER than the previous one, the backlog will be displayed in a dark color
  - If the current backlog sample is EQUAL to or HIGHER than the previous sample, the backlog will be displayed in red color

> In the interactive chart the backlog background color has also a meaning. Please, refer to the Interactive chart help page for a complete description.

<br>

--- 

### Overview of all the Replication settings

- Dashboard
  - `Refresh backlog`
  - `Process range`
- Discovery service
  - Servers
    - Timeouts
      - `Connection timeout`
      - `Response timeout`
    - 0 through 15:
      - `Instance name`
      - `Host`
      - `GUI Port`
      - `GUI Statistics port`
      - `Protocol`
      - `Certificate Chain location`
      - `Verify certificate`
- Topology
  - `Default layout orientation`
  - `Default refresh value`
  - `Undo buffer size`
  - Links
    - `Default connector`
    - `Default router`
  - Backlog
    - `Process range`

---

### Sharing the configuration between browsers

In order to share the configuration between browsers, you can Export the settings to a file and then Import them.

> In order to export: Database Administration / Replication / Configuration / Export...

> In order to import: Database Administration / Replication / Configuration / Import...

<img src="md/replication/configuration/img/export-import.png" width="400">
