<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# <a href="#" onclick="app.link('replication/index')">Replication</a>

## <a href="#" onclick="app.link('replication/dynamic/index')">Dynamic mode: monitoring the Replication instances</a>

## Instances and Links description

---

# The Instances

The instances appear, in the interactive chart, as a rounded-border rectangle.

Depending on the instance role (Primary, Secondary, Supplementary), different colors and fields are used.

## Primary

<img src="md/replication/dynamic/img/primary.png" width="400"> 

The Primary instance does NOT have the Update and Receiver processes health indicators, only the Source servers (the instances it is replicating to).

The Data access is always RW (Read / Write)

<br>

---

## Secondary (BC Replica)

<img src="md/replication/dynamic/img/secondary.png" width="400"> 

A Secondary instance has:

- An Update process
- A Receiver server
- The Source servers

health indicators.

<br>

---

## Supplementary (SI Replica)

<img src="md/replication/dynamic/img/supplementary.png" width="400"> 

A Supplementary instance has:

- An Update process
- A Receiver server
- The Source servers

health indicators; it is colored differently than a Secondary instance.

Depending on whether it's the first Supplementary instance or not, it may be RO (Read-Only) or RW (Read-Write).

---

# Instance health

In a working situation, the health status is GREEN.

By hovering over the Health status, we can see the related process id:

<img src="md/replication/dynamic/img/popup-source-servers.png" width="300">&emsp;&emsp;&emsp;&emsp;<img src="md/replication/dynamic/img/popup-update-process.png" width="270">

<br>

---

### Instance errors

When there is a problem (the related process is not running anymore / is dead) it is RED:

<img src="md/replication/dynamic/img/instance-error.png" width="300">

<br>

If one or more Sources is in error, you get the following:

<img src="md/replication/dynamic/img/instance-source-error.png" width="300">

<br>

and the popup tell you where the error is:

<img src="md/replication/dynamic/img/popup-source-error.png" width="300">

<br>

---

### Instance machine down

When a machine is DOWN or the GUI Web server is not available anymore, the machine gets grayed out and the error is displayed in the body of the Instance rectangle:

<img src="md/replication/dynamic/img/instance-down.png" width="300">

The text will display one of the following errors:

- `Can not connect to server`: we can not connect to the YottaDB Web server
- `Internal error occurred`: an internal error occurred in the YottaDB Web server
- `Replication not running`: the replication services are not running
- `Unknown`: the machine status in unknown, as the parent is not reachable
- `Bad REST response`: the GUI Web server returned a malformed response
- `SSL error`: an SSL error occured (only when TLS is configured)
- `Bad TLS file`: the provided TLS file cound not be located (only when TLS is configured)

If the faulty machine has children, we automatically mark all of them with status unknown.

<img src="md/replication/dynamic/img/instance-down-tree.png" width="600">

<br>

---

# The Links (and related backlog counters)

The links display different information about the connection between two instances:

- The connection between the two machines, which can be:
  - Active
  - Machine is dead or in error
  - Replication is down
- The backlog on the source, which can be active, inactive, dead
- The backlog on the destination, which can be active, inactive, dead

<br>

---

### Link when connection is ACTIVE

When the connection between the machines is Active, the line is GREEN.

<img src="md/replication/dynamic/img/link-green.png" width="400">

<br>

---

### Link when the machine is DEAD

When the parent can not reach the child, or gets an error from the web server, the line is BLACK and broken.

<img src="md/replication/dynamic/img/link-black.png" width="400">

<br>

---

### Link when the machine is in ERROR

When the machine replication services are in error, but the server is responding correctly, the line is RED and broken.

<img src="md/replication/dynamic/img/link-red.png" width="400">

<br>

---

### Backlog indicators / counters

The backlog indicator / counters display the following:

- The background color is:
  - GREEN if the YDB Replication service handling this node is alive and operational and the backlog equals to 0
  - LIGHT YELLOW if the YDB Replication service handling this node is alive and operational and the backlog is greater than 0 (there is activity)
  - RED if the YDB Replication service handling this node has errors OR the machine is dead
- The counter displays:
  - How many entries are in the buffer
  - The color depends on the setting: Replication/Topology/ Backlog/Process range in the Preferences dialog
    - If set to No, then the color is always dark (YDB purple)
    - If set to Yes, then:
      - If the value is 0 or the current sample is lower than the previous sample, it is the dark color (YDB purple)
      - If the value didn't change (and it is > 0) or it is greater than the previous sample, then it is RED
