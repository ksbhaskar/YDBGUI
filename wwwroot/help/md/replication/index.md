<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# Replication

---

### <a href="#" onclick="app.link('replication/pre-requisites')">Pre-requisites</a>

### <a href="#" onclick="app.link('replication/configuration/configuration')">Configuration</a>

### <a href="#" onclick="app.link('replication/dynamic/index')">Dynamic mode: monitoring the Replication instances</a>

### <a href="#" onclick="app.link('replication/modify-topology')">Modifying, saving and recalling the Topology interactive chart</a>

---

**The Replication Topology generates an interactive chart based on your replication topology.**

It displays all the instances found and the link between them.

Each instance displays:

- the instance name
- the health status of:
  - the receiver (if present)
  - the updater (if present)
  - the sources
- the machine status (running, down, errored, etc.)

Each link displays:

- The backlog value of the sources
- The backlog value of the receiver

<img src="md/replication/img/main-screen.png" width="900"> 

---

