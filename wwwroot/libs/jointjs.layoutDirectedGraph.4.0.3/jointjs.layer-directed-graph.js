/*! JointJS LayoutDirectedGraph v4.0.3 (2024-04-10) - LayoutDirectedGraph module for JointJS


This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
!function (e, t) {"object" == typeof exports && "undefined" != typeof module ? t(exports, require("@dagrejs/dagre"), require("@dagrejs/graphlib"), require("@joint/core")) : "function" == typeof define && define.amd ? define(["exports", "@dagrejs/dagre", "@dagrejs/graphlib", "@joint/core"], t) : t(((e = "undefined" != typeof globalThis ? globalThis : e || self).joint = e.joint || {}, e.joint.layout = e.joint.layout || {}), e.dagre, e.graphlib, e.joint)}(this, function (e, t, i, c) {
    "use strict";

    function n(i) {
        var n = Object.create(null);
        return i && Object.keys(i).forEach(function (e) {
            var t;
            "default" !== e && (t = Object.getOwnPropertyDescriptor(i, e), Object.defineProperty(n, e, t.get ? t : {enumerable: !0, get: function () {return i[e]}}))
        }), n.default = i, Object.freeze(n)
    }

    var a = n(t), h = n(i);
    const s = {
        exportElement: function (e) {return e.size()}, exportLink: function (e) {
            var t = e.get("labelSize") || {};
            return {minLen: e.get("minLen") || 1, weight: e.get("weight") || 1, labelpos: e.get("labelPosition") || "c", labeloffset: e.get("labelOffset") || 0, width: t.width || 0, height: t.height || 0}
        }, importElement: function (e, t, i) {
            var n = this.getCell(t), i = i.node(t);
            e.setPosition ? e.setPosition(n, i) : n.set("position", {x: i.x - i.width / 2, y: i.y - i.height / 2})
        }, importLink: function (e, t, i) {
            var n, r, o = this.getCell(t.name), i = i.edge(t), t = i.points || [], a = new c.g.Polyline(t);
            (e.setVertices || e.setLinkVertices) && (c.util.isFunction(e.setVertices) ? e.setVertices(o, t) : (a.simplify({threshold: .001}), r = (n = a.points.map(e => e.toJSON())).length, o.set("vertices", n.slice(1, r - 1)))), e.setLabels && "x" in i && "y" in i && (n = {
                x: i.x,
                y: i.y
            }, c.util.isFunction(e.setLabels) ? e.setLabels(o, n, t) : (r = a.closestPointLength(n), i = a.pointAtLength(r), e = r / a.length(), t = new c.g.Point(n).difference(i).toJSON(), o.label(0, {position: {distance: e, offset: t}})))
        }, layout: function (e, t) {
            var i = e instanceof c.dia.Graph ? e : (new c.dia.Graph).resetCells(e, {dry: !0, sort: !1}), n = (t = c.util.defaults(t || {}, {resizeClusters: !(e = null), clusterPadding: 10, exportElement: this.exportElement, exportLink: this.exportLink}), s.toGraphLib(i, {directed: !0, multigraph: !0, compound: !0, setNodeLabel: t.exportElement, setEdgeLabel: t.exportLink, setEdgeName: function (e) {return e.id}})), e = {}, r = t.marginX || 0, o = t.marginY || 0,
                e = (t.rankDir && (e.rankdir = t.rankDir), t.align && (e.align = t.align), t.nodeSep && (e.nodesep = t.nodeSep), t.edgeSep && (e.edgesep = t.edgeSep), t.rankSep && (e.ranksep = t.rankSep), t.ranker && (e.ranker = t.ranker), r && (e.marginx = r), o && (e.marginy = o), n.setGraph(e), a.layout(n, {debugTiming: !!t.debugTiming}), i.startBatch("layout"), s.fromGraphLib(n, {
                    importNode: this.importElement.bind(i, t),
                    importEdge: this.importLink.bind(i, t)
                }), t.resizeClusters && (e = n.nodes().filter(function (e) {return 0 < n.children(e).length}).map(i.getCell.bind(i)).sort(function (e, t) {return t.getAncestors().length - e.getAncestors().length}), c.util.invoke(e, "fitToChildren", {padding: t.clusterPadding})), i.stopBatch("layout"), n.graph());
            return new c.g.Rect(r, o, Math.abs(e.width - 2 * r), Math.abs(e.height - 2 * o))
        }, fromGraphLib: function (t, i) {
            var n = (i = i || {}).importNode || c.util.noop, r = i.importEdge || c.util.noop, o = this instanceof c.dia.Graph ? this : new c.dia.Graph;
            return t.nodes().forEach(function (e) {n.call(o, e, t, o, i)}), t.edges().forEach(function (e) {r.call(o, e, t, o, i)}), o
        }, toGraphLib: function (e, t) {
            for (var i = c.util.pick(t = t || {}, "directed", "compound", "multigraph"), n = new h.Graph(i), r = t.setNodeLabel || c.util.noop, o = t.setEdgeLabel || c.util.noop, a = t.setEdgeName || c.util.noop, s = e.get("cells"), l = 0, d = s.length; l < d; l++) {
                var g = s.at(l);
                if (g.isLink()) {
                    var p = g.get("source"), u = g.get("target");
                    if (!p.id || !u.id) break;
                    n.setEdge(p.id, u.id, o(g), a(g))
                } else n.setNode(g.id, r(g)), n.isCompound() && g.has("parent") && (p = g.get("parent"), s.has(p)) && n.setParent(g.id, p)
            }
            return n
        }
    };
    e.DirectedGraph = s
});
