/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const puppeteer = require('puppeteer');
const { PuppeteerScreenRecorder } = require('puppeteer-screen-recorder');
const { expect } = require('chai');
const _ = require('lodash');
const globalVariables = _.pick(global, ['browser', 'expect']);
const serverPort = 8089;
const libs = require('./libs');
const {env} = require('process');

// puppeteer options
const opts = {
  headless: true,
  ignoreHTTPSErrors: true,
  timeout: 180000,
  args: ['--no-sandbox']
};

before (async function () {
  global.expect = expect;

  global.browser = await puppeteer.launch(opts);
  global.page = await global.browser.newPage();
  //global.recorder = new PuppeteerScreenRecorder(page);
  await page.setViewport({width: 1024, height: 768});

  global.MDevPort = serverPort;
  global.protocol = process.env.protocol;
  //await recorder.start('./wwwroot/test/results/' + Date.now() + '.mp4');

  global.serverMode = await libs.getServerMode();
  global.charMode = env.ydb_chset
  console.log('*******************************')
  console.log('Server mode: ' + global.serverMode)
  console.log('Char mode: ', env.ydb_chset)
  console.log('*******************************')

  global.ydbRelease = env.ydb_rel;
});

// close browser and reset global variables
after (async function () {
  //await recorder.stop();
  await browser.close();
  global.browser = globalVariables.browser;
  global.expect = globalVariables.expect;
});

