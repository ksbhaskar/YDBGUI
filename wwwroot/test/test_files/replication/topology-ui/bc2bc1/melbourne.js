/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {browserPorts} = require('../../helper')

describe("CLIENT: REPL > Topology UI > bc2bc1 > Context menu status", async () => {
    it("Test # 3320: popup Melbourne, verify enabled / disabled status", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // verify enabled status
        let disabled = await page.evaluate(() => $('#menuReplContextOpenGui').hasClass('disabled'))
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#menuReplContextOpenLogFiles').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#menuReplContextOpenInstanceFile').hasClass('disabled'))
        expect(disabled).to.be.false
    })

    it("Test # 3321: popup Paris, verify enabled / disabled status", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // verify enabled status
        let disabled = await page.evaluate(() => $('#menuReplContextOpenGui').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#menuReplContextOpenLogFiles').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#menuReplContextOpenInstanceFile').hasClass('disabled'))
        expect(disabled).to.be.false
    })

    it("Test # 3322: popup Santiago, verify enabled / disabled status", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // verify enabled status
        let disabled = await page.evaluate(() => $('#menuReplContextOpenGui').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#menuReplContextOpenLogFiles').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#menuReplContextOpenInstanceFile').hasClass('disabled'))
        expect(disabled).to.be.false
    })

    it("Test # 3323: popup Rome, verify enabled / disabled status", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // verify enabled status
        let disabled = await page.evaluate(() => $('#menuReplContextOpenGui').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#menuReplContextOpenLogFiles').hasClass('disabled'))
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#menuReplContextOpenInstanceFile').hasClass('disabled'))
        expect(disabled).to.be.false
    })
})

describe("CLIENT: REPL > Topology UI > bc2bc1 > Log dialog", async () => {
    it("Test # 3350: select Melbourne popup, open dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');
    })

    it("Test # 3351: select Melbourne popup, should have two tabs only", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        const numOfEntries = await page.evaluate(() => $('#listReplLogFilesEntries').children().length)
        expect(numOfEntries === 2).to.be.true
    })

    it("Test # 3352: select Melbourne popup, verify they are both filled", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        // and tables are filled correctly
        let entries = await page.evaluate(() => $('#tblReplLogsSource-santiago >tbody').children())
        expect(entries.length >0).to.be.true

        let row = await page.evaluate(() => $('#tblReplLogsSource-santiago >tbody >tr:first-child').html())
        expect(row).to.have.string('/opt/yottadb/current/mupip replicate -source -start')


        entries = await page.evaluate(() => $('#tblReplLogsSource-paris >tbody').children())
        expect(entries.length >0).to.be.true

        row = await page.evaluate(() => $('#tblReplLogsSource-paris >tbody >tr:first-child').html())
        expect(row).to.have.string('/opt/yottadb/current/mupip replicate -source -start')
    })

    it("Test # 3353: select Melbourne popup, verify dialog caption is correct", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        const title = await page.evaluate(() => $('#lblReplLogsTitle').text())
        expect(title).to.have.string('melbourne')
    })

    it("Test # 3355: select Paris popup, open dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');
    })

    it("Test # 3356: select Paris popup, should have 4 tabs only", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        const numOfEntries = await page.evaluate(() => $('#listReplLogFilesEntries').children().length)
        expect(numOfEntries === 4).to.be.true
    })

    it("Test # 3357: select Paris popup, verify they are all filled", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        // and tables are filled correctly
        let entries = await page.evaluate(() => $('#tblReplLogsReceiver >tbody').children())
        expect(entries.length >0).to.be.true

        let row = await page.evaluate(() => $('#tblReplLogsReceiver >tbody >tr:first-child').html())
        expect(row).to.have.string('/opt/yottadb/current/mupip replicate -receive')


        entries = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children())
        expect(entries.length >0).to.be.true

        row = await page.evaluate(() => $('#tblReplLogsUpdater >tbody >tr:first-child').html())
        expect(row).to.have.string('Attached to existing jnlpool')


        entries = await page.evaluate(() => $('#tblReplLogsSource-dummy >tbody').children())
        expect(entries.length >0).to.be.true

        row = await page.evaluate(() => $('#tblReplLogsSource-dummy >tbody >tr:first-child').html())
        expect(row).to.have.string('/opt/yottadb/current/mupip replicate -source')


        entries = await page.evaluate(() => $('#tblReplLogsSource-rome >tbody').children())
        expect(entries.length >0).to.be.true

        row = await page.evaluate(() => $('#tblReplLogsSource-rome >tbody >tr:first-child').html())
        expect(row).to.have.string('/opt/yottadb/current/mupip replicate -source')
    })

    it("Test # 3358: select Paris popup, verify dialog caption is correct", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        const title = await page.evaluate(() => $('#lblReplLogsTitle').text())
        expect(title).to.have.string('paris')
    })

    it("Test # 3360: select Santiago popup, open dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');
    })

    it("Test # 3361: select Santiago popup, should have 3 tabs only", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        const numOfEntries = await page.evaluate(() => $('#listReplLogFilesEntries').children().length)
        expect(numOfEntries === 3).to.be.true
    })

    it("Test # 3362: select Santiago popup, verify they are all filled", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        // and tables are filled correctly
        let entries = await page.evaluate(() => $('#tblReplLogsReceiver >tbody').children())
        expect(entries.length >0).to.be.true

        let row = await page.evaluate(() => $('#tblReplLogsReceiver >tbody >tr:first-child').html())
        expect(row).to.have.string('/opt/yottadb/current/mupip replicate -receive')


        entries = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children())
        expect(entries.length >0).to.be.true

        row = await page.evaluate(() => $('#tblReplLogsUpdater >tbody >tr:first-child').html())
        expect(row).to.have.string('Attached to existing jnlpool')


        entries = await page.evaluate(() => $('#tblReplLogsSource-dummy >tbody').children())
        expect(entries.length >0).to.be.true

        row = await page.evaluate(() => $('#tblReplLogsSource-dummy >tbody >tr:first-child').html())
        expect(row).to.have.string('/opt/yottadb/current/mupip replicate -source')
    })

    it("Test # 3363: select Santiago popup, verify dialog caption is correct", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        const title = await page.evaluate(() => $('#lblReplLogsTitle').text())
        expect(title).to.have.string('santiago')
    })

    it("Test # 3365: select Rome popup, open dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');
    })

    it("Test # 3366: select Rome popup, should have 3 tabs only", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        const numOfEntries = await page.evaluate(() => $('#listReplLogFilesEntries').children().length)
        expect(numOfEntries === 3).to.be.true
    })

    it("Test # 3367: select Rome popup, verify they are all filled", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        // and tables are filled correctly
        let entries = await page.evaluate(() => $('#tblReplLogsReceiver >tbody').children())
        expect(entries.length >0).to.be.true

        let row = await page.evaluate(() => $('#tblReplLogsReceiver >tbody >tr:first-child').html())
        expect(row).to.have.string('/opt/yottadb/current/mupip replicate -receive')


        entries = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children())
        expect(entries.length >0).to.be.true

        row = await page.evaluate(() => $('#tblReplLogsUpdater >tbody >tr:first-child').html())
        expect(row).to.have.string('Attached to existing jnlpool')


        entries = await page.evaluate(() => $('#tblReplLogsSource-dummy >tbody').children())
        expect(entries.length >0).to.be.true

        row = await page.evaluate(() => $('#tblReplLogsSource-dummy >tbody >tr:first-child').html())
        expect(row).to.have.string('/opt/yottadb/current/mupip replicate -source')
    })

    it("Test # 3368: select Rome popup, verify dialog caption is correct", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        const title = await page.evaluate(() => $('#lblReplLogsTitle').text())
        expect(title).to.have.string('rome')
    })

    it("Test # 3369: select Rome popup, change the tail to 2 and refresh, verify list length has changed", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        // get the updater log entries length
        const entriesBefore = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children().length)

        await libs.delay(1000)

        // change the tail size to 2
        await page.evaluate(() => $('#inpReplLogsTail').val(2))

        await libs.delay(200)

        const refresh = await page.$(('#btnReplLogFilesRefresh'))
        await refresh.click({
            button: 'left'
        })

        await libs.delay(2000)

        // get the updater new log entries length
        const entriesAfter = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children().length)
        expect(entriesBefore !== entriesAfter).to.be.true
    })

    it("Test # 3370: select Rome popup, change the tail to -2 and refresh, verify list length has changed", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        // get the updater log entries length
        const entriesBefore = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children().length)

        await libs.delay(1000)

        // change the tail size to 2
        await page.evaluate(() => $('#inpReplLogsTail').val(-2))

        await libs.delay(200)

        const refresh = await page.$(('#btnReplLogFilesRefresh'))
        await refresh.click({
            button: 'left'
        })

        await libs.delay(2000)

        // get the updater new log entries length
        const entriesAfter = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children().length)
        expect(entriesBefore !== entriesAfter).to.be.true
    })

    it("Test # 3371: select Rome popup, change the tail to -2 and refresh, change it back to 0 and verify list length has changed", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open log
        let menuOption = await page.$(('#menuReplContextOpenLogFiles'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');

        // get the updater log entries length
        const entriesBefore = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children().length)

        await libs.delay(1000)

        // change the tail size to 2
        await page.evaluate(() => $('#inpReplLogsTail').val(-2))

        await libs.delay(200)

        const refresh = await page.$(('#btnReplLogFilesRefresh'))
        await refresh.click({
            button: 'left'
        })

        await libs.delay(2000)

        // get the updater new log entries length
        const entriesAfter = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children().length)
        expect(entriesBefore !== entriesAfter).to.be.true
    })

    await libs.delay(1000)

    // change the tail size to 0
    await page.evaluate(() => $('#inpReplLogsTail').val(0))

    await libs.delay(200)

    const refresh = await page.$(('#btnReplLogFilesRefresh'))
    await refresh.click({
        button: 'left'
    })

    await libs.delay(2000)

    // get the updater new log entries length
    const entriesAfterAll = await page.evaluate(() => $('#tblReplLogsUpdater >tbody').children().length)

    expect(entriesBefore !== entriesAfterAll).to.be.true
})


describe("CLIENT: REPL > Topology UI > bc2bc1 > Instance file dialog", async () => {
    it("Test # 3380: select Melbourne popup, open dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');
    })

    it("Test # 3381: select Melbourne popup, verify dialog caption is correct", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        const title = await page.evaluate(() => $('#lblReplInstanceFileTitle').text())
        expect(title).to.have.string('melbourne')
    })

    it("Test # 3382: select Melbourne popup, verify header receiver group is empty", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const row = await page.evaluate(() => $('#tblReplInstanceFileHeader >tbody > tr:nth-child(17)').text())
        expect(row).to.have.string('null')
    })

    it("Test # 3384: select Melbourne popup, verify 2 slots are present", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const rows = await page.evaluate(() => $('#tblReplInstanceFileSlots >tbody').children().length)
        expect(rows === 2).to.be.true
    })

    it("Test # 3385: select Melbourne popup, verify History contains 1 record", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-melbourne'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const rows = await page.evaluate(() => $('#tblReplInstanceFileHistory >tbody').children().length)
        expect(rows === 10).to.be.true
    })

    it("Test # 3390: select Paris popup, open dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');
    })

    it("Test # 3391: select Paris popup, verify dialog caption is correct", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        const title = await page.evaluate(() => $('#lblReplInstanceFileTitle').text())
        expect(title).to.have.string('paris')
    })

    it("Test # 3392: select Paris popup, verify header receiver group is populated", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const row = await page.evaluate(() => $('#tblReplInstanceFileHeader >tbody > tr:nth-child(17)').text())
        expect(row).to.not.have.string('null')
    })

    it("Test # 3394: select Paris popup, verify 2 slots are present", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const rows = await page.evaluate(() => $('#tblReplInstanceFileSlots >tbody').children().length)
        expect(rows === 2).to.be.true
    })

    it("Test # 3395: select Paris popup, verify History contains 1 record", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const rows = await page.evaluate(() => $('#tblReplInstanceFileHistory >tbody').children().length)
        expect(rows === 10).to.be.true
    })

    it("Test # 3400: select Rome popup, open dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');
    })

    it("Test # 3401: select Rome popup, verify dialog caption is correct", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        const title = await page.evaluate(() => $('#lblReplInstanceFileTitle').text())
        expect(title).to.have.string('rome')
    })

    it("Test # 3402: select Rome popup, verify header receiver group is populated", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const row = await page.evaluate(() => $('#tblReplInstanceFileHeader >tbody > tr:nth-child(17)').text())
        expect(row).to.not.have.string('null')

    })

    it("Test # 3404: select Rome popup, verify 1 slot are present", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const rows = await page.evaluate(() => $('#tblReplInstanceFileSlots >tbody').children().length)
        expect(rows === 1).to.be.true
    })

    it("Test # 3405: select Rome popup, verify History contains 1 record", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const rows = await page.evaluate(() => $('#tblReplInstanceFileHistory >tbody').children().length)
        expect(rows === 10).to.be.true
    })


    it("Test # 3410: select Santiago popup, open dialog", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');
    })

    it("Test # 3411: select Santiago popup, verify dialog caption is correct", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        const title = await page.evaluate(() => $('#lblReplInstanceFileTitle').text())
        expect(title).to.have.string('santiago')
    })

    it("Test # 3412: select Santiago popup, verify header receiver group is populated", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const row = await page.evaluate(() => $('#tblReplInstanceFileHeader >tbody > tr:nth-child(17)').text())
        expect(row).to.not.have.string('null')
    })

    it("Test # 3414: select Santiago popup, verify 1 slot are present", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const rows = await page.evaluate(() => $('#tblReplInstanceFileSlots >tbody').children().length)
        expect(rows === 1).to.be.true
    })

    it("Test # 3415: select Santiago popup, verify History contains 1 record", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open instance file
        let menuOption = await page.$(('#menuReplContextOpenInstanceFile'))
        await menuOption.click({
            button: 'left'
        })

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');

        // get the receiver first row
        const rows = await page.evaluate(() => $('#tblReplInstanceFileHistory >tbody').children().length)
        expect(rows === 10).to.be.true
    })
})

describe("CLIENT: REPL > Topology UI > bc2bc1 > Open GUI from popup", async () => {

    it("Test # 3420: open Paris popup, select open GUI, verify tab gets created", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-paris'))
        await cell.click({
            button: 'right'
        })

        // open browser tab
        let menuOption = await page.$(('#menuReplContextOpenGui'))
        await menuOption.click({
            button: 'left'
        })

        await libs.delay(2000)

        // get all the currently open pages as an array
        const pages = await browser.pages();

        expect(pages[2]._target._targetInfo.url).to.have.string('https://localhost:9089/')
    })

    it("Test # 3421: open Rome popup, select open GUI, verify tab gets created", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-rome'))
        await cell.click({
            button: 'right'
        })

        // open browser tab
        let menuOption = await page.$(('#menuReplContextOpenGui'))
        await menuOption.click({
            button: 'left'
        })

        await libs.delay(2000)

        // get all the currently open pages as an array
        const pages = await browser.pages();

        expect(pages[3]._target._targetInfo.url).to.have.string('https://localhost:11089/')
    })

    it("Test # 3422: open Paris popup, select open GUI, verify tab gets created", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // pop up
        let cell = await page.$(('#jointjs-santiago'))
        await cell.click({
            button: 'right'
        })

        // open browser tab
        let menuOption = await page.$(('#menuReplContextOpenGui'))
        await menuOption.click({
            button: 'left'
        })

        await libs.delay(2000)

        // get all the currently open pages as an array
        const pages = await browser.pages();

        expect(pages[2]._target._targetInfo.url).to.have.string('https://localhost:9089/')
    })
})
