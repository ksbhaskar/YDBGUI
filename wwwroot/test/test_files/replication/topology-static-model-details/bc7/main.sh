#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

echo '*****************************************'
echo '---Topology static model details > bc7'
echo '*****************************************'
echo

$PWD/replication/repl run bc7
while
	! docker logs --tail 1 melbourne | grep -q "Starting Server at port" ||
	! docker logs --tail 1 paris | grep -q "Starting Server at port" ||
	! docker logs --tail 1 santiago | grep -q "Starting Server at port" ||
	! docker logs --tail 1 rome | grep -q "Starting Server at port" ||
	! docker logs --tail 1 amsterdam | grep -q "Starting Server at port" ||
	! docker logs --tail 1 london | grep -q "Starting Server at port" ||
	! docker logs --tail 1 madrid  | grep -q "Starting Server at port" ||
	! docker logs --tail 1 tokio | grep -q "Starting Server at port";
do sleep 1
done

docker exec -e ydb_in_repl=melbourne melbourne npm test -- wwwroot/test/test_files/replication/topology-static-model-details/bc7/melbourne.js
script1=$?

docker exec -e ydb_in_repl=paris paris npm test -- wwwroot/test/test_files/replication/topology-static-model-details/bc7/paris.js
script2=$?

docker exec -e ydb_in_repl=rome rome npm test -- wwwroot/test/test_files/replication/topology-static-model-details/bc7/rome.js
script3=$?

docker exec -e ydb_in_repl=tokio tokio npm test -- wwwroot/test/test_files/replication/topology-static-model-details/bc7/tokio.js
script4=$?

$PWD/replication/repl down

exitCode=0
if [ $script1 != 0 ] || [ $script2 != 0 ] || [ $script3 != 0 ]  || [ $script4 != 0 ]; then
	exitCode=1
fi
exit $exitCode
