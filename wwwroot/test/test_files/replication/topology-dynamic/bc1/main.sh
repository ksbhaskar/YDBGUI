#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

echo '*************************************'
echo '---Topology dynamic > bc1'
echo '*************************************'
echo

$PWD/replication/repl run bc1
while
	! docker logs --tail 1 melbourne | grep -q "Starting Server at port" ||
	! docker logs --tail 1 paris | grep -q "Starting Server at port";
do sleep 1
done

exitCode=0
# the next file provides machines and backlogs in the background
$PWD/wwwroot/test/test_files/replication/topology-dynamic/bc1/docker-manager.sh &
if ! docker exec -e ydb_in_repl=melbourne melbourne npm test -- wwwroot/test/test_files/replication/topology-dynamic/bc1/melbourne.js; then
	exitCode=1
fi
# wait for all processes to terminate
wait
$PWD/replication/repl down
exit $exitCode
