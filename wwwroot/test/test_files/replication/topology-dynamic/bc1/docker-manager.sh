#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

# This file provides background support to the test by performing the following:
# Generating backlog
# Bringing paris down
# Generating backlog
# Bringing paris up
#
# Additionally, it creates a file "status1.dat" with a status code of the current operation

echo "1" > $PWD/wwwroot/test/test_files/replication/topology-dynamic/bc1/status1.dat
echo '>>> Status is: 1'
echo '>>> generating backlog...'
docker exec melbourne bash  -c '. /opt/yottadb/current/ydb_env_set && mumps -run %XCMD "for x=1:1:8E6 set ^x(x)=x"'
echo '>>> backlog generated'

sleep 3

echo '>>> Bringing Paris down...'
$PWD/replication/repl down paris
echo '>>> Paris is now down'

echo '>>> generating backlog...'
docker exec melbourne bash  -c '. /opt/yottadb/current/ydb_env_set && mumps -run %XCMD "for x=1:1:4E6 set ^y(x)=x"'
echo '>>> backlog generated'
echo "2" > $PWD/wwwroot/test/test_files/replication/topology-dynamic/bc1/status1.dat
echo '>>> Status is: 2'

sleep 8

echo '>>> Bringing Paris up...'
$PWD/replication/repl up paris
while
	! docker logs --tail 1 paris | grep -q "Starting Server at port";
do sleep 1
done
echo '>>> Paris is now up'

echo "3" > $PWD/wwwroot/test/test_files/replication/topology-dynamic/bc1/status1.dat
echo '>>> Status is: 3'
