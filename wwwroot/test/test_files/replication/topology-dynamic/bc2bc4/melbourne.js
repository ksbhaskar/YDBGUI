/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {forFileStatus, browserPorts, configTls} = require('../../helper')
const fs = require('fs')

describe("CLIENT: REPL > Topology dynamic > bc1 > Melbourne", async () => {
    it("Test # 3601: verify backlog is displayed on all nodes, london down, backlog steady and red, london up, backlog gets spooled, paris down, backlog steady and red, paris up, backlog gets spooled", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await configTls()

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');

        // waits for the drawing to be created
        await libs.delay(2000)

        // get the graph infos
        let graph = await page.evaluate(() => app.ui.replication.topology.joint.getGraph())

        // and find the backlogs
        let link = graph.find(element => element.id === 'paris-london')

        expect(link.backlog[0].attrs.labelText.text !== '0' || link.backlog[1].attrs.labelText.text !== '0').to.be.true

        // await for process sync
        await forFileStatus('/YDBGUI/wwwroot/test/test_files/replication/topology-dynamic/bc2bc4/status1.dat', '2')

        // give time to get a new sample
        await libs.delay(4000)

        // ****************************************
        // London is down
        // ****************************************

        // get the graph infos
        graph = await page.evaluate(async () => {
            app.userSettings.defaults.replication.discoveryService.responseType = 'HB'
            const res = await app.REST._replTopology(app.userSettings.defaults.replication.discoveryService)

            app.ui.replication.topology.joint.refresh(res)

            return app.ui.replication.topology.joint.getGraph()
        })

        // and find the backlogs
        link = graph.find(element => element.id === 'paris-london')

        expect(link.backlog[0].attrs.labelText.text === '4,000,000').to.be.true

        // await for process sync
        await forFileStatus('/YDBGUI/wwwroot/test/test_files/replication/topology-dynamic/bc2bc4/status1.dat', '3')

        // ****************************************
        // London is up
        // ****************************************

        // give time to the buffers to spool
        await libs.delay(15000)

        // get the graph infos
        graph = await page.evaluate(async () => {
            app.userSettings.defaults.replication.discoveryService.responseType = 'HB'
            const res = await app.REST._replTopology(app.userSettings.defaults.replication.discoveryService)

            app.ui.replication.topology.joint.refresh(res)

            return app.ui.replication.topology.joint.getGraph()
        })

        // and find the backlogs
        link = graph.find(element => element.id === 'paris-london')

        expect(link.backlog[0].attrs.labelText.text !== '4,000,000').to.be.true
        //expect(link.backlog[1].attrs.labelText.text === '0').to.be.true

        // await for process sync
        await forFileStatus('/YDBGUI/wwwroot/test/test_files/replication/topology-dynamic/bc2bc4/status1.dat', '4')

        // give time to get a new sample
        await libs.delay(4000)

        // ****************************************
        // Paris is down
        // ****************************************

        // get the graph infos
        graph = await page.evaluate(async () => {
            app.userSettings.defaults.replication.discoveryService.responseType = 'HB'
            const res = await app.REST._replTopology(app.userSettings.defaults.replication.discoveryService)

            app.ui.replication.topology.joint.refresh(res)

            return app.ui.replication.topology.joint.getGraph()
        })

        // and find the backlogs
        link = graph.find(element => element.id === 'melbourne-paris')

        expect(link.backlog[0].attrs.labelText.text === '4,000,000').to.be.true

        // await for process sync
        await forFileStatus('/YDBGUI/wwwroot/test/test_files/replication/topology-dynamic/bc2bc4/status1.dat', '5')

        // give time to get a new sample
        await libs.delay(12000)

        // ****************************************
        // Paris is up
        // ****************************************

        // get the graph infos
        graph = await page.evaluate(async () => {
            app.userSettings.defaults.replication.discoveryService.responseType = 'HB'
            const res = await app.REST._replTopology(app.userSettings.defaults.replication.discoveryService)

            app.ui.replication.topology.joint.refresh(res)

            return app.ui.replication.topology.joint.getGraph()
        })

        // and find the backlogs
        link = graph.find(element => element.id === 'melbourne-paris')

        expect(link.backlog[0].attrs.labelText.text === '0').to.be.true

    })
})
