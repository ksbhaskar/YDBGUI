/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../../libs');
const {expect} = require("chai");
const {browserPorts} = require('../../helper')

describe("CLIENT: REPL > Dashboard static > bc1 > Melbourne", async () => {
    it("Test # 3120: verify replication menu is enabled", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let disabled = await page.evaluate(() => $('#menuReplication').is(':disabled'))
        expect(disabled).to.be.false
    })

    it("Test # 3121: verify log files is working", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.logs.show(app.system.replication.logFiles, app.system.replication.instanceFile.flags.instanceName))

        // verify it opens correctly
        await libs.waitForDialog('#modalReplLogFiles');
    })

    it("Test # 3122: verify instance file is working", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.instanceFile.show())

        // verify it opens correctly
        await libs.waitForDialog('#modalReplInstanceFile');
    })

    it("Test # 3123: Try to launch topology", async () => {
        await page.goto(`https://localhost:${browserPorts.MELBOURNE}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // open the dialog
        await page.evaluate(() => app.ui.replication.topology.showTab())

        // verify it opens correctly
        await libs.waitForDialog('#tab-TDiv');
    })
})
