#!/bin/sh
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

echo '***************'
echo 'Pull-down menus'
echo '***************'
echo

$PWD/wwwroot/test/test_files/replication/pull-down-menus/bc1/main.sh
script1=$?

$PWD/wwwroot/test/test_files/replication/pull-down-menus/bc2bc1/main.sh
script2=$?

$PWD/wwwroot/test/test_files/replication/pull-down-menus/bc2si1si1/main.sh
script3=$?

exitCode=0
 if [ $script1 != 0 ] || [ $script2 != 0 ] || [ $script3 != 0 ]; then
 	exitCode=1
 fi
 exit $exitCode
