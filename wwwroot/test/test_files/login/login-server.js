/*
#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe(": SERVER: login", async () => {
    it("Test # 11000: execute the auth mode, verify response", async () => {

        const res = await libs._REST('auth-mode')
        expect(res.auth).to.be.true
    })

    it("Test # 11001: execute login with wrong credentials, expect 401", async () => {
        const res = await libs._RESTlogin('username', 'password')
        expect(res.error.code === 401).to.be.true
    })

    it("Test # 11002: execute login with admin credentials, expect RW mode", async () => {
        const res = await libs._RESTlogin('admin', 'admin')

        expect(res.authorization === 'RW').to.be.true
    })

    it("Test # 11003: execute login with user credentials, expect RO mode", async () => {
        const res = await libs._RESTlogin('user', 'user')

        expect(res.authorization === 'RO').to.be.true
    })

    it("Test # 11004: execute login, then logout, check response", async () => {
        let res = await libs._RESTlogin('user', 'user')
        const token = res.token

        res = await libs._REST('dashboard/get-all', token).catch(() => {});
        expect(res.result === 'OK').to.be.true

        const logoutRes = await libs._RESTlogout(token)

        expect(logoutRes.status === 'OK').to.be.true

        res = await libs._REST('dashboard/get-all').catch(() => {});
        expect(res.error.code === 403).to.be.true
    })
})
