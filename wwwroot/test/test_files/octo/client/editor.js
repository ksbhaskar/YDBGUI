/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");

describe("CLIENT: Octo: editor", async () => {
    it("Test # 2020: Open the tab, ensure tab caption is New query", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.octoViewer.addNew());

        // wait for Octo viewer to be set by the async call
        await libs.waitForDialog('#tab-O-1Div');

        const caption = await page.evaluate(() => $('#tab-O-1').text());

        expect(caption).to.have.string('New query')

    })

    it("Test # 2021: Type \\d and click on 'Run', verify that query returns data in table and tab is selected", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            //await libs.delay(300)

            await page.keyboard.type('\\d');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const isActive = await page.evaluate(() => $('#tabOctoTable-O-1-Q1').hasClass('active'));
            expect(isActive).to.be.true

            let data = await page.evaluate(() => $('#tblOctoResult-O-1-Q1 > tbody > tr:first-child td:nth-child(1)').text());
            expect(data).to.have.string('public')
        }
    })

    it("Test # 2022: Type '\\d suppliers' and click on 'Run', verify that query returns data in CLI and tab is selected", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\d suppliers');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const isActive = await page.evaluate(() => $('#divOctoText-O-1').hasClass('active'));
            expect(isActive).to.be.true

            let data = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(data).to.have.string('suppliers')
        }
    })

    it("Test # 2024: Type \\d and click on 'Run', verify that tab name changed into \\d", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\d');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q1').text());

            expect(caption).to.have.string('\\d')
        }
    })

    it("Test # 2026: Type select * from suppliers, verify that tab name changed into suppliers", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('select * from suppliers');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q1').text());
            expect(caption).to.have.string('suppliers')
        }
    })

    it("Test # 2030: Type  `create table testnew (id integer)', verify response", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('create table testnew (id integer)');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            let data = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(data).to.have.string('CREATE TABLE')
        }
    })

    it("Test # 2032: Type text, UNDO, ensure text is removed", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('This is a sample text');

            btnClick = await page.$("#btnOctoUndo-O-1");
            await btnClick.click();

            const text = await page.evaluate(() => app.ui.octoViewer.cm['-O-1'].getValue());

            expect(text === '').to.be.true
        }
    })

    it("Test # 2033: Type text, UNDO, ensure text is removed, REDO, ensure text appears again", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('This is a sample text');

            btnClick = await page.$("#btnOctoUndo-O-1");
            await btnClick.click();

            let text = await page.evaluate(() => app.ui.octoViewer.cm['-O-1'].getValue());
            expect(text === '').to.be.true

            btnClick = await page.$("#btnOctoRedo-O-1");
            await btnClick.click();

            text = await page.evaluate(() => app.ui.octoViewer.cm['-O-1'].getValue());
            expect(text === 'This is a sample text').to.be.true
        }
    })

    it("Test # 2034: Type text, empty line, text, move cursor on empty line and try to submit, should do nothing", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('this is a sample text');
            await page.keyboard.press('Enter')
            await page.keyboard.press('Enter')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            await libs.delay(1000)

            let data = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(data).to.have.string('   ')
        }
    })

    it("Test # 2035: Type: \\q, it should return Empty response", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\q');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            let data = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(data).to.have.string('Empty response')
        }
    })

    /* couldn't get the CTRL-<enter> to work
    it("Test # 2036: type \\d, use CTRL-Enter to submit, verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.ui.octoViewer.addNew());

        // wait for Octo viewer to be set by the async call
        await libs.waitForDialog('#tab-O-1Div');

        await page.keyboard.type('\\d');
        await page.keyboard.down('Control')
        await page.keyboard.press(String.fromCharCode(13))
        await page.keyboard.up('Control')

        // wait for Octo query to be set by the async call
        await libs.waitForDialog('#divOctoSqlArea-O-1');

        const caption = await page.evaluate(() => $('#tab-O-1').text());
        expect(caption).to.have.string('\d')
    })

     */

    it("Test # 2037: RO mode: type \\d, it should NOT execute", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\d');
            await page.keyboard.down('ControlLeft')
            await page.keyboard.press('KeyA')
            await page.keyboard.up('ControlLeft')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for msgbox to be set by the async call
            await libs.waitForDialog('#modalMsgbox');
        }
    })

    it("Test # 2038: RO mode: type \\q, it should NOT execute", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\q');
            await page.keyboard.down('ControlLeft')
            await page.keyboard.press('KeyA')
            await page.keyboard.up('ControlLeft')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for msgbox to be set by the async call
            await libs.waitForDialog('#modalMsgbox');
        }
    })

    it("Test # 2039: RO mode: type INSERT, should display msgbox", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('INSERT INTO suppliers');
            await page.keyboard.down('ControlLeft')
            await page.keyboard.press('KeyA')
            await page.keyboard.up('ControlLeft')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for msgbox to be set by the async call
            await libs.waitForDialog('#modalMsgbox');
        }
    })

    it("Test # 2040: RO mode: type UPDATE, should display msgbox", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('UPDATE suppliers');
            await page.keyboard.down('ControlLeft')
            await page.keyboard.press('KeyA')
            await page.keyboard.up('ControlLeft')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for msgbox to be set by the async call
            await libs.waitForDialog('#modalMsgbox');
        }
    })

    it("Test # 2041: RO mode: type SELECT, should display msgbox", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('SELECT suppliers');
            await page.keyboard.down('ControlLeft')
            await page.keyboard.press('KeyA')
            await page.keyboard.up('ControlLeft')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for msgbox to be set by the async call
            await libs.waitForDialog('#modalMsgbox');
        }
    })

    it("Test # 2042: RO mode: type DROP, should display msgbox", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('DROP suppliers');
            await page.keyboard.down('ControlLeft')
            await page.keyboard.press('KeyA')
            await page.keyboard.up('ControlLeft')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for msgbox to be set by the async call
            await libs.waitForDialog('#modalMsgbox');
        }
    })


    /* commented out because couldn't get to run it
    it("Test # 2044: execute one create, one insert, one update, one delete, one drop, verify CLI response", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type(
                'create table ttest (id int);' +
                'insert into ttest (id) values (1);' +
                'update ttest set id = 2;' +
                'delete FROM ttest;' +
                'drop table ttest;')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(2500)

            let text = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(text).to.have.string('CREATE TABLEINSERT 0 1UPDATE 1DELETE 1DROP TABLE')
        }
    })
     */

    it("Test # 2045: execute 1 select, one error (select * from notexist), one select, verify", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type(
                'select * from suppliers;' +
                'select * from notexist;' +
                'select * from suppliers;')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(2000)

            let text = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(text).to.have.string('[ERROR]')
        }
    })

    it("Test # 2046: execute select * from notexist, then \\d notexist, verify both are trapped correctly", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type(
                'select * from notexist;' +
                '\\d notexist;')

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(2000)

            let text = await page.evaluate(() => $('#divOctoCli-O-1').text());
            expect(text).to.have.string('[ERROR]')
        }
    })

    it("Test # 2047: click on edit icon on the tab, should display the tab edit dialog", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            await page.evaluate(() => app.ui.octoViewer.renameTab('-O-1'));

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#modalOctoTabEdit');
        }
    })

    it("Test # 2048: pop the tab edit dialog, change the text and submit, verify", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await libs.delay(200)

            await page.evaluate(() => app.ui.octoViewer.renameTab('-O-1'));

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#modalOctoTabEdit');

            await page.keyboard.type('123')

            btnClick = await page.$("#btnOctoTabEditOk");
            await btnClick.click();

            await libs.delay(250)

            const text = await page.evaluate(() => $('#tab-O-1').text());
            expect(text).to.have.string('123')
        }
    })

    it("Test # 2049: type \\d, verify", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('create view v1 as select * from categories');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\d');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const caption = await page.evaluate(() => $('#tblOctoResult-O-1-Q1 > tbody > tr:nth-child(64)').text());

            expect(caption).to.have.string('v1')
        }
    })

    it("Test # 2050: type \\d v1, verify", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\d v1');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const caption = await page.evaluate(() => $('#divOctoText-O-1').text());
            expect(caption).to.have.string('View "v1"Column|Type|Collation|Nullable|Defaultcategoryid|INTEGER||||categoryname|VARCHAR(32)||||description|VARCHAR(64)||||View definition:create view v1 as select * from categories;')
        }
    })

    it("Test # 2051: type \\dv, verify", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('\\dv');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const caption = await page.evaluate(() => $('#tblOctoResult-O-1-Q1 > tbody > tr:nth-child(1) >td:nth-child(2)').text());

            expect(caption).to.have.string('v1')
        }
    })

})
