/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");

describe("CLIENT: Data view", async () => {
    it("Test # 2150: type: select * from test, verify that table has one column", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('select * from orderdetails');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(200)

            const isActive = await page.evaluate(() => $('#tabOctoTable-O-1-Q1').hasClass('active'));
            expect(isActive).to.be.true

            let colsArray = await page.evaluate(() => $('#tblOctoResult-O-1-Q1 > tbody > tr:first-child').children());
            expect(colsArray.length === 4).to.be.true
        }
    })

    it("Test # 2151: type: select * from orderdetails, verify that table has 4 columns", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type('select * from orderdetails');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            const isActive = await page.evaluate(() => $('#divOctoTable-O-1-Q1').hasClass('active'));
            expect(isActive).to.be.true

            let colsArray = await page.evaluate(() => $('#tblOctoResult-O-1-Q1 > tbody > tr:first-child').children());
            expect(colsArray.length === 4).to.be.true
        }
    })

    it("Test # 2152: execute a multiple (8) chained select statements, verify all tabs are there, with correct table displayed", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type(
                'select * from CATEGORIES;' +
                'select * from nwCUSTOMERS;' +
                'select * from EMPLOYEES;' +
                'select * from ORDERDETAILS;' +
                'select * from nwORDERS;' +
                'select * from PRODUCTS;' +
                'select * from SHIPPERS;' +
                'select * from SUPPLIERS;');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(2000)

            let caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q1').text());
            expect(caption).to.have.string('CATEGORIES')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q2').text());
            expect(caption).to.have.string('CUSTOMERS')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q3').text());
            expect(caption).to.have.string('EMPLOYEES')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q4').text());
            expect(caption).to.have.string('ORDERDETAILS')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q5').text());
            expect(caption).to.have.string('ORDERS')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q6').text());
            expect(caption).to.have.string('PRODUCTS')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q7').text());
            expect(caption).to.have.string('SHIPPERS')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q8').text());
            expect(caption).to.have.string('SUPPLIERS')
        }
    })

    it("Test # 2153: execute two select, verify tabs, change limit, rerun and verify that new tabs are created", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type(
                'select * from CATEGORIES;' +
                'select * from nwCUSTOMERS;');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            let caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q1').text());
            expect(caption).to.have.string('CATEGORIES')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q2').text());
            expect(caption).to.have.string('nwCUSTOMERS')

            // change limit
            await page.evaluate(() => $('#inpOctoLimit-O-1').val(500));

            await page.keyboard.press('Enter')
            await page.keyboard.press('Enter')
            await page.keyboard.type(
                'select * from CATEGORIES;' +
                'select * from nwCUSTOMERS;');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(500)

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q3').text());
            expect(caption).to.have.string('CATEGORIES')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q4').text());
            expect(caption).to.have.string('nwCUSTOMERS')
        }
    })

    /* commented out as it doesn't run correctly
    it("Test # 2154: execute two select, verify tabs, close one tab, rerun and verify that only one tab is created", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type(
                'select * from CATEGORIES;' +
                'select * from CUSTOMERS;');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(2000)


            let caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q1').text());
            expect(caption).to.have.string('CATEGORIES')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q2').text());
            expect(caption).to.have.string('CUSTOMERS')

            // close one tab
            await page.evaluate(() => app.ui.octoViewer.closeTab('-O-1-Q1'));

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q3').text());
            expect(caption).to.have.string('CATEGORIES')

            caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q4').text());
            expect(caption === '').to.be.true
        }

    })
     */

    it("Test # 2155: execute a SELECT, insert a record, re-execute the select, verify that the same tab is used, but the new record appears", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            await page.evaluate(() => app.ui.octoViewer.addNew());

            // wait for Octo viewer to be set by the async call
            await libs.waitForDialog('#tab-O-1Div');

            await page.keyboard.type(
                'create table refreshtest (id int);' +
                'insert into refreshtest (id) values (1);' +
                'select * from refreshtest;'
            );

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(2000)

            btnClick = await page.$("#tabOctoTable-O-1-Q1");
            await btnClick.click();

            let caption = await page.evaluate(() => $('#tabOctoTable-O-1-Q1').text());
            expect(caption).to.have.string('refreshtest')

            let colsArray = await page.evaluate(() => $('#tblOctoResult-O-1-Q1 > tbody ').children());
            expect(colsArray.length === 2).to.be.true

            await page.keyboard.press('Enter')
            await page.keyboard.press('Enter')
            await page.keyboard.type('insert into refreshtest (id) values (1);select * from refreshtest;');

            btnClick = await page.$("#btnOctoRun-O-1");
            await btnClick.click();

            // wait for Octo query to be set by the async call
            await libs.waitForDialog('#divOctoSqlArea-O-1');

            await libs.delay(2000)

            colsArray = await page.evaluate(() => $('#tblOctoResult-O-1-Q1 > tbody ').children());

            expect(colsArray.length === 3).to.be.true

            colsArray = await page.evaluate(() => $('#tblOctoResult-O-1-Q1 > tbody').children());
            expect(colsArray.length > 0).to.be.true

            colsArray = await page.evaluate(() => $('#tblOctoResult-O-1-Q1 > tbody').children());
            expect(colsArray.length > 0).to.be.true
        }
    })
})

