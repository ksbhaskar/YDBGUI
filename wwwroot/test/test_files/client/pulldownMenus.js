/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe("CLIENT: Dashboard: PULLDOWN MENUS", async () => {
    it("Test # 200: Make at least one region with no file AND autodb = true and verify that create db menu is enabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=200`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // make sure menu entry has orange color
        const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionCreateDatabase');
        expect(menuColor).to.have.string('rgb(255, 127, 39)')
    });

    it("Test # 201: Remove all journals from all regions (state = 0) and verify that journaling on/off is disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=201`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // make sure menu entry has gray color
        const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionJournaling');
        expect(menuColor).to.have.string('rgb(170, 170, 170)')
    });

    it("Test # 202: If all no regions have a file, extend menu is disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=202`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // make sure menu entry has gray color
        const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionExtendDatabaseLi');
        expect(menuColor).to.have.string('rgb(170, 170, 170)')
    });

    it("Test # 203: Ensure the extend menu is disabled in RO mode", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            // make sure menu entry has gray color
            const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionExtendDatabaseLi');
            expect(menuColor).to.have.string('rgb(170, 170, 170)')
        }
    });

    it("Test # 204: Ensure the journal menu is disabled in RO mode", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            // make sure menu entry has gray color
            const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionJournaling');
            expect(menuColor).to.have.string('rgb(170, 170, 170)')
        }
    });

    it("Test # 205: Ensure the create database is disabled in RO mode", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            // make sure menu entry has orange color
            const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionCreateDatabase');
            expect(menuColor).to.have.string('rgb(170, 170, 170)')
        }
    });

    it("Test # 206: Ensure the create region menu is disabled in RO mode", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            // make sure menu entry has orange color
            const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionAdd');
            expect(menuColor).to.have.string('rgb(170, 170, 170)')
        }
    });

    it("Test # 207: Ensure the edit region menu is disabled in RO mode", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            // make sure menu entry has orange color
            const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionEdit');
            expect(menuColor).to.have.string('rgb(170, 170, 170)')
        }
    });

    it("Test # 208: Ensure the delete region menu is disabled in RO mode", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            // make sure menu entry has orange color
            const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionDelete');
            expect(menuColor).to.have.string('rgb(170, 170, 170)')
        }
    });

    it("Test # 209: Ensure the journal switch menu is disabled in RO mode", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            // make sure menu entry has gray color
            const menuColor = await libs.getCssColor('#menuSystemAdministrationRegionJournalingSwitch');
            expect(menuColor).to.have.string('rgb(170, 170, 170)')
        }
    });

});
