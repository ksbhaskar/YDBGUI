/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("ROUTINES: searchPath", async () => {
    it("Test # 1760: Display the dialog", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');
    })

    it("Test # 1761: Ensure that the tree is properly populated", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        const $ydb_dir = await page.evaluate(() => app.ui.getEnvVar('ydb_dir'));
        const $ydb_rel = await page.evaluate(() => app.ui.getEnvVar('ydb_rel'));
        const objPath = $ydb_dir + '/' + $ydb_rel + '/'

        if (global.charMode === 'M') {
            expect(tree[0].id === 'obj-/YDBGUI/objects*').to.be.true
            expect(tree[1].id === 'obj-/data/' + $ydb_rel + '/o*').to.be.true
            expect(tree[2].id === 'so-/opt/yottadb/current/plugin/o/_ydbaim.so').to.be.true
            expect(tree[3].id === 'sso-/opt/yottadb/current/plugin/o/_ydbgui.so').to.be.true
            expect(tree[4].id === 'sso-/opt/yottadb/current/plugin/o/_ydbmwebserver.so').to.be.true
            expect(tree[5].id === 'so-/opt/yottadb/current/plugin/o/_ydbocto.so').to.be.true
        } else {
            expect(tree[0].id === 'obj-/YDBGUI/objects*').to.be.true
            expect(tree[1].id === 'obj-/data/' + $ydb_rel + '/o*').to.be.true
            expect(tree[2].id === 'so-/opt/yottadb/current/plugin/o/utf8/_ydbaim.so').to.be.true
            expect(tree[3].id === 'sso-/opt/yottadb/current/plugin/o/utf8/_ydbgui.so').to.be.true
            expect(tree[4].id === 'sso-/opt/yottadb/current/plugin/o/utf8/_ydbmwebserver.so').to.be.true
            expect(tree[5].id === 'so-/opt/yottadb/current/plugin/o/utf8/_ydbocto.so').to.be.true

        }
    })

    it("Test # 1762: Ensure first item is selected: check status of all buttons", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        const selected = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_selected(true)[0]);

        expect(selected.id === 'obj-/YDBGUI/objects*').to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchAddObject').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSource').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSo').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchRemove').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveDown').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveUp').hasClass('disabled'));
        expect(disabled).to.be.true
    })

    it("Test # 1763: Select source path: check status of all buttons", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).deselect_all());
        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).select_node('child-/YDBGUI/mwebserver'));

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchAddObject').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSource').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSo').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchRemove').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveDown').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveUp').hasClass('disabled'));
        expect(disabled).to.be.false
    })

    it("Test # 1764: Select System SO: check status of all buttons", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).deselect_all());
        if (global.charMode === 'M') {
            await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).select_node('sso-/opt/yottadb/current/plugin/o/_ydbgui.so'));

        } else {
            await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).select_node('sso-/opt/yottadb/current/plugin/o/utf8/_ydbgui.so'));
        }

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchAddObject').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSource').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSo').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchRemove').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveDown').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveUp').hasClass('disabled'));
        expect(disabled).to.be.false
    })

    it("Test # 1765: Select first entry (OBJ): move down: confirm", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        const btnClick = await page.$("#btnRoutinesSearchMoveDown");
        await btnClick.click();

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        const $ydb_dir = await page.evaluate(() => app.ui.getEnvVar('ydb_dir'));
        const $ydb_rel = await page.evaluate(() => app.ui.getEnvVar('ydb_rel'));
        const objPath = $ydb_dir + '/' + $ydb_rel + '/'

        expect(tree[0].id === 'obj-' + objPath + 'o*').to.be.true
        expect(tree[1].id === 'obj-/YDBGUI/objects*').to.be.true
    })

    it("Test # 1766: Select second entry (SRC): move down: confirm", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).deselect_all());
        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).select_node('child-/YDBGUI/routines'));

        const btnClick = await page.$("#btnRoutinesSearchMoveDown");
        await btnClick.click();

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[0].children[0].id === 'child-/YDBGUI/mwebserver').to.be.true
        expect(tree[0].children[1].id === 'child-/YDBGUI/routines').to.be.true
    })

    it("Test # 1767: Select 2nd object: move up: confirm", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).deselect_all());
        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).select_node('obj-' + app.ui.getEnvVar('ydb_dir') + '/' + app.ui.getEnvVar('ydb_rel') + '/o*'));

        const btnClick = await page.$("#btnRoutinesSearchMoveUp");
        await btnClick.click();

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        const $ydb_dir = await page.evaluate(() => app.ui.getEnvVar('ydb_dir'));
        const $ydb_rel = await page.evaluate(() => app.ui.getEnvVar('ydb_rel'));
        const objPath = $ydb_dir + '/' + $ydb_rel + '/'

        expect(tree[0].id === 'obj-' + objPath + 'o*').to.be.true
        expect(tree[1].id === 'obj-/YDBGUI/objects*').to.be.true
    })

    it("Test # 1768: Select 2nd item (first source): DELETE: confirm only source is deleted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).deselect_all());
        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).select_node('child-/YDBGUI/routines'));

        let btnClick = await page.$("#btnRoutinesSearchRemove");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[0].children.length === 1).to.be.true
        expect(tree[0].children[0].id === 'child-/YDBGUI/mwebserver').to.be.true
    })

    it("Test # 1769: Select 4th item (2nd object): DELETE: confirm both obj and source are deleted", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).deselect_all());
        await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).select_node('obj-' + app.ui.getEnvVar('ydb_dir') + '/' + app.ui.getEnvVar('ydb_rel') + '/o*'));

        let btnClick = await page.$("#btnRoutinesSearchRemove");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.delay(100)

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));
        expect(tree[0].id === 'obj-/YDBGUI/objects*').to.be.true
        if (global.charMode === 'M') {
            expect(tree[1].id === 'so-/opt/yottadb/current/plugin/o/_ydbaim.so').to.be.true

        } else {
            expect(tree[1].id === 'so-/opt/yottadb/current/plugin/o/utf8/_ydbaim.so').to.be.true
        }
    })

    it("Test # 1770: Display dialog, select Add object path, should display dialog with Auto-relink option visible, Validate disabled, ok disabled and ... enabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        const visible = await page.evaluate(() => $('#divRoutineSearchPathAddAutoRelink').css('display'));
        expect(visible === 'block').to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddValidate').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddSelect').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.true
    })

    it("Test # 1771: Display dialog, select Add object path, type / as path and type validate, should validate ok and ok enabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        const isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        const disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.false
    })

    it("Test # 1772: Display dialog, select Add object path, type / as path and type validate, should validate ok and ok enabled, then type /123, ok should be disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.false

        await page.evaluate(() => $('#inpRoutinesSearchPathAdd').focus())
        await page.keyboard.type('/123');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');

        isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-invalid'));
        expect(isValid).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.true
    })

    it("Test # 1773: Display dialog, select Add object path, type /123 and validate, should display msgbox", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.evaluate(() => $('#inpRoutinesSearchPathAdd').focus())
        await page.keyboard.type('/123');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 1774: Display dialog, select Add object path, type /123 and ..., should display msgbox", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.evaluate(() => $('#inpRoutinesSearchPathAdd').focus())
        await page.keyboard.type('/123');

        btnClick = await page.$("#btnRoutinesSearchPathAddSelect");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 1775: Display dialog, select Add object path, type / and ..., should display dir selector", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.evaluate(() => $('#inpRoutinesSearchPathAdd').focus())
        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddSelect");
        await btnClick.click();

        await libs.waitForDialog('#modalDirSelect');
    })

    it("Test # 1776: Display dialog, select Add object path, press ..., should display dir selector and default to /", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        btnClick = await page.$("#btnRoutinesSearchPathAddSelect");
        await btnClick.click();

        await libs.waitForDialog('#modalDirSelect');

        const text = await page.evaluate(() => $('#txtDirSelectPath').text());
        expect(text === '/').to.be.true
    })

    it("Test # 1777: Display dialog, select Add object path, type / and validate, type ok, verify it has been added", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled === false).to.be.true

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[1].id === 'obj-/').to.be.true
    })

    it("Test # 1778: Display dialog, select Add object path, type / and validate, check the Auto-relink, type ok, verify it has been added with the * after the path", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#chkRoutineSearchPathAddAutoRelink");
        await btnClick.click();

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.false

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[1].id === 'obj-/*').to.be.true
    })

    it("Test # 1779: Display dialog, select Add object path, type /YDBGUI/objects and validate, should display a message saying it already exists", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/YDBGUI/objects');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.false

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');

        const text = await page.evaluate(() => $('#txtMsgboxText').text());
        expect(text === 'The path: /YDBGUI/objects is already in use.').to.be.true
    })

    it("Test # 1780: Display dialog, select Add source path, should display dialog with Validate disabled, ok disabled and ... enabled and no Auto-relink visible", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSource");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        const visible = await page.evaluate(() => $('#divRoutineSearchPathAddAutoRelink').css('display'));
        expect(visible === 'none').to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddValidate').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddSelect').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.true
    })

    it("Test # 1781: Display dialog, select Add source path, type / and validate, type ok, verify it has been added", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSource");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.false

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[0].children[0].id === 'child-/').to.be.true
    })

    it("Test # 1782: Display dialog, select Add source path, type /YDBGUI/routines and validate, type ok, should display a message saying that it already exists", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSource");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/YDBGUI/routines');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.false

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');

        const text = await page.evaluate(() => $('#txtMsgboxText').text());
        expect(text === 'The path: /YDBGUI/routines is already in use.').to.be.true
    })

    it("Test # 1783: Display dialog, select Add library, should display dialog with Validate disabled, ok disabled and ... enabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchFileAddValidate').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchFileAddSelect').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchFileAddOk').hasClass('disabled'));
        expect(disabled).to.be.true
    })

    it("Test # 1784: Display dialog, select Add library, type /, click validate, should display a dialog saying that it is not a file", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.false

        await libs.waitForDialog('#modalMsgbox');

        const text = await page.evaluate(() => $('#txtMsgboxText').text());
        expect(text === 'The path: / is a directory.').to.be.true
    })

    it("Test # 1785: Display dialog, select Add library, type /, click ..., should display the fileSelect dialog", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchFileAddSelect");
        await btnClick.click();

        await libs.waitForDialog('#modalFileSelect');
    })

    it("Test # 1786: Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, should validate ok", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/opt/yottadb/current/libgtmshr.so');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

    })

    it("Test # 1787: Display dialog, select Add library, type '/YDBGUI/package.json', validate, should return message that it is not an .so file", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/YDBGUI/package.json');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.false

        await libs.waitForDialog('#modalMsgbox');

        const text = await page.evaluate(() => $('#txtMsgboxText').text());
        expect(text === 'The path: /YDBGUI/package.json is not a valid .so file.').to.be.true
    })

    it("Test # 1788: Display dialog, select Add library, type '/', validate, should return message that it is not an .so file", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.false

        await libs.waitForDialog('#modalMsgbox');

        const text = await page.evaluate(() => $('#txtMsgboxText').text());
        expect(text === 'The path: / is a directory.').to.be.true
    })

    it("Test # 1789: Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK and verify that has been added", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/opt/yottadb/current/libgtmshr.so');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        btnClick = await page.$("#btnRoutinesSearchFileAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchFileAdd', 'close');

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[1].id === 'so-/opt/yottadb/current/libgtmshr.so').to.be.true
    })

    it("Test # 1790: Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK. Verify that buttons are correct", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/opt/yottadb/current/libgtmshr.so');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        btnClick = await page.$("#btnRoutinesSearchFileAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchFileAdd', 'close');

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[1].id === 'so-/opt/yottadb/current/libgtmshr.so').to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchAddObject').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSource').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSo').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchRemove').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveDown').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveUp').hasClass('disabled'));
        expect(disabled).to.be.false
    })

    it("Test # 1791: Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK. Move it up", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/opt/yottadb/current/libgtmshr.so');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        btnClick = await page.$("#btnRoutinesSearchFileAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchFileAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchMoveUp");
        await btnClick.click();

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[0].id === 'so-/opt/yottadb/current/libgtmshr.so').to.be.true
    })

    it("Test # 1792: Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK. Move it down once", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/opt/yottadb/current/libgtmshr.so');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        btnClick = await page.$("#btnRoutinesSearchFileAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchFileAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchMoveDown");
        await btnClick.click();

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[2].id === 'so-/opt/yottadb/current/libgtmshr.so').to.be.true

    })

    it("Test # 1793: Display dialog, select Add library, type '/opt/yottadb/current/libgtmshr.so', validate, select OK. Move it down until the System SO, button down should be disabled", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/opt/yottadb/current/libgtmshr.so');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        btnClick = await page.$("#btnRoutinesSearchFileAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchFileAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchMoveDown");
        await btnClick.click();

        const disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveDown').hasClass('disabled'));
        expect(disabled).to.be.false
    })

    it("Test # 1794: Display dialog, select Add library, type '/YDBGUI/package.json', type ..., should display the dialog with path set to /YDBGUI", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/YDBGUI/package.json');

        btnClick = await page.$("#btnRoutinesSearchFileAddSelect");
        await btnClick.click();

        await libs.waitForDialog('#modalFileSelect');

        const text = await page.evaluate(() => $('#txtFileSelectPath').text());
        expect(text === '/YDBGUI').to.be.true
    })

    it("Test # 1795: Perform one add, then delete, then select ok: should display alert message: Nothing changed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSo");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchFileAdd');

        await page.keyboard.type('/opt/yottadb/current/libgtmshr.so');

        btnClick = await page.$("#btnRoutinesSearchFileAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchFileAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        btnClick = await page.$("#btnRoutinesSearchFileAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchFileAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchRemove");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRoutinesSearchDirOk");
        await btnClick.click();

        await libs.waitForDialog('#modalMsgbox');

        const text = await page.evaluate(() => $('#txtMsgboxText').text());
        expect(text === 'Nothing has changed...').to.be.true
    })

    it("Test # 1796: Add one object path, submit and check the inputbox text for correctness", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchDirOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        const text = await page.evaluate(() => $('#txtInputboxText').text());
        const $ydb_dir = await page.evaluate(() => app.ui.getEnvVar('ydb_dir'));
        const $ydb_rel = await page.evaluate(() => app.ui.getEnvVar('ydb_rel'));
        const objPath = $ydb_dir + '/' + $ydb_rel + '/'
        if (global.charMode === 'M') {
            expect(text === 'Do you want to update the search path with the following $zroutines ?/YDBGUI/objects*(/YDBGUI/routines /YDBGUI/mwebserver) /() /data/' + $ydb_rel + '/o*(/data/' + $ydb_rel + '/r) /opt/yottadb/current/plugin/o/_ydbaim.so /opt/yottadb/current/plugin/o/_ydbgui.so /opt/yottadb/current/plugin/o/_ydbmwebserver.so /opt/yottadb/current/plugin/o/_ydbocto.so /opt/yottadb/current/plugin/o/_ydbposix.so /opt/yottadb/current/libyottadbutil.so').to.be.true

        } else {
            expect(text === 'Do you want to update the search path with the following $zroutines ?/YDBGUI/objects*(/YDBGUI/routines /YDBGUI/mwebserver) /() /data/' + $ydb_rel + '/o*(/data/' + $ydb_rel + '/r) /opt/yottadb/current/plugin/o/utf8/_ydbaim.so /opt/yottadb/current/plugin/o/utf8/_ydbgui.so /opt/yottadb/current/plugin/o/utf8/_ydbmwebserver.so /opt/yottadb/current/plugin/o/utf8/_ydbocto.so /opt/yottadb/current/plugin/o/utf8/_ydbposix.so /opt/yottadb/current/utf8/libyottadbutil.so').to.be.true

        }
    })

    it("Test # 1797: Add one object path, submit and check the inputbox text for correctness, re-open the window and confirm that the new path is displayed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchDirOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        const text = await page.evaluate(() => $('#txtInputboxText').text());
        const $ydb_dir = await page.evaluate(() => app.ui.getEnvVar('ydb_dir'));
        const $ydb_rel = await page.evaluate(() => app.ui.getEnvVar('ydb_rel'));
        const objPath = $ydb_dir + '/' + $ydb_rel + '/'

        if (global.charMode === 'M') {
            expect(text === 'Do you want to update the search path with the following $zroutines ?/YDBGUI/objects*(/YDBGUI/routines /YDBGUI/mwebserver) /() /data/' + $ydb_rel + '/o*(/data/' + $ydb_rel + '/r) /opt/yottadb/current/plugin/o/_ydbaim.so /opt/yottadb/current/plugin/o/_ydbgui.so /opt/yottadb/current/plugin/o/_ydbmwebserver.so /opt/yottadb/current/plugin/o/_ydbocto.so /opt/yottadb/current/plugin/o/_ydbposix.so /opt/yottadb/current/libyottadbutil.so').to.be.true

        } else {
            expect(text === 'Do you want to update the search path with the following $zroutines ?/YDBGUI/objects*(/YDBGUI/routines /YDBGUI/mwebserver) /() /data/' + $ydb_rel + '/o*(/data/' + $ydb_rel + '/r) /opt/yottadb/current/plugin/o/utf8/_ydbaim.so /opt/yottadb/current/plugin/o/utf8/_ydbgui.so /opt/yottadb/current/plugin/o/utf8/_ydbmwebserver.so /opt/yottadb/current/plugin/o/utf8/_ydbocto.so /opt/yottadb/current/plugin/o/utf8/_ydbposix.so /opt/yottadb/current/utf8/libyottadbutil.so').to.be.true

        }

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await page.evaluate(() => app.ui.rViewer.searchPath.show());
        await libs.waitForDialog('#modalRoutinesSearchPath');

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[1].id === 'obj-/')
    })

    it("Test # 1798: Create a routine bbb.m in /, add the path, perform a routine search, it should be found", async () => {
        // create the routine
        execSync('cd / && echo " new a\n" > bbb.m');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddSource");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled === false).to.be.true

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchDirOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        // now we start searching
        await page.evaluate(() => app.ui.rViewer.select.show());

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('bbb*');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        let cell = await page.$('#tblRviewerSelect >tbody >tr >td:nth-child(2)');
        let text = await page.evaluate(el => el.textContent, cell);
        expect(text === '/').to.be.true;
    })

    /*
    it("Test # 1799: Create a routine bb.m in /, compile it as object in /, add the object, perform a routine search, it should be found, but with no source", async () => {
        // compile the routine
        execSync('cd / && mumps -object=bbb.o bbb.m && rm bbb.m');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled === false).to.be.true

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchDirOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        // now we start searching
        await page.evaluate(() => app.ui.rViewer.select.show());

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('bbb*');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        let font = await page.evaluate(() => $('#tblRviewerSelect >tbody >tr ').css('font-style'));
        expect(font === 'italic').to.be.true;
    })

    it("Test # 1800: Create a routine bbb.m in /, compile it as object with the -emebed_source switch in /, add the object, perform a routine search, it should be found and ready to edit", async () => {
        // create the routine again
        execSync('cd / && echo " new a\n" > bbb.m');

        // compile the routine
        execSync('cd / && mumps -object=bbb.o -embed_source bbb.m && rm bbb.m');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled === false).to.be.true

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchMoveUp");
        await btnClick.click();

        await libs.delay(250)

        btnClick = await page.$("#btnRoutinesSearchDirOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        // now we start searching
        await page.evaluate(() => app.ui.rViewer.select.show());

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('bbb*');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        let font = await page.evaluate(() => $('#tblRviewerSelect >tbody >tr ').css('font-style'));
        expect(font !== 'italic').to.be.true;

        let cell = await page.$('#tblRviewerSelect >tbody >tr >td:nth-child(2)');
        let text = await page.evaluate(el => el.textContent, cell);
        expect(text === '/').to.be.true;
    })


    it("Test # 1801: add the object dir '/' (as 2nd entry), copy the object bbb.o in /YDBGUI/objects, perform a routine search, it should be found in /YDBGUI/objects", async () => {
        // copy the object
        execSync('cd / && cp bbb.o /YDBGUI/objects');

        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled === false).to.be.true

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchDirOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        // now we start searching
        await page.evaluate(() => app.ui.rViewer.select.show());

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('bbb*');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        let cell = await page.$('#tblRviewerSelect >tbody >tr >td:nth-child(2)');
        let text = await page.evaluate(el => el.textContent, cell);
        expect(text === '/YDBGUI/objects/').to.be.true;
    })

    it("Test # 1802: add the object dir '/' (as 1st entry), copy the object bbb.o in /YDBGUI/objects, perform a routine search, it should be found in /", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled === false).to.be.true

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        // move the object up
        btnClick = await page.$("#btnRoutinesSearchMoveUp");
        await btnClick.click();

        btnClick = await page.$("#btnRoutinesSearchDirOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        // now we start searching
        await page.evaluate(() => app.ui.rViewer.select.show());

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalRviewerSelect');

        //type a new name
        await page.keyboard.type('bbb*');
        await page.keyboard.press('Enter');

        await libs.delay(1000)

        let cell = await page.$('#tblRviewerSelect >tbody >tr >td:nth-child(2)');
        let text = await page.evaluate(el => el.textContent, cell);
        expect(text === '/').to.be.true;
    })

    it("Test # 1803: Display dialog, select Add object and source path, type / and validate, type ok, verify it has been added", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObjectSource");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(600)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.false

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        const tree = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_json('#'));

        expect(tree[1].id === 'om-/').to.be.true
    })

    it("Test # 1804: Display dialog, select Add object and source path, type / and validate, type ok, verify all buttons", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObjectSource");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(600)

        let isValid = await page.evaluate(() => $('#inpRoutinesSearchPathAdd').hasClass('is-valid'));
        expect(isValid).to.be.true

        let disabled = await page.evaluate(() => $('#btnRoutinesSearchPathAddOk').hasClass('disabled'));
        expect(disabled).to.be.false

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        const selected = await page.evaluate(() => $('#treeRoutinesSearch').jstree(true).get_selected(true)[0]);

        expect(selected.id === 'om-/').to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddObject').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSource').hasClass('disabled'));
        expect(disabled).to.be.true

        disabled = await page.evaluate(() => $('#btnRoutinesSearchAddSo').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchRemove').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveDown').hasClass('disabled'));
        expect(disabled).to.be.false

        disabled = await page.evaluate(() => $('#btnRoutinesSearchMoveUp').hasClass('disabled'));
        expect(disabled).to.be.false
    })
     */
})
