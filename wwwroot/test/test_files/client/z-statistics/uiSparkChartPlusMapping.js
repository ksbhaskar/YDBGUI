/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");

describe("Statistics: Stats: Sparkchart: Editor", async () => {
    it("Test # 2305: open when no sources, msgbox should be displayed", async () => {
        await utils.initStats()

        await page.evaluate(() => app.ui.stats.sparkChart.show())

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2306: open with disabled source, msgbox should be displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // disabling source
        let btnClick = await page.$("#sourcesEntryEnabled-0");
        await btnClick.click();

        let elem = await page.$('#btnStatsSourcesOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        await page.evaluate(() => app.ui.stats.sparkChart.show())

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2307: open with one valid source, verify population", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // check list fields
        list = await page.evaluate(() => $('#tblStatsSparkChartMain > tbody').children())
        expect(list.length === 1).to.be.true

        let colData = await page.evaluate(() => $('#tblStatsSparkChartMain > tbody > tr > td:nth-child(2)').text())
        expect(colData).to.have.string('SET, KIL, GET, ORD, DRD, DWT, JFL, JFS, JBB, JFB, JFW')

        colData = await page.evaluate(() => $('#tblStatsSparkChartMain > tbody > tr > td:nth-child(3)').text())
        expect(colData).to.have.string('Aggregate')

        colData = await page.evaluate(() => $('#tblStatsSparkChartMain > tbody > tr > td:nth-child(4)').text())
        expect(colData).to.have.string('DEFAULT')

        colData = await page.evaluate(() => $('#tblStatsSparkChartMain > tbody > tr > td:nth-child(5)').text())
        expect(colData).to.have.string('0.500')

        // number of rows in "Views" table
        list = await page.evaluate(() => $('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody').children())
        expect(list.length === 5).to.be.true
    })

    it("Test # 2308: open with one valid source and one disabled, verify population", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        // disabling source
        let btnClick = await page.$("#sourcesEntryEnabled-0");
        await btnClick.click();

        await utils.openSourceAdd()

        // check list fields
        await page.evaluate(() => $('#optStatsYgblstatsSelectorSamples option[value="SET"]').attr('selected', 'selected'))

        await page.evaluate(() => $('#optStatsYgblstatsSelectorRegions option[value="DEFAULT"]').attr('selected', 'selected'))

        let elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        elem = await page.$('#btnStatsSourcesOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        // open report definition
        await utils.openSparkchart()

        // number of rows in "Views" table
        list = await page.evaluate(() => $('#tblStatsSparkChartMain > tbody').children())
        expect(list.length === 1).to.be.true
    })

    it("Test # 2309: open sparkchart with no views, click ok, should display msgbox", async () => {
        await utils.initStats()

        await utils.openSource()

        await utils.openSourceAdd()
        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await utils.createSource()

        let elem = await page.$('#btnStatsSourcesOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        // open report definition
        await utils.openSparkchart()

        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChart', 'close');
    })

    it("Test # 2310: add report line, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // ensure it got created
        const list = await page.evaluate(() => $('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody').children())
        expect(list.length === 7).to.be.true
    })

    it("Test # 231: add report line, click ok, should display msgbox", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on icon
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr > td > i');
        await libs.clickOnElement(elem)

        // click on icon
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr > td > i');
        await libs.clickOnElement(elem)

        // number of rows in "Views" table
        const list = await page.evaluate(() => $('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody').children())
        expect(list.length === 3).to.be.true
    })

    it("Test # 2312: add report line, delete, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on delete icon
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(2) > td > i');
        await libs.clickOnElement(elem)

        // number of rows in "Views" table
        const list = await page.evaluate(() => $('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody').children())
        expect(list.length === 5).to.be.true
    })

    it("Test # 2313: add graph, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // number of rows in "Views" table
        const list = await page.evaluate(() => $('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody').children())
        expect(list.length === 7).to.be.true
    })

    it("Test # 2314: add graph, click ok, should close", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        const list = await page.evaluate(() => $('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody').children())
        expect(list.length === 7).to.be.true

        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChart', 'close');
    })

    it("Test # 2315: add graph, delete, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on icon
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(2) > td > i');
        await libs.clickOnElement(elem)

        // number of rows in "Views" table
        const list = await page.evaluate(() => $('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody').children())
        expect(list.length === 5).to.be.true
    })

    it("Test # 2316: add 2 graphs, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // number of rows in "Views" table
        const list = await page.evaluate(() => $('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody').children())
        expect(list.length === 9).to.be.true
    })


    it("Test # 2317: add 3 graphs, should display a msgbox", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2318: add 2 graphs, click ok, go to source, disable source, create new source, add graph, should be ok", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        await page.evaluate(() => app.ui.stats.sparkChart.okPressed())

        await libs.waitForDialog('#modalStatsSparkChart', 'close');

        await utils.openSource()

        let btnClick = await page.$("#sourcesEntryEnabled-0");
        await btnClick.click();

        await utils.openSourceAdd()

        await utils.createSource()

        let elem = await page.$('#btnStatsSourcesOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        await page.evaluate(() => app.ui.stats.sparkChart.show())

        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))
    })
})


describe("Statistics: Stats: Sparkchart: Mapping", async () => {
    it("Test # 2322: Create new report line, mapping should be properly populated and have no selections", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        // verify check boxes
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))).to.be.false

        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(2) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(3) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(4) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(5) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(6) input').prop('checked'))).to.be.false

        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(2) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(3) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(4) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(5) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(6) input').prop('checked'))).to.be.false

        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(2) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(3) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(4) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(5) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(6) input').prop('checked'))).to.be.false

        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(2) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(3) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(4) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(5) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(6) input').prop('checked'))).to.be.false

        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(6) td:nth-child(2) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(6) td:nth-child(3) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(6) td:nth-child(4) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(6) td:nth-child(5) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(6) td:nth-child(6) input').prop('checked'))).to.be.false

        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(7) td:nth-child(2) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(7) td:nth-child(3) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(7) td:nth-child(4) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(7) td:nth-child(5) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(7) td:nth-child(6) input').prop('checked'))).to.be.false
    })

    it("Test # 2323: Create new report line, select individuals, click cancel, reopen, should be empty", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        // verify check boxes
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked', true))
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked', true))
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked', true))

        elem = await page.$('#btnStatsSparkChartMappingCancel');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping', 'close')

        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        // verify check boxes
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))).to.be.false
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))).to.be.false
    })

    it("Test # 2324: Create new report line, select individuals, click ok, reopen, should be populated", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        // verify check boxes
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked', true))
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked', true))
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked', true))

        elem = await page.$('#btnStatsSparkChartMappingOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping', 'close')

        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        // verify check boxes
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))).to.be.true
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))).to.be.true
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))).to.be.true
    })

    it("Test # 2325: Create new report line, select individuals, click ok, close Spark chart with ok, reopen, reopen mapping, selection should be there", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        // verify check boxes
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked', true))
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked', true))
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked', true))

        elem = await page.$('#btnStatsSparkChartMappingOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping', 'close')

        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChart', 'close')

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        // verify check boxes
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))).to.be.true
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))).to.be.true
        expect(await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))).to.be.true

    })

    it("Test # 2326: Create new report line, select individuals, click ok, close Spark chart with cancel, reopen, reopen mapping, no selection should be there", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        // verify check boxes
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked', true))
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked', true))
        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked', true))

        elem = await page.$('#btnStatsSparkChartMappingOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping', 'close')

        elem = await page.$('#btnStatsSparkChartCancel');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChart', 'close')

        // open report definition
        await utils.openSparkchart()

        list = await page.evaluate(() => $('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody').children())
        expect(list.length === 5).to.be.true
    })

    it("Test # 2327: Create new report line, select all with header 1, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingAbs').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Abs'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2328: Create new report line, select all with header 1, then deselect all, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingAbs').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Abs'))

        await page.evaluate(() => $('#chkStatsSparkcChartMappingAbs').prop('checked', false))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Abs'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2329: Create new report line, select all with header 2, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingDelta').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Delta'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2330: Create new report line, select all with header 2, then deselect all, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingDelta').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Delta'))

        await page.evaluate(() => $('#chkStatsSparkcChartMappingDelta').prop('checked', false))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Delta'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2331: Create new report line, select all with header 3, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingMin').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Min'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2332: Create new report line, select all with header 3, then deselect all, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingMin').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Min'))

        await page.evaluate(() => $('#chkStatsSparkcChartMappingMin').prop('checked', false))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Min'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2333: Create new report line, select all with header 4, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingMax').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Max'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2334: Create new report line, select all with header 4, then deselect all, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingMax').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Max'))

        await page.evaluate(() => $('#chkStatsSparkcChartMappingMax').prop('checked', false))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Max'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2335: Create new report line, select all with header 4, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingAvg').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Avg'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.true

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2336: Create new report line, select all with header 4, then deselect all, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#chkStatsSparkcChartMappingAvg').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Avg'))

        await page.evaluate(() => $('#chkStatsSparkcChartMappingAvg').prop('checked', false))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Avg'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(6) input').prop('checked'))
        expect(status).to.be.false

        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(3) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(4) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(5) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2337: Create new report line, select one entry, then select all, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked', true))
        let checked = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(checked).to.be.true

        await page.evaluate(() => $('#chkStatsSparkcChartMappingAbs').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Abs'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
    })

    it("Test # 2338: Create new report line, select one entry, then deselect all, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        // open report definition
        await utils.openSparkchart()

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked', true))
        let checked = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(checked).to.be.true

        await page.evaluate(() => $('#chkStatsSparkcChartMappingAbs').prop('checked', true))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Abs'))

        await page.evaluate(() => $('#chkStatsSparkcChartMappingAbs').prop('checked', false))
        await page.evaluate(() => app.ui.stats.sparkChart.mapping.selectAll('Abs'))

        // verify check boxes
        let status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(1) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.true
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(2) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(3) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(4) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
        status = await page.evaluate(() => $('#tblStatsSparkcChartMapping  tbody tr:nth-child(5) td:nth-child(2) input').prop('checked'))
        expect(status).to.be.false
    })

    it("Test # 2339: Create source with 2 regions, open spark, create graph, select mapping, should have region combo displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(2))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(2))

        // open report definition
        await utils.openSparkchart()

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', 'af1a38b3-b4ee-464d-baed-b1cf2b108e25'))

        // click on mapping
        elem = await page.$('#options_af1a38b3-b4ee-464d-baed-b1cf2b108e25 > tbody > tr:nth-child(2) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        const status = await page.evaluate(() => $('#divStatsSparkChartMappingRegion').css('display'))
        expect(status).to.have.string('flex')
    })

    it("Test # 2340: Create source with 2 regions, open spark, create graph, select mapping, should have region combo displayed, close, select normal mapping, should NOT have region combo displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(2))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(2))

        // open report definition
        await utils.openSparkchart()

        // creates a graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', 'af1a38b3-b4ee-464d-baed-b1cf2b108e25'))

        // click on mapping
        elem = await page.$('#options_af1a38b3-b4ee-464d-baed-b1cf2b108e25 > tbody > tr:nth-child(2) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        let status = await page.evaluate(() => $('#divStatsSparkChartMappingRegion').css('display'))
        expect(status).to.have.string('flex')

        elem = await page.$('#btnStatsSparkChartMappingCancel');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping', 'close')

        // creates a reportLine view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('reportLine', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mapping
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping')

        status = await page.evaluate(() => $('#divStatsSparkChartMappingRegion').css('display'))
        expect(status).to.have.string('none')
    })
})
