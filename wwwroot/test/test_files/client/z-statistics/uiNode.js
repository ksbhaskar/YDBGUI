/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");

describe("Statistics: Node detection", async () => {
    it("Test # 2200: if missing, display msgbox", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?testStats`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await page.evaluate(() => app.system.node.status = 'N/A')

        await libs.delay(500)

        // init stats
        await page.evaluate(() => app.userSettings.stats.displayHelpAtTabOpen = false)
        await page.evaluate(() => app.ui.stats.showTab())

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2201: if ok, just display the tab", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?testStats`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await libs.delay(500)

        // init stats
        await page.evaluate(() => app.userSettings.stats.displayHelpAtTabOpen = false)
        await page.evaluate(() => app.ui.stats.showTab())

        await libs.waitForDialog('#tab-SDiv');
    })
})
