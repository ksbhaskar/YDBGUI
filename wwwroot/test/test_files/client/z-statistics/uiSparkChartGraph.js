/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../../libs');
const {expect} = require("chai");
const utils = require("./utils");

describe("Statistics: Stats: Sparkchart: Graph", async () => {
    it("Test # 2400: Create graph, open directly the settings, should display input box, select no, should close", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxNo");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');
    })

    it("Test # 2401: Create graph, open directly the settings, should display input box, select yes, should open the mapping", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('modalInputbox');

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('#modalStatsSparkChartMapping')
    })

    it("Test # 2402: Create graph and mapping, ensure series are displayed correctly", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        let items = await page.evaluate(() => $('#StatsSparkChartGraphSortable li ').text())
        items = items.split('...')

        expect(items.length === 4).to.be.true
        expect(items[0]).to.have.string('SET abs')
        expect(items[1]).to.have.string('SET Δ')
        expect(items[2]).to.have.string('SET Δ min')
    })

    it("Test # 2405: Create graph and mapping, click on Title, verify Preferences dialog opens with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#btnStatsSparkChartGraphTitle');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalPrefs')

        // verify header
        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Graph title')
    })

    /*
    it("Test # 2406: Create graph and mapping, click on Sub title, verify Preferences dialog opens with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#btnStatsSparkChartGraphSubTitle');
        await libs.clickOnElement(elem)

        //await libs.waitForDialog('#modalPrefs')

        // verify header
        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Graph sub title')
    })

     */

    it("Test # 2407: Create graph and mapping, click on Grid, verify Preferences dialog opens with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#btnStatsSparkChartGraphGrid');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalPrefs')

        // verify header
        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Graph Grid')
    })

    it("Test # 2408: Create graph and mapping, click on Legend, verify Preferences dialog opens with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#btnStatsSparkChartGraphLegend');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalPrefs')

        // verify header
        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Graph Legend')
    })

    it("Test # 2409: Create graph and mapping, click on Time scale, verify Preferences dialog opens with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#btnStatsSparkChartGraphXaxis');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalPrefs')

        // verify header
        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Timescale')
    })

    /*
    it("Test # 2410: Create graph and mapping, click on Y axis, verify Preferences dialog opens with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#btnStatsSparkChartGraphYaxis');
        await libs.clickOnElement(elem)

        await libs.delay(500)
        //await libs.waitForDialog('#modalPrefs')

        // verify header
        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Y axis')
    })

    it("Test # 2412: Create graph and mapping, click on Tooltips, verify Preferences dialog opens with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#btnStatsSparkChartGraphTooltip');
        await libs.clickOnElement(elem)

        //await libs.waitForDialog('#modalPrefs')

        // verify header
        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Graph Tooltips')
    })
     */
})


    describe("Statistics: Stats: Sparkchart: Graph: series", async () => {
        it("Test # 2430: Click on first Series button, ensure dialog is shown with correct title", async () => {
            await utils.initStats()

            // import test file
            await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

            // load it
            await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#StatsSparkChartGraphSortable > li > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSeries');

        // verify header
        const title = await page.evaluate(() => $('#lblStatsSparkChartGraphSeriesTitle').text())
        expect(title).to.have.string('Series properties: SET abs')
    })

    it("Test # 2431: Click on second Series button, ensure dialog is shown with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#StatsSparkChartGraphSortable > li:nth-child(2) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSeries');

        // verify header
        const title = await page.evaluate(() => $('#lblStatsSparkChartGraphSeriesTitle').text())
        expect(title).to.have.string('Series properties: SET Δ')
    })

    it("Test # 2432: Click on Line settings, ensure Preferences is loaded with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#StatsSparkChartGraphSortable > li:nth-child(2) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSeries');

        elem = await page.$('#btnStatsSparkChartGraphSeriesLineSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalPrefs')

        // verify header
        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Series line settings')
    })

    it("Test # 2433: Click on Y Axis, ensure Preferences is loaded with correct title", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(2)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraph');

        elem = await page.$('#StatsSparkChartGraphSortable > li:nth-child(2) > button');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSeries');

        elem = await page.$('#btnStatsSparkChartGraphSeriesYaxisSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalPrefs')

        // verify header
        const title = await page.evaluate(() => $('#lblPreferencesTitle').text())
        expect(title).to.have.string('Y axis settings')
    })
})

describe("Statistics: Stats: Sparkchart: Graph configuration", async () => {
    it("Test # 2440: Use report with no graphs, msgbox should be displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2441: Use report with one graph, msgbox should be displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 2442: Use report with two graphs, dialog should be displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(8) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        // click on mappings
        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings');
    })

    it("Test # 2443: Use report with two graphs, dialog should be displayed, ensure correct option is displayed", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on settings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(8) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings');

        const checked = await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationV').is(':checked'))
        expect(checked).to.be.true
    })

    it("Test # 2444: Use report with two graphs, dialog should be displayed, change the option, click cancel, verify it is NOT saved", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(8) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings');

        await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationH').prop('checked', true))
        let checked = await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationH').is(':checked'))
        expect(checked).to.be.true

        elem = await page.$('#btnStatsSparkChartGraphSettingsCancel');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings', 'close');

        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings');

        checked = await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationV').is(':checked'))
        expect(checked).to.be.true

    })

    it("Test # 2445: Use report with two graphs, dialog should be displayed, change the option, click ok, verify it is saved", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(8) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings');

        await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationH').prop('checked', true))
        let checked = await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationH').is(':checked'))
        expect(checked).to.be.true

        elem = await page.$('#btnStatsSparkChartGraphSettingsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings', 'close');

        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings');

        checked = await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationV').is(':checked'))
        expect(checked).to.be.false

    })
})

describe("Statistics: Stats: Dirty status", async () => {
    it("Test # 2500: Change source, click ok, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSource()

        let elem = await page.$('#row-0');
        await libs.clickOnElement(elem)

        let btnClick = await page.$("#btnStatsSourcesEdit");
        await btnClick.click();

        await libs.waitForDialog('#modalstatsYgblstatsSelector');

        await page.evaluate(() => $('#inpStatsYgblstatsSelectorRateSecs').val(3))
        await page.evaluate(() => app.ui.stats.sources.ygblstats.sampleRateUpdated())

        elem = await page.$('#btnStatsYgblstatsSelectorOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('modalInputbox');

        let text = await page.evaluate(() => $('#txtInputboxText').text())
        expect(text).to.have.string('Update the source?')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        await libs.waitForDialog('#modalstatsYgblstatsSelector', 'close');

        let colData = await page.evaluate(() => $('#tblStatsSources > tbody > tr > td:nth-child(6)').text())
        expect(colData).to.have.string('3')

        elem = await page.$('#btnStatsSourcesOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSources', 'close');

        // verify dirty status
        text = await page.evaluate(() => $('#lblStatsReportName').text())
        expect(text === '[test-1*]').to.be.true
    })

    it("Test # 2501: Change spark chart, click ok, verify", async () => {
        await utils.initStats()

        // import test file
        await page.evaluate(() => app.ui.stats.storage.test.importTestFile(1))

        // load it
        await page.evaluate(() => app.ui.stats.storage.test.loadTest(1))

        await utils.openSparkchart()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(6) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        // create graph view
        await page.evaluate(() => app.ui.stats.sparkChart.optionsTable.createView('graph', '99e29ff8-2565-4e63-866c-35ae46d24f44'))

        // click on mappings
        elem = await page.$('#options_99e29ff8-2565-4e63-866c-35ae46d24f44 > tbody > tr:nth-child(8) > td:nth-child(3) > button:nth-child(1)');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartMapping');

        await utils.createMapping()

        elem = await page.$('#btnStatsSparkChartGraphSettings');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings');

        await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationH').prop('checked', true))
        let checked = await page.evaluate(() => $('#optStatsSparkChartGraphSettingsOrientationH').is(':checked'))
        expect(checked).to.be.true

        elem = await page.$('#btnStatsSparkChartGraphSettingsOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChartGraphSettings', 'close');

        elem = await page.$('#btnStatsSparkChartOk');
        await libs.clickOnElement(elem)

        await libs.waitForDialog('#modalStatsSparkChart', 'close');

        // verify dirty status
        const text = await page.evaluate(() => $('#lblStatsReportName').text())
        expect(text === '[test-1*]').to.be.true
    })
})
