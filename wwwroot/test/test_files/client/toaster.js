/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

const libs = require('../../libs');
const {expect} = require("chai");

describe("CLIENT: Toaster", async () => {
    it("Test # 1750: Submit a REORG, check if the Async pill is visible, wait for reorg to complete, should disappear", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.defrag.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalDefrag');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await page.evaluate(() => $('#swCompactRunBackground').prop('checked', true));

            await libs.delay(250)

            // submit dialog
            await libs.delay(250)
            await libs.clickOnElement(await page.$('#btnCompactOk'))

            await libs.waitForDialog('modalInputbox');

            // confirm
            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            // ensure async pill is visible
            await libs.delay(5)
            let vis = await page.evaluate(() => $('#puAsync').css('display'));
            expect(vis === 'block').to.be.true

            await libs.delay(3000)

            const text = await page.evaluate(() => $('#toastTitle-0').text());
            expect(text).to.have.string('Success: Reorg')

            // ensure async pill is invisible
            vis = await page.evaluate(() => $('#puAsync').css('display'));
            expect(vis === 'none').to.be.true
        }
    })
})

