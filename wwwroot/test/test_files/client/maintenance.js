/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

const libs = require('../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

/*
NOTE: all replicated related tests are temporarily commented out, due to changes needed
 */


describe("MAINTENANCE: Integ", async () => {
    it("Test # 1600: RO mode: Menu entry should be disabled", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let disabled = await page.evaluate(() => $('#menuSystemAdministrationIntegrity').hasClass('disabled'));
            expect(disabled).to.be.true
        }
    })

    it("Test # 1601: Display dialog: ok button should be disabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            const disabled = await page.evaluate(() => $('#btnIntegOk').prop('disabled'));
            expect(disabled).to.be.true
        }
    })

    it("Test # 1602: Display dialog: select region, ok button should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.delay(500)

            const disabled = await page.evaluate(() => $('#btnIntegOk').prop('disabled'));
            expect(disabled).to.be.false
        }
    })

    it("Test # 1603: Display dialog: select region, deselect, ok button should be disabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.delay(750)

            // select DEFAULT
            elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.delay(500)

            const disabled = await page.evaluate(() => $('#btnIntegOk').prop('disabled'));
            expect(disabled).to.be.true
        }
    })

    it("Test # 1604: Select type: fast'. Reporting detailed` should be disabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            await libs.clickOnElement(await page.$('#optIntegFast'))

            await libs.delay(400)

            const disabled = await page.evaluate(() => $('#optIntegDetailed').prop('disabled'))
            expect(disabled).to.be.true
        }
    })

    it("Test # 1605: Select type: fast and then complete. Reporting detailed should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            await libs.clickOnElement(await page.$('#optIntegFast'))
            await libs.delay(400)

            await libs.clickOnElement(await page.$('#optIntegComplete'))
            await libs.delay(400)

            const disabled = await page.evaluate(() => $('#optIntegDetailed').prop('disabled'))
            expect(disabled).to.be.false
        }
    })

    it("Test # 1606: select Advanced option, options should be visible", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            await libs.clickOnElement(await page.$('#swIntegAdvancedOptions'))
            await libs.delay(750)

            const visible = await page.evaluate(() => $('#collapseIntegAdvanced').hasClass('show'))
            expect(visible).to.be.true
        }
    })

    it("Test # 1607: select Advanced option twice, options should be hidden", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            await libs.clickOnElement(await page.$('#swIntegAdvancedOptions'))
            await libs.delay(750)

            await libs.clickOnElement(await page.$('#swIntegAdvancedOptions'))
            await libs.delay(750)

            const visible = await page.evaluate(() => $('#collapseIntegAdvanced').hasClass('show'))
            expect(visible).to.be.false
        }
    })

    it("Test # 1608: change all options on dialog, close and open up again, everything should be reset", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            await page.$('#DEFAULT_anchor');

            await libs.clickOnElement(await page.$('#optIntegFast'))

            await libs.clickOnElement(await page.$('#swIntegAdvancedOptions'))
            await libs.delay(750)
            await libs.clickOnElement(await page.$('#swIntegKeyRanges'))
            await page.evaluate(() => $('#inpIntegMap').val(11))
            await page.evaluate(() => $('#inpIntegKeySize').val(12))
            await page.evaluate(() => $('#inpIntegTransaction').val(13))

            await libs.delay(750)

            await libs.clickOnElement(await page.$('#btnIntegCancel'))

            await libs.waitForDialog('#modalInteg', 'close');

            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            let checked = await page.evaluate(() => $('#optIntegComplete').prop('checked'))
            expect(checked).to.be.true
            checked = await page.evaluate(() => $('#optIntegSummary').prop('checked'))
            expect(checked).to.be.true
            let visible = await page.evaluate(() => $('#collapseIntegAdvanced').hasClass('show'))
            expect(visible).to.be.false

            checked = await page.evaluate(() => $('#swIntegKeyRanges').prop('checked'))
            expect(checked).to.be.true
            let value = await page.evaluate(() => $('#inpIntegMap').val())
            expect(value === '10').to.be.true
            value = await page.evaluate(() => $('#inpIntegKeySize').val())
            expect(value === '10').to.be.true
            value = await page.evaluate(() => $('#inpIntegTransaction').val())
            expect(value === '10').to.be.true
        }
    })

    it("Test # 1609: Select DEFAULT region, press Ok, inputbox should be displayed", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnIntegOk'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox');
        }
    })

    it("Test # 1610: Select DEFAULT region, press Ok, inputbox should be displayed, press cancel, should close", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnIntegOk'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox');

            await libs.clickOnElement(await page.$('#btnInputboxNo'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox', 'close');
        }
    })

    it("Test # 1611: Select DEFAULT region, press Ok, inputbox should be displayed, press Ok, should generate report complete, summary", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnIntegOk'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox');

            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalIntegResult');
        }
    })

    it("Test # 1612: Ensure \"Run background\" is reset after dialog close / open", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            await libs.clickOnElement(await page.$('#swIntegRunBackground'))
            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnIntegCancel'))

            await libs.waitForDialog('#modalInteg', 'close');

            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            let value = await page.evaluate(() => $('#swIntegRunBackground').prop('checked'))
            expect(value).to.be.false
        }
    })

    it("Test # 1613: Submit using \"Run background\": message box should display extra text", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.clickOnElement(await page.$('#swIntegRunBackground'))

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnIntegOk'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox');

            const text = await page.evaluate(() => $('#txtInputboxText').text());
            expect(text).to.have.string('You have chosen to run the Integrity check')
        }
    })

    it("Test # 1614: Run using \"Run background\": toaster should appear", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.clickOnElement(await page.$('#swIntegRunBackground'))

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnIntegOk'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox');

            // confirm
            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            await libs.delay(3000)

            const text = await page.evaluate(() => $('#toastTitle-0').text());
            expect(text).to.have.string('Success: Integrity check')
        }
    })

    it("Test # 1615: Run using \"Run background\": click on toast, should display result dialog", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.clickOnElement(await page.$('#swIntegRunBackground'))

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnIntegOk'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox');

            // confirm
            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            await libs.delay(3000)

            await libs.clickOnElement(await page.$('#btnToastBody-0'))

            await libs.waitForDialog('#modalIntegResult');
        }
    })

    it("Test # 1616: Select DEFAULT region, press Ok, inputbox should be displayed, press Ok, should generate report complete, summary", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.integ.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalInteg');

            // select DEFAULT
            let elem = await page.$('#DEFAULT_anchor');
            await libs.clickOnElement(elem)

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnIntegOk'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('modalInputbox');

            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalIntegResult');

            // check the content
            const text = await page.evaluate(() => $('#divIntegResult').text());
            expect(text).to.have.string('Integ of region DEFAULT')
        }
    })

})

describe("MAINTENANCE: Backup", async () => {
    it("Test # 1650: Display dialog, ensure all default values are correct", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');
        }
    })

    it("Test # 1651: Change all values, close dialog, reopen it and expect all default values correct", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await libs.clickOnElement(await page.$('#swBackupExistingFilesReplace'))
            await libs.clickOnElement(await page.$('#swBackupDisableJournaling'))
            await libs.clickOnElement(await page.$('#swBackupRecord'))

            await libs.clickOnElement(await page.$('#optBackupJournalNoLink'))
            await libs.clickOnElement(await page.$('#swBackupSyncIo'))

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupCancel'))

            // wait for dialog to be closed by the async call
            await libs.waitForDialog('#modalBackup', 'close');

            await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            let val = await page.evaluate(() => $('#inpBackupTargetDir').val())
            expect(val !== '').to.be.true

            val = await page.evaluate(() => $('#swBackupExistingFilesReplace').prop('checked'))
            expect(val).to.be.false
            val = await page.evaluate(() => $('#swBackupDisableJournaling').prop('checked'))
            expect(val).to.be.false
            val = await page.evaluate(() => $('#swBackupRecord').prop('checked'))
            expect(val).to.be.false

            val = await page.evaluate(() => $('#optBackupJournalLink').prop('checked'))
            expect(val).to.be.true
            val = await page.evaluate(() => $('#swBackupSyncIo').prop('checked'))
            expect(val).to.be.false
        }
    })

    it("Test # 1652: Select DEFAULT, Ok button should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            const val = await page.evaluate(() => $('#btnBackupOk').prop('disabled'))
            expect(val).to.be.false
        }
    })

    /*
    // these test were commented out due to unknown errors in the pipeline server
    it("Test # 1653: Enter / as target directory, Validate should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.delay(1000)

            const val = await page.evaluate(() => $('#btnBackupValidateTarget').prop('disabled'))
            expect(val).to.be.false
        }
    })

    it("Test # 1654: Enter / as target directory, press validate, it should be valid", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(1000)

            const val = await page.evaluate(() => $('#inpBackupTargetDir').hasClass('is-valid'))
            expect(val).to.be.true
        }
    })

     */

    it("Test # 1655: Enter / as target directory, press validate, ok button should be disabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(1000)

            const val = await page.evaluate(() => $('#btnBackupOk').prop('disabled'))
            expect(val).to.be.true
        }
    })

    /*
    it("Test # 1656: Enter / as target directory, press validate, select DEFAULT, ok button should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(1000)

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            const val = await page.evaluate(() => $('#btnBackupOk').prop('disabled'))
            expect(val).to.be.false
        }
    })

     */

    it("Test # 1658: Select Create New Journal Files: Yes, link,  SyncIO should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await libs.clickOnElement(await page.$('#optBackupJournalLink'))

            await libs.delay(250)

            const val = await page.evaluate(() => $('#swBackupSyncIo').prop('disabled'))
            expect(val).to.be.false
        }
    })

    it("Test # 1659: Select Create New Journal Files: Yes, nolink,  SyncIO should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await libs.clickOnElement(await page.$('#optBackupJournalNoLink'))

            await libs.delay(250)

            const val = await page.evaluate(() => $('#swBackupSyncIo').prop('disabled'))
            expect(val).to.be.false
        }
    })

    it("Test # 1660: Select Create New Journal Files: No,  SyncIO should be disabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await libs.clickOnElement(await page.$('#optBackupJournalNone'))

            await libs.delay(250)

            const val = await page.evaluate(() => $('#swBackupSyncIo').prop('disabled'))
            expect(val).to.be.true
        }
    })

    it("Test # 1661: Mark DEFAULT as repl and clear repl env vars, select DEFAULT, msgbox should appear", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?test=1661`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await libs.waitForDialog('#modalMsgbox');

            const val = await page.evaluate(() => $('#txtMsgboxText').text())
            expect(val).to.have.string('The env var $ydb_repl_instance or $gtm_repl_instance ')
        }
    })

    it("Test # 1662: Mark DEFAULT as repl, select DEFAULT, msgbox should appear", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?test=1662`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await libs.waitForDialog('#modalMsgbox');

            const val = await page.evaluate(() => $('#txtMsgboxText').text())
            expect(val).to.have.string('The path: /data/r1.34_x86_64/g/yottadb.repl')
        }
    })

    /*
    it("Test # 1663: Mark DEFAULT as repl, create file, select DEFAULT, repl options should be visible", async () => {
        if (global.serverMode === 'RW') {
            // create repl instance file
            execSync('. /opt/yottadb/current/ydb_env_set && mupip set -replication=on -region default', {stdio: 'ignore'});
            execSync('. /opt/yottadb/current/ydb_env_set && mupip replicate -instance_create -name=DUMMY', {stdio: 'ignore'});

            // use now the local replication flag
            await page.goto(`https://localhost:${MDevPort}//index.html?`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await libs.delay(250)

            const val = await page.evaluate(() => $('#divBackupReplication').css('display'))
            expect(val === 'block').to.be.true
        }
    })

     */

    /*
    // these test were commented out due to unknown errors in the pipeline server
    it("Test # 1664: Mark DEFAULT as repl, create file, select DEFAULT, validate path, repl options should be visible and ok button should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await libs.delay(500)

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(1000)

            let val = await page.evaluate(() => $('#divBackupReplication').css('display'))
            expect(val === 'block').to.be.true

            val = await page.evaluate(() => $('#btnBackupOk').prop('disabled'))
            expect(val).to.be.false
        }
    })

    it("Test # 1665: Mark DEFAULT as repl, create file, select DEFAULT, uncheck Backup Replication Instance, other repl options should be invisible", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await libs.delay(500)
            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#swBackupReplication'))

            await libs.delay(750)

            let val = await page.evaluate(() => $('#divBackupReplicationOptions').css('display'))
            expect(val === 'none').to.be.true
        }
    })

    it("Test # 1666: Mark DEFAULT as repl, create file, select DEFAULT, uncheck Backup Replication Instance, ok button should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#swBackupReplication'))

            await libs.delay(500)

            val = await page.evaluate(() => $('#btnBackupOk').prop('disabled'))
            expect(val).to.be.false
        }
    })

    it("Test # 1667: Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, extra fields should be visible", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await libs.delay(200)
            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#optBackupReplNewPath'))

            await libs.delay(500)

            let val = await page.evaluate(() => $('#divBackupReplicationTargetDir').css('display'))
            expect(val === 'flex').to.be.true
        }
    })


    it("Test # 1668: Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, ok button should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await libs.delay(200)
            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#optBackupReplNewPath'))

            await libs.delay(500)

            await page.evaluate(() => $('#inpBackupReplicationTargetDir').focus())
            await page.keyboard.type('/newdir');

            val = await page.evaluate(() => $('#btnBackupOk').prop('disabled'))
            expect(val).to.be.true
        }
    })
     */

    /*
    // these test were commented out due to unknown errors in the pipeline server
    it("Test # 1669: Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, validate should be disabled, type in / and validate should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#optBackupReplNewPath'))

            await libs.delay(500)

            await page.evaluate(() => $('#inpBackupReplicationTargetDir').focus())
            await page.keyboard.type('/');

            let val = await page.evaluate(() => $('#btnBackupValidateReplicationTarget').prop('disabled'))
            expect(val).to.be.false
        }
    })

    it("Test # 1670: Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, validate should be disabled, type in /, validate: it should validate ok", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#optBackupReplNewPath'))

            await libs.delay(500)

            await page.evaluate(() => $('#inpBackupReplicationTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateReplicationTarget'))

            await libs.delay(1000)

            let val = await page.evaluate(() => $('#inpBackupReplicationTargetDir').hasClass('is-valid'))
            expect(val).to.be.true
        }
    })

    it("Test # 1671: Mark DEFAULT as repl, create file, select DEFAULT, select Specify a Different Directory, validate should be disabled, type in /, validate: ok button should be enabled", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html?`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#optBackupReplNewPath'))

            await libs.delay(500)

            await page.evaluate(() => $('#inpBackupReplicationTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateReplicationTarget'))

            await libs.delay(1000)

            let val = await page.evaluate(() => $('#inpBackupReplicationTargetDir').hasClass('is-valid'))
            expect(val).to.be.true

            val = await page.evaluate(() => $('#btnBackupOk').prop('disabled'))
            expect(val).to.be.false
        }
    })

    it("Test # 1672: Select DEFAULT, enter / in path and validate, submit, msgbox should appear", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnBackupOk'))

            await libs.waitForDialog('modalInputbox');

            const val = await page.evaluate(() => $('#txtInputboxText').text())
            expect(val).to.have.string('Do you want to proceed with the backup operation ?')
        }
    })

    it("Test # 1673: Select DEFAULT, enter / in path and validate, submit, msgbox should appear, cancel, should close", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnBackupOk'))

            await libs.waitForDialog('modalInputbox');

            await libs.clickOnElement(await page.$('#btnInputboxNo'))

            await libs.waitForDialog('modalInputbox', 'close');
        }
    })

    it("Test # 1674: Select DEFAULT, enter / in path and validate, submit, msgbox should appear, submit, should display the result dialog", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnBackupOk'))

            //await libs.waitForDialog('modalInputbox');

            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            await libs.waitForDialog('#modalMsgbox');
        }
    })

    it("Test # 1675: Select DEFAULT, enter / in path and validate, submit, msgbox should appear, submit, should check the result dialog for correctness", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            await libs.clickOnElement(await page.$('#btnBackupOk'))

            await libs.waitForDialog('modalInputbox');

            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            //await libs.waitForDialog('#modalMsgbox');

            const val = await page.evaluate(() => $('#txtMsgboxText').text())
            expect(val).to.have.string('BACKUP COMPLETED')

        }
    })


    it("Test # 1676: click on ..., it should display the Dir Select dialog and have the path set to /", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await libs.clickOnElement(await page.$('#btnBackupSelectTarget'))

            await libs.waitForDialog('#modalDirSelect');

            const val = await page.evaluate(() => $('#txtDirSelectPath').text())
            expect(val === '/').to.be.true
        }
    })
     */

    /*
    // these test were commented out due to unknown errors in the pipeline server
    it("Test # 1677: type $ydb_dir, click on ..., it should display the Dir Select dialog and have the path set to /data", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('$ydb_dir');

            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupSelectTarget'))

            await libs.waitForDialog('#modalDirSelect');

            const val = await page.evaluate(() => $('#txtDirSelectPath').text())
            expect(val === '/data').to.be.true
        }
    })

    it("Test # 1678: type $ydb_dir, click on validate, should be ok", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('$ydb_dir');

            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(1000)

            const val = await page.evaluate(() => $('#inpBackupTargetDir').hasClass('is-valid'))
            expect(val).to.be.true
        }
    })

    it("Test #  1679: type /notexist, click on validate, should be error", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/notexist');

            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(1000)

            let val = await page.evaluate(() => $('#inpBackupTargetDir').hasClass('is-invalid'))
            expect(val).to.be.true

            await libs.waitForDialog('#modalMsgbox');

            val = await page.evaluate(() => $('#txtMsgboxText').text())
            expect(val).to.have.string('is not a valid path.')
        }
    })

    it("Test #  1680: type /YDBGUI/wwwroot/index.html, validate, should return error", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/YDBGUI/wwwroot/index.html');

            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(1000)

            let val = await page.evaluate(() => $('#inpBackupTargetDir').hasClass('is-invalid'))
            expect(val).to.be.true

            await libs.waitForDialog('#modalMsgbox');

            val = await page.evaluate(() => $('#txtMsgboxText').text())
            expect(val).to.have.string('is a file and not a directory.')
        }
    })

     */

    it("Test # 1681: RO mode, should be disabled", async () => {
        if (global.serverMode === 'RO') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');

            let disabled = await page.evaluate(() => $('#menuSystemAdministrationBackup').hasClass('disabled'));
            expect(disabled).to.be.true
        } else {
            // and reset the replication flag
            execSync('. /opt/yottadb/current/ydb_env_set && mupip set -replication=off -region default', {stdio: 'ignore'});

        }
    })

    /*
    it("Test # 1682: Ensure \"Run background\" is reset after dialog close / open", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            await libs.clickOnElement(await page.$('#swBackupRunBackground'))
            await libs.delay(250)

            await libs.clickOnElement(await page.$('#btnBackupCancel'))

            // wait for dialog to be closed by the async call
            await libs.waitForDialog('#modalBackup', 'close');

            await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            const val = await page.evaluate(() => $('#swBackupRunBackground').prop('checked'))
            expect(val).to.be.false
        }
    })

     */

    /*
    // these test were commented out due to unknown errors in the pipeline server
    it("Test # 1683: Submit using \"Run background\": message box should display extra text", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))


            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#swBackupRunBackground'))
            await libs.delay(150)

            await libs.clickOnElement(await page.$('#btnBackupOk'))

            await libs.waitForDialog('modalInputbox');

            const text = await page.evaluate(() => $('#txtInputboxText').text());
            expect(text).to.have.string('You have chosen to run the backup')
        }
    })

    it("Test # 1684: Run using \"Run background\": toaster should appear", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))


            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#swBackupRunBackground'))
            await libs.delay(150)

            await libs.clickOnElement(await page.$('#btnBackupOk'))

            await libs.waitForDialog('modalInputbox');

            // confirm
            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            await libs.delay(3000)

            const text = await page.evaluate(() => $('#toastTitle-0').text());
            expect(text).to.have.string('Success: Backup')
        }
    })

    it("Test # 1685: Run using \"Run background\": click on toast, should display result dialog", async () => {
        if (global.serverMode === 'RW') {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

            // wait for dashboard to be set by the async call
            await libs.waitForDialog('#modalDashboard');
            let state = await page.evaluate(() => app.ui.backup.show());
            // wait for dialog to be set by the async call
            await libs.waitForDialog('#modalBackup');

            // select DEFAULT
            await libs.clickOnElement(await page.$('#DEFAULT_anchor'))


            await page.evaluate(() => $('#inpBackupTargetDir').focus())
            await page.keyboard.type('/');

            await libs.clickOnElement(await page.$('#btnBackupValidateTarget'))

            await libs.delay(500)

            for (let ix = 0; ix < 8; ix++) await page.keyboard.press('Tab');

            await libs.clickOnElement(await page.$('#swBackupRunBackground'))
            await libs.delay(150)

            await libs.clickOnElement(await page.$('#btnBackupOk'))

            await libs.waitForDialog('modalInputbox');

            // confirm
            await libs.clickOnElement(await page.$('#btnInputboxYes'))

            await libs.delay(3000)

            await libs.clickOnElement(await page.$('#btnToastBody-0'))

            await libs.waitForDialog('#modalMsgbox');
        }
    })

     */
})

