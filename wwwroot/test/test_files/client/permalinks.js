/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');

describe("CLIENT: PERMALINKS: global links", async () => {
    it("Test # 1900: append ?P=, should work fine", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
    })

    it("Test # 1901: append ?testing, should work fine", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?testing`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');
    })

    it("Test # 1902: append ?P=fsdffsdfsdfsd, should return the message: The permalink is not valid...", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=fsdffsdfsdfsd`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 1903: append ?P=Z2xvYmFsfF5QU05ERkBA, should work and display the ^PSNDF global", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=Z2xvYmFsfF5QU05ERkBA`, {
            waitUntil: "domcontentloaded"
        });

        // wait for  dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const text = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1) td:nth-child(2)').text());
        expect(text).to.have.string('^PSNDF(50.6,0)')
    })

    it("Test # 1904: append ?P=Z2xvYmFsfF5QU05ERig1MC42LDUsKilAQA==, should work and display the ^PSNDF(50.6,5,*) global", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=Z2xvYmFsfF5QU05ERig1MC42LDUsKilAQA==`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const text = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1) td:nth-child(2)').text());
        expect(text).to.have.string('^PSNDF(50.6,5,0)')
    })

    it("Test # 1905: append ?P=Z2xvYmFsfF5QU05ERkB, should return the message: The permalink is not valid...", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=Z2xvYmFsfF5QU05ERkB`, {
            waitUntil: "domcontentloaded"
        });

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 1906: append ?P=Z2xvYmFsfF5QU05ERkA, should return the message: The permalink is not valid...", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=Z2xvYmFsfF5QU05ERkA`, {
            waitUntil: "domcontentloaded"
        });

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 1907: append ?P=Z2xvYmFsfF5QU05ERBA, should return the message: The permalink is not valid...", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=Z2xvYmFsfF5QU05ERBA`, {
            waitUntil: "domcontentloaded"
        });

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 1908: append ?P=2xvYmFsfF5QU05ERkBA, should return the message: The permalink is not valid...", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=2xvYmFsfF5QU05ERkBA`, {
            waitUntil: "domcontentloaded"
        });

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 1909: append ?P=ZxvYmFsfF5QU05ERBA, should return the message: The permalink is not valid...", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=ZxvYmFsfF5QU05ERBA`, {
            waitUntil: "domcontentloaded"
        });

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
    })

    it("Test # 1910: append ?P=ZxvYfF5QU05ERBA, should return the message: The permalink is not valid...", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=ZxvYfF5QU05ERBA`, {
            waitUntil: "domcontentloaded"
        });

        // wait for msgbox to be set by the async call
        await libs.waitForDialog('#modalMsgbox');
    })
})

describe("CLIENT: PERMALINKS: routine links", async () => {
    it("Test # 1911: append ?P=cm91dGluZXwleWRiZ3VpQEA=, should display the %ydbgui routine", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=cm91dGluZXwleWRiZ3VpQEA=`, {
            waitUntil: "domcontentloaded"
        });

        // wait for routine viewer to be set by the async call
        await page.$('#tabRviewer-R-1Div');

        await libs.delay(500)
        const text = await page.evaluate(() => $('#divRviewerDiv-R-1').text());
        expect(text).to.have.string('%ydbgui')
    })

    it("Test # 1912: append ?P=cm91dGluZXwlZ29AQA==, should display the %go routine", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=cm91dGluZXwlZ29AQA==`, {
            waitUntil: "domcontentloaded"
        });

        // wait for routine viewer to be set by the async call
        await page.$('#tabRviewer-R-1Div');

        await libs.delay(500)
        const text = await page.evaluate(() => $('#divRviewerDiv-R-1').text());
        expect(text).to.have.string('%GO')
    })

    /*
    it("Test # 1913: append ?P=cm91dGluZXwlZ29AQA==, should display the %go routine", async () => {
        if (global.charMode === 'M') {
            await page.goto(`https://localhost:${MDevPort}//index.html?P=cm91dGluZXwlZ298L2RhdGEvcjEuMzhfeDg2XzY0L28qKC9kYXRhL3IxLjM4X3g4Nl82NC9yKSAvWURCR1VJL29iamVjdHMqKC9ZREJHVUkvcm91dGluZXMgL1lEQkdVSS9td2Vic2VydmVyKSAvb3B0L3lvdHRhZGIvY3VycmVudC9wbHVnaW4vby9feWRiYWltLnNvIC9vcHQveW90dGFkYi9jdXJyZW50L3BsdWdpbi9vL195ZGJndWkuc28gL29wdC95b3R0YWRiL2N1cnJlbnQvcGx1Z2luL28vX3lkYm13ZWJzZXJ2ZXIuc28gL29wdC95b3R0YWRiL2N1cnJlbnQvcGx1Z2luL28vX3lkYm9jdG8uc28gL29wdC95b3R0YWRiL2N1cnJlbnQvcGx1Z2luL28vX3lkYnBvc2l4LnNvIC9vcHQveW90dGFkYi9jdXJyZW50L2xpYnlvdHRhZGJ1dGlsLnNvQEA=`, {
                waitUntil: "domcontentloaded"
            });

            // wait for routine viewer to be set by the async call
            await page.$('#tabRviewer-R-1Div');

            await libs.delay(1000)
            text = await page.evaluate(() => $('#divRviewerDiv-R-1').text());
            expect(text).to.have.string('%GO')
        }
    })
     */
})

describe("CLIENT: PERMALINKS: UI", async () => {
    it("Test # 1930: Select the global ^PSNDF, click on icon and verify the permalink", async () => {
        if (global.charMode === 'M') {
            await page.goto(`https://localhost:${MDevPort}//index.html?P=Z2xvYmFsfF5QU05ERkBA`, {
                waitUntil: "domcontentloaded"
            });

        } else {
            await page.goto(`https://localhost:${MDevPort}//index.html?P=Z2xvYmFsfF5QU05ERig1MC42LDApQEA=`, {
                waitUntil: "domcontentloaded"
            });
        }

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // verify that we have the right global
        let text = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1) td:nth-child(2)').text());
        expect(text).to.have.string('^PSNDF(50.6,0)')

        // click on the Permalink icon
        btnClick = await page.$("#btnGviewerPermalink-G-1");
        await btnClick.click();

        // wait for permalinks dialog to be set by the async call
        await libs.waitForDialog('#modalPermalinks');

        // read the permalink (remove the readonly first or text is not available)
        await page.evaluate(() => $('#inpPermalink').attr('readonly', false));
        text = await page.evaluate(() => $('#inpPermalink').val());
        if (global.charMode === 'M') {
            expect(text).to.have.string('https://localhost:8089?P=Z2xvYmFsfF5QU05ERkBA')

        } else {
            expect(text).to.have.string('https://localhost:8089?P=Z2xvYmFsfF5QU05ERig1MC42LDApQEA=')

        }

    })

    it("Test # 1931: Select the global ^PSNDF(50.6,0), click on icon and verify the permalink", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=Z2xvYmFsfF5QU05ERig1MC42LDUsKilAQA==`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        await libs.delay(500)

        // verify that we have the right global
        let text = await page.evaluate(() => $('#tblGviewerTable-G-1 > tbody > tr:nth-child(1) td:nth-child(2)').text());
        expect(text).to.have.string('^PSNDF(50.6,5,0)')

        // click on the Permalink icon
        btnClick = await page.$("#btnGviewerPermalink-G-1");
        await btnClick.click();

        // wait for permalinks dialog to be set by the async call
        await libs.waitForDialog('#modalPermalinks');

        // read the permalink (remove the readonly first or text is not available)
        await page.evaluate(() => $('#inpPermalink').attr('readonly', false));
        text = await page.evaluate(() => $('#inpPermalink').val());
        expect(text).to.have.string('https://localhost:8089?P=Z2xvYmFsfF5QU05ERig1MC42LDUsKilAQA==')
    })

    it("Test # 1932: Select the routine %ydbgui, click on icon and verify the permalink", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=cm91dGluZXwleWRiZ3VpQEA=`, {
            waitUntil: "domcontentloaded"
        });

        // wait for routine viewer to be set by the async call
        await page.$('#tabRviewer-R-1Div');

        await libs.delay(1500)
        let text = await page.evaluate(() => $('#divRviewerDiv-R-1').text());
        expect(text).to.have.string('%ydbgui')

        // click on the Permalink icon
        btnClick = await page.$("#btnRviewerPermalink-R-1");
        await btnClick.click();

        // wait for permalinks dialog to be set by the async call
        await libs.waitForDialog('#modalPermalinks');

        // read the permalink (remove the readonly first or text is not available)
        await page.evaluate(() => $('#inpPermalink').attr('readonly', false));
        text = await page.evaluate(() => $('#inpPermalink').val());
        expect(text).to.have.string('cm91dGluZXwleWRiZ3VpQEA=')
    })

    /*
    it("Test # 1933: Modify the zroutines, select the routine %go, click on icon and verify the permalink", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?P=cm91dGluZXwlZ298L2RhdGEvcjEuMzhfeDg2XzY0L28qKC9kYXRhL3IxLjM4X3g4Nl82NC9yKSAvWURCR1VJL29iamVjdHMqKC9ZREJHVUkvcm91dGluZXMgL1lEQkdVSS9td2Vic2VydmVyKSAvb3B0L3lvdHRhZGIvY3VycmVudC9wbHVnaW4vby9feWRiYWltLnNvIC9vcHQveW90dGFkYi9jdXJyZW50L3BsdWdpbi9vL195ZGJndWkuc28gL29wdC95b3R0YWRiL2N1cnJlbnQvcGx1Z2luL28vX3lkYm13ZWJzZXJ2ZXIuc28gL29wdC95b3R0YWRiL2N1cnJlbnQvcGx1Z2luL28vX3lkYm9jdG8uc28gL29wdC95b3R0YWRiL2N1cnJlbnQvcGx1Z2luL28vX3lkYnBvc2l4LnNvIC9vcHQveW90dGFkYi9jdXJyZW50L2xpYnlvdHRhZGJ1dGlsLnNvQEA=`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');


        // ***********************************
        // modify the zroutines
        // ***********************************

        let state = await page.evaluate(() => app.ui.rViewer.searchPath.show());
        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPath');

        let btnClick = await page.$("#btnRoutinesSearchAddObject");
        await btnClick.click();

        // wait for dialog to be set by the async call
        await libs.waitForDialog('#modalRoutinesSearchPathAdd');

        await page.keyboard.type('/');

        btnClick = await page.$("#btnRoutinesSearchPathAddValidate");
        await btnClick.click();

        await libs.delay(200)

        btnClick = await page.$("#btnRoutinesSearchPathAddOk");
        await btnClick.click();

        await libs.waitForDialog('#modalRoutinesSearchPathAdd', 'close');

        btnClick = await page.$("#btnRoutinesSearchDirOk");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox');

        let text = await page.evaluate(() => $('#txtInputboxText').text());

        expect(text).to.have.string('Do you want to update')

        btnClick = await page.$("#btnInputboxYes");
        await btnClick.click();

        await libs.waitForDialog('modalInputbox', 'close');

        // ***********************************
        // Proceed with testing
        // ***********************************

        // wait for routine viewer to be set by the async call
        await page.$('#tabRviewer-R-1Div');

        await libs.delay(500)
        text = await page.evaluate(() => $('#divRviewerDiv-R-1').text());
        expect(text).to.have.string('%GO')

        // click on the Permalink icon
        btnClick = await page.$("#btnRviewerPermalink-R-1");
        await btnClick.click();

        // wait for permalinks dialog to be set by the async call
        await libs.waitForDialog('#modalPermalinks');

        // read the permalink (remove the readonly first or text is not available)
        await page.evaluate(() => $('#inpPermalink').attr('readonly', false));
        text = await page.evaluate(() => $('#inpPermalink').val());
        console.log(text)
        expect(text === 'https://localhost:8089?P=cm91dGluZXwlZ298L2RhdGEvcjEuMzhfeDg2XzY0L28qKC9kYXRhL3IxLjM4X3g4Nl82NC9yKSAvKCkgL1lEQkdVSS9vYmplY3RzKigvWURCR1VJL3JvdXRpbmVzIC9ZREJHVUkvbXdlYnNlcnZlcikgL29wdC95b3R0YWRiL2N1cnJlbnQvcGx1Z2luL28vX3lkYmFpbS5zbyAvb3B0L3lvdHRhZGIvY3VycmVudC9wbHVnaW4vby9feWRiZ3VpLnNvIC9vcHQveW90dGFkYi9jdXJyZW50L3BsdWdpbi9vL195ZGJtd2Vic2VydmVyLnNvIC9vcHQveW90dGFkYi9jdXJyZW50L3BsdWdpbi9vL195ZGJvY3RvLnNvIC9vcHQveW90dGFkYi9jdXJyZW50L3BsdWdpbi9vL195ZGJwb3NpeC5zbyAvb3B0L3lvdHRhZGIvY3VycmVudC9saWJ5b3R0YWRidXRpbC5zb0BA').to.be.true
    })

     */
})
