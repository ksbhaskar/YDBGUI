/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/
const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("Globals Viewer: Parser > Global", async () => {
    it("Test # 800: Test text: ^ only: error", async () => {
        // create test global
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'s (^%,^%test,^TEST)=0 h\'');

        await page.goto(`https://localhost:${MDevPort}//index.html?test=800`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The global name is missing');
    });

    it("Test # 805: Test text: ^%v0.1 error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=805`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('%v0.1');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The global name can contain only alph');
    });

    it("Test # 806: Test text: ^%a%b error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=806`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('%a%b');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('You can not have more than one % char');
    });

    it("Test # 807: Test text: ^TE%ST error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=807`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TE%ST');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The % char can only be the first char');
    });

    it("Test # 808: Test text: ^a123456789012345678901234567890abc error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=808`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('a123456789012345678901234567890abc');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The global name can have a maximum le');
    });

    it("Test # 809: Test text: ^a123456789012345678901234567890 good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=809`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('a123456789012345678901234567890');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The global doesn\'t exist on the ser');
    });

    it("Test # 810: Test text: ^AA$% error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=810`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('AA$%');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The % char can only be the first char');
    });

    it("Test # 811: Test text: ^a@ error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=811`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('a@');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The global name can contain only alph');
    });

    it("Test # 812: Test text: ^A: error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=812`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('A:');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The global name can contain only alph');
    });

    it("Test # 813: Test text: ^notexist error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=813`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('notexist');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The global doesn\'t exist on the ser');
    });

    it("Test # 814: Create global ^TEST Test text: ^TEST good", async () => {
        // create a global
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'s ^TEST=0 h\'');

        await page.goto(`https://localhost:${MDevPort}//index.html?test=813`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 815: Create global ^TEST Submit text using button: ^TEST good", async () => {
        // create a global
        exec('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'s ^TEST=0 h\'');

        await page.goto(`https://localhost:${MDevPort}//index.html?test=815`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST');

        // submit using button
        let btnClick = await page.$("#btnGviewerPathSubmit-G-1");
        await btnClick.click();

        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 816: Submit text using button: ^% good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=815`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('%');

        // submit using button
        let btnClick = await page.$("#btnGviewerPathSubmit-G-1");
        await btnClick.click();

        await libs.delay(100);

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 817: Submit text using button: ^%test good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=815`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('%test');

        // submit using button
        let btnClick = await page.$("#btnGviewerPathSubmit-G-1");
        await btnClick.click();

        await libs.delay(100);

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });
});

describe("Globals Viewer: Parser > Parens", async () => {
    it("Test # 850: Test text: ^( error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=850`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('(');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('No naked reference allowed here');
    });

    it("Test # 851: Test text: ^(( error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=851`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('((');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('No naked reference allowed here');
    });

    it("Test # 852: Test text: ^) error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=852`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type(')');
        await page.keyboard.press('Enter');

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The first char of the global name');
    });

});

describe("Globals Viewer: Parser > Subscripts > Commas and stars", async () => {
    it("Test # 870: Test text: ^TEST(*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=870`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 871: Test text: ^TEST(,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=871`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 872: Test text: ^TEST(,,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=872`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(,,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 873: Test text: ^TEST(,,,) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=873`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(,,,)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 874: Test text: ^TEST(,) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=874`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(,)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 875: Test text: ^TEST(*,23) error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=875`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(*,23)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('Only the ) char is allowed after a *');
    });

    it("Test # 924: Test text: ^TEST(23 bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=924`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(23');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The paren is not closed');
    });

});

describe("Globals Viewer: Parser > Subscripts > Numeric", async () => {
    it("Test # 900: Test text: ^TEST(100) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=900`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 901: Test text: ^TEST(100,100) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=901`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,100)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 902: Test text: ^TEST(-100) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=902`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(-100)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 903: Test text: ^TEST(100.23) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=903`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100.23)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 904: Test text: ^TEST(.100) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=904`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(.100)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 905: Test text: ^TEST(100e24) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=905`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100e24)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 906: Test text: ^TEST(100E24) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=906`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100E24)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 907: Test text: ^TEST(-100.2) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=907`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(-100.2)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 908: Test text: ^TEST(-.100) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=908`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(-100)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 909: Test text: ^TEST(100,) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=909`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 910: Test text: ^TEST(100,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=910`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 911: Test text: ^TEST(100,) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=911`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 912: Test text: ^TEST(100,23) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=912`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,23)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 913: Test text: ^TEST(100,45,,,) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=913`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,45,,,)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 914: Test text: ^TEST(100,23,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=914`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,23,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 915: Test text: ^TEST(100,,23) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=915`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,,23)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 916: Test text: ^TEST(100,,23,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=916`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,,23,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 917: Test text: ^TEST(23e12e12) bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=917`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(23e12e12)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The char e can not appear more than o');
    });

    it("Test # 918: Test text: ^TEST(23e) bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=917`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(23e)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The char e can not be the last chara');
    });

    it("Test # 919: Test text: ^TEST(23.12.34) bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=919`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(23.12.34)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The char . can not appear more than ');
    });

    it("Test # 920: Test text: ^TEST(23.12.) bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=920`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(23.12.)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The char . can not appear more than ');
    });

    it("Test # 921: Test text: ^TEST(2312-3) bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=921`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(2312-3)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('Invalid character');
    });

    it("Test # 922: Test text: ^TEST(23Ee12e12) bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=922`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(23Ee12e12)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The char e can not appear more than');
    });

    it("Test # 923: Test text: ^TEST(23E) bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=923`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(23E)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The char E can not be the last character');
    });

    it("Test # 924: Test text: ^TEST(23 bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=924`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(23');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The paren is not closed');
    });

});

describe("Globals Viewer: Parser > Subscripts > String", async () => {
    it("Test # 950: Test text: ^TEST(\"100\") good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=950`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"100\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 951: Test text: ^TEST(\"abc\") good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=951`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"abc\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 952: Test text: ^TEST(\"1\"0\") error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=952`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"1\"0\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('Quote inside string is not escaped');
    });

    it("Test # 953: Test text: ^TEST(\"a\"c\"100\") error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=953`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"a\"c\"100\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('Quote inside string is not escaped');
    });

    it("Test # 954: Test text: ^TEST(\"%$#^%^&*\") good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=954`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"%$#^%^&*\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 955: Test text: ^TEST(\"100\",*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=955`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"100\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 956: Test text: ^TEST(\"100\",\"test\") good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=956`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"100\",\"test\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 957: Test text: ^TEST(\"100\",,\"test\") good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=957`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"100\",,\"test\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 958: Test text: ^TEST(\"100\",,,\"test\") good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=958`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"100\",,,\"test\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 959: Test text: ^TEST(\"100\",,,\"test\",*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=959`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"100\",,,\"test\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 960: Test text: ^TEST(\"abc bad", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=960`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"abc');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The paren is not closed');
    });

    it("Test # 961: Test text: ^TEST(\"abc) error", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=961`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"abc)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('The string is not terminated');
    });

    it("Test # 962: Test text: ^TEST(\"abc:\") good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=962`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"abc:\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });
});

describe("Globals Viewer: Parser > Subscripts > Ranges", async () => {
    it("Test # 1000: Test text: ^TEST(:,:,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1000`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(:,:,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1001: Test text: ^TEST(:23) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1001`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(:23)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1002: Test text: ^TEST(:\"abc\") good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1002`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(:\"abc\")');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1003: Test text: ^TEST(123:) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1003`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(123:)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1004: Test text: ^TEST(:,:,\"abc\":) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1004`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(:,:,"abc":)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1005: Test text: ^TEST(:,:,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1005`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(:,:,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1006: Test text: ^TEST(\"abc\":\"def\",23,\"B\",*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1006`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(:,:,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1007: Test text: ^TEST(123:456,23,\"B\",*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1007`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(123:456,23,\"B\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1008: Test text: ^TEST(123:\"abc\",23,\"B\",*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1008`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(123:\"abc\",23,\"B\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1009: Test text: ^TEST(\"abc\":456,23,\"B\",*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1009`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"abc\":456,23,\"B\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1010: Test text: ^TEST(12:23,56:48,23,\"B\",*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1010`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(12:23,56:48,23,\"B\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1011: Test text: ^TEST(\"abc\":\"def\",12:34,23,\"B\",*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1011`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"abc\":\"def\",12:34,23,\"B\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1012: Test text: ^TEST(:233,\"a\":,23,\"B\",*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1012`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(:233,\"a\":,23,\"B\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1013: Test text: ^TEST(:233,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1013`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(:233,*)');
        await page.keyboard.press('Enter');
        await libs.delay(500)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1014: Test text: ^TEST(233:,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1014`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(233:,*)');
        await page.keyboard.press('Enter');
        await libs.delay(500)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1015: Test text: ^TEST(233:,:,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1015`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(233:,:,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });

    it("Test # 1016: Test text: ^TEST(233:,:33,*) good", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1016`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(233:,:33,*)');
        await page.keyboard.press('Enter');
        await libs.delay(100)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 3).to.be.true;

        const cell = await page.$('#lblGviewerPath-G-1');
        const text = await page.evaluate(el => el.textContent, cell);
        expect(text).to.have.string('');
    });
});

describe("Globals Viewer: Parser > Verify payload", async () => {
    /*
    it("Test # 1050: Test text: ^TEST", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1050`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 0).to.be.true;
    });

     */

    it("Test # 1051: Test text: ^TEST(*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1051`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 1).to.be.true;
    });


    it("Test # 1052: Test text: ^TEST(,)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1052`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(,)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 2).to.be.true;
    });

    it("Test # 1053: Test text: ^TEST(,*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1053`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(,*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 2).to.be.true;
    });

    it("Test # 1054: Test text: ^TEST(12,,23,*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1054`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(12,,23,*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 4).to.be.true;
    });

    it("Test # 1055: Test text: ^TEST(12,,,*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1055`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(12,,,*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 4).to.be.true;
    });

    it("Test # 1056: Test text: ^TEST(12,\"test\",,*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1056`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(12,\"test\",,*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 4).to.be.true;
    });

    it("Test # 1057: Test text: ^TEST(12:,:33,,\"a\",*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1057`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(12:,:33,,\"a\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 5).to.be.true;
    });

    it("Test # 1058: Test text: ^TEST(\"test\":\"zzz\",*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1058`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"test\":\"zzz\",*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 2).to.be.true;
    });

    it("Test # 1059: Test text: ^TEST(\"test\":\"zzz\",,*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1059`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(\"test\":\"zzz\",,*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 3).to.be.true;
    });

    it("Test # 1060: Test text: ^TEST(100,23456,0)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1060`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,23456,0)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 3).to.be.true;
    });

    it("Test # 1061: Test text: ^TEST(100,,23456,,0)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1061`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,,23456,,0)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 5).to.be.true;
    });

    it("Test # 1062: Test text: ^TEST(:100,23456:,0)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1062`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(:100,23456:,0)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 3).to.be.true;
    });

    it("Test # 1063: Test text: ^TEST(100,,23456,,0,*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1063`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(100,,23456,,0,*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 6).to.be.true;
    });

    it("Test # 1064: Test text: ^TEST(,0)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1064`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(,0)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 2).to.be.true;
    });

    it("Test # 1065: Test text: ^TEST(,0,*)", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1065`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#tab-G-1Div');

        //type a new name
        await page.keyboard.type('TEST(,0,*)');
        await page.keyboard.press('Enter');
        await libs.delay(250)

        const payload = await page.evaluate(() => app.ui.gViewer.parser.payload);
        expect(payload.subscripts.length === 3).to.be.true;
    });
});

