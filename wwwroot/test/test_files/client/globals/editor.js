/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

const libs = require('../../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("CLIENT: Globals > Editor: first tab: Global save to global", async () => {
    it("Test # 1300: Change ALL Data buffer parameters, apply them to the current tab and inspect the class to verify", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1300`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // display settings
        await page.evaluate(() => app.ui.gViewer.settings.show('-G-1'));
        await libs.delay(250);

        // change values
        await page.evaluate(() => $('#inpGviewerSettingsTotalBufferSize').val(''));
        await page.evaluate(() => $('#inpGviewerSettingsInitialFetchSize').val(''));
        await page.evaluate(() => $('#inpGviewerSettingsLazyFetchSize').val(''));
        await page.evaluate(() => $('#inpGviewerSettingsMaxDataLength').val(''));
        await page.evaluate(() => $('#inpGviewerSettingsLazyFetchTriggerPages').val(''));

        let elem = await page.$('#inpGviewerSettingsTotalBufferSize');
        await libs.clickOnElement(elem);
        await page.keyboard.type('500');
        elem = await page.$('#inpGviewerSettingsInitialFetchSize');
        await libs.clickOnElement(elem);
        await page.keyboard.type('200');
        elem = await page.$('#inpGviewerSettingsLazyFetchSize');
        await libs.clickOnElement(elem);
        await page.keyboard.type('100');
        elem = await page.$('#inpGviewerSettingsMaxDataLength');
        await libs.clickOnElement(elem);
        await page.keyboard.type('1024');
        elem = await page.$('#inpGviewerSettingsLazyFetchTriggerPages');
        await libs.clickOnElement(elem);
        await page.keyboard.type('1');

        await libs.delay(250);

        // display confirmation
        let btnClick = await page.$("#btnGviewerSettingsOk");
        await btnClick.click();

        // check box for global
        await page.evaluate(() => $('#chkGviewerSettingsConfirmSettings').prop('checked', true));
        await libs.delay(250);

        // submit ok
        btnClick = await page.$("#btnGviewerSettingsConfirmOk");
        await btnClick.click();

        await libs.delay(250);

        const newSettings = await page.evaluate(() => app.userSettings.globalViewer.options.table);

        expect(newSettings.buffering.maxSize === 500).to.be.true
        expect(newSettings.buffering.initialFetchSize === 200).to.be.true
        expect(newSettings.buffering.lazyFetchSize === 100).to.be.true
        expect(newSettings.buffering.startFetchingPages === 1).to.be.true

        expect(newSettings.maxDataLength === 1024).to.be.true
    });

    /*
    it("Test # 1301: Change the Last Used Count parameter, apply and verify that the menu size has changed", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1301`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // display settings
        await page.evaluate(() => app.ui.gViewer.settings.show('-G-1'));
        await libs.delay(250);

        // change values
        await page.evaluate(() => $('#inpGviewerSettingsLastUsedCount').val(''));
        let elem = await page.$('#inpGviewerSettingsLastUsedCount');
        await libs.clickOnElement(elem);
        await page.keyboard.type('5');

        await libs.delay(250);

        // display confirmation
        let btnClick = await page.$("#btnGviewerSettingsOk");
        await btnClick.click();

        await libs.delay(250);

        // check box for global
        await page.evaluate(() => $('#chkGviewerSettingsConfirmSettings').prop('checked', true));
        await libs.delay(250);

        // submit ok
        btnClick = await page.$("#btnGviewerSettingsConfirmOk");
        await btnClick.click();

        await libs.delay(250);

        const newMenuSize = await page.evaluate(() => $('#menuDevelopmentGlobalsViewerRecentListEntries').find('li'));
        expect(newMenuSize.length === 0).to.be.true

    });

     */

    it("Test # 1302: Change the default: Piece char, apply and verify that the new tab gets the new value", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1302`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // display settings
        await page.evaluate(() => app.ui.gViewer.settings.show('-G-1'));
        await libs.delay(250);


        // change values
        let elem = await page.$('#inpGviewerSettingsDefaultPieceChar');
        await libs.clickOnElement(elem);
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // display confirmation
        let btnClick = await page.$("#btnGviewerSettingsOk");
        await btnClick.click();

        await libs.delay(250);

        // check box for global
        await page.evaluate(() => $('#chkGviewerSettingsConfirmSettings').prop('checked', true));
        await libs.delay(250);

        // submit ok
        btnClick = await page.$("#btnGviewerSettingsConfirmOk");
        await btnClick.click();

        await libs.delay(250);

        const newSettings = await page.evaluate(() => app.userSettings.globalViewer.themes.light.table.content.values);
        expect(newSettings.pieceChar.char === '@').to.be.true
    });

    it("Test # 1303: Change the default: Piece View, apply and verify that the new tab gets the new value", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1303`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // display settings
        await page.evaluate(() => app.ui.gViewer.settings.show('-G-1'));
        await libs.delay(250);


        // change values
        let elem = await page.$('#inpGviewerSettingsDefaultPieceView');
        await libs.clickOnElement(elem);
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // display confirmation
        let btnClick = await page.$("#btnGviewerSettingsOk");
        await btnClick.click();

        await libs.delay(250);

        // check box for global
        await page.evaluate(() => $('#chkGviewerSettingsConfirmSettings').prop('checked', true));
        await libs.delay(250);

        // submit ok
        btnClick = await page.$("#btnGviewerSettingsConfirmOk");
        await btnClick.click();

        await libs.delay(250);

        const newSettings = await page.evaluate(() => app.userSettings.globalViewer.themes.light.table.content.values);
        expect(newSettings.showPieces === 'no').to.be.true
    });

    it("Test # 1304: Change the default: Theme, apply and verify that the new tab gets the new value", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1304`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // display settings
        await page.evaluate(() => app.ui.gViewer.settings.show('-G-1'));
        await libs.delay(250);


        // change values
        let elem = await page.$('#inpGviewerSettingsDefaultTheme');
        await libs.clickOnElement(elem);
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // display confirmation
        let btnClick = await page.$("#btnGviewerSettingsOk");
        await btnClick.click();

        await libs.delay(250);

        // check box for global
        await page.evaluate(() => $('#chkGviewerSettingsConfirmSettings').prop('checked', true));
        await libs.delay(250);

        // submit ok
        btnClick = await page.$("#btnGviewerSettingsConfirmOk");
        await btnClick.click();

        await libs.delay(250);

        const newSettings = await page.evaluate(() => app.userSettings.globalViewer.defaultTheme);
        expect(newSettings === 'dark').to.be.true
    });

    it("Test #  1305: Change the default: Bookmark color, apply and verify that the new tab gets the new value", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1305`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.delay(2000);

        // display settings
        await page.evaluate(() => app.ui.gViewer.settings.show('-G-1'));
        await libs.delay(250);


        // change values
        let elem = await page.$('#inpGviewerSettingsDefaultBookmarkColor');
        await libs.clickOnElement(elem);
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('Enter');

        await libs.delay(250);

        // display confirmation
        let btnClick = await page.$("#btnGviewerSettingsOk");
        await btnClick.click();

        await libs.delay(250);

        // check box for global
        await page.evaluate(() => $('#chkGviewerSettingsConfirmSettings').prop('checked', true));
        await libs.delay(250);

        // submit ok
        btnClick = await page.$("#btnGviewerSettingsConfirmOk");
        await btnClick.click();

        await libs.delay(250);

        const newSettings = await page.evaluate(() => app.userSettings.globalViewer.themes.light.table.bookmarkColor);
        expect(newSettings === 'green').to.be.true
    });
});

// COMMENTED OUT BECAUSE OF FAILURES IN SERVER, WILL BE FIXED ON NEXT SPRINT

/*
describe("CLIENT: Globals > Editor: first tab: Path bar as global", async () => {
    it("Test #  1310: Change the path bar: Global, apply and verify that the new tab gets the new value", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html?test=1305`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        // display settings
        await page.evaluate(() => app.ui.gViewer.settings.show('-G-1'));
        await libs.delay(250);

        // scroll down
        await page.evaluate(() => $('#tabGviewerGlobalDivScroll').scrollTop(50000));
        await libs.delay(250);

        /*
        // change values
        await page.evaluate(() => $('#inpGviewerSettingsPathBarGlobal').focus());
        await page.keyboard.press('Enter');
        await libs.delay(100)



        //await page.keyboard.press('Tab');
        //await page.keyboard.press('Tab');
        //await page.keyboard.type('255');
        //await page.keyboard.press('Enter');

        await libs.delay(250);

                // display confirmation
                let btnClick = await page.$("#btnGviewerSettingsOk");
                await btnClick.click();

                await libs.delay(250);

                // check box for global
                await page.evaluate(() => $('#chkGviewerSettingsConfirmSettings').prop('checked', true));
                await libs.delay(250);

                // submit ok
                btnClick = await page.$("#btnGviewerSettingsConfirmOk");
                await btnClick.click();


                await libs.delay(250);

                const newSettings = await page.evaluate(() => app.userSettings.globalViewer.themes.light.table.bookmarkColor);
                expect(newSettings === 'green').to.be.true

    });

});

      */
