/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');
const fs = require('fs');
let regionToBeDeleted;

describe("SERVER: Create Region", async () => {
    it("Test # 5310: Create random region with default params and verify creation + file existence", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [],
                    region: []
                },
                journalEnabled: false,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.dbAccess.data[0].RECORD_SIZE;
            expect(val === 4080).to.be.true;

            // filename is correct
            val = res.data.dbFile.data[0].FILE_NAME;
            expect(val).to.have.string('/data/' + global.ydbRelease + '/g/' + regionName + '.dat')

            // db file exists
            val = res.data.dbFile.flags.fileExist;
            expect(val).to.be.true;

            // journal state
            val = res.data.journal.flags.state;
            expect(val === 0).to.be.true;

            // names
            val = res.data.names[0].name;
            expect(val).to.have.string(regionName)

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5311: Create random region with different filename and verify creation + file existence", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [],
                    region: []
                },
                journalEnabled: false,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + 'T.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + 'T.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.dbAccess.data[0].RECORD_SIZE;
            expect(val === 4080).to.be.true;

            // filename is correct
            val = res.data.dbFile.data[0].FILE_NAME;
            expect(val).to.have.string('/data/' + global.ydbRelease + '/g/' + regionName + 'T.dat')

            // db file exists
            val = res.data.dbFile.flags.fileExist;
            expect(val).to.be.true;

            // journal state
            val = res.data.journal.flags.state;
            expect(val === 0).to.be.true;

            // names
            val = res.data.names[0].name;
            expect(val).to.have.string(regionName)

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5312: Create random region with default params, but no db file creation and verify + no file", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [],
                    region: []
                },
                journalEnabled: false,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: false,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // filename is correct
            val = res.data.dbFile.data[0].FILE_NAME;
            expect(val).to.have.string('/data/' + global.ydbRelease + '/g/' + regionName + '.dat');

            // db file does NOT exists
            val = res.data.dbFile.flags.fileExist;
            expect(val).to.be.false;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5313: Create random region with default params and journal, turned on", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [
                        {
                            id: 'journalEnabled',
                            value: 1
                        }
                    ],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: true
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // db file exists
            val = res.data.dbFile.flags.fileExist;
            expect(val).to.be.true;

            // journal state
            val = res.data.journal.flags.state;
            expect(val === 2).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5314: Create random region with default params and journal, turned off", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [
                        {
                            id: 'journalEnabled',
                            value: 1
                        }
                    ],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // db file exists
            val = res.data.dbFile.flags.fileExist;
            expect(val).to.be.true;

            // journal state
            val = res.data.journal.flags.state;
            expect(val === 1).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5315: Create random region with default params and journal, turned off, with different journal path", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [
                        {
                            id: 'journalEnabled',
                            value: 1
                        }
                    ],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // db file exists
            val = res.data.dbFile.flags.fileExist;
            expect(val).to.be.true;

            // journal state
            val = res.data.journal.flags.state;
            expect(val === 1).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5316: Create random region with Auto Db = on", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [],
                    region: [
                        {
                            id: 'autoDb',
                            value: 1
                        }
                    ]
                },
                journalEnabled: false,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.dbFile.data[1].AUTO_DB;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });


    it("Test # 5317: Create random region with Access Method = MM", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [],
                    region: []
                },
                journalEnabled: false,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: false,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.dbFile.data[2].ACCESS_METHOD === 'MM';
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5318: Create random region with all dbAccess fields different and verify them all", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [],
                    region: [
                        {
                            id: 'recordSize',
                            value: 4082
                        },
                        {
                            id: 'keySize',
                            value: 567
                        },
                        {
                            id: 'lockCriticalSeparate',
                            value: 0
                        },
                        {
                            id: 'qbRundown',
                            value: 0
                        },
                        {
                            id: 'stats',
                            value: 0
                        },
                    ]
                },
                journalEnabled: false,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: false,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.dbAccess.data[0].RECORD_SIZE === 4082;
            expect(val).to.be.true;

            val = res.data.dbAccess.data[1].KEY_SIZE === 567;
            expect(val).to.be.true;

            val = res.data.dbAccess.data[4].LOCK_CRIT_SEPARATE === true;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5319: Create random region with all fields different and verify them all", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [],
                    region: []
                },
                journalEnabled: false,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [
                    {
                        id: 'globalBufferCount',
                        value: 1001
                    },
                    {
                        id: 'lockSpace',
                        value: 113152
                    },
                    {
                        id: 'deferAllocate',
                        value: 0
                    },
                    {
                        id: 'extensionCount',
                        value: 100001
                    },
                    {
                        id: 'initialAllocation',
                        value: 4999
                    },
                    {
                        id: 'blockSize',
                        value: 4608
                    },
                    {
                        id: 'mutexSlots',
                        value: 1025
                    },
                    {
                        id: 'reservedBytes',
                        value: 1
                    },

                ],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.dbFile.data[3].GLOBAL_BUFFER_COUNT === 1001;
            expect(val).to.be.true;

            val = res.data.dbFile.data[4].LOCK_SPACE === 113152;
            expect(val).to.be.true;

            val = res.data.dbFile.data[6].DEFER_ALLOCATE === false;
            expect(val).to.be.true;

            val = res.data.dbFile.data[7].EXTENSION_COUNT === 100001;
            expect(val).to.be.true;

            val = res.data.dbFile.data[10].ALLOCATION === 4999;
            expect(val).to.be.true;

            val = res.data.dbFile.data[11].BLOCK_SIZE === 4608;
            expect(val).to.be.true;

            val = res.data.dbFile.data[13].MUTEX_SLOTS === 1025;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5320: Create random region with asyncio =true and blocksize= 8192 and verify them all", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [],
                    region: []
                },
                journalEnabled: false,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [
                    {
                        id: 'asyncIo',
                        value: 1
                    },
                    {
                        id: 'blockSize',
                        value: 8192
                    },

                ],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.dbFile.flags.fileExist;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5321: Create random region with journal/ON and default fields", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [
                        {
                            id: 'journalEnabled',
                            value: 1
                        },
                    ],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: true
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.journal.flags.state === 2;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5322: Create random region with journal/OFF and default fields", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [
                        {
                            id: 'journalEnabled',
                            value: 1
                        },
                    ],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.journal.flags.state === 1;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5323: Create random region with all journal fields different and verify them all", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            const regionName = libs.randomRegionName();

            const body = {
                dbAccess: {
                    journal: [
                        {
                            id: 'journalEnabled',
                            value: 1
                        },
                        {
                            id: 'beforeImage',
                            value: 0
                        },
                        {
                            id: 'allocation',
                            value: 2049
                        },
                        {
                            id: 'autoSwitchLimit',
                            value: 8386561
                        },
                        {
                            id: 'bufferSize',
                            value: 2313
                        },
                        {
                            id: 'extension',
                            value: 2049
                        },
                        {
                            id: 'epochTaper',
                            value: 0
                        },
                    ],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            let val = res.data.journal.flags.state === 1;
            expect(val).to.be.true;

            val = res.data.journal.data[1].BEFORE === false;
            expect(val).to.be.true;

            val = res.data.journal.data[3].EPOCH_TAPER === false;
            expect(val).to.be.true;

            val = res.data.journal.data[5].AUTO_SWITCH_LIMIT === 8386557;
            expect(val).to.be.true;

            val = res.data.journal.data[7].BUFFER_SIZE === 2320;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5324: Create random region with all segment field different, store them on template, create a new region and verify", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            let regionName = libs.randomRegionName();

            let body = {
                dbAccess: {
                    journal: [],
                    region: [
                        {
                            id: 'recordSize',
                            value: 4082
                        },
                        {
                            id: 'keySize',
                            value: 567
                        },
                        {
                            id: 'lockCriticalSeparate',
                            value: 0
                        },
                        {
                            id: 'qbRundown',
                            value: 0
                        },
                        {
                            id: 'stats',
                            value: 0
                        },
                    ]
                },
                journalEnabled: false,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [
                    {
                        id: 'globalBufferCount',
                        value: 1001
                    },
                    {
                        id: 'lockSpace',
                        value: 113152
                    },
                    {
                        id: 'deferAllocate',
                        value: 0
                    },
                    {
                        id: 'extensionCount',
                        value: 100001
                    },
                    {
                        id: 'initialAllocation',
                        value: 4999
                    },
                    {
                        id: 'blockSize',
                        value: 4608
                    },
                    {
                        id: 'mutexSlots',
                        value: 1025
                    },
                    {
                        id: 'reservedBytes',
                        value: 1
                    },

                ],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: true,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // recreate the region with no new params
            regionName = libs.randomRegionName();

            body = {
                dbAccess: {
                    journal: [],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: false
                }
            };

            // execute the call
            res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and have the correct properties
            val = res.data.journal.flags.state === 1;


            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and have the correct properties
            val = res.data.dbFile.data[3].GLOBAL_BUFFER_COUNT === 1001;
            expect(val).to.be.true;

            val = res.data.dbFile.data[4].LOCK_SPACE === 113152;
            expect(val).to.be.true;

            val = res.data.dbFile.data[6].DEFER_ALLOCATE === false;
            expect(val).to.be.true;

            val = res.data.dbFile.data[7].EXTENSION_COUNT === 100001;
            expect(val).to.be.true;

            val = res.data.dbFile.data[10].ALLOCATION === 4999;
            expect(val).to.be.true;

            val = res.data.dbFile.data[11].BLOCK_SIZE === 4608;
            expect(val).to.be.true;

            val = res.data.dbFile.data[13].MUTEX_SLOTS === 1025;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5325: Create random region with all journal field different, store them on template, create a new region and verify", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            regionToBeDeleted = libs.randomRegionName();

            let body = {
                dbAccess: {
                    journal: [
                        {
                            id: 'journalEnabled',
                            value: 1
                        },
                        {
                            id: 'beforeImage',
                            value: 0
                        },
                        {
                            id: 'allocation',
                            value: 2049
                        },
                        {
                            id: 'autoSwitchLimit',
                            value: 8386561
                        },
                        {
                            id: 'bufferSize',
                            value: 2313
                        },
                        {
                            id: 'extension',
                            value: 2049
                        },
                        {
                            id: 'epochTaper',
                            value: 0
                        },
                    ],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionToBeDeleted + '.mjl',
                names: [
                    {value: regionToBeDeleted}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionToBeDeleted,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionToBeDeleted + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: true
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // recreate the region with no new params
            const regionName = libs.randomRegionName();

            body = {
                dbAccess: {
                    journal: [
                        {
                            id: 'journalEnabled',
                            value: 1
                        },
                    ],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.mjl',
                names: [
                    {value: regionName}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionName,
                segmentData: [],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionName + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: true
                }
            };

            // execute the call
            res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionName);

            // and have the correct properties
            let val = res.data.journal.flags.state === 1;
            expect(val).to.be.true;

            val = res.data.journal.data[1].BEFORE === false;
            expect(val).to.be.true;

            val = res.data.journal.data[3].EPOCH_TAPER === false;
            expect(val).to.be.true;

            val = res.data.journal.data[5].AUTO_SWITCH_LIMIT === 8386557;
            expect(val).to.be.true;

            val = res.data.journal.data[7].BUFFER_SIZE === 2313;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5326: Add region with fields from Region and Journal changed", async () => {
        if (global.serverMode === 'RW') {
            // generate a random region name
            regionToBeDeleted = libs.randomRegionName();

            let body = {
                dbAccess: {
                    journal: [
                        {
                            id: 'journalEnabled',
                            value: 1
                        },
                        {
                            id: 'beforeImage',
                            value: 0
                        },
                        {
                            id: 'allocation',
                            value: 2049
                        },
                        {
                            id: 'autoSwitchLimit',
                            value: 8386561
                        },
                        {
                            id: 'bufferSize',
                            value: 2313
                        },
                        {
                            id: 'extension',
                            value: 2049
                        },
                        {
                            id: 'epochTaper',
                            value: 0
                        },
                    ],
                    region: []
                },
                journalEnabled: true,
                journalFilename: '/data/' + global.ydbRelease + '/g/' + regionToBeDeleted + '.mjl',
                names: [
                    {value: regionToBeDeleted}
                ],
                postProcessing: {
                    createDbFile: true,
                    switchJournalOn: false
                },
                regionName: regionToBeDeleted,
                segmentData: [
                    {
                        id: 'globalBufferCount',
                        value: 1001
                    },
                    {
                        id: 'lockSpace',
                        value: 113152
                    },
                    {
                        id: 'deferAllocate',
                        value: 0
                    },
                    {
                        id: 'extensionCount',
                        value: 100001
                    },
                    {
                        id: 'initialAllocation',
                        value: 4999
                    },
                    {
                        id: 'blockSize',
                        value: 4608
                    },
                    {
                        id: 'mutexSlots',
                        value: 1025
                    },
                    {
                        id: 'reservedBytes',
                        value: 1
                    },
                ],
                segmentFilename: '/data/' + global.ydbRelease + '/g/' + regionToBeDeleted + '.dat',
                segmentTypeBg: true,
                templates: {
                    updateTemplateDb: false,
                    updateTemplateJournal: true
                }
            };

            // execute the call
            let res = await libs._RESTpost('regions/add', body).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // and verify that the region exists
            res = await libs._REST('regions/' + regionToBeDeleted);

            // and have the correct journal properties
            let val = res.data.journal.flags.state === 1;
            expect(val).to.be.true;

            val = res.data.journal.data[1].BEFORE === false;
            expect(val).to.be.true;

            val = res.data.journal.data[3].EPOCH_TAPER === false;
            expect(val).to.be.true;

            val = res.data.journal.data[5].AUTO_SWITCH_LIMIT === 8386557;
            expect(val).to.be.true;

            val = res.data.journal.data[7].BUFFER_SIZE === 2313;
            expect(val).to.be.true;

            // and have the correct segment properties
            val = res.data.dbFile.data[3].GLOBAL_BUFFER_COUNT === 1001;
            expect(val).to.be.true;

            val = res.data.dbFile.data[4].LOCK_SPACE === 113152;
            expect(val).to.be.true;

            val = res.data.dbFile.data[6].DEFER_ALLOCATE === false;
            expect(val).to.be.true;

            val = res.data.dbFile.data[7].EXTENSION_COUNT === 100001;
            expect(val).to.be.true;

            val = res.data.dbFile.data[10].ALLOCATION === 4999;
            expect(val).to.be.true;

            val = res.data.dbFile.data[11].BLOCK_SIZE === 4608;
            expect(val).to.be.true;

            val = res.data.dbFile.data[13].MUTEX_SLOTS === 1025;
            expect(val).to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });
});

describe("SERVER: Delete Region", async () => {
    it("Test # 5400: Delete using a bad region name", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            let res = await libs._RESTdelete('regions/idontexist', {}).catch(() => {});

            const error = res.error.description;
            expect(error).to.have.string('doesn\'t exist')

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5401: Delete using a null region name", async () => {
        if (global.serverMode === 'RW') {
            // execute the call
            let res = await libs._RESTdelete('regions/', {}).catch(() => {});

            const error = res.error.description;
            expect(error).to.have.string('The parameter "region" is missing or empty')

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5402: Delete an existing region", async () => {
        if (global.serverMode === 'RW') {
            // WARNING: This test is related to 1325 and it uses the region name for deleting

            // execute the call
            let res = await libs._RESTdelete('regions/' + regionToBeDeleted, {}).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5403: Delete an existing region and delete the files", async () => {
        if (global.serverMode === 'RW') {
            // get the YDBOCTO region to extract the filenames
            let res = await libs._REST('regions/YDBOCTO');

            const segmentFilename = res.data.dbFile.flags.file;
            const journalFilename = res.data.journal.flags.file;

            // perform the delete
            res = await libs._RESTdelete('regions/YDBOCTO?deleteFiles=true', {}).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;

            // now check for the db file
            expect(fs.existsSync(segmentFilename)).to.be.false;

            // and journal
            expect(fs.existsSync(journalFilename)).to.be.false;

        } else {
            console.log('RO mode');
            res = await libs._RESTpost('os/processes/' + 0 + '/terminate').catch(() => {});
            expect(res.error.code === 403)
        }
    });

    it("Test # 5404: Delete an existing region and delete the files with segment name different than region name", async () => {
        if (global.serverMode === 'RW') {
            // use the GDE to create the region
            execSync('. /opt/yottadb/current/ydb_env_set && yottadb -r GDE  <<< \'add -name DELETETEST -r=DELETETEST\nadd -segment DELSEG -file=delseg.dat\nadd -region DELETETEST -dynamic=DELSEG\nexit\n\'', {shell: '/bin/bash'});
            await libs.delay(200)

            // perform the delete
            res = await libs._RESTdelete('regions/DELETETEST?deleteFiles=false', {}).catch(() => {});

            // and check the result to be OK
            expect(res.result === 'OK').to.be.true;
        }
    })
});

describe("SERVER: Journal files", async () => {
    it("Test # 6100: Call the REST /regions/[region]/getJournalInfo with a bad region, error should be returned", async () => {
        // execute the call
        let res = await libs._REST('regions/badregion/get-journal-info').catch(() => {});

        isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        expect(res.result === 'ERROR')
        expect(res.error.description).to.have.string('doesn\'t exist')
    })

    it("Test # 6101: Create a full view situation (multiple journals, with orphans, and active journal file and verify that response is correct", async () => {
        // execute the call
        let res = await libs._REST('regions/default/get-journal-info').catch(() => {});

        isObject = typeof res === 'object';
        expect(isObject).to.be.true;

        expect(res.result === 'OK')

        expect(res.data[0].chainCnt === 1).to.be.true
        if (global.serverMode === 'RW') {
            expect(res.data[0].filename === 'default2.mjl').to.be.true

        } else {
            expect(res.data[0].filename === 'yottadb.mjl').to.be.true
        }

        // ensure that all the 41 fields are returned
        expect(res.data[0].params.length === 41).to.be.true
    })
})
