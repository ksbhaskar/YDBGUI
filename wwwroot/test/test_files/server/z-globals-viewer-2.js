/*
#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe("SERVER: Traverse globals >>> Range test", async () => {
    it("Test # 5700: ^ORD(100.01,\"AN\":\"AZ\",*) ", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ':',
                        type1: "S",
                        type2: "S",
                        value: '\"AN\"',
                        valueEnd: '\"AZ\"'
                    },
                    {
                        type: '*'
                    }
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[0].subscripts[1] === 'AVUID').to.be.true;
        expect(res.data.list[15].subscripts[3] === 11).to.be.true;
        expect(res.data.recordCount === 16).to.be.true;

    });

    it("Test # 5701: ^ORD(100.01,\"AN\":\"AZ\",4501091:4501096,*) ", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ':',
                        type1: "S",
                        type2: "S",
                        value: '"AN"',
                        valueEnd: '"AZ"'
                    },
                    {
                        type: ":",
                        type1: "N",
                        type2: "N",
                        value: 4501091,
                        valueEnd: 4501096
                    },
                    {
                        type: '*'
                    }
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[0].subscripts[3] === 10).to.be.true;
        expect(res.data.list[2].subscripts[3] === 4).to.be.true;
        expect(res.data.recordCount === 3).to.be.true;

    });

    it("Test # 5702: ^ORD(100.01,:\"AZ\",*) ", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ':',
                        type1: "S",
                        type2: "S",
                        value: '',
                        valueEnd: '"AZ"'
                    },
                    {
                        type: '*'
                    }
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[0].subscripts[1] === 'AMASTERVUID').to.be.true;

    });

    it("Test # 5703: ^ORD(100.01,\"AN\":,*) ", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ':',
                        type1: "S",
                        type2: "S",
                        value: '"AN"',
                        valueEnd: ''
                    },
                    {
                        type: '*'
                    }
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[0].subscripts[1] === 'AVUID').to.be.true;
        expect(res.data.recordCount === 32).to.be.true;

    });

    it("Test # 5704: ^ORD(100.01,\"AN\":\"AZ\",4501091:,*) ", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ':',
                        type1: "S",
                        type2: "S",
                        value: '"AN"',
                        valueEnd: '"AZ"'
                    },
                    {
                        type: ":",
                        type1: "N",
                        type2: "S",
                        value: 4501091,
                        valueEnd: ''
                    },
                    {
                        type: '*'
                    }
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[0].subscripts[2] === 4501091).to.be.true;
        expect(res.data.recordCount === 9).to.be.true;

    });

    it("Test # 5705: ^ORD(100.01,\"AN\":\"AZ\",:4501096,*) ", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: ':',
                        type1: "S",
                        type2: "S",
                        value: '"AN"',
                        valueEnd: '"AZ"'
                    },
                    {
                        type: ":",
                        type1: "N",
                        type2: "N",
                        value: '',
                        valueEnd: 4501096
                    },
                    {
                        type: '*'
                    }
                ]
            },
            size: 10000
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[0].subscripts[2] === 4500659).to.be.true;
        expect(res.data.recordCount === 10).to.be.true;
    });
});
