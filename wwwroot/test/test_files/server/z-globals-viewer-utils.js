/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");
const {execSync, exec} = require('child_process');

describe("SERVER: Find globals", async () => {
    it("Test # 5500: List globals using no mask parameter", async () => {
        // create test globals
        execSync('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'k ^AAAA s (^AAAA,^AABA,^ABBA,^CCCC,^CCCC1234,^CCCCC234)=0 h\'');
        execSync('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'s (^DDDZ,^DDDz,^DDE,^DDEE)=0 h\'');

        // execute the call
        let res = await libs._REST('globals/find?').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;
    });

    it("Test # 5501: List globals using an empty mask", async () => {
        // execute the call
        let res = await libs._REST('globals/find?mask=').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

    });

    it("Test # 5502: List globals using AA as mask", async () => {
        // execute the call
        let res = await libs._REST('globals/find?mask=AA').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.length === 2).to.be.true;
    });

    it("Test # 5503: List globals using AAA as mask", async () => {
        // execute the call
        let res = await libs._REST('globals/find?mask=AAA').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.length === 1).to.be.true;
    });

    it("Test # 5504: List globals using AAB as mask", async () => {
        // execute the call
        let res = await libs._REST('globals/find?mask=AAB').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.length === 1).to.be.true;
    });

    it("Test # 5505: List globals using CC as mask", async () => {
        // execute the call
        let res = await libs._REST('globals/find?mask=CC').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.length === 3).to.be.true;
    });

    it("Test # 5506: List globals using CCCCC", async () => {
        // execute the call
        let res = await libs._REST('globals/find?mask=CCCCC').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.length === 1).to.be.true;
    });

    it("Test # 5507: List globals using CCCC1", async () => {
        // execute the call
        let res = await libs._REST('globals/find?mask=CCCC1').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.length === 1).to.be.true;
    });

    it("Test # 5508: List globals using DDDZ", async () => {
        // execute the call
        let res = await libs._REST('globals/find?mask=DDD').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.length === 2).to.be.true;

    });

    it("Test # 5509: List globals using DDDa", async () => {
        // execute the call
        let res = await libs._REST('globals/find?mask=DDD').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.length === 2).to.be.true;
    });

});

describe("SERVER: getData", async () => {
    it("Test # 5525: Try to supply no path at all", async () => {
        // execute the call
        let res = await libs._RESTpost('globals/get-data').catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'ERROR').to.be.true;

    });

    it("Test # 5526: Try to supply not existing path", async () => {
        // execute the call
        let res = await libs._RESTpost('globals/get-data', {
            path: '^Idonotexist'
        }).catch(() => {});

        // and check the result to be ERROR
        expect(res.result === 'ERROR').to.be.true;
    });

    it("Test # 5527: Try to get the path and check its length", async () => {
        // create global
        execSync('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'s ^TEST="12345678901234567890123456789012345678901234567890" h\'');

        await libs.delay(100);
        // execute the call
        let res = await libs._RESTpost('globals/get-data', {
            path: '^TEST'
        }).catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        expect(res.data.length === 50).to.be.true;
    });
});

describe("SERVER: dollarData", async () => {
    it("Test # 5530: Verify that a bad name global returns -1", async () => {
        // execute the call
        let res = await libs._REST('globals/dollar-data?globalName=^1s4').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data === -1).to.be.true;
    });

    it("Test # 5531: Verify that a not existing global returns the correct $data value: expect 0", async () => {
        // execute the call
        let res = await libs._REST('globals/dollar-data?globalName=^notExists').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data === 0).to.be.true;
    });

    it("Test # 5532:  Verify that a global returns the correct $data value: expect 1", async () => {
        // execute the call
        let res = await libs._REST('globals/dollar-data?globalName=^AAAA').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data === 1).to.be.true;
    });

    it("Test # 5533: Verify that a global returns the correct $data value: expect 10", async () => {
        // create test global
        execSync('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'s ^DATATEST(1)=0 h\'');

        // execute the call
        let res = await libs._REST('globals/dollar-data?globalName=^DATATEST').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data === 10).to.be.true;
    });

    it("Test # 5534: Verify that a global returns the correct $data value: expect 11", async () => {
        // create test node
        execSync('. /opt/yottadb/current/ydb_env_set && yottadb -run %XCMD \'s ^AAAA(1)=0 h\'');

        // execute the call
        let res = await libs._REST('globals/dollar-data?globalName=^AAAA').catch(() => {});

        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data === 11).to.be.true;
    });
});
