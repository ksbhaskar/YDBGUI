/*
#################################################################
#                                                               #
# Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs');
const {expect} = require("chai");

describe("SERVER: Traverse globals >>> With forward offset", async () => {
    it("Test # 5750: ^ORD(100.01,*) 15 recs", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: '*'
                    }
                ]
            },
            size: 15
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        //get the last item and call again
        const path = res.data.list[res.data.recordCount - 1].path;

        body.offset = path;

        // execute the call
        res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[14].path === '^ORD(100.01,3,"TERMSTATUS","B",3070607.115705,1)').to.be.true;

    });
});

describe("SERVER: Traverse globals >>> With backward offset", async () => {
    it("Test # 5800: ^ORD(100.01,*) 100 recs then the first 10", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: 'V',
                        value: 100.01
                    },
                    {
                        type: '*'
                    }
                ]
            },
            size: 100
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        body.offset = '';
        body.upOffset = 10;
        body.size = 10;
        body.direction = 'up';

        // execute the call
        res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[9].path === '^ORD(100.01,3,0)').to.be.true;

    });
});

describe("SERVER: Traverse globals >>> Test with different dataSize param value", async () => {
    it("Test # 5825: Set dataSize to 10 and verify", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: '*'
                    }
                ]
            },
            dataSize: 10
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[1].value === 'DISCONTINU').to.be.true;

    });

    it("Test # 5826: Set dataSize to 5 and verify", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: '*'
                    }
                ]
            },
            dataSize: 5
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[1].value === 'DISCO').to.be.true;
    });


    it("Test # 5827: do NOT set dataSize and verify", async () => {
        let body = {
            namespace: {
                global: '^ORD',
                subscripts: [
                    {
                        type: '*'
                    }
                ]
            },
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[1].value === 'DISCONTINUED^dc').to.be.true;

    });

    it("Test # 5828: Jump to end", async () => {
        let body = {
            namespace: {
                global: '^PSNDF',
                subscripts: [
                    {
                        type: '*'
                    }
                ],
            },
            size: 100,
            returnAll: false,
            direction: 'end'
        };

        // execute the call
        let res = await libs._RESTpost('globals/traverse', body).catch(() => {});
        // and check the result to be OK
        expect(res.result === 'OK').to.be.true;

        // and the subscripts are ok
        expect(res.data.list[1].path === '^PSNDF(50.6,"B","VANCOMYCIN",24)').to.be.true;
    });
});
