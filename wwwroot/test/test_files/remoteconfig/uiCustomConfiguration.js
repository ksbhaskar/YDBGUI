/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const libs = require('../../libs')
const {expect} = require("chai")
const fs = require('fs')

describe("Custom config: Remote Client Configuration", async () => {
    // this helps prevent errors in the job due to ERR:NETWORK:CHANGED returned (for some mysterious reason) by some jobs
    it("Test # 20000: Dummy", async () => {
        try {
            await page.goto(`https://localhost:${MDevPort}//index.html`, {
                waitUntil: "domcontentloaded"
            });

        } catch (err) {
        }
    })

    it("Test # 20000: try to load the file in test directory, verify that storage usage color is different", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalDashboard');

        const val = await page.evaluate(() => $('#pgsDashStorageUsage0').hasClass('ydb-status-red'))
        expect(val).to.be.true
    })

    it("Test # 20001: remove a key from the json file, startup, should display msgbox", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        const res = JSON.parse(fs.readFileSync('/YDBGUI/wwwroot/test/ydbgui-range-settings.json'))

        delete res.autoExtend

        fs.writeFileSync('/YDBGUI/wwwroot/test/ydbgui-range-settings.json', JSON.stringify(res))

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('was found, but the content is not correct')
    })

    it("Test # 20002: invalidate the json file, startup, should display msgbox", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        let res = JSON.parse(fs.readFileSync('/YDBGUI/wwwroot/test/ydbgui-range-settings.json'))

        delete res.regions

        res = JSON.stringify(res)
        res = res.slice(0, -3)

        fs.writeFileSync('/YDBGUI/wwwroot/test/ydbgui-range-settings.json', res)

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('A parsing error occurred while parsing the client config file')
    })

    it("Test # 20003: delete file, should display msgbox", async () => {
        await page.goto(`https://localhost:${MDevPort}//index.html`, {
            waitUntil: "domcontentloaded"
        });

        fs.unlinkSync('/YDBGUI/wwwroot/test/ydbgui-range-settings.json')

        // wait for dashboard to be set by the async call
        await libs.waitForDialog('#modalMsgbox');

        const val = await page.evaluate(() => $('#txtMsgboxText').text())
        expect(val).to.have.string('was not found on the server')
    })
})
