<!--
/****************************************************************
*                                                              *
* Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
* All rights reserved.                                         *
*                                                              *
* This source code contains the intellectual property          *
* of its copyright holder(s), and is made available            *
* under a license.  If you do not know the terms of            *
* the license, please stop and do not read further.            *
*                                                              *
****************************************************************/
-->

# CLIENT

2730 - 2900 Free for stats

3000 - 4999 Free

# SERVER

6350 - 9999
