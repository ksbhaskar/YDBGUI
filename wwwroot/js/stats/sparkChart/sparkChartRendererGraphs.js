/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file performs the graph rendering for the spark charts.
// It gets called by the file sparkChartRenderer.js (or by other modules to perform a graph preview)

app.ui.stats.sparkChart.renderer.sections.graphs = {
    // this function performs the initialization of the graph. The graph must initialized and the instance returned should be passed as "chart".
    // By populating the "preview" object it will populate the series with the passed series data.
    init: async function (el, view, preview, graphIx) {
        // (optional) preview = {series: []}
        // (optional) graphIx (when rendering on tab)

        let settings = {}
        let options = {}
        const defaults = view.defaults
        const mapping = view.graphMapping
        let currentSource = {}

        // find source first
        const entryObject = preview ? app.ui.stats.sparkChart.entry : app.ui.stats.entry

        entryObject.data.forEach(source => {
            const res = source.data.views.find(view2 => view2.id === view.id)
            if (res) currentSource = source
        })

        // updates the sampleRate and the samples in view used by the custom tool to auto-scale the slider
        if (this.graphData[graphIx]) {
            this.graphData[graphIx].sampleRate = currentSource.sampleRate
        }

        settings.type = 'line'
        options.responsive = true
        options.maintainAspectRatio = false
        options.plugins = {}

        options.animation = {
            duration: 10,
            easing: 'linear'
        }

        options.animations = {
            x: {
                duration: currentSource.sampleRate * 1000,
                easing: 'linear',
            },
        }

        // process legend
        options.plugins.legend = {
            display: defaults.legend.display,
            position: defaults.legend.position,
            align: defaults.legend.hAlign,
            labels: {
                font: {
                    family: defaults.legend.font.fontFamily,
                    size: defaults.legend.font.size,
                    weight: defaults.legend.font.weight,
                },
                color: defaults.legend.font.color
            }
        }

        // process title
        options.plugins.title = {
            display: defaults.title.display,
            text: defaults.title.text,
            align: defaults.title.align,
            color: defaults.title.style.color,
            position: 'top',
            font: {
                family: defaults.title.style.fontFamily,
                size: defaults.title.style.size,
                weight: defaults.title.style.weight
            }
        }

        // process sub title
        options.plugins.subtitle = {
            display: defaults.subTitle.display,
            text: defaults.subTitle.text,
            align: defaults.subTitle.align,
            color: defaults.subTitle.style.color,
            position: 'top',
            font: {
                family: defaults.subTitle.style.fontFamily,
                size: defaults.subTitle.style.size,
                weight: defaults.subTitle.style.weight,
                lineHeight: 1.5
            }
        }

        // process timescale
        options.scales = {
            x: {
                type: 'time',
                time: {
                    unit: 'seconds',
                    displayFormats: {
                        seconds: 'HH:mm:ss'
                    }
                },
                ticks: {
                    color: 'silver',
                    display: defaults.xaxis.labels.show,
                    font: {
                        size: defaults.xaxis.labels.fontSize,
                        weight: defaults.xaxis.labels.fontWeight,
                        family: defaults.xaxis.labels.fontFamily,
                    },
                },
                border: {
                    color: 'silver'
                },
                grid: {
                    color: 'silver',
                    display: defaults.grid.xAxisLines,
                    drawTicks: true,
                },
            }
        }

        // don't rescale the xAxis if we are in preview mode
        if (preview === false) {
            options.scales.x.min = app.ui.stats.sparkChart.renderer.sections.graphs.timestamp - (app.ui.stats.sparkChart.renderer.sections.graphs.graphData[graphIx].secondsInView * 1000)
            options.scales.x.max = app.ui.stats.sparkChart.renderer.sections.graphs.timestamp

        } else {
            options.scales.x.min = undefined
            options.scales.x.max = undefined
        }

        // process tooltip
        options.plugins.tooltip = {
            enabled: defaults.tooltips.enabled,
            displayColors: true,
            intersect: false,
            mode: 'nearest',
            callbacks: {
                title: item => {
                    return ([3, 4].map(x => item[0].label.split(' ')[x])).join(' ')
                },
                label: item => {
                    return ' ' + app.ui.formatThousands(item.parsed.y)
                }
            }
        }

        // ***************************************
        // Data sets
        // ***************************************
        const datasets = []
        mapping.forEach((map, ix) => {
            let seriesData = {}
            const seriesSettings = map.settings.default === 'line' ? map.settings.line : map.settings.area

            seriesData = {
                borderColor: seriesSettings.stroke.color,
                backgroundColor: seriesSettings.stroke.color,
                borderWidth: seriesSettings.stroke.width,
                tension: seriesSettings.stroke.type === 'smooth' ? seriesSettings.stroke.smoothTension : 0,
                stepped: seriesSettings.stroke.type === 'stepline',
                borderDash: [seriesSettings.stroke.dashArray, seriesSettings.stroke.dashArray],
                pointRadius: map.settings[map.settings.default].marking.type === 'markers' ? map.settings[map.settings.default].marking.markers.size : 0,
                pointBackgroundColor: map.settings[map.settings.default].marking.type === 'markers' ? map.settings[map.settings.default].marking.markers.color : seriesSettings.stroke.color,
                pointBorderColor: map.settings[map.settings.default].marking.type === 'markers' ? map.settings[map.settings.default].marking.markers.color : seriesSettings.stroke.color,
                pointStyle: map.settings[map.settings.default].marking.markers.shape,
                yAxisID: defaults.yAxis.common === true ? 'y' : app.ui.stats.utils.graph.convertSeriesName(map.type)
            }

            // finalize
            if (preview) {
                seriesData.label = app.ui.stats.utils.graph.convertSeriesName(map.type, currentSource)
                seriesData.data = preview.series[ix]

            } else {
                const viewId = view.id
                let sourceId = ''

                // find source id first
                app.ui.stats.entry.data.forEach(source => {
                    const res = source.data.views.find(view => view.id === viewId)
                    if (res) sourceId = source.id
                })

                // then the node in the data array
                if (app.statistics.data.length > 0) {
                    const node = app.statistics.data.find(node => node.id === sourceId)
                    if (node) {
                        const mapTypeA = map.type.split('-')
                        if (mapTypeA[0] === '*') mapTypeA[0] = 0

                        const process = node.regions[view.region][mapTypeA[0]]
                        const data = process.find(dataNode => dataNode.sample === mapTypeA[1]).gData[mapTypeA[2]]

                        seriesData.label = app.ui.stats.utils.graph.convertSeriesName(map.type, currentSource)
                        if (currentSource.type !== 'ygbl') seriesData.label = app.ui.stats.sparkChart.renderer.sections.convertLongSampleNames(seriesData.label)
                        seriesData.originalLabel = app.ui.stats.utils.graph.convertSeriesName(map.type)
                        seriesData.data = data //data.length > 0 ? data : []
                    }

                } else {
                    seriesData.label = app.ui.stats.utils.graph.convertSeriesName(map.type)
                    seriesData.data = []
                }
            }

            datasets.push(seriesData)
        })

        if (mapping.length > 0 &&
            (currentSource.data.processes.type === 'top'
                && currentSource.data.processes.count > 1
                || currentSource.data.processes.type === 'pids'
                && currentSource.data.processes.pids.length > 0)) {
            const data = []

            // samples information
            mapping.forEach(map => {
                const typeA = map.type.split('-')
                let sampleType = ''

                switch (typeA[2]) {
                    case 'abs':
                        sampleType = typeA[2];
                        break
                    case 'delta':
                        sampleType = 'Δ';
                        break
                    default:
                        sampleType = 'Δ ' + typeA[2]
                }

                data.push(typeA[1] + ' ' + sampleType)
            })

            app.statistics.sourceMapping.push({
                target: graphIx,
                section: 'graph',
                type: 'process',
                source: currentSource.id + '|' + view.id + '|' + view.region,
                data: data
            })
        }

        settings.data = {
            datasets: datasets
        }
        settings.options = options

        // y Axis
        this.updateYaxis(view, settings)

        if (graphIx === 0 && app.ui.stats.sparkChart.renderer.sections.graphs.chart1) {
            await app.ui.stats.sparkChart.renderer.sections.graphs.chart1.destroy()
        }
        if (graphIx === 1 && app.ui.stats.sparkChart.renderer.sections.graphs.chart2) {
            await app.ui.stats.sparkChart.renderer.sections.graphs.chart2.destroy()
        }

        this.graphData[graphIx].sampleRate = currentSource.sampleRate
        this.graphData[graphIx].samplesInView = 10

        let c
        try {
            c = await new Chart(el, settings)
        } catch (e) {
        }

        return c
    },

    updateYaxis: function (view, settings) {
        const defaults = view.defaults
        const mapping = view.graphMapping

        mapping.forEach((map, ix) => {
            // set the yAxisID
            if (defaults.yAxis.common === true) {
                settings.data.datasets[ix].yAxisID = 'y'

            } else {
                settings.data.datasets[ix].yAxisID = settings.data.datasets[ix].label

                // set the y scale
                const y = settings.options.scales[settings.data.datasets[ix].label] = {}
                y.beginAtZero = true
                y.display = map.settings.yAxis.show === true ? 'auto' : false
                y.type = map.settings.yAxis.type
                y.min = map.settings.yAxis.useManualSetting === true ? defaults.yAxis.min : 0
                y.max = map.settings.yAxis.useManualSetting === true ? defaults.yAxis.max : undefined
                y.position = (ix / 2).toString().split('.')[1] !== undefined ? 'right' : 'left'
                y.title = {
                    display: true,
                    text: settings.data.datasets[ix].label,
                    color: settings.data.datasets[ix].borderColor,
                    align: map.settings.yAxis.title.align,
                    font: {
                        size: map.settings.yAxis.title.style.fontSize,
                        family: map.settings.yAxis.title.style.fontFamily,
                        weight: map.settings.yAxis.title.style.fontWeight,
                    }
                }
                if (map.settings.yAxis.title.text !== settings.data.datasets[ix].label) y.title.text = map.settings.yAxis.title.text

                y.grid = {
                    color: 'silver',
                    drawOnChartArea: ix === 0
                }
                y.ticks = {
                    textStrokeColor: settings.data.datasets[ix].borderColor,
                    textStrokeWidth: 1,
                    color: settings.data.datasets[ix].borderColor,
                    display: map.settings.yAxis.labels.show,
                    font: {
                        size: map.settings.yAxis.labels.style.fontSize,
                        family: map.settings.yAxis.labels.style.fontFamily,
                        weight: map.settings.yAxis.labels.style.fontWeight,
                    }
                }
                y.border = {
                    color: map.settings.yAxis.axisBorder.show === true ? settings.data.datasets[ix].borderColor : undefined
                }
            }
        })

        if (defaults.yAxis.common === true) {
            // set the y scale
            const y = settings.options.scales.y = {}

            y.beginAtZero = true
            y.display = defaults.yAxis.defaultsCommon.show === true ? 'auto' : false
            y.type = defaults.yAxis.defaultsCommon.type
            y.min = defaults.yAxis.defaultsCommon.useManualSetting === true ? defaults.yAxis.defaultsCommon.min : 0
            y.max = defaults.yAxis.defaultsCommon.useManualSetting === true ? defaults.yAxis.defaultsCommon.max : undefined
            y.position = 'left'
            y.title = {
                display: true,
                text: defaults.yAxis.defaultsCommon.title.text,
                color: defaults.yAxis.defaultsCommon.color,
                align: defaults.yAxis.defaultsCommon.title.align,
                font: {
                    size: defaults.yAxis.defaultsCommon.title.style.fontSize,
                    family: defaults.yAxis.defaultsCommon.title.style.fontFamily,
                    weight: defaults.yAxis.defaultsCommon.title.style.fontWeight,
                }
            }
            y.grid = {
                color: 'silver',
                drawOnChartArea: true//ix === 0
            }

            y.ticks = {
                textStrokeColor: defaults.yAxis.defaultsCommon.color,
                textStrokeWidth: 1,
                color: defaults.yAxis.defaultsCommon.color,
                display: defaults.yAxis.defaultsCommon.labels.show,
                font: {
                    size: defaults.yAxis.defaultsCommon.labels.style.fontSize,
                    family: defaults.yAxis.defaultsCommon.labels.style.fontFamily,
                    weight: defaults.yAxis.defaultsCommon.labels.style.fontWeight,
                }
            }

            y.border = {
                color: defaults.yAxis.defaultsCommon.axisBorder.show === true ? defaults.yAxis.defaultsCommon.color : undefined
            }
        }
    },

    // *********************************************
    // TOOLS
    // *********************************************
    tools: {
        range: {
            init: function () {
                const handle = $("#custom-handle");

                $("#rngStatsGraphToolbarResize").slider({
                    value: 10,
                    min: 10,
                    max: 60,
                    step: 10,
                    slide: function (event, ui) {
                        this.scroll(event, ui);
                    },
                    create: function () {
                        handle.text($(this).slider("value"));
                    },
                })
            },

            show: function (chartIx, e) {
                if ($('#divStatsGraphToolbarResize').css('display') === 'block') {
                    $('#divStatsGraphToolbarResize').css('display', 'none')

                    return
                }
                const graphIx = chartIx - 1
                const graphData = app.ui.stats.sparkChart.renderer.sections.graphs.graphData[graphIx]
                const sampleRate = graphData.sampleRate
                const max = sampleRate * app.userSettings.stats.sparkLines.sections.graph.buffering

                $('#divStatsGraphToolbarResize')
                    .css('display', 'block')
                    .css('left', e.clientX - 230)
                    .css('top', e.clientY + 10)

                $("#rngStatsGraphToolbarResize")
                    .slider({
                        value: graphData.secondsInView,
                        min: 10,
                        max: max,
                        step: 5,
                        slide: function (event, ui) {
                            $("#custom-handle").text(ui.value);

                            app.ui.stats.sparkChart.renderer.sections.graphs.graphData[graphIx].secondsInView = ui.value

                            app.ui.stats.sparkChart.renderer.sections.graphs['chart' + chartIx].options.scales.x.min = app.ui.stats.sparkChart.renderer.sections.graphs.timestamp - (graphData.secondsInView * 1000)
                            app.ui.stats.sparkChart.renderer.sections.graphs['chart' + chartIx].options.scales.x.max = app.ui.stats.sparkChart.renderer.sections.graphs.timestamp
                            app.ui.stats.sparkChart.renderer.sections.graphs['chart' + chartIx].update()
                        },
                        create: function () {
                            $("#custom-handle").text(graphData.secondsInView)
                        }
                    })

                $("#custom-handle").text(graphData.secondsInView)
            },
        },

        yAxis: {
            init: function () {
                $('#chkStatsGraphToolbarYaxisCommon').on('click', () => this.sharedClicked())
                $('#chkStatsGraphToolbarYaxisIndividual').on('click', () => this.individualClicked())
            },

            show: function (chartIx, e) {
                if ($('#divStatsGraphToolbarYaxis').css('display') === 'block') {
                    $('#divStatsGraphToolbarYaxis').css('display', 'none')

                    return
                }

                this.graphIx = chartIx - 1
                this.chart = app.ui.stats.sparkChart.renderer.sections.graphs['chart' + chartIx]
                this.view = app.ui.stats.sparkChart.renderer.sections.graphs.graphData[this.graphIx].view
                let rows = ''

                // set the option
                $('#' + (this.view.defaults.yAxis.common === true ? 'chkStatsGraphToolbarYaxisCommon' : 'chkStatsGraphToolbarYaxisIndividual')).prop('checked', true)

                // and populate the dialog according
                if (this.view.defaults.yAxis.common === true) {
                    // shared
                    $('#divStatsGraphToolbarYaxis').css('height', '150px')

                    $('#divStatsGraphToolbarYaxisShared').css('display', 'block')
                    $('#divStatsGraphToolbarYaxisIndividual').css('display', 'none')

                } else {
                    // individual
                    $('#divStatsGraphToolbarYaxis').css('height', '331px')

                    // build the list
                    this.populateList()

                    $('#divStatsGraphToolbarYaxisShared').css('display', 'none')
                    $('#divStatsGraphToolbarYaxisIndividual').css('display', 'block')
                }

                $('#divStatsGraphToolbarYaxis')
                    .css('left', e.clientX - 430)
                    .css('top', e.clientY + 10)
                    .css('display', 'block')
            },

            populateList: function () {
                let rows = ''
                const source = app.ui.stats.utils.getSourceFromViewId(this.view.id)

                this.view.graphMapping.forEach((map, ix) => {
                    const fullName = app.ui.stats.utils.graph.convertSeriesName(map.type, source)

                    rows += '<li class="ui-state-default" id="StatsSparkChartGraphSortable-' + map.type + '" style="color: ' + map.settings.line.stroke.color + '; cursor: pointer; background: #f8f8f8;">'
                    rows += '<input type="checkbox" onclick="app.ui.stats.sparkChart.renderer.sections.graphs.tools.yAxis.checkClicked(' + ix + ')"' + (map.settings.yAxis.show === true ? ' checked' : '') + '>'
                    rows += '&nbsp;&nbsp;' + fullName + '&nbsp;&nbsp;' +
                        '<button class="btn btn-outline-info btn-sm" tabIndex="10" type="button" style="height: 22px; padding-bottom: 20px; float: right;"' +
                        ' onclick="app.ui.stats.sparkChart.renderer.sections.graphs.tools.yAxis.individualSettingsClicked(' + ix + ')">Settings...</button>'
                    rows += '</li>'
                })

                $('#ulStatsGraphToolbarYaxisIndividual')
                    .empty()
                    .append(rows)

                setTimeout(() => {
                    const height = ($('#ulStatsGraphToolbarYaxisIndividual').height() < 331 ? 81 + $('#ulStatsGraphToolbarYaxisIndividual').height() : 331) + 'px'
                    $('#divStatsGraphToolbarYaxis').css('height', height)

                }, 10)
            },

            checkClicked: function (ix) {
                const children = $('#ulStatsGraphToolbarYaxisIndividual').children()
                const status = $(children[ix]).find('input').is(':checked')

                this.view.graphMapping[ix].settings.yAxis.show = status

                this.refreshGraph()
            },

            individualSettingsClicked: function (ix) {
                app.ui.prefs.show({
                    title: 'Y axis',
                    manifest: app.ui.stats.manifest.graph.chart.yAxisSeries,
                    target: this.view.graphMapping[ix].settings.yAxis,
                    name: 'statsSparkChartGraphYaxisIndividualTools'
                })
            },

            sharedSettingsClicked: function () {
                app.ui.prefs.show({
                    title: 'Y axis',
                    manifest: app.ui.stats.manifest.graph.chart.yAxisSeries,
                    target: this.view.defaults.yAxis,
                    name: 'statsSparkChartGraphYaxisSharedTools'
                })
            },

            sharedClicked: function () {
                if ($('#divStatsGraphToolbarYaxisShared').css('display') === 'block') return

                $('#divStatsGraphToolbarYaxis').css('height', '150px')

                $('#divStatsGraphToolbarYaxisShared').css('display', 'block')
                $('#divStatsGraphToolbarYaxisIndividual').css('display', 'none')

                this.view.defaults.yAxis.common = true

                this.refreshGraph()
            },

            individualClicked: function () {
                this.populateList()

                if ($('#divStatsGraphToolbarYaxisShared').css('display') === 'none') return

                $('#divStatsGraphToolbarYaxisShared').css('display', 'none')
                $('#divStatsGraphToolbarYaxisIndividual').css('display', 'block')

                this.view.defaults.yAxis.common = false

                this.refreshGraph()
            },

            refreshGraph: async function () {
                app.ui.stats.sparkChart.renderer.sections.graphs.updateYaxis(this.view, this.chart.config)

                this.chart.update()
            },

            chart: {},
            options: {},
            graphIx: -1,
            view: {}
        },

        theme: {
            switch: function (chart, options, view) {
                //return
                let currentView
                let newOptions
                const status = app.ui.stats.entry.sparkChart.theme

                if (view === undefined) {
                    const graphIdA = options.config.chart.id.split('|')
                    const viewId = graphIdA[1]
                    currentView = app.ui.stats.utils.getViewFromId(viewId)
                } else {
                    currentView = view
                }

                if (!currentView) return

                if (status === 'dark') {
                    currentView.defaults.globals.theme = 'dark'

                    newOptions = {
                        chart: {
                            background: currentView.defaults.globalsDark.background,
                            foreColor: currentView.defaults.globalsDark.color,
                        },
                        grid: {
                            row: {opacity: 0}
                        }
                    }

                } else {
                    currentView.defaults.globals.theme = 'light'

                    newOptions = {
                        chart: {
                            background: currentView.defaults.globals.background,
                            foreColor: currentView.defaults.globals.color,
                        },
                        grid: {
                            row: {opacity: currentView.defaults.grid.row.opacity}
                        }
                    }
                }
            },
        },

        reset: function () {
            $('#divStatsGraphToolbarResize').css('display', 'none')
            $('#divStatsGraphToolbarYaxis').css('display', 'none')
        }
    },

    resize: e => {
        const orientation = app.ui.stats.entry.sparkChart.graphsConfig.orientation
        const divStatsSparkChartGraph1 = $('#divStatsSparkChartGraph1')
        const divStatsSparkChartGraph2 = $('#divStatsSparkChartGraph2')
        const tools1 = $('#spanStatsGraph1Tools')
        const tools2 = $('#spanStatsGraph2Tools')

        if (divStatsSparkChartGraph1.css('display') === 'none') return

        if (divStatsSparkChartGraph2.css('display') !== 'none') {
            // 2 graphs
            if (orientation === 'h') {
                tools1.css('top', (divStatsSparkChartGraph1.height() + 13) + 'px')

            } else {
                tools1.css('left', (divStatsSparkChartGraph1.width() - 45) + 'px')
            }
        }
    },

    chart1: null,
    chart2: null,
    chartData: [
        10,
        10
    ],
    graphData: [
        {
            source: {},         // the source used in the chart
            view: {},           // the view used in the chart
            series: [],
            processes: [],
            sampleRate: 0,
            secondsInView: 10,
        },
        {
            source: {},
            view: {},
            series: [],
            processes: [],
            sampleRate: 0,
            secondsInView: 10,
        }
    ],
    bufferIsRotating: false,
}
