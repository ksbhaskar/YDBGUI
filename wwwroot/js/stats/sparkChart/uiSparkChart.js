/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file manages the sparkChart dialog

app.ui.stats.sparkChart = {
    init: function () {
        $('#btnStatsSparkChartGraphSettings').on('click', () => this.graphSettingsPressed())
        $('#btnStatsSparkChartOk').on('click', () => this.okPressed())
        $('#btnStatsSparkChartHelp').on('click', () => app.ui.help.show('stats/spark-chart/index'))

        app.ui.setupDialogForTest('modalStatsSparkChart')
    },

    show: function () {
        let found = false

        app.ui.stats.entry.data.forEach(source => {
            if (source.data.enabled === true) found = true
        })

        if (found === false) {
            app.ui.msgbox.show('There are no sources or they are not enabled.<br><br>Create or enable at least one source to create a Spark Chart report', app.ui.msgbox.INPUT_ERROR)

            return
        }

        this.entry = JSON.parse(JSON.stringify(app.ui.stats.entry))
        this.refresh()

        $('#modalStatsSparkChart')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    refresh: function () {
        const tbl = $('#tblStatsSparkChartMain > tbody')
        let rows = ''

        // populate main list
        tbl.empty()

        this.entry.data.forEach(source => {
            if (source.data.enabled === true) {
                rows = '<tr id="' + source.id + '">'

                // type
                rows += '<td>' + source.type.toUpperCase() + '</td>'


                // samples
                rows += '<td>' + source.data.samples.join(', ') + '</td>'

                // process
                rows += '<td>'

                if (source.type === 'ygbl') {
                    switch (source.data.processes.type) {
                        case 'agg':
                            rows += 'Aggregate';
                            break
                        case 'pids':
                            rows += 'PIDs: ' + source.data.processes.pids.join(', ');
                            break
                        case 'top':
                            rows += 'Top ' + source.data.processes.count + ' of ' + source.data.processes.sampleCount
                    }
                }
                rows += '</td>'

                // regions
                rows += '<td>' + source.data.regions.join(', ') + '</td>'

                // sample rate
                rows += '<td style="text-align: center;">' + source.sampleRate.toFixed(3) + '</td>'

                // populate options
                const options = {
                    views: source.data.views,
                    sourceId: source.id
                }

                rows += this.optionsTable.createOptionsCell(options)

                rows += '</tr>'

                tbl.append(rows)
            }
        })
    },

    optionsTable: {
        createOptionsCell: function (options) {
            let cell = ''

            cell += this.createHeader(options)

            options.views.forEach(view => {
                const rowOptions = {
                    type: view.type,
                    id: view.id,
                    sourceId: options.sourceId,
                    newRow: false,
                }

                cell += this.createRow(rowOptions)
            })

            options.newRow = true
            cell += this.createRow(options)

            cell += this.createFooter()

            return cell
        },

        createHeader: function (options) {
            return '<td style="display: flex;">' +
                '<table class="table table-sm table-borderless" style="margin-bottom: 0;" id="options_' + options.sourceId + '">' +
                '<thead>' +
                '<tr style="display: none;">' +
                '<th scope="col" style="width: 30px; vertical-align: middle;"></th>' +
                '<th scope="col" style="width: 40px; vertical-align: middle;"></th>' +
                '<th scope="col" style="width: 100%; vertical-align: middle;"></th>' +
                '</tr>' +
                '</thead><tbody>'
        },

        createFooter: function () {
            return '<tbody</table></td>'
        },

        createRow: function (options) {
            let row = ''

            if (options.newRow === true) {
                row = '<tr>' + this.createAddCell(options) + '</tr>'

            } else {
                row = '<tr><td style="display: flex;">' +
                    '<tr>' +
                    this.createDeleteCell(options) +
                    this.createIconCell(options.type) +
                    this.createButtonsCell(options)
            }

            return row
        },

        createDeleteCell: function (options) {
            return '<td style="vertical-align: middle;">' + this.iconRemove(options) + '</td>'
        },

        createAddCell: function (options) {
            return '<td class="stats-sparkchart-options-add-cell" colspan="4">' + this.iconAdd(options) + '</td>'
        },

        createIconCell: function (type) {               // type: graph | reportLine | separator
            let icon

            switch (type) {
                case 'graph':
                    icon = 'bi-graph-down';
                    break
                case 'reportLine':
                    icon = 'bi-grid-3x2';
                    break
                case 'separator':
                    icon = 'bi-distribute-vertical'
            }

            return '<td style="vertical-align: middle;"><div style="vertical-align: middle;"><i class="' + icon + ' stats-sparkchart-options-icon"></i></div></td>'
        },

        createButtonsCell: function (options) {
            return '<td style="display: flex;">' +
                '<button class="btn btn-outline-info btn-sm" style="width: 150px; height: 28px;" type="button" onclick="app.ui.stats.sparkChart.optionsTable.mappingPressed(\'' + options.sourceId + '\',\'' + options.id + '\')">' +
                'Mapping...' +
                '</button>&nbsp;&nbsp;&nbsp;' +
                '<button class="btn btn-outline-info btn-sm" style="width: 150px; height: 28px;" type="button" onclick="app.ui.stats.sparkChart.optionsTable.propsPressed(\'' + options.sourceId + '\',\'' + options.id + '\')">' +
                'Properties...' +
                '</button>' +
                '</td>'
        },

        createIdCell: function (id) {
            return '<td style="width: 60px; vertical-align: middle; text-align: center;"><span  class="stats-sparkchart-options-id">' + id + '</span></td>'
        },

        iconAdd: function (options) {
            return '<ul class="nav nav-pills">' +
                '<li class="nav-item dropdown">' +
                '<a class="nav-link stats-sparkchart-options-add-dropdown" data-toggle="dropdown" href="#" role="button" >' +
                '<i class="bi-plus-square-fill stats-sparkchart-options-add"></i>' +
                '</a>' +
                '<div class="dropdown-menu">' +
                '<a class="dropdown-item" href="#" onclick="app.ui.stats.sparkChart.optionsTable.createView(\'graph\', \'' + options.sourceId + '\')">Add graph to view</a>' +
                '<a class="dropdown-item" href="#" onclick="app.ui.stats.sparkChart.optionsTable.createView(\'reportLine\', \'' + options.sourceId + '\')">Add table to view</a>' +
                '</div></li></ul>'
        },

        iconRemove: function (options) {
            return '<i class="bi-x-square-fill stats-sparkchart-options-delete" onclick="app.ui.stats.sparkChart.optionsTable.removePressed(\'' + options.sourceId + '\', \'' + options.id + '\')"></i>'
        },

        // EVENTS

        createView: function (type, sourceId) {
            const source = app.ui.stats.findEntryById(sourceId, app.ui.stats.sparkChart.entry)
            const views = source.data.views
            const id = app.ui.GUID()
            const mapping = []
            const highlights = []

            // if type = graph, we need to check if we reached the overall limit of 2 graphs...
            if (type === 'graph') {
                let graphCount = 0
                app.ui.stats.sparkChart.entry.data.forEach(source => {
                    if (source.data.enabled === true) {
                        source.data.views.forEach(view => {
                            if (view.type === 'graph') graphCount++
                        })
                    }
                })

                if (graphCount > 1) {
                    app.ui.msgbox.show('You reached the maximum number of graphs in this report.', 'Warning')

                    return
                }
            }

            // create an empty mapping and highlights (for report line)
            source.data.samples.forEach(sample => {
                const mnemonic = app.ui.stats.sources.ygblstats.findSampleByMnemonic(sample)
                // regular sample, all data available
                mapping.push({
                    sample: sample,
                    abs: false,
                    delta: false,
                    min: false,
                    max: false,
                    avg: false
                })

                if (type === 'reportLine') {
                    const highlightsData = []
                    highlightsData.push({sampleType: 'abs', type: 'normal'})
                    highlightsData.push({sampleType: 'delta', type: 'normal'})
                    highlightsData.push({sampleType: 'min', type: 'normal'})
                    highlightsData.push({sampleType: 'max', type: 'normal'})
                    highlightsData.push({sampleType: 'avg', type: 'normal'})

                    highlights.push({
                        sample: sample,
                        data: highlightsData
                    })
                }
            })

            // create the entry in the object
            const objEntry = {
                id: id,
                type: type,
                mapping: mapping,
                highlights: highlights,
                options: {},
            }

            // initialize the reportLine object, if needed
            if (type === 'reportLine') {
                objEntry.options = {
                    orientation: 'h',
                }

                objEntry.defaults = JSON.parse(JSON.stringify(app.userSettings.stats.sparkLines.sections[type]))
                objEntry.defaults.process.show = source.data.processes.type !== 'agg'
                objEntry.defaults.region.text = source.data.regions.length === 1 ? source.data.regions.join(' ') : ''

                app.ui.stats.sparkChart.renderer.setThemeOnReportLine(objEntry.defaults)
            }

            // initialize the graph object, if needed
            if (type === 'graph') {
                objEntry.graphMapping = []
                objEntry.defaults = JSON.parse(JSON.stringify(app.userSettings.stats.sparkLines.sections.graph.chart))

                delete objEntry.defaults.yAxis.defaultsSeries

                // defaults to common if source has a top processes
                objEntry.defaults.yAxis.common = source.data.processes.type === 'top' || source.data.processes.type === 'pids'
            }

            views.push(objEntry)
            objEntry.renderIx = views.length - 1

            // create the row
            const options = {
                type: type,
                id: id,
                sourceId: sourceId,
                newRow: false,
            }
            const row = this.createRow(options)

            const lastRow = $('#options_' + sourceId + ' > tbody > tr:last-child')
            $(row).insertBefore(lastRow)
        },

        mappingPressed: function (sourceId, viewId) {
            const source = app.ui.stats.findEntryById(sourceId, app.ui.stats.sparkChart.entry)
            app.ui.stats.sparkChart.mapping.show(source, viewId)
        },

        propsPressed: function (sourceId, viewId) {
            const source = app.ui.stats.findEntryById(sourceId, app.ui.stats.sparkChart.entry)
            const view = source.data.views.find(view => view.id === viewId)
            switch (view.type) {
                case 'graph':
                    const view = source.data.views.find(view => view.id === viewId)
                    const maps = app.ui.stats.utils.mapObjToArray(view)

                    if (maps.length === 0) {
                        app.ui.inputbox.show('There are no mappings specified for this view. Do you want to create new mappings?', 'Warning', ret => {
                            if (ret === 'YES') {
                                app.ui.stats.sparkChart.optionsTable.mappingPressed(sourceId, viewId)

                                $('#modalStatsSparkChartReportLine').modal('hide')
                            }
                        })

                    } else {
                        app.ui.stats.sparkChart.graph.show(source, viewId);
                    }

                    break

                case 'reportLine': {
                    const view = JSON.parse(JSON.stringify(source.data.views.find(view => view.id === viewId)))     // dereference object
                    const maps = app.ui.stats.utils.mapObjToArray(view)

                    if (maps.length === 0) {
                        app.ui.inputbox.show('There are no mappings specified for this view. Do you want to create new mappings?', 'Warning', ret => {
                            if (ret === 'YES') {
                                app.ui.stats.sparkChart.optionsTable.mappingPressed(sourceId, viewId)

                                $('#modalStatsSparkChartReportLine').modal('hide')
                            }
                        })
                    } else {
                        app.ui.stats.sparkChart.reportLine.show(source, viewId);
                    }

                    break
                }
            }
        },

        removePressed: function (sourceId, viewId) {
            const source = app.ui.stats.findEntryById(sourceId, app.ui.stats.sparkChart.entry)
            const viewIx = source.data.views.findIndex(view => view.id === viewId)
            const lView = source.data.views.find(view => view.id === viewId)

            source.data.views.splice(viewIx, 1)

            // remove graphData
            if (lView.type === 'graph') {
                for (let ix = 0; ix < 1; ix++) {
                    if (app.ui.stats.sparkChart.renderer.sections.graphs.graphData[ix].view.id === lView.id) {
                        app.ui.stats.sparkChart.renderer.sections.graphs.graphData[ix] = {
                            source: {},
                            view: {},
                            series: [],
                            processes: [],
                            sampleRate: 0,
                            secondsInView: 10
                        }
                        try {
                            app.ui.stats.sparkChart.renderer.sections.graphs[('chart' + (ix + 1))].destroy()
                        } catch (err) {
                        }
                        app.ui.stats.sparkChart.renderer.sections.graphs[('chart' + (ix + 1))] = null
                    }
                }
            }

            app.ui.stats.sparkChart.refresh()
        },
    },

    graphSettingsPressed: function () {
        app.ui.stats.sparkChart.graphSettings.show(this.entry)
    },

    okPressed: function () {
        let mapCount = 0

        this.entry.data.forEach(source => {
            if (source.data.views) {
                source.data.views.forEach(view => {
                    if (app.ui.stats.utils.mapObjToArray(view).length > 0) mapCount++

                })
            }
        })

        app.ui.stats.entry = JSON.parse(JSON.stringify(this.entry))

        app.ui.stats.tab.toolbar.status = 'stopped'

        app.ui.stats.tab.toolbar.computeToolbarStatus()

        app.ui.stats.sparkChart.renderer.render($('#divStatsSparkchart'))

        // update tab header with dirty status
        $('#lblStatsReportName').html('[' + app.ui.stats.entry.name + '<span style="color: red;">*</span>]')
        app.ui.stats.entryDirty = true

        $('#modalStatsSparkChart').modal('hide')
    },

    entry: {}
}
