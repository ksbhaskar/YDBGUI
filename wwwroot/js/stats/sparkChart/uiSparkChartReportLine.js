/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file manages the sparkChart report line configuration

app.ui.stats.sparkChart.reportLine = {
    init: function () {
        $('#optStatsSparkChartReportLineH').on('click', () => this.hClicked())
        $('#optStatsSparkChartReportLineV').on('click', () => this.vClicked())

        $('#chkStatsSparkChartReportLineVScrollable').on('click', () => this.scrollableClicked())
        $('#inpStatsSparkChartReportLineVScrollableHeight').on('change', () => this.maxHeightChanged())

        $('#divStatsSparkChartReportLineHleft').on('click', () => this.hClicked())
        $('#divStatsSparkChartReportLineHright').on('click', () => this.hClicked())

        $('#divStatsSparkChartReportLineVleft').on('click', () => this.vClicked())
        $('#divStatsSparkChartReportLineVright').on('click', () => this.vClicked())

        $('#btnStatsSparkChartReportLineResetSettings').on('click', () => this.resetSettingsClicked())
        $('#btnStatsSparkChartReportLineSettings').on('click', () => this.settingsClicked())
        $('#btnStatsSparkChartReportLineSaveSettings').on('click', () => this.saveSettingsClicked())
        $('#btnStatsSparkChartReportLineOk').on('click', () => this.okClicked())
        $('#btnStatsSparkChartReportLineHelp').on('click', () => app.ui.help.show('stats/spark-chart/report-line'))

        app.ui.setupDialogForTest('modalStatsSparkChartReportLine')
    },

    show: function (source, viewId) {
        this.source = source
        this.viewId = viewId
        const view = this.view = JSON.parse(JSON.stringify(source.data.views.find(view => view.id === viewId)))     // dereference object
        this.originalView = source.data.views.find(view => view.id === viewId)
        const maps = app.ui.stats.utils.mapObjToArray(view)
        let mapAsString = ''

        // populate objects
        $('#lblStatsSparkChartReportLineRegions').text(source.data.regions.join(', '))

        $('#lblStatsSparkChartReportLineProcesses').text(app.ui.stats.utils.procObjToString(source))

        maps.forEach(map => {
            mapAsString += map.sample + ' ( '

            map.data.forEach(stat => {
                mapAsString += stat + ','
            })
            mapAsString = mapAsString.slice(0, -1) + ' ), '
        })
        mapAsString = mapAsString.slice(0, -2)

        this.preview()

        $('#lblStatsSparkChartReportLineSamples').text(mapAsString)

        // orientation
        if (view.options.orientation === 'h') {
            $('#optStatsSparkChartReportLineH').prop('checked', 'checked')
            this.hClicked()

        } else {
            $('#optStatsSparkChartReportLineV').prop('checked', 'checked')
            this.vClicked()
        }

        // display dialog
        $('#modalStatsSparkChartReportLine')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    hClicked: function () {
        const oldStatus = $('#optStatsSparkChartReportLineH').prop('checked')

        $('#optStatsSparkChartReportLineH').prop('checked', true)

        $('#divStatsSparkChartReportLineHleft').addClass('stats-sparkchart-settings-report-orientation-left')
        $('#divStatsSparkChartReportLineHright').addClass('stats-sparkchart-settings-report-orientation-right')

        $('#divStatsSparkChartReportLineVleft').removeClass('stats-sparkchart-settings-report-orientation-left')
        $('#divStatsSparkChartReportLineVright').removeClass('stats-sparkchart-settings-report-orientation-right')

        $('#chkStatsSparkChartReportLineVScrollable').attr('disabled', true)
        $('#inpStatsSparkChartReportLineVScrollableHeight').attr('disabled', true)

        this.view.options.orientation = 'h'

        if (oldStatus === false) this.preview()
    },

    vClicked: function () {
        const oldStatus = $('#optStatsSparkChartReportLineV').prop('checked')

        $('#optStatsSparkChartReportLineV').prop('checked', true)

        $('#divStatsSparkChartReportLineVleft').addClass('stats-sparkchart-settings-report-orientation-left')
        $('#divStatsSparkChartReportLineVright').addClass('stats-sparkchart-settings-report-orientation-right')

        $('#divStatsSparkChartReportLineHleft').removeClass('stats-sparkchart-settings-report-orientation-left')
        $('#divStatsSparkChartReportLineHright').removeClass('stats-sparkchart-settings-report-orientation-right')

        $('#chkStatsSparkChartReportLineVScrollable').prop('disabled', false)
        $('#inpStatsSparkChartReportLineVScrollableHeight').prop('disabled', false)

        this.view.options.orientation = 'v'

        if (oldStatus === false) this.preview()
    },

    scrollableClicked: function () {
        this.preview()
    },

    maxHeightChanged: function () {
        this.preview()
    },

    preview: function () {
        const divStatsSparkChartReportLinePreview = $('#divStatsSparkChartReportLinePreview')

        divStatsSparkChartReportLinePreview.empty()
        app.ui.stats.sparkChart.renderer.sections.reportLine(divStatsSparkChartReportLinePreview, this.source, this.view, true)
    },

    settingsClicked: function () {
        app.ui.prefs.show({
            title: 'Table settings',
            manifest: app.ui.stats.manifest.reportLine,
            target: this.view.defaults,
            name: 'statsSparkChartReportLine'
        })
    },

    resetSettingsClicked: function () {
        app.ui.inputbox.show('Are you sure you want to reset the settings to default ?', '', ret => {
            if (ret === 'YES') {

                this.view.defaults = app.userSettings.stats.sparkLines.sections.reportLine

                this.preview()
            }
        })
    },

    saveSettingsClicked: function () {
        app.ui.inputbox.show('Are you sure you want to overwrite the default settings ?', '', ret => {
            if (ret === 'YES') {

                app.userSettings.stats.sparkLines.sections.reportLine = this.view.defaults

                app.ui.storage.save('stats', app.userSettings.stats);
            }
        })

    },

    highlighterClicked: function (viewId, sample, sampleType) {
        const sampleHighlight = this.view.highlights.find(entry => entry.sample === sample)
        const highlight = sampleHighlight.data.find(entry => entry.sampleType === sampleType)

        app.ui.stats.sparkChart.highlighters.show(this.source, this.view, highlight)
    },

    okClicked: function () {
        this.source.data.views.forEach((view, ix) => {
            if (view.id === this.viewId) {
                this.source.data.views[ix] = this.view
            }
        })

        $('#modalStatsSparkChartReportLine').modal('hide')
    },

    source: {},
    view: [],
    viewId: '',
    originalView: [],
    options: {}
}
