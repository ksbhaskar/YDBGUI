/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file performs the rendering of the spark chart. It contains main entry points and all the code to
// render the reportLines. Graph rendering is performed in a separate file (called from here)

app.ui.stats.sparkChart.renderer = {
    sections: {
        // ************************************
        // report line
        // ************************************
        // renders a single report line inside the $parent element
        reportLine: function ($parent, source, view, preview = false) {
            let rows = ''

            // determine the view type
            const processesMultiple = source.data.processes.type === 'top' && source.data.processes.count > 1
                || source.data.processes.type === 'pids' && source.data.processes.pids.length > 0 // true if proc > 1, false if proc = 1
            const regionsMultiple = source.data.regions.length > 1                                              // true if regions > 1
            const orientation = view.options.orientation                                                        // 'h' or 'v'


            if (regionsMultiple === false) {
                // ***********************
                // Only ONE region ( * or single)
                // ***********************
                if (processesMultiple === false) {
                    rows = this.renderRegionProcess(source, view, 0, 0, preview)

                    rows += this.renderBlock(source, view, 0, 0, {orientation: orientation}, preview)

                } else {
                    // ***********************
                    // Multiple processes
                    // ***********************
                    rows = this.renderRegion(source, view, 0, preview)
                    const endCounter = source.data.processes.type === 'top' ? source.data.processes.count : source.data.processes.pids.length

                    for (let ix = 0; ix < endCounter; ix++) {
                        rows += this.renderBlock(source, view, 0, ix, {orientation: orientation}, preview)
                    }
                }
            } else {
                // ***********************
                // Multiple regions
                // ***********************
                source.data.regions.forEach((region, regionIx) => {
                    if (processesMultiple === false) {
                        // ***********************
                        // Single process
                        // ***********************
                        rows += this.renderRegionProcess(source, view, regionIx, 0, preview)

                        rows += this.renderBlock(source, view, regionIx, 0, {orientation: orientation}, preview)

                    } else {
                        // ***********************
                        // Multiple processes
                        // ***********************
                        rows += this.renderRegion(source, view, regionIx)
                        const endCounter = source.data.processes.type === 'top' ? source.data.processes.count : source.data.processes.pids.length

                        for (let ix = 0; ix < endCounter; ix++) {
                            rows += this.renderBlock(source, view, regionIx, ix, {orientation: orientation}, preview)
                        }
                    }
                })
            }

            $parent.append(rows)

            app.ui.regeneratePopups()
        },

        computeUniqueMapping: function (mapping) {
            const ret = {
                abs: false,
                delta: false,
                min: false,
                max: false,
                avg: false
            }

            mapping.forEach(map => {
                if (map.abs === true) ret.abs = true
                if (map.delta === true) ret.delta = true
                if (map.min === true) ret.min = true
                if (map.max === true) ret.max = true
                if (map.avg === true) ret.avg = true
            })

            return ret
        },

        computeRegionTd: function (view) {
            const regSettings = view.defaults.region
            let decoration = ''

            if (regSettings.font.decoration.type !== 'normal') {
                decoration = 'text-decoration: ' + regSettings.font.decoration.type + ' ' +
                    regSettings.font.decoration.color + ' ' +
                    regSettings.font.decoration.style + ' ' +
                    regSettings.font.decoration.thickness + 'px;'

            } else {
                decoration = ''
            }

            return '<td contentEditable="true" style="width: 50%; background-color: ' + regSettings.background + '; border: solid ' + regSettings.border.color + ' ' + regSettings.border.thickness + 'px; ' +
                'font-size: ' + regSettings.font.size + 'px; color: ' +
                regSettings.font.color + '; font-family: ' +
                regSettings.font.fontFamily + '; text-align: ' + regSettings.align + '; ' +
                'font-weight: ' + regSettings.font.weight + '; font-style: ' +
                regSettings.font.style + '; ' + decoration + '" oninput="app.ui.stats.sparkChart.renderer.sections.updateRegionCaption(\'' + view.id + '\',\'' + 'GUID' + '\')">'
        },

        computeProcessTd: function (view, withWidth = false) {
            const procSettings = view.defaults.process
            let decoration = ''
            let width = withWidth === true ? 'width: 100px; ' : ''

            if (procSettings.font.decoration.type !== 'normal') {
                decoration = 'text-decoration: ' + procSettings.font.decoration.type + ' ' + procSettings.font.decoration.color + ' ' + procSettings.font.decoration.style + ' ' + procSettings.font.decoration.thickness + 'px;'

            } else {
                decoration = ''
            }

            return '<td contentEditable="true" style="width: 50%; background-color: ' + procSettings.background + '; border: solid ' +
                procSettings.border.color + ' ' + procSettings.border.thickness + 'px; ' +
                width + 'font-size: ' + procSettings.font.size + 'px; color: ' +
                procSettings.font.color + '; font-family: ' + procSettings.font.fontFamily + '; text-align: ' + procSettings.align + '; ' +
                'font-weight: ' + procSettings.font.weight + '; font-style: ' + procSettings.font.style + '; ' + decoration + '">'
        },

        computeSampleTd: function (source, view, withWidth = false, sample = '') {
            const sampleSettings = view.defaults.sample
            let decoration = ''
            let sampleHelp = ''
            let cursor = ''

            if (sampleSettings.font.decoration.type !== 'normal') {
                decoration = 'text-decoration: ' + sampleSettings.font.decoration.type + ' ' + sampleSettings.font.decoration.color + ' ' + sampleSettings.font.decoration.style + ' ' + sampleSettings.font.decoration.thickness + 'px;'

            } else {
                decoration = ''
            }

            let cellWidth = ''

            if (sample !== '') {
                switch (source.type) {
                    case 'ygbl': {
                        sampleHelp = 'data-placement="bottom" data-toggle="popover" data-trigger="hover" title="' + sample + '" data-content="' + app.ui.stats.sources.ygblstats.findSampleByMnemonic(sample).caption + '" '

                        break
                    }
                    case 'fhead': {
                        sampleHelp = 'data-placement="bottom" data-toggle="popover" data-trigger="hover" title="' + sample + '" data-content="' + app.ui.stats.sources.ygblstats.findSampleByFhead(sample).caption + '" '

                        break
                    }
                    case 'peekByName': {
                        sampleHelp = 'data-placement="bottom" data-toggle="popover" data-trigger="hover" title="' + sample + '" data-content="' + app.ui.stats.sources.ygblstats.findSampleByPeekByName(sample).caption + '" '
                    }
                }

                cursor = 'cursor: pointer;'
            }

            if (withWidth === true) cellWidth = 'width: 100px; '
            if (typeof withWidth === 'number') cellWidth = 'width: ' + withWidth + '%; '

            return '<td style="' + cellWidth + 'background-color: ' + sampleSettings.background +
                '; border: solid ' + sampleSettings.border.color + ' ' + sampleSettings.border.thickness + 'px; ' +
                'font-size: ' + sampleSettings.font.size + 'px; color: ' + sampleSettings.font.color + '; font-family: ' +
                sampleSettings.font.fontFamily + '; ' + 'text-align: ' + sampleSettings.align + '; ' +
                'font-weight: ' + sampleSettings.font.weight + '; font-style: ' + sampleSettings.font.style + '; ' + decoration
                + cursor + '"' +
                sampleHelp + '" >'
        },

        computeSampleMapTd: function (view, withWidth = false, mapValue, mapValueVal) {
            const sampleMap = view.defaults.sampleMap
            let decoration = ''
            let sampleMapHelp = ''
            let cursor = ''

            if (sampleMap.font.decoration.type !== 'normal') {
                decoration = 'text-decoration: ' + sampleMap.font.decoration.type + ' ' + sampleMap.font.decoration.color + ' ' +
                    sampleMap.font.decoration.style + ' ' + sampleMap.font.decoration.thickness + 'px;'

            } else {
                decoration = ''
            }

            if (mapValue !== undefined) {
                sampleMapHelp = 'data-placement="bottom" data-toggle="popover" data-trigger="hover" title="' + mapValueVal + '" data-content="' + app.statistics.samplesDescription[mapValue] + '" '
                cursor = 'cursor: pointer;'
            }

            let cellWidth = ''

            if (withWidth === true) cellWidth = 'width: 100px; '
            if (typeof withWidth === 'number') cellWidth = 'width: ' + withWidth + '%; '


            return '<td style="' + cellWidth + 'background-color: ' + sampleMap.background + '; border: solid ' + sampleMap.border.color + ' ' + sampleMap.border.thickness + 'px; ' +
                'font-size: ' + sampleMap.font.size + 'px; color: ' + sampleMap.font.color + '; font-family: ' + sampleMap.font.fontFamily + '; text-align: ' + sampleMap.align + '; ' +
                'font-weight: ' + sampleMap.font.weight + '; font-style: ' + sampleMap.font.style + '; ' + decoration
                + cursor + '"' +
                sampleMapHelp + '" >'
        },

        computeValueTd: function (view, highlight) {
            const value = view.defaults.value
            let decoration = ''

            if (value.font.decoration.type !== 'normal') {
                decoration = 'text-decoration: ' + value.font.decoration.type + ' ' + value.font.decoration.color + ' ' + value.font.decoration.style + ' ' + value.font.decoration.thickness + 'px;'

            } else {
                decoration = ''
            }

            return '<td style="background-color: ' + (highlight.type === 'range' ? 'var(--ydb-status-green)' : value.background) +
                '; border: solid ' + value.border.color + ' ' + value.border.thickness + 'px; ' +
                'font-size: ' + value.font.size + 'px; color: ' + (highlight.type === 'range' ? 'white' : value.font.color) +
                '; font-family: ' + value.font.fontFamily + '; text-align: ' + value.align + '; ' +
                'font-weight: ' + value.font.weight + '; font-style: ' + value.font.style + '; ' + decoration + '">'
        },

        computeNoValueTd: function (view) {
            const noValue = view.defaults.noValue

            return '<td style="background-color: ' + noValue.background + '; border: solid ' + noValue.border.color + ' ' + noValue.border.thickness + 'px; ">'
        },

        computeHeaderTableStyle: function (view) {
            const header = view.defaults.header

            return ' style="border: solid ' + header.border.thickness + 'px ' + header.border.color + ';"'
        },

        renderRegionProcess: function (source, view, regionIx, processIx, preview) {
            let rows = ''

            // start drawing the table
            rows += '<table class="stats-sparkchart-render-report-line-table"' + this.computeHeaderTableStyle(view) + '>'

            // overview - region
            rows += '<tr>'

            // REGION Source mapping
            const regionId = app.ui.GUID()
            if (preview === false) {
                app.statistics.sourceMapping.push({
                    target: regionId,
                    section: 'reportLine',
                    type: 'region',
                    source: source.id + '|' + view.id + '|' + regionIx
                })
            }

            rows += '<tr>' + this.computeRegionTd(view) +
                '<span id="' + regionId + '">' +
                (view.defaults.region.text === '' ? source.data.regions[regionIx] : view.defaults.region.text) +
                '</span></td>'

            rows = rows.replace('GUID', regionId)

            if (view.defaults.process.show === true) {
                // overview - processes
                let process
                if (source.data.processes.type === 'agg') {
                    process = '*'

                } else if (source.data.processes.type === 'pids') {
                    process = '[' + source.data.processes.pids[processIx] + ']'

                } else {
                    process = '[' + processIx + ']'
                }

                // PROCESS Source mapping
                const processId = app.ui.GUID()
                if (preview === false) {
                    app.statistics.sourceMapping.push({
                        target: processId,
                        section: 'reportLine',
                        type: 'process',
                        source: source.id + '|' + view.id + '|' + regionIx + '|' + processIx
                    })
                }

                rows += this.computeProcessTd(view) +
                    '<span id="' + processId + '">' +
                    (preview === true ? 'Process: ' + process : 'Process:') +
                    '</span></td>'

            }

            return rows + '</tr></table>'
        },

        renderRegion: function (source, view, regionIx, preview) {
            let rows = ''

            // start drawing the table
            rows += '<table class="stats-sparkchart-render-report-line-table"' + this.computeHeaderTableStyle(view) + '>'

            // overview - region
            rows += '<tr>'

            // REGION Source mapping
            const regionId = app.ui.GUID()
            if (preview === false) {
                app.statistics.sourceMapping.push({
                    target: regionId,
                    section: 'reportLine',
                    type: 'region',
                    source: source.id + '|' + view.id + '|' + regionIx
                })
            }

            rows += '<tr>' + this.computeRegionTd(view) +
                '<span id="' + regionId + '">' +
                (view.defaults.region.text === '' ? source.data.regions[regionIx] : view.defaults.region.text) +
                '</span></td>'

            rows = rows.replace('GUID', regionId)

            return rows + '</tr></table>'
        },

        renderProcess: function (source, view, regionIx, processIx, preview, processesMultiple) {
            // overview - processes
            let process
            if (source.data.processes.type === 'agg') {
                process = '*'

            } else if (source.data.processes.type === 'pids') {
                process = '[' + source.data.processes.pids[processIx] + ']'

            } else {
                process = '[' + processIx + ']'
            }
            let processId = ''
            if (processesMultiple === true) {
                // PROCESS Source mapping
                processId = app.ui.GUID()
                if (preview === false) {
                    app.statistics.sourceMapping.push({
                        target: processId,
                        section: 'reportLine',
                        type: 'process',
                        source: source.id + '|' + view.id + '|' + regionIx + '|' + processIx
                    })
                }
            }

            return this.computeProcessTd(view, true) +
                '<span id="' + processId + '">' +
                (preview === true ? 'Process: ' + process : processesMultiple === true ? 'P' + process : '') +
                '</span>'
        },

        renderBlock: function (source, view, regionIx, processIx, options, preview) {
            let rows = ''
            const orientation = options.orientation
            const processesMultiple = source.data.processes.type === 'top' && source.data.processes.count > 1   // true if proc > 1, false if proc = 1
                || source.data.processes.type === 'pids' && source.data.processes.pids.length > 0
            // prepare mapping
            const map = this.computeUniqueMapping(view.mapping)
            rows += '<table class="stats-sparkchart-render-report-line-table">'

            if (orientation === 'h') {
                // ***********************
                // horizontal
                // ***********************
                // header
                if (processesMultiple === true) {
                    rows += this.renderProcess(source, view, regionIx, processIx, preview, processesMultiple) + '</td>'
                } else {
                    rows += this.computeSampleTd(source, view) + '</td>'
                }

                // compute the percent for each header
                let itemsCount = 0
                view.mapping.forEach(map => {
                    if (this.computeMapStatus(map)) return

                    itemsCount++
                })

                const percent = parseInt((90 / itemsCount).toString()) // we use 90 and not 100 because we have the first (fixed width) cell that is not included

                view.mapping.forEach(map => {
                    if (this.computeMapStatus(map)) return

                    const sample = source.type === 'ygbl' ? map.sample : this.convertLongSampleNames(map.sample)


                    rows += this.computeSampleTd(source, view, percent, map.sample) + sample + '</td>'
                })

                rows += '</tr>'

                // data
                for (const mapEntry in map) {
                    if (map[mapEntry] === false) continue

                    let mapEntryVal = mapEntry

                    if (mapEntry === 'min' || mapEntry === 'max' || mapEntry === 'avg') {
                        mapEntryVal = 'Δ ' + mapEntry

                    } else if (mapEntry === 'delta') mapEntryVal = 'Δ'

                    rows += '<tr>'
                    rows += this.computeSampleMapTd(view, false, mapEntry, mapEntryVal) + mapEntryVal + '</td>'
                    view.mapping.forEach(mapE => {
                        if (this.computeMapStatus(mapE)) return

                        // fetch the highlight
                        const sampleHighlight = view.highlights.find(entry => entry.sample === mapE.sample)
                        const highlight = sampleHighlight.data.find(entry => entry.sampleType === mapEntry)

                        if (mapE[mapEntry] === true) {
                            // push into mapping
                            const dataId = app.ui.GUID()
                            if (preview === false) {
                                app.statistics.sourceMapping.push({
                                    target: dataId,
                                    section: 'reportLine',
                                    type: 'data',
                                    source: source.id + '|' + view.id + '|' + regionIx + '|' + processIx + '|' + mapE.sample + '|' + mapEntry,
                                    highlight: highlight
                                })
                            }

                            let highlighter = ''
                            if (preview === true) {
                                highlighter += '&nbsp;<button class="btn btn-outline-info btn-sm" style="font-size: 8px; font-weight: bold;" onclick="app.ui.stats.sparkChart.reportLine.highlighterClicked(\'' + view.id + '\', \'' + mapE.sample + '\', \'' + mapEntry + '\')" tabindex="10" type="button">...</button>'
                            }

                            let highlightHelp = ''
                            let cursor = ''
                            if (highlight.type === 'range') {
                                const highlightDescription = 'RED: > ' + app.ui.formatThousands(highlight.range.red) + '<br>YELLOW: > ' + app.ui.formatThousands(highlight.range.yellow + '<br>GREEN: <= ' + app.ui.formatThousands(highlight.range.yellow))
                                highlightHelp = 'data-placement="bottom" data-toggle="popover" data-trigger="hover" title="Color range" data-content="' + highlightDescription + '" '
                                cursor = 'cursor: pointer;'
                            }

                            rows += this.computeValueTd(view, highlight) +
                                '<span id="' + dataId + '"' + highlightHelp + ' style="' + cursor + '">' +
                                (preview === true ? highlight.type + highlighter : '0') +
                                '</span></td>'
                        } else {
                            rows += this.computeNoValueTd(view) + '</td>'
                        }
                    })

                    rows += '</tr>'
                }

            } else {
                // ***********************
                // vertical
                // ***********************

                // header
                //rows += this.renderProcess(source, view, regionIx, processIx, preview) + '</td>'
                if (processesMultiple === true) {
                    rows += this.renderProcess(source, view, regionIx, processIx, preview, processesMultiple) + '</td>'
                } else {
                    rows += this.computeSampleMapTd(view) + '</td>'
                }


                // compute the percent for each header
                let itemsCount = 0

                for (const mapEntry in map) {
                    if (map[mapEntry] === false) continue

                    itemsCount++
                }

                const percent = parseInt((90 / itemsCount).toString())

                for (const mapEntry in map) {
                    if (map[mapEntry] === false) continue

                    let mapEntryVal = mapEntry

                    if (mapEntry === 'min' || mapEntry === 'max' || mapEntry === 'avg') {
                        mapEntryVal = 'Δ ' + mapEntry

                    } else if (mapEntry === 'delta') mapEntryVal = 'Δ'

                    rows += this.computeSampleMapTd(view, percent, mapEntry, mapEntryVal) + mapEntryVal + '</td>'
                }
                rows += '</tr>'

                // samples
                view.mapping.forEach(mapE => {
                    if (this.computeMapStatus(mapE)) return

                    const sample = source.type === 'ygbl' ? mapE.sample : this.convertLongSampleNames(mapE.sample)

                    rows += '<tr>' + this.computeSampleTd(source, view, false, mapE.sample) + sample + '</td>'

                    for (const subMap in mapE) {
                        if (subMap === 'sample' || map[subMap] === false) continue

                        if (mapE[subMap] === true) {
                            // fetch the highlight
                            const sampleHighlight = view.highlights.find(entry => entry.sample === mapE.sample)
                            const highlight = sampleHighlight.data.find(entry => entry.sampleType === subMap)

                            // push into mapping
                            const dataId = app.ui.GUID()
                            if (preview === false) {
                                app.statistics.sourceMapping.push({
                                    target: dataId,
                                    section: 'reportLine',
                                    type: 'data',
                                    source: source.id + '|' + view.id + '|' + regionIx + '|' + processIx + '|' + mapE.sample + '|' + subMap,
                                    highlight: highlight
                                })
                            }

                            let highlighter = ''
                            if (preview === true) {
                                highlighter += '&nbsp;<button class="btn btn-outline-info btn-sm" style="font-size: 8px; font-weight: bold;" onclick="app.ui.stats.sparkChart.reportLine.highlighterClicked(\'' + view.id + '\', \'' + mapE.sample + '\', \'' + subMap + '\')" tabindex="10" type="button">...</button>'
                            }

                            let highlightHelp = ''
                            let cursor = ''
                            if (highlight.type === 'range') {
                                const highlightDescription = 'RED: > ' + app.ui.formatThousands(highlight.range.red) + '<br>YELLOW: > ' + app.ui.formatThousands(highlight.range.yellow + '<br>GREEN: <= ' + app.ui.formatThousands(highlight.range.yellow))
                                highlightHelp = 'data-placement="bottom" data-toggle="popover" data-trigger="hover" title="Color range" data-content="' + highlightDescription + '" '
                                cursor = 'cursor: pointer;'
                            }

                            rows += this.computeValueTd(view, highlight) +
                                '<span id="' + dataId + '"' + highlightHelp + ' style="' + cursor + '">' +
                                (preview === true ? highlight.type + highlighter : '0') +
                                '</span></td>'
                        } else {
                            rows += this.computeNoValueTd(view) + '</td>'
                        }
                    }

                    rows += '</tr>'
                })
            }

            return rows + '</table>'
        },

        computeMapStatus: function (map) {
            return (map.abs === false && map.delta === false && map.min === false && map.max === false && map.avg === false)
        },

        updateRegionCaption: function (viewId, elementId) {
            const view = app.ui.stats.utils.getViewFromId(viewId)

            view.defaults.region.text = $('#' + elementId).text()
        },

        convertLongSampleNames: function (sample) {
            const sampleA = sample.split('.')

            return sampleA[sampleA.length - 1]
        },
    },

    // MAIN ENTRY POINT FOR RENDERING
    render: async function () {
        const divStatsSparkChartGraphsContainer = $('#divStatsSparkChartGraphsContainer')
        const divStatsSparkChartSplitter = $('#divStatsSparkChartSplitter')
        const divReport = $('#divStatsSparkchartReport')
        let graphCount = 0
        const graphViews = []
        let graphSource = null

        app.statistics.sourceMapping = []

        $('#divStatsFullContainer').css('background', app.ui.stats.entry.sparkChart.theme === 'dark' ? 'black' : 'white')

        // determine the "no views" visibility status
        let canRender = false

        app.ui.stats.entry.data.forEach(source => {
            if (source.data.enabled === true && source.data.views.length > 0) {
                canRender = true
            }
        })

        $('#spanStatsEmptySparkchart').css('display', canRender === true ? 'none' : 'block')

        // ********************************
        // reports
        // ********************************
        divReport.empty()

        app.ui.stats.entry.data.forEach(source => {
            if (source.data.enabled === true) {
                source.data.views.forEach(view => {
                    if (view.type === 'reportLine' && app.ui.stats.utils.mapObjToArray(view).length > 0) {
                        this.sections.reportLine(divReport, source, view)

                    } else if (view.type === 'graph' && app.ui.stats.utils.mapObjToArray(view).length > 0) {
                        graphCount++

                        graphViews.push(view)
                        graphSource = source
                        view.graphId = graphCount - 1

                        app.ui.stats.sparkChart.renderer.sections.graphs.graphData[graphCount - 1].source = source
                        app.ui.stats.sparkChart.renderer.sections.graphs.graphData[graphCount - 1].view = view
                    }
                })
            }
        })

        app.ui.regeneratePopups()

        // ********************************
        // graphs
        // ********************************
        let chart1 = app.ui.stats.sparkChart.renderer.sections.graphs.chart1
        let chart2 = app.ui.stats.sparkChart.renderer.sections.graphs.chart2

        //app.ui.stats.tab.resetGraphs()

        if (graphCount === 0) {
            // no graphs, hide pane
            divStatsSparkChartGraphsContainer.css('display', 'none')
            divStatsSparkChartSplitter.css('display', 'none')

            $('#divStatsSparkChartSparkChartContainer').css('height', 'calc(100vh - 233px)')


        } else {
            // graph, display panes and populate
            const defaultGraphHeight = app.ui.stats.entry.sparkChart.graphsConfig.height || 300
            const divStatsSparkChartGraph1 = $('#divStatsSparkChartGraph1')
            const divStatsSparkChartGraph2 = $('#divStatsSparkChartGraph2')
            const divStatsSparkChartGraphsContainer = $('#divStatsSparkChartGraphsContainer')

            if (divStatsSparkChartGraphsContainer.css('display') === 'none') {
                divStatsSparkChartGraphsContainer
                    .css('display', 'flex')
                    .css('height', defaultGraphHeight + 'px')
                divStatsSparkChartSplitter.css('display', 'block')
                // commented out in case we need the function again
                //app.ui.stats.onSplitterResize(null, {size: {height: defaultGraphHeight}})
            }

            if (graphCount === 1) {
                $('#divStatsSparkChartGraphsContainer').css('height', app.ui.stats.entry.sparkChart.graphsConfig.height)

                divStatsSparkChartGraph2.css('display', 'none')
                divStatsSparkChartGraph1
                    .css('width', '100%')
                    .css('height', '100%')

                $('#spanStatsGraph1Tools')
                    .css('right', '10px')
                    .css('left', '')

                try {
                    const el = document.getElementById('graphStatsSparkChartGraph1')
                    app.ui.stats.sparkChart.renderer.sections.graphs.chart1 = await app.ui.stats.sparkChart.renderer.sections.graphs.init(el, graphViews[0], undefined, 0)
                    app.ui.stats.sparkChart.renderer.sections.graphs.chart2 = null

                } catch (err) {
                    console.log(err)
                }

                $('#spanStatsGraph1Tools')
                    .css('left', '')
                    .css('top', '10px')

            } else {
                const graphsConfig = app.ui.stats.entry.sparkChart.graphsConfig

                $('#divStatsSparkChartGraphsContainer').css('height', app.ui.stats.entry.sparkChart.graphsConfig.height)

                if (graphsConfig.orientation === 'h') {
                    divStatsSparkChartGraph1
                        .css('width', '100%')
                        .css('height', '50%')

                    divStatsSparkChartGraph2
                        .css('display', 'block')
                        .css('width', '100%')
                        .css('height', '50%')

                    divStatsSparkChartGraphsContainer.css('display', 'block')

                    $('#spanStatsGraph1Tools')
                        .css('top', (divStatsSparkChartGraph1.height() + 13) + 'px')
                        .css('left', '')

                } else {
                    divStatsSparkChartGraph1
                        .css('width', '50%')
                        .css('height', '100%')

                    divStatsSparkChartGraph2
                        .css('display', 'block')
                        .css('width', '50%')
                        .css('height', '100%')

                    divStatsSparkChartGraphsContainer.css('display', 'flex')

                    $('#spanStatsGraph1Tools')
                        .css('left', (divStatsSparkChartGraph1.width() - 45) + 'px')
                        .css('top', '10px')
                }

                let el = document.getElementById('graphStatsSparkChartGraph1')
                app.ui.stats.sparkChart.renderer.sections.graphs.chart1 = await app.ui.stats.sparkChart.renderer.sections.graphs.init(el, graphViews[0], undefined, 0)

                el = document.getElementById('graphStatsSparkChartGraph2')
                app.ui.stats.sparkChart.renderer.sections.graphs.chart2 = await app.ui.stats.sparkChart.renderer.sections.graphs.init(el, graphViews[1], undefined, 1)
            }
        }
    },

    switchTheme: function () {
        let chartCount = 0

        app.ui.stats.entry.data.find(source => {
            source.data.views.forEach(view => {
                if (view.type === 'graph') {
                    // switch graph
                    app.ui.stats.sparkChart.renderer.sections.graphs.tools.theme.switch(app.ui.stats.sparkChart.renderer.sections.graphs['chart' + (chartCount + 1)], null, view)
                    chartCount++

                    this.render()

                } else {
                    // switch report line
                    this.setThemeOnReportLine(view.defaults)

                    app.ui.stats.sparkChart.renderer.render($('#divStatsSparkchart'))
                }
            })
        })
    },

    setThemeOnReportLine: function (target) {
        const settings = app.ui.stats.entry.sparkChart.theme === 'dark' ? app.userSettings.stats.sparkLines.sections.reportLineDark : app.userSettings.stats.sparkLines.sections.reportLine

        target.header.border.color = settings.header.border.color

        target.region.background = settings.region.background
        target.region.border.color = settings.region.border.color
        target.region.font.color = settings.region.font.color
        target.region.font.decoration.color = settings.region.font.decoration.color

        target.process.background = settings.process.background
        target.process.border.color = settings.process.border.color
        target.process.font.color = settings.process.font.color
        target.process.font.decoration.color = settings.process.font.decoration.color

        target.sample.background = settings.sample.background
        target.sample.border.color = settings.sample.border.color
        target.sample.font.color = settings.sample.font.color
        target.sample.font.decoration.color = settings.sample.font.decoration.color

        target.sampleMap.background = settings.sampleMap.background
        target.sampleMap.border.color = settings.sampleMap.border.color
        target.sampleMap.font.color = settings.sampleMap.font.color
        target.sampleMap.font.decoration.color = settings.sampleMap.font.decoration.color

        target.value.background = settings.value.background
        target.value.border.color = settings.value.border.color
        target.value.font.color = settings.value.font.color
        target.value.font.decoration.color = settings.value.font.decoration.color

        target.noValue.background = settings.noValue.background
        target.noValue.border.color = settings.noValue.border.color
    },

    update: function (data) {
        app.statistics.sourceMapping.forEach(entry => {
            if (entry.section === 'reportLine') {
                switch (entry.type) {
                    /*
                    case 'region': {
                        const sourceSplit = entry.source.split('|')
                        const sourceId = sourceSplit[0]
                        const regionIx = sourceSplit[2]

                        const dataSet = app.statistics.data.find(source => source.id === sourceId)


                        if (dataSet && dataSet.regions) {
                            const region = Object.keys(dataSet.regions)[regionIx]

                            $('#' + entry.target).text('Region: ' + region)
                        }

                        break
                    }
                     */

                    case 'process': {
                        const sourceSplit = entry.source.split('|')
                        const sourceId = sourceSplit[0]
                        const regionIx = sourceSplit[2]
                        const processIx = sourceSplit[3]

                        const dataSet = app.statistics.data.find(source => source.id === sourceId)
                        if (dataSet && dataSet.regions) {
                            const region = Object.keys(dataSet.regions)[regionIx]
                            const process = dataSet.regions[region][processIx][0].pid

                            $('#' + entry.target).text(process)
                        }

                        break
                    }

                    case 'data': {
                        const sourceSplit = entry.source.split('|')
                        const sourceId = sourceSplit[0]
                        const source = app.ui.stats.entry.data.find(source => source.id === sourceId)
                        const viewId = sourceSplit[1]
                        const view = source.data.views.find(view => view.id === viewId)
                        const regionIx = sourceSplit[2]
                        const processIx = sourceSplit[3]
                        const sample = sourceSplit[4]
                        const sampleMap = sourceSplit[5]

                        const dataSet = app.statistics.data.find(source => source.id === sourceId)
                        const region = Object.keys(dataSet.regions)[regionIx]

                        if (dataSet && dataSet.regions) {
                            const data = dataSet.regions[region][processIx].find(entry => entry.sample === sample)

                            if (data[sampleMap] >= 0) {
                                const rawValue = data[sampleMap]
                                const value = rawValue === 0 ? 0 : app.ui.formatThousands(data[sampleMap])
                                const target = $('#' + entry.target)

                                target.text(value)

                                // highlight with color if needed
                                if (entry.highlight.type === 'range') {
                                    let color

                                    if (data[sampleMap] > parseInt(entry.highlight.range.red)) color = view.defaults.highlighters.high
                                    else if (data[sampleMap] > parseInt(entry.highlight.range.yellow)) color = view.defaults.highlighters.mid
                                    else color = view.defaults.highlighters.low

                                    target
                                        .parent().css('background', color)
                                        .css('color', 'white')

                                } else if (entry.highlight.type === 'max') {
                                    // loop through the processes and find the top
                                    let topArray = []
                                    for (let ix = 0; ix < dataSet.regions[region].length; ix++) {
                                        const data = dataSet.regions[region][ix].find(entry => entry.sample === sample)
                                        topArray.push({
                                            ix: ix,
                                            data: data[sampleMap]
                                        })
                                    }

                                    // sort it
                                    topArray = topArray.sort((a, b) => {
                                        if (a.data > b.data) return 1
                                        if (a.data < b.data) return -1
                                        return 0
                                    })

                                    // and  check if top index = current index
                                    if (topArray[topArray.length - 1].ix === parseInt(processIx)) {
                                        target
                                            .parent().css('background', view.defaults.highlighters.topProcess)
                                            .css('color', 'white')

                                    } else {
                                        if (target.parent().css('background') !== view.defaults.highlighters.topProcess) {
                                            const values = view.defaults.value

                                            target
                                                .parent().css('background', values.background)
                                                .css('color', values.font.color)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } else {
                // graph processIxs
                const sourceSplit = entry.source.split('|')
                const sourceId = sourceSplit[0]
                const region = sourceSplit[2]
                let chart

                if (entry.target === 0) chart = app.ui.stats.sparkChart.renderer.sections.graphs.chart1
                else chart = app.ui.stats.sparkChart.renderer.sections.graphs.chart2

                const dataSet = app.statistics.data.find(source => source.id === sourceId)
                if (dataSet && dataSet.regions) {
                    const samplesCount = chart.data.datasets.length / dataSet.regions[region].length
                    for (let sampleIx = 0; sampleIx < samplesCount; sampleIx++) {
                        dataSet.regions[region].forEach((processData, processIx) => {
                            chart.data.datasets[processIx].label = processData[0].pid + ' ' + entry.data[processIx]
                        })
                        chart.update()
                    }
                }
            }
        })
    }
}
