/*
#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This file contains all the settings displayed in the Preferences dialog (as custom entries)

app.ui.stats.manifest = {
    reportLine: [
        {
            name: 'Header',
            children: [
                {
                    name: 'Border',
                    items: [
                        {
                            field: 'thickness',
                            display: 'Thickness',
                            description: 'The thickness of the border',

                            path: 'header/border/thickness',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0,
                            max: 10,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the border',

                            path: 'header/border/color',
                            hive: 'defaults',

                            type: 'color',
                        }
                    ]
                }
            ]
        },
        {
            name: 'Region',
            children: [
                {
                    name: 'Border',
                    items: [
                        {
                            field: 'thickness',
                            display: 'Thickness',
                            description: 'The thickness of the border',

                            path: 'region/border/thickness',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0,
                            max: 10,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the border',

                            path: 'region/border/color',
                            hive: 'stats',

                            type: 'color',
                        }
                    ]
                },
                {
                    name: 'Font',
                    children: [
                        {
                            name: 'Decoration',
                            items: [
                                {
                                    field: 'type',
                                    display: 'Type',
                                    description: 'The font decoration type',

                                    path: 'region/font/decoration/type',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Underline', value: 'underline'},
                                    ]
                                },
                                {
                                    field: 'style',
                                    display: 'Style',
                                    description: 'The font decoration style',

                                    path: 'region/font/decoration/style',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Solid', value: 'solid'},
                                        {text: 'Wavy', value: 'wavy'},
                                        {text: 'Dotted', value: 'dotted'},
                                        {text: 'Dashed', value: 'dashed'},
                                        {text: 'Double', value: 'double'},
                                    ]
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the font decoration',

                                    path: 'region/font/decoration/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                                {
                                    field: 'thickness',
                                    display: 'Thickness',
                                    description: 'The thickness of the font decoration',

                                    path: 'region/font/decoration/thickness',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 0,
                                    max: 10,
                                    step: 1,
                                    unit: 'px',
                                },

                            ]
                        }
                    ],
                    items: [
                        {
                            field: 'size',
                            display: 'Size',
                            description: 'The size of the font',

                            path: 'region/font/size',
                            hive: 'stats',

                            type: 'numeric',
                            min: 6,
                            max: 100,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the font',

                            path: 'region/font/color',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'fontFamily',
                            display: 'Family',
                            description: 'The font family',

                            path: 'region/font/fontFamily',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Inconsolata', value: 'Inconsolata'},
                                {text: 'FiraGO', value: 'FiraGO'}
                            ]
                        },
                        {
                            field: 'weight',
                            display: 'Weight',
                            description: 'The font weight',

                            path: 'region/font/weight',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Bold', value: 'bold'}
                            ]
                        },
                        {
                            field: 'style',
                            display: 'Style',
                            description: 'The font style',

                            path: 'region/font/style',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Italic', value: 'italic'},
                                {text: 'Oblique', value: 'oblique'},
                            ]
                        },
                    ]
                }
            ],
            items: [
                {
                    field: 'text',
                    display: 'Text',
                    description: 'The text of the region',

                    path: 'region/text',
                    hive: 'stats',

                    type: 'text',
                },
                {
                    field: 'color',
                    display: 'Background color',
                    description: 'The background color of the region',

                    path: 'region/background',
                    hive: 'stats',

                    type: 'color',
                },
                {
                    field: 'align',
                    display: 'Align',
                    description: 'The alignment of the region',

                    path: 'region/align',
                    hive: 'stats',

                    type: 'select',
                    options: [
                        {text: 'Left', value: 'left'},
                        {text: 'Center', value: 'center'},
                        {text: 'Right', value: 'right'},
                    ]
                },
            ]
        },
        {
            name: 'Process',
            children: [
                {
                    name: 'Border',
                    items: [

                        {
                            field: 'thickness',
                            display: 'Thickness',
                            description: 'The thickness of the border',

                            path: 'process/border/thickness',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0,
                            max: 10,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the border',

                            path: 'process/border/color',
                            hive: 'stats',

                            type: 'color',
                        }
                    ]
                },
                {
                    name: 'Font',
                    children: [
                        {
                            name: 'Decoration',
                            items: [
                                {
                                    field: 'type',
                                    display: 'Type',
                                    description: 'The font decoration type',

                                    path: 'process/font/decoration/type',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Underline', value: 'underline'},
                                    ]
                                },
                                {
                                    field: 'style',
                                    display: 'Style',
                                    description: 'The font decoration style',

                                    path: 'process/font/decoration/style',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Solid', value: 'solid'},
                                        {text: 'Wavy', value: 'wavy'},
                                        {text: 'Dotted', value: 'dotted'},
                                        {text: 'Dashed', value: 'dashed'},
                                        {text: 'Double', value: 'double'},
                                    ]
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the font decoration',

                                    path: 'process/font/decoration/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                                {
                                    field: 'thickness',
                                    display: 'Thickness',
                                    description: 'The thickness of the font decoration',

                                    path: 'process/font/decoration/thickness',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 0,
                                    max: 10,
                                    step: 1,
                                    unit: 'px',
                                },

                            ]
                        }
                    ],
                    items: [
                        {
                            field: 'size',
                            display: 'Size',
                            description: 'The size of the font',

                            path: 'process/font/size',
                            hive: 'stats',

                            type: 'numeric',
                            min: 6,
                            max: 100,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the font',

                            path: 'process/font/color',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'fontFamily',
                            display: 'Family',
                            description: 'The font family',

                            path: 'process/font/fontFamily',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Inconsolata', value: 'Inconsolata'},
                                {text: 'FiraGO', value: 'FiraGO'}
                            ]
                        },
                        {
                            field: 'weight',
                            display: 'Weight',
                            description: 'The font weight',

                            path: 'process/font/weight',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Bold', value: 'bold'}
                            ]
                        },
                        {
                            field: 'style',
                            display: 'Style',
                            description: 'The font style',

                            path: 'process/font/style',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Italic', value: 'italic'},
                                {text: 'Oblique', value: 'oblique'},
                            ]
                        },
                    ]
                }
            ],
            items: [
                {
                    field: 'show',
                    display: 'Show',
                    description: 'If display the processes or not',

                    path: 'process/show',
                    hive: 'stats',

                    type: 'boolean',
                }, {
                    field: 'color',
                    display: 'Background color',
                    description: 'The background color of the process',

                    path: 'process/background',
                    hive: 'stats',

                    type: 'color',
                },
                {
                    field: 'align',
                    display: 'Align',
                    description: 'The alignment of the process',

                    path: 'process/align',
                    hive: 'stats',

                    type: 'select',
                    options: [
                        {text: 'Left', value: 'left'},
                        {text: 'Center', value: 'center'},
                        {text: 'Right', value: 'right'},
                    ]
                },
            ]
        },
        {
            name: 'Sample name',
            children: [
                {
                    name: 'Border',
                    items: [
                        {
                            field: 'thickness',
                            display: 'Thickness',
                            description: 'The thickness of the border',

                            path: 'sample/border/thickness',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0,
                            max: 10,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the border',

                            path: 'sample/border/color',
                            hive: 'stats',

                            type: 'color',
                        }
                    ]
                },
                {
                    name: 'Font',
                    children: [
                        {
                            name: 'Decoration',
                            items: [
                                {
                                    field: 'type',
                                    display: 'Type',
                                    description: 'The font decoration type',

                                    path: 'sample/font/decoration/type',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Underline', value: 'underline'},
                                    ]
                                },
                                {
                                    field: 'style',
                                    display: 'Style',
                                    description: 'The font decoration style',

                                    path: 'sample/font/decoration/style',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Solid', value: 'solid'},
                                        {text: 'Wavy', value: 'wavy'},
                                        {text: 'Dotted', value: 'dotted'},
                                        {text: 'Dashed', value: 'dashed'},
                                        {text: 'Double', value: 'double'},
                                    ]
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the font decoration',

                                    path: 'sample/font/decoration/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                                {
                                    field: 'thickness',
                                    display: 'Thickness',
                                    description: 'The thickness of the font decoration',

                                    path: 'sample/font/decoration/thickness',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 0,
                                    max: 10,
                                    step: 1,
                                    unit: 'px',
                                },

                            ]
                        }
                    ],
                    items: [
                        {
                            field: 'size',
                            display: 'Size',
                            description: 'The size of the font',

                            path: 'sample/font/size',
                            hive: 'stats',

                            type: 'numeric',
                            min: 6,
                            max: 100,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the font',

                            path: 'sample/font/color',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'fontFamily',
                            display: 'Family',
                            description: 'The font family',

                            path: 'sample/font/fontFamily',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Inconsolata', value: 'Inconsolata'},
                                {text: 'FiraGO', value: 'FiraGO'}
                            ]
                        },
                        {
                            field: 'weight',
                            display: 'Weight',
                            description: 'The font weight',

                            path: 'sample/font/weight',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Bold', value: 'bold'}
                            ]
                        },
                        {
                            field: 'style',
                            display: 'Style',
                            description: 'The font style',

                            path: 'sample/font/style',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Italic', value: 'italic'},
                                {text: 'Oblique', value: 'oblique'},
                            ]
                        },
                    ]
                }
            ],
            items: [
                {
                    field: 'color',
                    display: 'Background color',
                    description: 'The background color of the sample name',

                    path: 'sample/background',
                    hive: 'stats',

                    type: 'color',
                },
                {
                    field: 'align',
                    display: 'Align',
                    description: 'The alignment of the sample name',

                    path: 'sample/align',
                    hive: 'stats',

                    type: 'select',
                    options: [
                        {text: 'Left', value: 'left'},
                        {text: 'Center', value: 'center'},
                        {text: 'Right', value: 'right'},
                    ]
                },
            ]
        },
        {
            name: 'Sample type',
            children: [
                {
                    name: 'Border',
                    items: [
                        {
                            field: 'thickness',
                            display: 'Thickness',
                            description: 'The thickness of the border',

                            path: 'sampleMap/border/thickness',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0,
                            max: 10,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the border',

                            path: 'sampleMap/border/color',
                            hive: 'stats',

                            type: 'color',
                        }
                    ]
                },
                {
                    name: 'Font',
                    children: [
                        {
                            name: 'Decoration',
                            items: [
                                {
                                    field: 'type',
                                    display: 'Type',
                                    description: 'The font decoration type',

                                    path: 'sampleMap/font/decoration/type',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Underline', value: 'underline'},
                                    ]
                                },
                                {
                                    field: 'style',
                                    display: 'Style',
                                    description: 'The font decoration style',

                                    path: 'sampleMap/font/decoration/style',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Solid', value: 'solid'},
                                        {text: 'Wavy', value: 'wavy'},
                                        {text: 'Dotted', value: 'dotted'},
                                        {text: 'Dashed', value: 'dashed'},
                                        {text: 'Double', value: 'double'},
                                    ]
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the font decoration',

                                    path: 'sampleMap/font/decoration/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                                {
                                    field: 'thickness',
                                    display: 'Thickness',
                                    description: 'The thickness of the font decoration',

                                    path: 'sampleMap/font/decoration/thickness',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 0,
                                    max: 10,
                                    step: 1,
                                    unit: 'px',
                                },

                            ]
                        }
                    ],
                    items: [
                        {
                            field: 'size',
                            display: 'Size',
                            description: 'The size of the font',

                            path: 'sampleMap/font/size',
                            hive: 'stats',

                            type: 'numeric',
                            min: 6,
                            max: 100,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the font',

                            path: 'sampleMap/font/color',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'fontFamily',
                            display: 'Family',
                            description: 'The font family',

                            path: 'sampleMap/font/fontFamily',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Inconsolata', value: 'Inconsolata'},
                                {text: 'FiraGO', value: 'FiraGO'}
                            ]
                        },
                        {
                            field: 'weight',
                            display: 'Weight',
                            description: 'The font weight',

                            path: 'sampleMap/font/weight',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Bold', value: 'bold'}
                            ]
                        },
                        {
                            field: 'style',
                            display: 'Style',
                            description: 'The font style',

                            path: 'sampleMap/font/style',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Italic', value: 'italic'},
                                {text: 'Oblique', value: 'oblique'},
                            ]
                        },
                    ]
                }
            ],
            items: [
                {
                    field: 'color',
                    display: 'Background color',
                    description: 'The background color of the sample type',

                    path: 'sampleMap/background',
                    hive: 'stats',

                    type: 'color',
                },
                {
                    field: 'align',
                    display: 'Align',
                    description: 'The alignment of the sample type',

                    path: 'sampleMap/align',
                    hive: 'stats',

                    type: 'select',
                    options: [
                        {text: 'Left', value: 'left'},
                        {text: 'Center', value: 'center'},
                        {text: 'Right', value: 'right'},
                    ]
                },
            ]
        },
        {
            name: 'Stat value',
            children: [
                {
                    name: 'Border',
                    items: [
                        {
                            field: 'thickness',
                            display: 'Thickness',
                            description: 'The thickness of the border',

                            path: 'value/border/thickness',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0,
                            max: 10,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the border',

                            path: 'value/border/color',
                            hive: 'stats',

                            type: 'color',
                        }
                    ]
                },
                {
                    name: 'Font',
                    children: [
                        {
                            name: 'Decoration',
                            items: [
                                {
                                    field: 'type',
                                    display: 'Type',
                                    description: 'The font decoration type',

                                    path: 'value/font/decoration/type',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Underline', value: 'underline'},
                                    ]
                                },
                                {
                                    field: 'style',
                                    display: 'Style',
                                    description: 'The font decoration style',

                                    path: 'value/font/decoration/style',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Solid', value: 'solid'},
                                        {text: 'Wavy', value: 'wavy'},
                                        {text: 'Dotted', value: 'dotted'},
                                        {text: 'Dashed', value: 'dashed'},
                                        {text: 'Double', value: 'double'},
                                    ]
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the font decoration',

                                    path: 'value/font/decoration/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                                {
                                    field: 'thickness',
                                    display: 'Thickness',
                                    description: 'The thickness of the font decoration',

                                    path: 'value/font/decoration/thickness',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 0,
                                    max: 10,
                                    step: 1,
                                    unit: 'px',
                                },

                            ]
                        }
                    ],
                    items: [
                        {
                            field: 'size',
                            display: 'Size',
                            description: 'The size of the font',

                            path: 'value/font/size',
                            hive: 'stats',

                            type: 'numeric',
                            min: 6,
                            max: 100,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the font',

                            path: 'value/font/color',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'fontFamily',
                            display: 'Family',
                            description: 'The font family',

                            path: 'value/font/fontFamily',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Inconsolata', value: 'Inconsolata'},
                                {text: 'FiraGO', value: 'FiraGO'}
                            ]
                        },
                        {
                            field: 'weight',
                            display: 'Weight',
                            description: 'The font weight',

                            path: 'value/font/weight',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Bold', value: 'bold'}
                            ]
                        },
                        {
                            field: 'style',
                            display: 'Style',
                            description: 'The font style',

                            path: 'value/font/style',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Italic', value: 'italic'},
                                {text: 'Oblique', value: 'oblique'},
                            ]
                        },
                    ]
                }
            ],
            items: [
                {
                    field: 'color',
                    display: 'Background color',
                    description: 'The background color of the stat value',

                    path: 'value/background',
                    hive: 'stats',

                    type: 'color',
                },
                {
                    field: 'align',
                    display: 'Align',
                    description: 'The alignment of the stat value',

                    path: 'value/align',
                    hive: 'stats',

                    type: 'select',
                    options: [
                        {text: 'Left', value: 'left'},
                        {text: 'Center', value: 'center'},
                        {text: 'Right', value: 'right'},
                    ]
                },
            ]
        },
        {
            name: 'No value (empty cell)',
            children: [
                {
                    name: 'Border',
                    items: [
                        {
                            field: 'thickness',
                            display: 'Thickness',
                            description: 'The thickness of the border',

                            path: 'noValue/border/thickness',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0,
                            max: 10,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the border',

                            path: 'noValue/border/color',
                            hive: 'stats',

                            type: 'color',
                        }
                    ]
                },
            ],
            items: [
                {
                    field: 'color',
                    display: 'Background color',
                    description: 'The background color of the empty cell',

                    path: 'noValue/background',
                    hive: 'stats',

                    type: 'color',
                },
            ]
        },
        {
            name: 'Highlighters',
            items: [
                {
                    field: 'low',
                    display: 'Range: Low',
                    description: 'The background color of the lowest range',

                    path: 'highlighters/low',
                    hive: 'stats',

                    type: 'color',
                },
                {
                    field: 'mid',
                    display: 'Range: Mid',
                    description: 'The background color of the middle range',

                    path: 'highlighters/mid',
                    hive: 'stats',

                    type: 'color',
                },
                {
                    field: 'high',
                    display: 'Range: High',
                    description: 'The background color of the highest range',

                    path: 'highlighters/high',
                    hive: 'stats',

                    type: 'color',
                },
                {
                    field: 'topProcess',
                    display: 'Top process',
                    description: 'The background color of the Top Process',

                    path: 'highlighters/topProcess',
                    hive: 'stats',

                    type: 'color',
                },
            ]
        },

    ],

    graph: {
        series: {
            line: [
                {
                    name: 'Stroke',
                    items: [
                        {
                            field: 'type',
                            display: 'Type',
                            description: 'The line type',

                            path: 'stroke/type',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Smooth', value: 'smooth'},
                                {text: 'Straight', value: 'straight'},
                                {text: 'Step Line', value: 'stepline'},
                            ]
                        },
                        {
                            field: 'smoothTension',
                            display: 'Smooth tension',
                            description: 'The tension of the interpolator',

                            path: 'stroke/smoothTension',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0.1,
                            max: 0.8,
                            step: 0.1,
                            unit: '',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the line',

                            path: 'stroke/color',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'width',
                            display: 'Width',
                            description: 'The width of the line',

                            path: 'stroke/width',
                            hive: 'stats',

                            type: 'numeric',
                            min: 1,
                            max: 10,
                            step: 1,
                            unit: '',
                        },
                        {
                            field: 'dashArray',
                            display: 'Dashing',
                            description: 'The optional dash spacing of the line',

                            path: 'stroke/dashArray',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0,
                            max: 10,
                            step: 1,
                            unit: '',
                        },
                    ]
                },
                {
                    name: 'Marking',
                    children: [
                        {
                            name: 'Markers',
                            items: [
                                {
                                    field: 'shape',
                                    display: 'Shape',
                                    description: 'The shape of the marker',

                                    path: 'marking/markers/shape',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Circle', value: 'circle'},
                                        {text: 'Square', value: 'rect'},
                                        {text: 'Rounded square', value: 'rectRounded'},
                                        {text: 'Cross', value: 'cross'},
                                        {text: 'Star', value: 'star'},
                                        {text: 'Triangle', value: 'triangle'},
                                    ]
                                },
                                {
                                    field: 'size',
                                    display: 'Size',
                                    description: 'The size of the marker',

                                    path: 'marking/markers/size',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 0,
                                    max: 20,
                                    step: 1,
                                    unit: '',
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the marking',

                                    path: 'marking/markers/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                            ]
                        },
                    ],
                    items: [
                        {
                            field: 'type',
                            display: 'Type',
                            description: 'A Marker will highlight the data point.',

                            path: 'marking/type',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'None', value: 'none'},
                                {text: 'Markers', value: 'markers'},
                            ]
                        },

                    ]
                }
            ],
            area: [
                {
                    name: 'Stroke',
                    items: [
                        {
                            field: 'type',
                            display: 'Type',
                            description: 'The line type',

                            path: 'stroke/type',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Smooth', value: 'smooth'},
                                {text: 'Straight', value: 'straight'},
                                {text: 'Step Line', value: 'stepline'},
                            ]
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the line',

                            path: 'stroke/color',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'width',
                            display: 'Width',
                            description: 'The width of the line',

                            path: 'stroke/width',
                            hive: 'stats',

                            type: 'numeric',
                            min: 1,
                            max: 10,
                            step: 1,
                            unit: '',
                        },
                        {
                            field: 'dashArray',
                            display: 'Dashing',
                            description: 'The optional dash spacing of the line',

                            path: 'stroke/dashArray',
                            hive: 'stats',

                            type: 'numeric',
                            min: 0,
                            max: 10,
                            step: 1,
                            unit: '',
                        },
                    ]
                },
                {
                    name: 'Marking',
                    children: [
                        {
                            name: 'Markers',
                            items: [
                                {
                                    field: 'shape',
                                    display: 'Shape',
                                    description: 'The shape of the marker',

                                    path: 'marking/markers/shape',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Circle', value: 'circle'},
                                        {text: 'Square', value: 'square'},
                                    ]
                                },
                                {
                                    field: 'size',
                                    display: 'Size',
                                    description: 'The size of the marker',

                                    path: 'marking/markers/size',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 0,
                                    max: 20,
                                    step: 1,
                                    unit: '',
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the marking',

                                    path: 'marking/markers/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                            ]
                        },
                        {
                            name: 'Data labels',
                            items: [
                                {
                                    field: 'size',
                                    display: 'Size',
                                    description: 'The size of the font',

                                    path: 'marking/dataLabels/size',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 100,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the font',

                                    path: 'marking/dataLabels/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'marking/dataLabels/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                                {
                                    field: 'weight',
                                    display: 'Weight',
                                    description: 'The font weight',

                                    path: 'marking/dataLabels/weight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },
                            ],
                        },
                    ],
                    items: [
                        {
                            field: 'type',
                            display: 'Type',
                            description: 'A Marker will highlight the data point.<br> A Data label will also display the value.',

                            path: 'marking/type',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'None', value: 'none'},
                                {text: 'Markers', value: 'markers'},
                                {text: 'Data labels', value: 'dataLabels'},
                            ]
                        },

                    ]
                },
                {
                    name: 'Fill',
                    items: [
                        {
                            field: 'type',
                            display: 'Type',
                            description: 'A gradient fill will apply a gardient fill to the area',

                            path: 'fill/type',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Solid', value: 'solid'},
                                {text: 'Gradient', value: 'gradient'},
                            ]
                        },
                    ],
                    children: [
                        {
                            name: 'Gradient',
                            items: [
                                {
                                    field: 'type',
                                    display: 'Type',
                                    description: 'The gradient direction',

                                    path: 'fill/gradient/type',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Vertical', value: 'vertical'},
                                        {text: 'Horizontal', value: 'horizontal'},
                                        {text: 'Diagonal1', value: 'diagonal1'},
                                        {text: 'Diagonal2', value: 'diagonal2'},
                                    ]
                                },
                                {
                                    field: 'shade',
                                    display: 'Shade',
                                    description: 'The gradient shade',

                                    path: 'fill/gradient/shade',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Light', value: 'light'},
                                        {text: 'Dark', value: 'dark'},
                                    ]
                                },
                                {
                                    field: 'opacityFrom',
                                    display: 'Opacity From',
                                    description: 'The start transparency of the gradient',

                                    path: 'fill/gradient/opacityFrom',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 0,
                                    max: 1,
                                    step: .1,
                                },
                                {
                                    field: 'opacityTo',
                                    display: 'Opacity to',
                                    description: 'The end transparency of the gradient',

                                    path: 'fill/gradient/opacityTo',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 0,
                                    max: 1,
                                    step: .1,
                                },
                            ]
                        }
                    ]
                }
            ],
            yAxis: [
                {
                    name: 'Y axis',
                    children: [
                        {
                            name: 'Labels',
                            items: [
                                {
                                    field: 'text',
                                    display: 'Text',
                                    description: 'The labels text.',

                                    path: 'labels/text',
                                    hive: 'stats',

                                    type: 'text',
                                },
                                {
                                    field: 'fontSize',
                                    display: 'Size',
                                    description: 'The size of the font',

                                    path: 'labels/fontSize',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 100,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'labels/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                                {
                                    field: 'fontWeight',
                                    display: 'Weight',
                                    description: 'The font weight',

                                    path: 'labels/fontWeight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },
                            ]
                        },
                        {
                            name: 'Title',
                            items: [
                                {
                                    field: 'text',
                                    display: 'Text',
                                    description: 'The title text.',

                                    path: 'title/text',
                                    hive: 'stats',

                                    type: 'text',
                                },
                                {
                                    field: 'fontSize',
                                    display: 'Size',
                                    description: 'The size of the font',

                                    path: 'title/fontSize',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 100,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'title/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                                {
                                    field: 'fontWeight',
                                    display: 'Weight',
                                    description: 'The font weight',

                                    path: 'title/fontWeight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },
                            ]
                        }
                    ]
                }
            ]
        },
        chart: {
            globalSettings: [
                {
                    name: 'Global settings',
                    items: [
                        {
                            field: 'background',
                            display: 'Background color',
                            description: 'The background color of the chart',

                            path: 'background',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'color',
                            display: 'Foreground color',
                            description: 'The foreground color of the chart',

                            path: 'color',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'fontFamily',
                            display: 'Font family',
                            description: 'The font family used in the chart. This value can be overridden for specific items.',

                            path: 'fontFamily',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Inconsolata', value: 'Inconsolata'},
                                {text: 'FiraGO', value: 'FiraGO'}
                            ]
                        },
                    ]
                },
            ],
            title: [
                {
                    name: 'Title',
                    items: [
                        {
                            field: 'display',
                            display: 'Display',
                            description: 'If display the title or not',

                            path: 'display',
                            hive: 'stats',

                            type: 'boolean',
                        },
                        {
                            field: 'text',
                            display: 'Text',
                            description: 'The title text<br>On a multi-region source, it will be automatically filled with the selected region instead, but it can be overridden by changing the title in the editor.',

                            path: 'text',
                            hive: 'stats',

                            type: 'text',
                        },
                        {
                            field: 'align',
                            display: 'Text alignment',
                            description: 'The alignment of the text',

                            path: 'align',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Start', value: 'start'},
                                {text: 'Center', value: 'center'},
                                {text: 'End', value: 'end'},
                            ]
                        },
                        {
                            field: 'position',
                            display: 'Text position',
                            description: 'The position of the text',

                            path: 'position',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Top', value: 'top'},
                                {text: 'Left', value: 'left'},
                                {text: 'Bottom', value: 'bottom'},
                                {text: 'Right', value: 'right'},
                            ]
                        },
                    ],
                    children: [
                        {
                            name: 'Style',
                            items: [
                                {
                                    field: 'size',
                                    display: 'Size',
                                    description: 'The size of the font',

                                    path: 'style/size',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 100,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the font',

                                    path: 'style/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'style/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                                {
                                    field: 'weight',
                                    display: 'Weight',
                                    description: 'The font weight',

                                    path: 'style/weight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },

                            ]
                        }
                    ]
                },
            ],
            subTitle: [
                {
                    name: 'Sub title',
                    items: [
                        {
                            field: 'display',
                            display: 'Display',
                            description: 'If display the title or not',

                            path: 'display',
                            hive: 'stats',

                            type: 'boolean',
                        },
                        {
                            field: 'text',
                            display: 'Text',
                            description: 'The sub title text<br>It is populated with the process mode, but it can be overridden by changing the title in the editor.',

                            path: 'text',
                            hive: 'stats',

                            type: 'text',
                        },
                        {
                            field: 'align',
                            display: 'Text alignment',
                            description: 'The alignment of the text',

                            path: 'align',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Start', value: 'start'},
                                {text: 'Center', value: 'center'},
                                {text: 'End', value: 'end'},
                            ]
                        },
                        {
                            field: 'position',
                            display: 'Text position',
                            description: 'The position of the text',

                            path: 'position',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Top', value: 'top'},
                                {text: 'Left', value: 'left'},
                                {text: 'Bottom', value: 'bottom'},
                                {text: 'Right', value: 'right'},
                            ]
                        },
                    ],
                    children: [
                        {
                            name: 'Style',
                            items: [
                                {
                                    field: 'size',
                                    display: 'Size',
                                    description: 'The size of the font',

                                    path: 'style/size',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 100,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'The color of the font',

                                    path: 'style/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'style/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                                {
                                    field: 'weight',
                                    display: 'Weight',
                                    description: 'The font weight',

                                    path: 'style/weight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },

                            ]
                        }
                    ]
                },
            ],
            legend: [
                {
                    name: 'Legend',
                    items: [
                        {
                            field: 'display',
                            display: 'Display',
                            description: 'Whether to show or hide the legend container',

                            path: 'display',
                            hive: 'stats',

                            type: 'boolean',
                        },
                        {
                            field: 'position',
                            display: 'Position',
                            description: 'The position of the legend',

                            path: 'position',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Top', value: 'top'},
                                {text: 'Bottom', value: 'bottom'},
                                {text: 'Left', value: 'left'},
                                {text: 'Right', value: 'right'},
                            ]
                        },
                        {
                            field: 'hAlign',
                            display: 'H align',
                            description: 'The alignment of the text',

                            path: 'hAlign',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Start', value: 'start'},
                                {text: 'Center', value: 'center'},
                                {text: 'End', value: 'end'},
                            ]
                        },

                    ],
                    children: [
                        {
                            name: 'Font',
                            items: [
                                {
                                    field: 'size',
                                    display: 'Size',
                                    description: 'The size of the font',

                                    path: 'font/size',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 100,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'font/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                                {
                                    field: 'weight',
                                    display: 'Weight',
                                    description: 'The font weight',

                                    path: 'font/weight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },
                            ]
                        },
                    ]
                },
            ],
            grid: [
                {
                    name: 'Grid',
                    items: [
                        {
                            field: 'xAxisLines',
                            display: 'X Axis Lines',
                            description: 'Whether to show / hide x-axis lines',

                            path: 'xAxisLines',
                            hive: 'stats',

                            type: 'boolean',
                        },
                        {
                            field: 'yAxisLines',
                            display: 'Y Axis Lines',
                            description: 'Whether to show / hide y-axis lines',

                            path: 'yAxisLines',
                            hive: 'stats',

                            type: 'boolean',
                        },
                    ],
                    children: [
                        {
                            name: 'Row',
                            items: [
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'Grid background colors filling in row pattern. It will alternate with "transparent"',

                                    path: 'row/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                            ]
                        },
                        {
                            name: 'Column',
                            items: [
                                {
                                    field: 'color',
                                    display: 'Color',
                                    description: 'Grid background colors filling in row pattern. It will alternate with "transparent"',

                                    path: 'column/color',
                                    hive: 'stats',

                                    type: 'color',
                                },
                            ]
                        }
                    ]
                },
            ],
            xaxis: [
                {
                    name: 'Timescale',
                    items: [
                        {
                            field: 'show',
                            display: 'Show',
                            description: 'If display the labels or not',

                            path: 'labels/show',
                            hive: 'stats',

                            type: 'boolean',
                        },

                        {
                            field: 'colors',
                            display: 'Color',
                            description: 'The color of the labels',

                            path: 'labels/colors',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'fontSize',
                            display: 'Font size',
                            description: 'The size of the font',

                            path: 'labels/fontSize',
                            hive: 'stats',

                            type: 'numeric',
                            min: 6,
                            max: 100,
                            step: 1,
                            unit: 'px',
                        },
                        {
                            field: 'fontFamily',
                            display: 'Family',
                            description: 'The font family',

                            path: 'labels/fontFamily',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Inconsolata', value: 'Inconsolata'},
                                {text: 'FiraGO', value: 'FiraGO'}
                            ]
                        },
                        {
                            field: 'fontWeight',
                            display: 'Font weight',
                            description: 'The font weight',

                            path: 'labels/fontWeight',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Normal', value: 'normal'},
                                {text: 'Bold', value: 'bold'}
                            ]
                        },

                    ]
                },
            ],
            tooltips: [
                {
                    name: 'Tooltips',
                    items: [
                        {
                            field: 'enabled',
                            display: 'Enabled',
                            description: 'Whether to show / hide the tooltips on hover',

                            path: 'enabled',
                            hive: 'stats',

                            type: 'boolean',
                        },
                    ]
                }
            ],
            yAxisCommon: [
                {
                    name: 'Y axis',
                    items: [
                        {
                            field: 'common',
                            display: 'Shared across series',
                            description: 'Set it to true to share the y-axis across all the series',

                            path: 'common',
                            hive: 'common',

                            type: 'boolean',
                        },
                        {
                            field: 'show',
                            display: 'Show',
                            description: 'Set it to true to display the y axis',

                            path: 'defaultsCommon/show',
                            hive: 'stats',

                            type: 'boolean',
                        },
                        {
                            field: 'color',
                            display: 'Color',
                            description: 'The color of the border',

                            path: 'defaultsCommon/color',
                            hive: 'stats',

                            type: 'color',
                        },
                        {
                            field: 'type',
                            display: 'Linear or Logarithmic',
                            description: 'Choose between a linear or logarithmic scale',

                            path: 'defaultsCommon/type',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Linear', value: 'linear'},
                                {text: 'Logarithmic', value: 'logarithmic'}
                            ]
                        },
                        {
                            field: 'useManualSetting',
                            display: 'Manually force the scaling',
                            description: 'When set to true it will use the min and max value to set the scale',

                            path: 'defaultsCommon/useManualSetting',
                            hive: 'stats',

                            type: 'boolean',
                        },
                        {
                            field: 'min',
                            display: 'Minimum value',
                            description: 'The minimum value of the scale.',

                            path: 'defaultsCommon/min',
                            hive: 'stats',

                            type: 'numeric',
                            min: 1,
                            max: 1000000000,
                            step: 1,
                        },
                        {
                            field: 'max',
                            display: 'Maximum value',
                            description: 'The maximum value of the scale.',

                            path: 'defaultsCommon/max',
                            hive: 'stats',

                            type: 'numeric',
                            min: 1,
                            max: 1000000000,
                            step: 1,
                        },
                    ],
                    children: [
                        {
                            name: 'Labels',
                            items: [
                                {
                                    field: 'show',
                                    display: 'Show the labels',
                                    description: 'Set it to true if you want to display the labels',

                                    path: 'defaultsCommon/labels/show',
                                    hive: 'stats',

                                    type: 'boolean',
                                },
                                {
                                    field: 'fontSize',
                                    display: 'Font size',
                                    description: 'The size of the font',

                                    path: 'defaultsCommon/labels/style/fontSize',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 48,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'fontWeight',
                                    display: 'Weight',
                                    description: 'The weight of the font',

                                    path: 'defaultsCommon/labels/style/fontWeight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'defaultsCommon/labels/style/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                            ]
                        },
                        {
                            name: 'Border',
                            items: [
                                {
                                    field: 'show',
                                    display: 'Show the border',
                                    description: 'Set it to true if you want to display the border',

                                    path: 'defaultsCommon/axisBorder/show',
                                    hive: 'stats',

                                    type: 'boolean',
                                },
                            ]
                        },
                        {
                            name: 'Title',
                            items: [
                                {
                                    field: 'text',
                                    display: 'Text',
                                    description: 'The text to display as description',

                                    path: 'defaultsCommon/title/text',
                                    hive: 'stats',

                                    type: 'text',
                                },
                                {
                                    field: 'align',
                                    display: 'Alignment',
                                    description: 'The alignment of the title',

                                    path: 'defaultsCommon/title/align',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Start', value: 'start'},
                                        {text: 'Center', value: 'center'},
                                        {text: 'End', value: 'end'}
                                    ]
                                },
                                {
                                    field: 'fontSize',
                                    display: 'Font size',
                                    description: 'The size of the font',

                                    path: 'defaultsCommon/title/style/fontSize',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 48,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'fontWeight',
                                    display: 'Size',
                                    description: 'The weight of the font',

                                    path: 'defaultsCommon/title/style/fontWeight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'defaultsCommon/title/style/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                            ]
                        },
                    ]
                },
            ],
            yAxisSeries: [
                {
                    name: 'Y axis',
                    items: [
                        {
                            field: 'show',
                            display: 'Show',
                            description: 'Set it to true to display the y axis',

                            path: 'show',
                            hive: 'stats',

                            type: 'boolean',
                        },
                        {
                            field: 'type',
                            display: 'Linear or Logarithmic',
                            description: 'Choose between a linear or logarithmic scale',

                            path: 'type',
                            hive: 'stats',

                            type: 'select',
                            options: [
                                {text: 'Linear', value: 'linear'},
                                {text: 'Logarithmic', value: 'logarithmic'}
                            ]
                        },
                        {
                            field: 'useManualSetting',
                            display: 'Manually force the scaling',
                            description: 'When set to true it will use the min and max value to set the scale',

                            path: 'useManualSetting',
                            hive: 'stats',

                            type: 'boolean',
                        },
                        {
                            field: 'min',
                            display: 'Minimum value',
                            description: 'The minimum value of the scale.',

                            path: 'min',
                            hive: 'stats',

                            type: 'numeric',
                            min: 1,
                            max: 1000000000,
                            step: 1,
                        },
                        {
                            field: 'max',
                            display: 'Maximum value',
                            description: 'The maximum value of the scale.',

                            path: 'max',
                            hive: 'stats',

                            type: 'numeric',
                            min: 1,
                            max: 1000000000,
                            step: 1,
                        },
                    ],
                    children: [
                        {
                            name: 'Labels',
                            items: [
                                {
                                    field: 'show',
                                    display: 'Show the labels',
                                    description: 'Set it to true if you want to display the labels',

                                    path: 'labels/show',
                                    hive: 'stats',

                                    type: 'boolean',
                                },
                                {
                                    field: 'fontSize',
                                    display: 'Font size',
                                    description: 'The size of the font',

                                    path: 'labels/style/fontSize',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 48,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'fontWeight',
                                    display: 'Weight',
                                    description: 'The weight of the font',

                                    path: 'labels/style/fontWeight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'labels/style/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                            ]
                        },
                        {
                            name: 'Border',
                            items: [
                                {
                                    field: 'show',
                                    display: 'Show the border',
                                    description: 'Set it to true if you want to display the border',

                                    path: 'axisBorder/show',
                                    hive: 'stats',

                                    type: 'boolean',
                                },
                            ]
                        },
                        {
                            name: 'Title',
                            items: [
                                {
                                    field: 'text',
                                    display: 'Text',
                                    description: 'The text to display as description',

                                    path: 'title/text',
                                    hive: 'stats',

                                    type: 'text',
                                },
                                {
                                    field: 'align',
                                    display: 'Alignment',
                                    description: 'The alignment of the title',

                                    path: 'title/align',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Start', value: 'start'},
                                        {text: 'Center', value: 'center'},
                                        {text: 'End', value: 'end'}
                                    ]
                                },
                                {
                                    field: 'fontSize',
                                    display: 'Font size',
                                    description: 'The size of the font',

                                    path: 'title/style/fontSize',
                                    hive: 'stats',

                                    type: 'numeric',
                                    min: 6,
                                    max: 48,
                                    step: 1,
                                    unit: 'px',
                                },
                                {
                                    field: 'fontWeight',
                                    display: 'Size',
                                    description: 'The weight of the font',

                                    path: 'title/style/fontWeight',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Normal', value: 'normal'},
                                        {text: 'Bold', value: 'bold'}
                                    ]
                                },
                                {
                                    field: 'fontFamily',
                                    display: 'Family',
                                    description: 'The font family',

                                    path: 'title/style/fontFamily',
                                    hive: 'stats',

                                    type: 'select',
                                    options: [
                                        {text: 'Inconsolata', value: 'Inconsolata'},
                                        {text: 'FiraGO', value: 'FiraGO'}
                                    ]
                                },
                            ]
                        },
                    ]
                }
            ],
        }
    },

    manifestRebuild: function (name) {
        let object = {}

        switch (name) {
            case 'statsSparkChartGraphSeriesArea':
                object = this.graph.series.area;
                break
            case 'statsSparkChartGraphSeriesLine':
                object = this.graph.series.line;
                break
            case 'statsSparkChartReportLine':
                object = this.reportLine
                break
            case 'statsSparkChartGraphGlobals':
                object = this.graph.chart.globalSettings
                break
            case 'statsSparkChartGraphTitle':
                object = this.graph.chart.title
                break
            case 'statsSparkChartGraphSubTitle':
                object = this.graph.chart.subTitle
                break
            case 'statsSparkChartGraphShadow':
                object = this.graph.chart.shadow
                break
            case 'statsSparkChartGraphLegend':
                object = this.graph.chart.legend
                break
            case 'statsSparkChartGraphGrid':
                object = this.graph.chart.grid
                break
            case 'statsSparkChartGraphToolbar':
                object = this.graph.chart.toolbar
                break
            case 'statsSparkChartGraphxAxis':
                object = this.graph.chart.xaxis
                break
            case 'statsSparkChartGraphTooltips':
                object = this.graph.chart.tooltips
                break
            case 'statsSparkChartGraphYaxisCommon':
                object = this.graph.chart.yAxisCommon
                break
            case 'statsSparkChartGraphSeriesYaxis':
                object = this.graph.chart.yAxisSeries
                break
            case 'statsSparkChartGraphYaxisSharedTools':
                object = this.graph.chart.yAxisCommon
                break
            case 'statsSparkChartGraphYaxisIndividualTools':
                object = this.graph.chart.yAxisSeries
                break
        }

        app.prefs.manifest = JSON.parse(JSON.stringify(object))
    }
}
