/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

app.ui.stats.results.table = {
    init: function () {
    },

    create: function ($tabContent, tabData) {
        let table = '<div style="overflow: auto; width: 100%; height: calc(100vh - 224px); border: 1px solid silver;">'

        table += '<table class="table table-hover stats-result-table" id="tblStatsResults-' + tabData.ix + '" style="font-size: ' + tabData.zoomValue + 'px;"></table>'
        table += '</div>'

        $tabContent.append(table)

        tabData.$table = $('#tblStatsResults-' + tabData.ix)
        tabData.table = 'tblStatsResults-' + tabData.ix

        this.createHeader(tabData)

        this.createNavigation($tabContent, tabData)

        // mount handlers
        $('#btnStatsToolbarResultsZoomIn')
            .off()
            .on('click', () => this.zoomInPressed())

        $('#btnStatsToolbarResultsZoomOut')
            .off()
            .on('click', () => this.zoomOutPressed())

        $('#btnStatsToolbarResultsProperties')
            .off()
            .on('click', () => this.propertiesPressed())

        $('#btnStatsToolbarResultsExport')
            .off()
            .on('click', () => this.exportPressed())

        $('#btnStatsToolbarResultsHelp')
            .off()
            .on('click', () => this.helpPressed())
    },

    createHeader: async function (tabData) {
        const $table = tabData.$table
        let headers = '<thead>'
        let colspan

        tabData.data = []

        $table.empty()

        // regions layer
        headers += '<tr>'
        headers += '<th class="sticky-col" rowspan="4" style="background: #f0f0f0; vertical-align: middle; top: 0; left: 0; width: 80px;">Timestamp</th>'

        colspan = tabData.selection.samples.length * tabData.selection.sampleTypes.length * tabData.selection.processes.length
        tabData.selection.regions.forEach(region => {
            headers += '<th colspan="' + colspan + '" style="border: 1px solid white; background: #f0f0f0;">' + (region === '*' ? 'All regions' : 'Region: ' + region) + '</th>'
        })
        headers += '</tr>'

        // processes layer
        headers += '<tr>'
        colspan = tabData.selection.samples.length * tabData.selection.sampleTypes.length
        tabData.selection.regions.forEach(region => {
            tabData.selection.processes.forEach(process => {
                if (process !== '') headers += '<th colspan="' + colspan + '" style="border: 1px solid white; background: #e0e0e0;">' + (process === '*' ? 'All processes' : 'P-' + process) + '</th>'
            })
        })
        headers += '</tr>'

        // samples layer
        headers += '<tr>'
        tabData.selection.regions.forEach(region => {
            tabData.selection.processes.forEach(process => {
                tabData.selection.samples.forEach(sample => {
                    headers += '<th colspan="' + tabData.selection.sampleTypes.length + '" style="border: 1px solid white; background: #d0d0d0;">' + sample + '</th>'
                })
            })
        })
        headers += '</tr>'

        // sampleTypes layer
        headers += '<tr>'
        tabData.selection.regions.forEach(region => {
            tabData.selection.processes.forEach(process => {
                tabData.selection.samples.forEach(sample => {
                    tabData.selection.sampleTypes.forEach(sampleType => {
                        const dataContent = region + '-' + process + '-' + sample + '-' + sampleType
                        headers += '<th style="border: 1px solid white; background: #c0c0c0;" data-content="' + dataContent + '">' + sampleType + '</th>'

                        // prepare the data pointers
                        this.getDataPointers(tabData, region, process, sample, sampleType)
                    })
                })
            })
        })
        headers += '</tr>'

        headers += '</thead><tbody style="font-family: \'Inconsolata\', sans-serif;"></tbody>'

        $table.append(headers)

        const colSize = 70
        const tableWidth = tabData.selection.samples.length * tabData.selection.sampleTypes.length * tabData.selection.processes.length * tabData.selection.regions.length * colSize

        $table.css('width', tableWidth + 'px')

        if (tabData.data.length === 0) {
            await app.ui.msgbox.show('There is no data to display in the result tab.', '', true)

            $('#tabStatsResult').attr('disabled', 'disabled')

            return

        } else {
            $('#tabStatsResult').prop('disabled', false)
        }

        this.populate(tabData)
    },

    createNavigation: function ($tabContent, tabData) {
        let rows = '<div style="text-align: center; margin-top: 10px;">'

        rows += '<button class="btn btn-outline-info btn-sm disabled" disabled type="button" id="btnStatsResultsHome-' + tabData.ix + '">' +
            '<i class="bi bi-chevron-bar-left stats-toolbar-icons" style="color: var(--ydb-purple)"></i>' +
            '</button>&nbsp;'
        rows += '<button class="btn btn-outline-info btn-sm disabled " disabled type="button" id="btnStatsResultsPrev-' + tabData.ix + '">' +
            '<i class="bi bi-chevron-left stats-toolbar-icons" style="color: var(--ydb-purple)"></i>' +
            '</button>'
        rows += '<span style="margin: 0 12px;" id="lblStatsResultsLabel-' + tabData.ix + '">' + this.computePages(tabData) + '</span>'
        rows += '<button class="btn btn-outline-info btn-sm' + (tabData.numberOfPages === 1 ? ' disabled' : '') + '" type="button" ' + (tabData.numberOfPages === 1 ? ' disabled' : '') + ' id="btnStatsResultsNext-' + tabData.ix + '">' +
            '<i class="bi bi-chevron-right stats-toolbar-icons" style="color: var(--ydb-purple)"></i>' +
            '</button>&nbsp;'
        rows += '<button class="btn btn-outline-info btn-sm' + (tabData.numberOfPages === 1 ? ' disabled' : '') + '" type="button"' + (tabData.numberOfPages === 1 ? ' disabled' : '') + ' id="btnStatsResultsEnd-' + tabData.ix + '">' +
            '<i class="bi bi-chevron-bar-right stats-toolbar-icons" style="color: var(--ydb-purple)"></i>' +
            '</button>'

        rows += '</div>'

        $tabContent.append(rows)

        // events
        $('#btnStatsResultsHome-' + tabData.ix).on('click', () => this.homeClicked(tabData))
        $('#btnStatsResultsPrev-' + tabData.ix).on('click', () => this.previousClicked(tabData))
        $('#btnStatsResultsNext-' + tabData.ix).on('click', () => this.nextClicked(tabData))
        $('#btnStatsResultsEnd-' + tabData.ix).on('click', () => this.endClicked(tabData))
    },


    getDataPointers: function (tabData, region, process, sample, sampleType) {
        const data = app.statistics.data
        let computePages = false

        data.forEach(source => {
            if (source.id === tabData.source.id) {
                source.regions[region].forEach(pid => {
                    if (pid[0].pid === 'P' + process || pid[0].pid === 'P[' + process + ']' || pid[0].pid === process) {
                        pid.forEach(sampleBlock => {
                            if (sampleBlock.sample === sample) {
                                let txtSampleType = sampleType
                                let originalText = ''

                                if (txtSampleType === 'Δ') originalText = txtSampleType, txtSampleType = 'delta'
                                else if (txtSampleType.indexOf('Δ') > -1) originalText = txtSampleType, txtSampleType = txtSampleType.slice(2)
                                else originalText = txtSampleType

                                txtSampleType = txtSampleType.toLowerCase()

                                tabData.data.push({
                                    array: sampleBlock.data,
                                    sample: sample,
                                    sampleType: txtSampleType,
                                    originalSampleType: originalText,

                                    pid: pid,
                                    region: region
                                })
                            }

                            // compute max number of pages
                            if (computePages === false) {
                                const length = sampleBlock.data.length

                                tabData.numberOfPages = Math.floor(length / tabData.pageSize) + 1
                                computePages = true
                            }
                        })
                    }
                })
            }
        })
    },

    populate: function (tabData) {
        let row = ''
        const offset = (tabData.currentPage - 1) * tabData.pageSize

        for (let ix = offset; ix < (offset + tabData.pageSize); ix++) {
            if (tabData.data[0].array[ix] === undefined) break

            const timestamp = this.formatTimestamp(tabData.data[0].array[ix].timestamp)

            row += '<tr>'
            row += '<td class="sticky-col" style="background: #f0f0f0; z-index: 1;">' + timestamp + '</td>'

            tabData.data.forEach(cell => {
                if (cell.array === undefined || cell.array[ix] === undefined) {

                    return
                }
                row += '<td>' + app.ui.formatThousands(cell.array[ix][cell.sampleType]) + '</td>'
            })
            row += '</tr>'
        }
        $('#' + tabData.table + ' tbody')
            .empty()
            .append(row + '</tbody>')
    },

    homeClicked: function (tabData) {
        tabData.currentPage = 1

        app.ui.button.disable($('#btnStatsResultsPrev-' + tabData.ix))
        app.ui.button.disable($('#btnStatsResultsHome-' + tabData.ix))

        if (tabData.numberOfPages > 1) {
            app.ui.button.enable($('#btnStatsResultsNext-' + tabData.ix))
            app.ui.button.enable($('#btnStatsResultsEnd-' + tabData.ix))
        }

        this.populate(tabData)

        $('#lblStatsResultsLabel-' + tabData.ix).text(this.computePages(tabData))
    },


    previousClicked: function (tabData) {
        tabData.currentPage--

        if (tabData.currentPage === 1) {
            app.ui.button.disable($('#btnStatsResultsPrev-' + tabData.ix))
            app.ui.button.disable($('#btnStatsResultsHome-' + tabData.ix))
        }

        if (tabData.currentPage >= 1 && tabData.numberOfPages > 1) {
            app.ui.button.enable($('#btnStatsResultsNext-' + tabData.ix))
            app.ui.button.enable($('#btnStatsResultsEnd-' + tabData.ix))
        }

        this.populate(tabData)

        $('#lblStatsResultsLabel-' + tabData.ix).text(this.computePages(tabData))
    },

    nextClicked: function (tabData) {
        tabData.currentPage++

        if (tabData.currentPage === tabData.numberOfPages) {
            app.ui.button.disable($('#btnStatsResultsNext-' + tabData.ix))
            app.ui.button.disable($('#btnStatsResultsEnd-' + tabData.ix))
        }

        if (tabData.currentPage <= tabData.numberOfPages && tabData.numberOfPages > 1) {
            app.ui.button.enable($('#btnStatsResultsPrev-' + tabData.ix))
            app.ui.button.enable($('#btnStatsResultsHome-' + tabData.ix))
        }

        this.populate(tabData)

        $('#lblStatsResultsLabel-' + tabData.ix).text(this.computePages(tabData))
    },

    endClicked: function (tabData) {
        tabData.currentPage = tabData.numberOfPages

        app.ui.button.disable($('#btnStatsResultsNext-' + tabData.ix))
        app.ui.button.disable($('#btnStatsResultsEnd-' + tabData.ix))

        if (tabData.numberOfPages > 1) {
            app.ui.button.enable($('#btnStatsResultsPrev-' + tabData.ix))
            app.ui.button.enable($('#btnStatsResultsHome-' + tabData.ix))
        }
        this.populate(tabData)

        $('#lblStatsResultsLabel-' + tabData.ix).text(this.computePages(tabData))

    },

    computePages: function (tabData) {
        return tabData.currentPage + ' of ' + tabData.numberOfPages
    },

    zoomInPressed: function () {
        const tabData = app.ui.stats.results.tabsData[app.ui.stats.results.tabIx]

        if (tabData.zoomValue > 20) return

        tabData.zoomValue++

        tabData.$table.css('font-size', tabData.zoomValue + 'px')
    },

    zoomOutPressed: function () {
        const tabData = app.ui.stats.results.tabsData[app.ui.stats.results.tabIx]

        if (tabData.zoomValue < 8) return

        tabData.zoomValue--

        tabData.$table.css('font-size', tabData.zoomValue + 'px')
    },

    propertiesPressed: function () {
        app.ui.stats.results.sourceProperties.show()
    },

    exportPressed: function () {
        app.ui.stats.results.export.show()
    },

    helpPressed: function () {
        app.ui.help.show('stats/results')
    },

    formatTimestamp: function (timestamp) {
        const ts = new Date(timestamp)
        const ms = ts.getMilliseconds()
        return ts.getHours() + ':' + ts.getMinutes().toString().padStart(2, '0') + ':' + ts.getSeconds().toString().padStart(2, '0') + '.' + ms.toString().padEnd(3, '0')
    }
}
