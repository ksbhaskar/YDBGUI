/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

// This file manages the loading and saving of stats reports

app.ui.stats.storage = {
    load: {
        init: function () {
            $('#selStatsSourcesLoadNames').on('click', () => this.nameSelected())
            $('#selStatsSourcesLoadNames').on('dblclick', () => this.okPressed())

            $('#btnStatsLoadOk').on('click', () => this.okPressed())
            $('#btnStatsLoadDelete').on('click', () => this.deletePressed())
            $('#stats-import-file-selector').on('change', (e) => this.importPressed(e))
            $('#btnStatsLoadHelp').on('click', () => app.ui.help.show('stats/load'))

            app.ui.setupDialogForTest('modalStatsSourcesLoad')
        },

        show: function () {
            let options = ''
            let firstValue = ''

            // get the sources from the storage
            app.ui.stats.storage.data = app.ui.storage.get('statsSources')

            if (Array.isArray(app.ui.stats.storage.data) && app.ui.stats.storage.data.length > 0) {

                // populate the list
                app.ui.stats.storage.data.sort((a, b) => {
                    if (a.name > b.name) return 1
                    if (a.name < b.name) return -1
                    return 0

                }).forEach(entry => {
                    if (firstValue === '') firstValue = entry.name
                    options += '<option value="' + entry.name + '">' + entry.name + '</option>'
                })

                $('#selStatsSourcesLoadNames')
                    .empty()
                    .append(options)

                // select first entry
                $('#selStatsSourcesLoadNames option[value="' + firstValue + '"]').attr('selected', 'selected')
                this.nameSelected()

                app.ui.button.enable($('#btnStatsLoadOk'))
                app.ui.button.enable($('#btnStatsLoadDelete'))

            } else {
                $('#selStatsSourcesLoadNames').empty()

                app.ui.button.disable($('#btnStatsLoadOk'))
                app.ui.button.disable($('#btnStatsLoadDelete'))
            }

            $('#modalStatsSourcesLoad')
                .modal({show: true, backdrop: 'static'})
                .draggable({handle: '.modal-header'})
        },

        nameSelected: function () {
            const name = $('#selStatsSourcesLoadNames').val()
            if (name === null || name === '') return

            const entry = this.findDescriptionByName(name)

            entry.description = entry.description.replaceAll('\n', '<br>')

            $('#divStatsSourcesLoadDescription').html(entry && entry.description ? entry.description : '')
        },

        findDescriptionByName: function (name) {
            return app.ui.stats.storage.data.find(entry => name === entry.name)
        },

        importPressed: async function (e) {
            const file = e.target.files[0]

            try {
                const res = await (this.fileReader(file))

                this.okPressed(res)

            } catch (err) {
                app.ui.msgbox.show(app.REST.parseError(err), 'Error')
            }

            e.target.value = ''
        },

        fileReader: function (file) {
            return new Promise(function (resolve, reject) {
                const reader = new FileReader()

                reader.addEventListener('load', (event) => {
                    try {
                        const data = JSON.parse(reader.result)

                        if (data.version && data.fileType === 'ydb-stats-report') {
                            resolve(data)

                        } else {
                            reject('File is not a valid YottaDB Statistics Report')
                        }

                    } catch (err) {
                        reject(err)
                    }
                });

                reader.addEventListener('error', (event) => {
                    reject(reader.error)
                });

                reader.readAsText(file)
            })
        },

        okPressed: async function (entry, fileName) {
            const name = fileName || $('#selStatsSourcesLoadNames').val()
            entry = entry || this.findDescriptionByName(name)

            try {
                await this.processDirty()

            } catch (e) {
                return
            }

            if (entry) {
                $('#modalStatsSourcesLoad').modal('hide')

                switch (app.ui.stats.entry.version) {
                    case 1: {
                        app.ui.msgbox.show('This report file has version 1.0 and is no longer valid.')

                        return
                    }
                    case 2: {
                        app.ui.stats.entry.version = 2.1
                        app.ui.stats.entry.data.forEach(source => {
                            source.data.processes.pids = []
                        })

                        app.ui.msgbox.show('This report file had version 2.0 and needed to be upgraded<br><br></br>' +
                            'You can use the file, but will need to save it again for the changes to be permanent.', 'Warning')
                    }
                }

                // validate region names inside the sources
                let regionNotFound = ''

                entry.data.forEach(source => {
                    source.data.regions.forEach(region => {
                        if (app.system.regions[region] === undefined && region !== '*') {
                            regionNotFound = region
                        }
                    })
                })

                if (regionNotFound !== '') {
                    app.ui.msgbox.show('The region: ' + regionNotFound + ' was not found in the database configuration.<br><br>Aborting load...')

                    return
                }

                // proceed
                app.ui.stats.entry = entry

                app.ui.stats.switchTheme()

                app.statistics.initData()

                await app.ui.stats.tab.resetGraphs()
                app.ui.stats.sparkChart.renderer.render($('#divStatsSparkchart'))
                app.ui.button.enable($('#btnStatsNew'))
                app.ui.stats.tab.toolbar.status = app.ui.stats.sources.sourcesStatus(true) === false ? 'noSources' : 'stopped'
                app.ui.stats.tab.toolbar.computeToolbarStatus()

                // commented out in case we need the function again
                //if (app.ui.stats.utils.reportHasGraphs() === true) app.ui.stats.onSplitterResize(null, {size: {height: app.ui.stats.entry.sparkChart.graphsConfig.height}})

                // update tab header
                app.ui.stats.entryDirty = false
                $('#lblStatsReportName').text('[' + app.ui.stats.entry.name + ']')

                app.ui.stats.utils.computeStatusBarSampleRate()
            }
        },

        processDirty: function () {
            return new Promise(async function (resolve, reject) {
                if (app.ui.stats.entryDirty === true) {
                    app.ui.inputbox.show('The report file has been modified.<br><br>Do you want to save it first?', 'WARNING', ret => {
                        if (ret === 'YES') {
                            $('#modalStatsSourcesLoad').modal('hide')

                            app.ui.stats.tab.toolbar.save()

                            reject()

                        } else {
                            resolve(false)
                        }
                    })

                } else {
                    resolve(true)
                }
            })
        },

        deletePressed: function () {
            const name = $('#selStatsSourcesLoadNames').val()
            const ix = app.ui.stats.storage.validateStatsName(name)

            app.ui.inputbox.show('Do you want to delete the entry: ' + name + ' ?', 'WARNING', async ret => {
                if (ret === 'YES') {
                    app.ui.stats.storage.data.splice(ix, 1)
                    app.ui.storage.save('statsSources', app.ui.stats.storage.data)

                    this.show()
                }
            })
        },
    },

    save: {
        init: function () {
            $('#btnStatsSaveOk').on('click', () => this.okPressed())
            $('#btnStatsSaveExport').on('click', () => this.exportPressed())
            $('#btnStatsSaveHelp').on('click', () => app.ui.help.show('stats/save'))
            $('#txtStatsName').on('keypress', e => this.nameKeyPressed(e))

            $('#modalStatsSourcesSave')
                .on('shown.bs.modal', () => {
                    $('#txtStatsName').focus()
                })
                .on('hidden.bs.modal', () => {
                    app.ui.stats.storage.data = null
                })

            app.ui.setupDialogForTest('modalStatsSourcesSave')
        },

        show: function () {
            // fetch data from storage to check the name
            app.ui.stats.storage.data = app.ui.storage.get('statsSources')
            if (app.ui.stats.storage.data === null) app.ui.stats.storage.data = []

            $('#txtStatsName').val(app.ui.stats.entry.name)
            $('#txtStatsDescription').val(app.ui.stats.entry.description.replaceAll('<br>', '\n'))

            $('#modalStatsSourcesSave')
                .modal({show: true, backdrop: 'static'})
                .draggable({handle: '.modal-header'})
        },

        exportPressed: async function () {
            const data = JSON.stringify(app.ui.stats.entry)

            this.okPressed(true)

            if (navigator.userAgent.indexOf("Firefox") != -1) {
                // *************************************
                // firefox
                // *************************************
                try {
                    let blob = new Blob([data], {
                        type: "script"
                    })
                    let a = document.createElement("a")
                    a.href = URL.createObjectURL(blob)
                    a.download = (app.ui.stats.entry.name === '' ? 'untitled' : app.ui.stats.entry.name) + '.ysr'
                    a.hidden = true
                    document.body.appendChild(a)
                    a.innerHTML =
                        ""
                    a.click()
                    a.remove()
                } catch (err) {
                    app.ui.msgbox.show(app.REST.parseError(err), 'Warning')
                }

            } else if (navigator.userAgent.indexOf("Chrome") != -1) {
                // *************************************
                // chrome
                // *************************************
                try {
                    const handle = await showSaveFilePicker({
                        suggestedName: (app.ui.stats.entry.name === '' ? 'untitled' : app.ui.stats.entry.name) + '.ysr',
                        types: [{
                            description: 'YottaDB Statistics Report',
                            accept: {'text/json': ['.ysr']},
                        }],
                    });

                    const blob = new Blob([data]);

                    const writableStream = await handle.createWritable();
                    await writableStream.write(blob);
                    await writableStream.close();

                } catch (err) {
                    if (err.toString().indexOf('aborted') > -1) return

                    const errorMessage = app.REST.parseError(err)

                    app.ui.msgbox.show(errorMessage.indexOf('showSaveFilePicker') > -1 ? 'This option is available only when using HTTP. Consider using Firefox for this operation' : errorMessage, 'Warning')
                }

            } else {
                app.ui.msgbox.show('Your browser is not supported for this operation.<br><br>Please report your browser type and vendor to YDB support.', 'Warning')
            }
        },

        okPressed: async function (silent = false) {
            let txtStatsName = $('#txtStatsName').val()
            if (txtStatsName === null || txtStatsName === '') {
                if (silent === false) {

                    await app.ui.msgbox.show('You must enter a name', app.ui.msgbox.INPUT_ERROR, true)

                    $('#txtStatsName').focus()

                    return
                } else {
                    $('#txtStatsName').val('untitled')
                    txtStatsName = 'untitled'
                }
            }

            let existing = app.ui.stats.storage.validateStatsName(txtStatsName)

            app.ui.stats.entry.name = txtStatsName
            app.ui.stats.entry.description = $('#txtStatsDescription').val()

            // check if name already exists
            if (existing > -1) {
                if (silent === false) {
                    app.ui.inputbox.show('The name is already used.<br>Do you want to overwrite the data ?', 'WARNING', async ret => {
                        if (ret === 'YES') {
                            this.saveData(existing)

                            $('#modalStatsSourcesSave').modal('hide')
                        }

                        // update tab header
                        $('#lblStatsReportName').text('[' + txtStatsName + ']')
                        app.ui.stats.entryDirty = false
                    })

                } else {
                    this.saveData(existing)

                    $('#modalStatsSourcesSave').modal('hide')
                }


            } else {
                app.ui.stats.storage.data.push(app.ui.stats.entry)
                app.ui.storage.save('statsSources', app.ui.stats.storage.data)

                $('#modalStatsSourcesTitle').text('Sources: [' + txtStatsName + ']')

                $('#modalStatsSourcesSave').modal('hide')

                // update tab header
                $('#lblStatsReportName').text('[' + txtStatsName + ']')
                app.ui.stats.entryDirty = false
            }
        },

        nameKeyPressed: function (e) {
            const char = e.which || e.originalEvent.which

            if (char === 13) this.okPressed()
        },

        saveData: function (existing) {
            app.ui.stats.storage.data.splice(existing, 1)
            app.ui.stats.storage.data.push(app.ui.stats.entry)
            app.ui.storage.save('statsSources', app.ui.stats.storage.data)

            $('#modalStatsSourcesTitle').text('Sources: [' + txtStatsName + ']')
        },
    },

    validateStatsName: function (name) {
        return app.ui.stats.storage.data.findIndex(entry => name === entry.name)
    },

    data: []
}
