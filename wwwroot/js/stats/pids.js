/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

app.ui.stats.pids = {
    init: function () {
        $('#btnStatsPidsOk').on('click', (e) => this.okPressed());

        app.ui.setupDialogForTest('statsYgblPids')

    },

    checkReport: async function () {
        // check if any source has pids
        for (let sourceIx in app.ui.stats.entry.data) {
            const source = app.ui.stats.entry.data[sourceIx]
            if (source.data.processes.type === 'pids' && source.data.enabled === true) {
                await this.show(source)
            }
        }
    },

    show: async function (source) {
        return new Promise(async function (resolve, reject) {
            const res = await app.REST._ygblEnumPids(source.data.regions)

            if (!res.data) {
                // no processes found
                await app.ui.msgbox.show('No processes found on the server', 'WARNING', true)

                resolve()

                return

            } else {
                // data.processes.[]
                const selStatsPidsPids = $('#selStatsPidsPids')
                const found = []
                const notFound = []
                let options = ''

                // check the pids
                source.data.processes.pids.forEach(pid => {
                    if (res.data.processes.find(lPid => lPid === parseInt(pid)) === undefined) notFound.push(pid)
                    else found.push(pid)
                })

                // return and continue if all found
                if (notFound.length === 0) {
                    resolve()

                    return
                }

                // clear the <select>
                selStatsPidsPids.empty()

                // not found
                options += '<optgroup label="Not found">'
                notFound.forEach(pid => {
                    options += '<option value="' + pid + '" disabled style="font-style: italic; color: silver;">' + pid + '</option>'
                })
                options += '</optgroup>'

                // found
                options += '<optgroup label="Available">'
                res.data.processes.forEach(pid => {
                    const used = source.data.processes.pids.find(lPid => parseInt(lPid) === parseInt(pid))

                    options += '<option value="' + pid + '" ' + (used ? 'selected' : '') + '>' + pid + '</option>'
                })
                options += '</optgroup>'

                selStatsPidsPids.append(options)

                this.source = source
            }

            $('#statsYgblPids')
                .modal({show: true, backdrop: 'static', keyboard: true})
                .off('hide.bs.modal')
                .on('hide.bs.modal', () => {
                    resolve()
                })
        })
    },

    okPressed: function () {
        const selStatsPidsPids = $('#selStatsPidsPids').val()

        if (selStatsPidsPids === null || selStatsPidsPids === '' || (Array.isArray(selStatsPidsPids) && selStatsPidsPids.length === 0)) {
            app.ui.msgbox.show('You must select at least one process.', app.ui.msgbox.INPUT_ERROR)

            return
        }
        source.data.processes.pids = selStatsPidsPids

        source.data.views.forEach(view => {
            if (view.type === 'graph') {
                app.ui.stats.sources.ygblstats.recreateGraphMappings(view, selStatsPidsPids)
            }
        })

        $('#statsYgblPids').modal('hide')
    },

    source: {}
}
