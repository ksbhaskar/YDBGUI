/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

// app scope property
app.ui.gldFileSelect.altGld = ''

app.ui.gldFileSelect.init = () => {
    $('#btnGldFilePathFileSelect').on('click', () => app.ui.gldFileSelect.fileSelectPressed())

    $('#btnGldFilePathOk').on('click', () => app.ui.gldFileSelect.okPressed())

    $('#lblGldFilePath')
        .on('keyup', e => app.ui.gldFileSelect.pathChanged(e))
        .on('paste', e => app.ui.gldFileSelect.pathChanged(e))

    $('#modalGldFileSelector').on('shown.bs.modal', () => {
        $('#lblGldFilePath').focus()
    })

    app.ui.setupDialogForTest('modalGldFileSelector')
}

app.ui.gldFileSelect.show = () => {
    $('#lblGldFilePath').val(app.ui.envSettings.currentGld)

    app.ui.button.disable($('#btnGldFilePathOk'))

    $('#modalGldFileSelector').modal({show: true, backdrop: 'static'})
}

app.ui.gldFileSelect.fileSelectPressed = () => {
    let path = ''

    const pathSplitted = app.ui.envSettings.currentGld.split('/')
    pathSplitted.splice(pathSplitted.length - 1, 1)
    path = pathSplitted.join('/')

    app.ui.fileSelect.show(path, '.gld', app.ui.gldFileSelect.fileSelectedReturn)
}

// this function is not anonymous because we need to call it directly for testing
app.ui.gldFileSelect.fileSelectedReturn = path => {
    $('#lblGldFilePath').val(path)
    app.ui.envSettings.currentGld = path

    app.ui.button.enable($('#btnGldFilePathOk'))
}

app.ui.gldFileSelect.okPressed = async () => {

    app.ui.envSettings.currentGld = $('#lblGldFilePath').val()
    $('#lblEnvSettingsGld')
        .val(app.ui.envSettings.currentGld)
        .removeClass('is-valid is-invalid')
    $('#lblEnvSettingsCwd')
        .val('Not set')
        .removeClass('is-valid is-invalid')
    $('#lblEnvSettingsEnvVars')
        .val('Not set')
        .removeClass('is-valid is-invalid')

    $('#valEnvSettingsGldInvalid').text('')
    $('#valEnvSettingsGldValid').text('')
    $('#valEnvSettingsCwdInvalid').text('')
    $('#valEnvSettingsCwdValid').text('')
    $('#valEnvSettingsEnvVarsInvalid').text('')
    $('#valEnvSettingsEnvVarsValid').text('')

    app.ui.envSettings.gldFileSelectResponse = {}

    app.ui.button.disable($('#btnEnvSettingsSet'))
    app.ui.button.enable($('#btnEnvSettingsValidate'))

    $('#modalGldFileSelector').modal('hide')

    app.ui.envSettings.buttonValidatePressed()
}

app.ui.gldFileSelect.pathChanged = e => {
    if ($('#lblGldFilePath').val() === '') {
        app.ui.button.disable($('#btnGldFilePathOk'))

    } else {
        app.ui.button.enable($('#btnGldFilePathOk'))
    }
}
