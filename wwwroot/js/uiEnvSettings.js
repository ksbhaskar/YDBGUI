/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.envSettings = {
    init: function () {
        $('#btnEnvSettingsGld').on('click', () => this.buttonGldPressed())
        $('#btnEnvSettingsCwd').on('click', () => this.buttonCwdPressed())
        $('#btnEnvSettingsEnvVars').on('click', () => this.buttonEnvVarsPressed())
        $('#btnEnvSettingsRoutinesSearchPath').on('click', () => app.ui.rViewer.searchPath.show())

        $('#btnEnvSettingsSet').on('click', () => this.buttonSetPressed())
        $('#btnEnvSettingsReset').on('click', () => this.buttonResetPressed())

        app.ui.setupDialogForTest('modalEnvSettings')
    },

    show: function (gldAlert = false) {
        // reset buttons
        app.ui.button.enable($('#btnEnvSettingsGld'))
        app.ui.button.enable($('#btnEnvSettingsCwd'))
        app.ui.button.enable($('#btnEnvSettingsEnvVars'))

        app.ui.button.disable($('#btnEnvSettingsValidate'))

        // update screen
        $('#lblEnvSettingsGld')
            .val('Not set')
            .removeClass('is-valid is-invalid')
        $('#lblEnvSettingsCwd')
            .val('Not set')
            .removeClass('is-valid is-invalid')
        $('#lblEnvSettingsEnvVars')
            .val('Not set')
            .removeClass('is-valid is-invalid')

        $('#valEnvSettingsGldInvalid').text('')
        $('#valEnvSettingsGldValid').text('')
        $('#valEnvSettingsCwdInvalid').text('')
        $('#valEnvSettingsCwdValid').text('')
        $('#valEnvSettingsEnvVarsInvalid').text('')
        $('#valEnvSettingsEnvVarsValid').text('')

        // reset button status
        if (app.systemEnvironment) {
            if (

                this.gld !== '' && app.systemEnvironment.gld !== this.gld ||
                app.systemEnvironment.cwd !== this.cwd ||
                app.systemEnvironment.envVars !== this.envVars
            ) {
                app.ui.button.enable($('#btnEnvSettingsReset'))

            } else {
                app.ui.button.disable($('#btnEnvSettingsReset'))

            }
        } else {
            app.ui.button.disable($('#btnEnvSettingsReset'))

        }

        app.ui.button.disable($('#btnEnvSettingsSet'))

        this.setupDefault()

        app.ui.envSettings.gldFileSelectResponse = app.system.environment

        // display no-gld alert if needed
        $('#altGldFileAlert').css('display', gldAlert === true ? 'block' : 'none')

        $('#modalEnvSettings').modal({show: true, backdrop: 'static'})
    },

    buttonGldPressed: function () {
        app.ui.gldFileSelect.show()
    },

    buttonCwdPressed: function () {
        app.ui.cwdSelector.show()
    },

    buttonEnvVarsPressed: function () {
        app.ui.envEnvVars.show()
    },

    buttonSetPressed: async function () {
        // update dashboard alert
        let extraText = ''

        if (this.currentCwd !== '') {
            extraText += '<br>Working directory: ' + this.currentCwd
        }
        if (this.currentEnvVars !== '') {
            extraText += '<br>Environment variables: ' + this.currentEnvVars
        }

        $('#txtGldAltFilename').text(this.currentGld)
        $('#txtGldAltExtra').html(extraText)

        $('#alertDashboardGld').css('display', 'block')

        this.gld = this.currentGld
        this.cwd = this.currentCwd
        this.envVars = this.currentEnvVars

        // refresh dashboard
        await app.ui.dashboard.refresh()

        // close dialog
        $('#modalEnvSettings').modal('hide')
    },

    buttonResetPressed: async function () {
        // set properties
        this.currentGld = this.gld = ''
        this.currentCwd = this.cwd = ''
        this.currentEnvVars = this.envVars = ''
        this.gldFileSelectResponse = {}

        // remove dashboard alert
        $('#alertDashboardGld').css('display', 'none')

        // refresh dashboard
        await app.ui.dashboard.refresh()

        // close dialog
        $('#modalEnvSettings').modal('hide')
    },

    buttonValidatePressed: async function () {
        const gldFile = this.currentGld
        const cwd = this.currentCwd

        if (gldFile === '') {
            app.ui.msgbox.show('No gld file has been specified...', app.ui.msgbox.INPUT_ERROR)

            return
        }

        try {
            let envVars = []
            let emptyFound = false

            for (const envVar in app.ui.envSettings.gldFileSelectResponse.envVars) {
                if (app.ui.envSettings.gldFileSelectResponse.envVars[envVar] !== '') envVars.push({[envVar]: app.ui.envSettings.gldFileSelectResponse.envVars[envVar]})
                else emptyFound = true
            }

            if (emptyFound) envVars = []

            const res = await app.REST._validateGld(gldFile, envVars, cwd)

            if (res.result === 'ERROR') {

                // File is not valid, GDE validation didn't passed
                app.ui.msgbox.show('The file is not a valid gld file.', 'ERROR')

                $('#lblEnvSettingsGld').addClass('is-invalid')
                $('#valEnvSettingsGldInvalid').text('The gld file has NOT been validated by the GDE')

                app.ui.button.disable($('#btnEnvSettingsSet'))

                // reset all local pointers
                this.currentGld = ''
                this.currentCwd = ''
                this.gldFileSelectResponse.data = {envVars: {}}

                return
            }

            // ****************************
            // set the response props
            // ****************************
            // add env vars is present
            if (res.data.envVars) {
                app.ui.envSettings.gldFileSelectResponse.envVars = res.data.envVars
            }

            // if no env vars were passed && no env vars are present, remove node
            if (res.data.envVarsFlag === false && !app.ui.envSettings.gldFileSelectResponse.envVars) delete app.ui.envSettings.gldFileSelectResponse.envVars

            if (res.data.pathsInError) {
                app.ui.envSettings.gldFileSelectResponse.pathsInError = res.data.pathsInError
            }

            if (res.data.relativeFlag) {
                app.ui.envSettings.gldFileSelectResponse.relativeFlag = res.data.relativeFlag
            }

            // ************************
            // process the response
            // ************************

            if (res.data.envVars) {
                let envVarsText = ''

                for (const envVar in res.data.envVars) {
                    envVarsText += envVar.slice(1) + '=' + res.data.envVars[envVar] + '; '
                }
                envVarsText = envVarsText.slice(0, -2)

                $('#lblEnvSettingsEnvVars').val(envVarsText)
            }

            // All good we can enable SET
            if (!res.data.pathsInError) {
                // all good, no extras are needed
                $('#lblEnvSettingsGld')
                    .removeClass('is-invalid')
                    .addClass('is-valid')
                $('#valEnvSettingsGldValid').text('The gld file and all the file paths have been validated')

                $('#lblEnvSettingsCwd').removeClass('is-valid is-invalid')


                if (res.data.relativeFlag === true) {
                    $('#lblEnvSettingsCwd')
                        .addClass('is-valid')
                    $('#valEnvSettingsCwdValid').text('The current working directory has been set and validated.')

                }

                $('#lblEnvSettingsEnvVars').removeClass('is-valid is-invalid')
                if (app.ui.envSettings.gldFileSelectResponse.envVars) {
                    $('#lblEnvSettingsEnvVars').addClass('is-valid')
                    $('#valEnvSettingsEnvVarsValid').text('All the environment variables have been validated')
                }

                app.ui.button.enable($('#btnEnvSettingsSet'))

                app.ui.msgbox.show('Validation was successful!<br><br>You can now press "Set" to apply the changes...', 'Success')

                return
            }

            // disable set, as we have errors
            app.ui.button.disable($('#btnEnvSettingsSet'))

            // we need now to find out what happened
            if (!res.data.envVars && res.data.relativeFlag === false && !res.data.pathsInError) {
                app.ui.msgbox.show('Validation was successful!<br><br>You can now press "Set" to apply the changes...', 'Success')

                app.ui.button.enable($('#btnEnvSettingsSet'))

                $('#valEnvSettingsGldValid').text('The gld file has been validated by the GDE')

                return

            } else {
                const pluralFlag = res.data.relativeFlag === true && res.data.envVars

                let msg = 'The file is a valid gld file. However, the following issue' + (pluralFlag === false ? ' was' : 's were') + ' found:<br><br>'

                if (res.data.pathsInError) {
                    // some file could not be found in the server
                    let fileList = ''
                    Object.keys(res.data.pathsInError).forEach(path => {
                        fileList += path + '<br>'
                    })

                    msg += '- The following path could not be resolved:<br>' + fileList + '<br>'
                }

                if (res.data.relativeFlag === true) {
                    // we have a relative path, we may need to set the cwd

                    // let's see if the bad paths have a relative path
                    if (res.data.pathsInError) {
                        // some file could not be found in the server
                        let fileInError = false
                        Object.keys(res.data.pathsInError).forEach(path => {
                            if (path.charAt(0) !== '/' && path.charAt(0) !== '$') {
                                fileInError = true

                            }
                        })

                        if (fileInError === true) {
                            msg += '- One or more paths have a relative path. You need to set up the current working directory.<br><br>'

                            // mark cwd as invalid and with message
                            $('#lblEnvSettingsCwd')
                                .removeClass('is-valid')
                                .addClass('is-invalid')
                            $('#valEnvSettingsCwdInvalid').text('You need to set up the current working directory to proceed')
                        }
                    }
                }

                if (res.data.envVars) {
                    // we need env vars to proceed...

                    // enable env vars
                    app.ui.button.enable($('#btnEnvSettingsEnvVars'))

                    // crfeate the text for both empty and filled vars
                    let isEmpty = false
                    let isFilled = false

                    for (const envVar in res.data.envVars) {
                        if (res.data.envVars[envVar] === '') isEmpty = true
                        else isFilled = true
                    }

                    // display message
                    let msgEmpty = '- The following env vars MUST be set:<br>'
                    let msgFilled = '- The following env vars need MAY be changed:<br>'

                    for (const envVar in res.data.envVars) {
                        if (res.data.envVars[envVar] === '') msgEmpty += envVar.slice(1) + '<br>'
                        else msgFilled += envVar.slice(1) + '<br>'
                    }

                    if (isEmpty === true) msg += msgEmpty
                    if (isFilled === true) msg += msgFilled

                    // mark en vars as invalid and with message
                    $('#lblEnvSettingsEnvVars')
                        .removeClass('is-valid')
                        .addClass('is-invalid')
                    $('#valEnvSettingsEnvVarsInvalid').text('You need to set up the environment variables to proceed')
                }

                app.ui.msgbox.show(msg, 'Warning')
            }

            $('#lblEnvSettingsGld').val(app.ui.envSettings.currentGld)


        } catch (err) {
            console.log(err)
            app.ui.msgbox.show(app.REST.parseError(err), 'WARNING')
        }
    },

    setupDefault: function () {
        let msg = ''

        if (!app.system.environment) {
            app.system.environment = {
                gld: '',
                cwd: '',
                envVars: ''
            }
        }

        $('#lblEnvSettingsGld').val(app.system.environment.gld)
        $('#lblEnvSettingsCwd').val(app.system.environment.cwd)

        for (const envVar in app.system.environment.envVars) {
            msg += envVar.slice(1) + '=' + app.system.environment.envVars[envVar] + '; '
        }

        msg = msg.slice(0, -2)

        $('#lblEnvSettingsEnvVars').val(msg)

        this.gld = this.currentGld = app.system.environment.gld
        this.cwd = this.currentCwd = app.system.environment.cwd
        this.envVars = this.currentEnvVars = msg
    },

    // properties
    currentGld: '',             // ok
    currentCwd: '',             // ok
    currentEnvVars: '',

    gldFileSelectResponse: {},

    gld: '',
    envVars: '',
    cwd: '',
}
