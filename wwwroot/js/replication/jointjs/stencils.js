/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.replication.topology.joint.stencils = {
    instanceObject: function (instance, instanceName) {
        if (instance.ERROR) {
            const secondary = this.secondary(instance, instanceName);

            secondary.setDead()

            return secondary

        } else {

            if (instance.instanceFile.flags.isPrimaryRoot === 1 && instance.instanceFile.flags.isSupplementary === 0) {
                const primary = this.primary(instance, instanceName)

                primary.setSourceServers(instance.health)

                return primary

            } else if (instance.instanceFile.flags.isSupplementary === 1) {
                const supplementary = this.supplementary(instance, instanceName);

                supplementary.setSourceServers(instance.health)
                supplementary.setUpdateProcess(instance.health)
                supplementary.setReceiverServer(instance.health)

                return supplementary

            } else {
                const secondary = this.secondary(instance, instanceName);

                secondary.setSourceServers(instance.health)
                secondary.setUpdateProcess(instance.health)
                secondary.setReceiverServer(instance.health)

                return secondary
            }
        }
    },

    instanceLinks: function (child, parent) {
        let link = new replLink({
            id: parent.id + '-' + child.id,
            data: {
                child: child,
                parent: parent
            },
            type: 'replLink',
            markup: [{
                tagName: 'path',
                selector: 'line'
            }, {
                tagName: 'path',
                selector: 'crossing',
            }],
            attrs: {
                line: {
                    stroke: 'green',
                    fill: 'none',
                    strokeWidth: 4
                },
                crossing: {
                    atConnectionRatio: .5,
                    d: 'M -10 -20 0 20 M 0 -20 10 20',
                    fill: 'none',
                    stroke: 'none',
                    strokeWidth: 4
                }
            }
        })
        link.router($('#selTopologyToolbarRouter').val())
        link.connector($('#selTopologyToolbarConnector').val())
        link.labels([
            {
                markup: [{
                    tagName: 'rect',
                    selector: 'labelBody'
                }, {
                    tagName: 'text',
                    selector: 'labelText'
                }],
                attrs: {
                    labelText: {
                        text: '0',
                        fill: 'blue',
                        fontFamily: 'Inconsolata',
                        fontSize: 30,
                        textAnchor: 'middle',
                        textVerticalAnchor: 'middle'
                    },
                    labelBody: {
                        ref: 'labelText',
                        x: 'calc(x - 10)',
                        y: 'calc(y - 5)',
                        width: 'calc(w + 20)',
                        height: 'calc(h + 10)',
                        stroke: 'blue',
                        fill: 'lightgreen',
                        strokeWidth: 2,
                        rx: 5,
                        ry: 5
                    }
                },
                position: {
                    distance: 0.15,
                    angle: 0,
                }
            }, {
                markup: [{
                    tagName: 'rect',
                    selector: 'labelBody'
                }, {
                    tagName: 'text',
                    selector: 'labelText'
                }],
                attrs: {
                    labelText: {
                        text: '0',
                        fill: 'blue',
                        fontFamily: 'Inconsolata',
                        fontSize: 30,
                        textAnchor: 'middle',
                        textVerticalAnchor: 'middle'
                    },
                    labelBody: {
                        ref: 'labelText',
                        x: 'calc(x - 10)',
                        y: 'calc(y - 5)',
                        width: 'calc(w + 20)',
                        height: 'calc(h + 10)',
                        stroke: 'blue',
                        fill: 'lightgreen',
                        strokeWidth: 2,
                        rx: 5,
                        ry: 5
                    }
                },
                position: {
                    distance: 0.85,
                    angle: 0
                }
            }
        ])

        link.source(parent)
        link.target(child)

        return link
    },

    // ***************************************
    // PRIMARY
    // ***************************************
    primary: function (instance, instanceName) {
        const rect = new Primary({
            position: {x: 300, y: 330},
            net: instance.net || null,
            id: instanceName,
            data: instance,
            z: 5,
            type: 'Primary',
            attrs: {
                role: {
                    html: instance.instanceFile ? instance.instanceFile.flags.isSupplementary === 1 ? 'Primary / Supp' : "Primary" : '?'
                },
                'data-access': {
                    html: 'RW'
                },
                header: {
                    html: instanceName.toUpperCase(),
                    id: 'jointjs-' + instanceName
                }
            }
        })

        if (instance.ERROR) rect.setDead()

        return rect
    },

    // ***************************************
    // SUPPLEMENTARY
    // ***************************************
    supplementary: function (instance, instanceName) {
        const rect = new Supplementary({
            position: {x: 300, y: 330},
            net: instance.net || null,
            id: instanceName,
            data: instance,
            z: 5,
            type: 'Supplementary',
            attrs: {
                role: {
                    html: instance.instanceFile.flags.isSupplementary === 1 ? 'Supplementary' : 'Primary'
                },
                'data-access': {
                    html: instance.instanceFile.flags.isReadWrite === 1 ? 'RW' : 'RO'
                },
                header: {
                    html: instanceName.toUpperCase(),
                    id: 'jointjs-' + instanceName
                }
            }
        });

        return rect
    },

    // ***************************************
    // SECONDARY
    // ***************************************
    secondary: function (instance, instanceName) {
        const rect = new Secondary({
            position: {x: 300, y: 330},
            net: instance.net || null,
            id: instanceName,
            data: instance,
            z: 5,
            type: 'Secondary',
            attrs: {
                body: {
                    fill: '#ddddff'
                },
                role: {
                    html: 'Secondary'
                },
                'data-access': {
                    html: 'RO'
                },
                header: {
                    html: instanceName.toUpperCase(),
                    id: 'jointjs-' + instanceName
                }
            }
        });

        if (instance.ERROR) rect.setDead()

        return rect
    }
}

class replLink extends joint.shapes.standard.DoubleLink {
    defaults() {
        return {
            ...super.defaults,
        };
    }

    lineGray = 'var(--ydb-darkgray)'
    lineRed = 'var(--ydb-status-red)'
    lineGreen = 'var(--ydb-status-green)'
    backlogErrorFore = 'var(--ydb-status-amber)'
    backlogDeadFore = 'var(--ydb-status-amber)'
    backlogActiveForeGt = 'black'
    backlogActiveForeGtWarn = 'red'
    backlogActiveBgndGt = 'lightYellow'
    backlogActiveForeEq = 'blue'
    backlogActiveBgndEq = 'lightgreen'

    setSourceBacklog(value, status) {
        // status === true => good, false, color it as it is not changing
        this.label(0, {
            attrs: {
                labelText: {
                    text: app.ui.formatThousands(value),
                    fill: value === 0 ? this.backlogActiveForeEq : this.backlogActiveForeGt,
                    stroke: status === true ? this.backlogActiveForeGtWarn : this.backlogActiveForeGt
                },
                labelBody: {
                    fill: value === 0 ? this.backlogActiveBgndEq : this.backlogActiveBgndGt,
                }
            }
        })
    }

    setTargetBacklog(value, status) {
        this.label(1, {
            attrs: {
                labelText: {
                    text: app.ui.formatThousands(value),
                    fill: value === 0 ? this.backlogActiveForeEq : this.backlogActiveForeGt,
                    stroke: status === true ? this.backlogActiveForeGtWarn : this.backlogActiveForeGt
                },
                labelBody: {
                    fill: value === 0 ? this.backlogActiveBgndEq : this.backlogActiveBgndGt,
                }
            }
        })
    }

    setLine(state) {        // dead, active, broken
        this.attr('line/stroke', state === 'dead' ? this.lineGray : state === 'active' ? this.lineGreen : this.lineRed)

        if (state === 'active') {
            this.attr('crossing/stroke', 'none')
            this.attr('crossing/d', '')

        } else {
            this.attr('crossing/stroke', state === 'dead' ? this.lineGray : this.lineRed)
            this.attr('crossing/d', 'M -10 -20 0 20 M 0 -20 10 20')
        }
    }

    setTargetState(state) {      // error, dead, broken
        this.label(1, {
            attrs: {
                labelText: {
                    fill: state === 'broken' ? this.backlogErrorFore : this.backlogDeadFore,
                },
                labelBody: {
                    fill: state === 'broken' ? this.lineRed : this.lineGray,
                }
            }
        })
    }

    setSourceState(state) {      // active, broken, dead
        this.label(0, {
            attrs: {
                labelText: {
                    fill: state === 'broken' ? this.backlogErrorFore : this.backlogDeadFore,
                },
                labelBody: {
                    fill: state === 'broken' ? this.lineRed : this.lineGray,
                }
            }
        })
    }
}


class ForeignObjectElement extends joint.dia.Element {
    defaults() {
        return {
            ...super.defaults,
        };
    }

    borderColor = 'var(--ydb-status-green)'
    dead = false

    MSG_CAN_NOT_CONNECT = 'Can not connect to server'
    MSG_INTERNAL_ERROR = 'Internal error occurred'

    MSG_NOREPL = 'Replication not running'

    MSG_UNKNOWN = 'Unknown'

    MSG_BAD_RESPONSE = 'Bad REST response'

    MSG_SSL_ERROR = "SSL error"

    MSG_BAD_TLS_FILE = "Bad TLS file"

    setSourceServers(health) {
        if (health === undefined) {
            this.attr('body/stroke', 'var(--ydb-status-red)')
            this.attr('body/stroke-width', 6)

            return
        }

        const healthy = health.sourcesGlobal.allAlive
        let popup = ''

        this.attr('sourceServersStatus/class', healthy === true ? 'repl-rect-status-healthy' : healthy === null ? 'repl-rect-status-warning' : 'repl-rect-status-error')
        this.attr('sourceServersStatus/html', healthy === true ? 'Healthy' : healthy === null ? 'Warning' : 'Error')
        this.attr('body/stroke', healthy === true ? this.borderColor : healthy === null ? 'var(--ydb-status-amber)' : 'var(--ydb-status-red)')
        this.attr('body/stroke-width', healthy === true ? 3 : 6)

        popup += '<div class="row" style="display: flex; padding-bottom: 0;">'
        popup += '<div class="col-4"><strong>Instance</strong></div>' +
            '<div class="col-4 align-right"><strong>PID</strong></div>' +
            '<div class="col-4"><strong>Mode</strong></div>'
        popup += '</div>'

        popup += '_'.repeat(43)

        health.sources.forEach((source, ix) => {
            popup += '<div class="row" style="display: flex;">'
            popup += '<div class="col-6 pu-width-100"><span class="' + (source.isAlive === true ? 'repl-popup-green' : 'repl-popup-red') + '">' + source.instanceName + '</span></div>' +
                '<div class="col-2 align-right pu-width-120"><span class="' + (source.isAlive === true ? 'repl-popup-green' : 'repl-popup-red') + '">' + source.pid + '</span></div>' +
                '<div class="col-4 pu-width-100"><span class="' + (source.isAlive === true ? 'repl-popup-green' : 'repl-popup-red') + '">' + source.mode.toUpperCase() + '</span></div>'
            popup += '</div>'

        })

        this.attr('sourceServersPopup/dataContent', popup)
    }

    setUpdateProcess(health) {
        if (health === undefined) {
            this.attr('body/stroke', 'var(--ydb-status-red)')
            this.attr('body/stroke-width', 6)

            return
        }
        if (!health.updateProcess) return

        const healthy = health.updateProcess.isAlive === true || health.updateProcess.isAlive === null
        let popup = ''

        this.attr('updateProcessStatus/class', healthy === true ? 'repl-rect-status-healthy' : 'repl-rect-status-error')
        this.attr('updateProcessStatus/html', healthy === true ? 'Healthy' : 'Error')
        this.attr('body/stroke', healthy === true ? this.borderColor : 'var(--ydb-status-red)')
        this.attr('body/stroke-width', healthy === true ? 3 : 6)

        if (healthy === true) {
            popup += '<div class="row" style="display: flex; padding-bottom: 0;">'
            popup += '<div class="col-6 align-left pu-width-80"><strong>PID</strong></div>'
            popup += '<div class="col-6 align-right pu-width-80"><span class="repl-popup-green">' + health.updateProcess.pid + '</span></div>'
            popup += '</div>'

        } else {
            popup = '<span class="repl-popup-only-text repl-popup-red">The process is NOT alive</span>'
        }

        this.attr('updateProcessPopup/dataContent', popup)
    }

    setReceiverServer(health) {
        if (health === undefined) {
            this.attr('body/stroke', 'var(--ydb-status-red)')
            this.attr('body/stroke-width', 6)

            return
        }

        if (!health.receiverServer) return

        const healthy = health.updateProcess.isAlive === true || health.updateProcess.isAlive === null
        let popup = ''

        this.attr('receiverServerStatus/class', healthy === true ? 'repl-rect-status-healthy' : 'repl-rect-status-error')
        this.attr('receiverServerStatus/html', healthy === true ? 'Healthy' : 'Error')
        this.attr('body/stroke', healthy === true ? this.borderColor : 'var(--ydb-status-red)')
        this.attr('body/stroke-width', healthy === true ? 3 : 6)

        if (healthy === true) {
            popup += '<div class="row" style="display: flex; padding-bottom: 0;">'
            popup += '<div class="col-6 align-left pu-width-80"><strong>PID</strong></div>'
            popup += '<div class="col-6 align-right pu-width-80"><span class="repl-popup-green">' + health.receiverServer.pid + '</span></div>'
            popup += '</div>'

        } else {
            popup = '<span class="repl-popup-only-text repl-popup-red">The process is NOT alive</span>'
        }

        this.attr('receiverServerPopup/dataContent', popup)
    }
}

class Primary extends ForeignObjectElement {
    bodyFill = '#ddffdd'

    defaults() {
        return {
            ...super.defaults(),
            type: 'Primary',
            size: {
                width: 320,
                height: 180
            },
            attrs: {
                body: {
                    rx: 10,
                    ry: 10,
                    width: 'calc(w)',
                    height: 'calc(h)',
                    stroke: '#green',
                    fill: '#ddffdd',
                    'stroke-width': 2
                },
                foreignObject: {
                    width: 'calc(w)',
                    height: 'calc(h)'
                }
            },
        };
    }

    preinitialize() {
        this.markup = joint.util.svg`
            <rect @selector="body" />
            <foreignObject @selector="foreignObject" overflow="hidden">
                <div @selector="content"
                    class="repl-rect-form"
                    xmlns="http://www.w3.org/1999/xhtml">
                    <div class="primary-header">
                        <span @selector="header"></span>
                    </div>
                    <div @selector="dead-pane" class="repl-dead-pane" style="display: none;">
                    </div>
                    <div @selector="alive-pane" style="display: block;">
                        <div class="row repl-rect-row" style="margin-bottom: 9px;">
                            <div class="col-4 repl-rect-col-left">
                                    <span >Role:</span>
                            </div>
                            <div class="col-8 repl-rect-col-right">
                                <span @selector="role"></span>
                            </div>   
                        </div>
                        <div class="row repl-rect-row" style="margin-bottom: 9px;">
                            <div class="col-6 repl-rect-col-left">
                                    Data access:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <span @selector="data-access"></span>
                            </div>                    
                        </div>
                        <div class="row repl-rect-row">
                            <div class="col-6 repl-rect-col-left">
                                    Source servers:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <a @selector="sourceServersPopup" tabindex="-1" role="button" data-placement="left" data-toggle="popover" data-trigger="hover" title="Source servers" data-content="" data-original-title="">                        
                                    <span @selector="sourceServersStatus"></span>
                                </a>
                            </div>                    
                        </div>
                   </div>
                </div>
            </foreignObject>
        `;
    }

    setDead(type) {
        let msg
        switch (type) {
            case 1:
                msg = this.MSG_CAN_NOT_CONNECT
                break
            case 2:
                msg = this.MSG_INTERNAL_ERROR
                break
            case 3:
                msg = this.MSG_UNKNOWN
                break
            case 4:
                msg = this.MSG_NOREPL
                break
            case 5:
                msg = this.MSG_BAD_RESPONSE
                break
            case 6:
                msg = this.MSG_SSL_ERROR
                break
            case 7:
                msg = this.MSG_BAD_TLS_FILE
                break
            default:
                msg = this.MSG_UNKNOWN
                break
        }

        this.attr('body/stroke', 'gray')
        this.attr('body/stroke-width', 6)
        this.attr('body/fill', 'lightgray')
        this.attr('sourceServersStatus/html', '   ')
        this.attr('sourceServersStatus/class', 'repl-rect-status-error')

        // visibility
        this.attr('alive-pane/style', 'display: none;')
        this.attr('dead-pane/style', 'display: block; padding-top: 12px;')
        this.attr('dead-pane/html', msg)

        this.dead = true
    }

    setAlive() {
        this.attr('body/stroke', this.borderColor)
        this.attr('body/stroke-width', 3)
        this.attr('body/fill', this.bodyFill)

        // visibility
        this.attr('alive-pane/style', 'display: block;')
        this.attr('dead-pane/style', 'display: none;')

        this.dead = false
    }
}

class Secondary extends ForeignObjectElement {
    defaults() {
        return {
            ...super.defaults(),
            type: 'Secondary',
            size: {
                width: 320,
                height: 220
            },
            attrs: {
                body: {
                    rx: 10,
                    ry: 10,
                    width: 'calc(w)',
                    height: 'calc(h)',
                    stroke: 'purple',
                    fill: '#ddddff',
                    'stroke-width': 2
                },
                foreignObject: {
                    width: 'calc(w)',
                    height: 'calc(h)'
                }
            },
        };
    }

    bodyFill = '#ddddff'
    preinitialize() {
        this.markup = joint.util.svg`
            <rect @selector="body" />
            <foreignObject @selector="foreignObject" overflow="hidden">
                <div @selector="content"
                    class="repl-rect-form"
                    xmlns="http://www.w3.org/1999/xhtml">
                    <div class="secondary-header">
                        <span @selector="header"></span>
                    </div>
                    <div @selector="dead-pane" class="repl-dead-pane" style="display: none;">
                    </div>
                    <div @selector="alive-pane" >
                        <div class="row repl-rect-row" style="margin-bottom: 9px;">
                            <div class="col-6 repl-rect-col-left">
                                    <span>Role:</span>
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <span @selector="role"></span>
                            </div>   
                        </div>
                        <div class="row repl-rect-row">
                            <div class="col-6 repl-rect-col-left">
                                    Data access:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <span @selector="data-access"></span>
                            </div>                    
                        </div>
                        <div class="row repl-rect-row">
                            <div class="col-6 repl-rect-col-left">
                                    Update process:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <a @selector="updateProcessPopup" tabindex="-1" role="button" data-placement="left" data-toggle="popover" data-trigger="hover" title="Update process" data-content="" data-original-title="">                        
                                    <span @selector="updateProcessStatus"></span>
                                </a>
                            </div>                    
                        </div>
                        <div class="row repl-rect-row">
                            <div class="col-6 repl-rect-col-left">
                                    Receiver server:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <a @selector="receiverServerPopup" tabindex="-1" role="button" data-placement="left" data-toggle="popover" data-trigger="hover" title="Receiver server" data-content="" data-original-title="">                        
                                    <span @selector="receiverServerStatus"></span>
                                </a>
                            </div>                    
                        </div>
                        <div class="row repl-rect-row">
                            <div class="col-6 repl-rect-col-left">
                                    Source servers:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <a @selector="sourceServersPopup" tabindex="-1" role="button" data-placement="left" data-toggle="popover" data-trigger="hover" title="Source servers" data-content="" data-original-title="">                        
                                    <span @selector="sourceServersStatus"></span>
                                </a>
                            </div>                    
                        </div>
                    </div>
                </div>
            </foreignObject>
        `;
    }

    setDead(type) {
        let msg
        switch (type) {
            case 1:
                msg = this.MSG_CAN_NOT_CONNECT
                break
            case 2:
                msg = this.MSG_INTERNAL_ERROR
                break
            case 3:
                msg = this.MSG_UNKNOWN
                break
            case 4:
                msg = this.MSG_NOREPL
                break
            case 5:
                msg = this.MSG_BAD_RESPONSE
                break
            case 6:
                msg = this.MSG_SSL_ERROR
                break
            case 7:
                msg = this.MSG_BAD_TLS_FILE
                break
            default:
                msg = this.MSG_UNKNOWN
                break
        }

        this.attr('body/stroke', 'gray')
        this.attr('body/stroke-width', 6)
        this.attr('body/fill', 'lightgray')
        this.attr('receiverServerStatus/html', '   ')
        this.attr('receiverServerStatus/class', 'repl-rect-status-error')
        this.attr('updateProcessStatus/html', '   ')
        this.attr('updateProcessStatus/class', 'repl-rect-status-error')
        this.attr('sourceServersStatus/html', '   ')
        this.attr('sourceServersStatus/class', 'repl-rect-status-error')

        // visibility
        this.attr('alive-pane/style', 'display: none;')
        this.attr('dead-pane/style', 'display: block;')
        this.attr('dead-pane/html', msg)

        this.dead = true
    }

    setAlive() {
        this.attr('body/stroke', this.borderColor)
        this.attr('body/stroke-width', 3)
        this.attr('body/fill', this.bodyFill)

        // visibility
        this.attr('alive-pane/style', 'display: block;')
        this.attr('dead-pane/style', 'display: none;')

        this.dead = false
    }
}

class Supplementary extends ForeignObjectElement {
    bodyFill = '#ffdddd'


    defaults() {
        return {
            ...super.defaults(),
            type: 'Primary',
            size: {
                width: 320,
                height: 220
            },
            attrs: {
                body: {
                    rx: 10,
                    ry: 10,
                    width: 'calc(w)',
                    height: 'calc(h)',
                    stroke: 'blue',
                    fill: '#ffdddd',
                    'stroke-width': 2
                },
                foreignObject: {
                    width: 'calc(w)',
                    height: 'calc(h)'
                }
            },
        };
    }

    preinitialize() {
        this.markup = joint.util.svg`
            <rect @selector="body" />
            <foreignObject @selector="foreignObject" overflow="hidden">
                <div @selector="content"
                    class="repl-rect-form"
                    xmlns="http://www.w3.org/1999/xhtml">
                    <div class="primary-header">
                        <span @selector="header"></span>
                    </div>
                    <div @selector="dead-pane" class="repl-dead-pane" style="display: none;">
                    </div>
                    <div @selector="alive-pane" style="display: block;">
                        <div class="row repl-rect-row" style="margin-bottom: 9px;">
                            <div class="col-4 repl-rect-col-left">
                                    <span >Role:</span>
                            </div>
                            <div class="col-8 repl-rect-col-right">
                                <span @selector="role"></span>
                            </div>   
                        </div>
                        <div class="row repl-rect-row">
                            <div class="col-6 repl-rect-col-left">
                                    Data access:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <span @selector="data-access"></span>
                            </div>                    
                        </div>
                        <div class="row repl-rect-row">
                            <div class="col-6 repl-rect-col-left">
                                    Update process:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <a @selector="updateProcessPopup" tabindex="-1" role="button" data-placement="left" data-toggle="popover" data-trigger="hover" title="Update process" data-content="" data-original-title="">                        
                                    <span @selector="updateProcessStatus"></span>
                                </a>
                            </div>                    
                        </div>
                        <div class="row repl-rect-row">
                            <div class="col-6 repl-rect-col-left">
                                    Receiver server:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <a @selector="receiverServerPopup" tabindex="-1" role="button" data-placement="left" data-toggle="popover" data-trigger="hover" title="Receive server" data-content="" data-original-title="">                        
                                    <span @selector="receiverServerStatus"></span>
                                </a>
                            </div>                    
                        </div>
                        <div class="row repl-rect-row">
                            <div class="col-6 repl-rect-col-left">
                                    Source servers:
                            </div>
                            <div class="col-6 repl-rect-col-right">
                                <a @selector="sourceServersPopup" tabindex="-1" role="button" data-placement="left" data-toggle="popover" data-trigger="hover" title="Source servers" data-content="" data-original-title="">                        
                                    <span @selector="sourceServersStatus"></span>
                                </a>
                            </div>                    
                        </div>
                    </div>
                </div>
            </foreignObject>`;
    }

    setDead(type) {
        let msg
        switch (type) {
            case 1:
                msg = this.MSG_CAN_NOT_CONNECT
                break
            case 2:
                msg = this.MSG_INTERNAL_ERROR
                break
            case 3:
                msg = this.MSG_UNKNOWN
                break
            case 4:
                msg = this.MSG_NOREPL
                break
            case 5:
                msg = this.MSG_BAD_RESPONSE
                break
            case 6:
                msg = this.MSG_SSL_ERROR
                break
            case 7:
                msg = this.MSG_BAD_TLS_FILE
                break
            default:
                msg = this.MSG_UNKNOWN
                break
        }

        this.attr('body/stroke', 'gray')
        this.attr('body/stroke-width', 6)
        this.attr('body/fill', 'lightgray')
        this.attr('receiverServerStatus/html', '   ')
        this.attr('receiverServerStatus/class', 'repl-rect-status-error')
        this.attr('updateProcessStatus/html', '   ')
        this.attr('updateProcessStatus/class', 'repl-rect-status-error')
        this.attr('sourceServersStatus/html', '   ')
        this.attr('sourceServersStatus/class', 'repl-rect-status-error')

        // visibility
        this.attr('alive-pane/style', 'display: none;')
        this.attr('dead-pane/style', 'display: block;')
        this.attr('dead-pane/html', msg)

        this.dead = true
    }

    setAlive() {
        this.attr('body/stroke', this.borderColor)
        this.attr('body/stroke-width', 3)
        this.attr('body/fill', this.bodyFill)

        // visibility
        this.attr('alive-pane/style', 'display: block;')
        this.attr('dead-pane/style', 'display: none;')

        this.dead = false
    }
}
