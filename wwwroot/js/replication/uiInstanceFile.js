/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.replication = {}

app.ui.replication.instanceFile = {
    init: function () {
        $('#btnReplInstanceFileHelp').on('click', () => this.help())

        app.ui.setupDialogForTest('modalReplInstanceFile')
    },

    show: function (instanceData = null) {
        if (instanceData === null) instanceData = app.system.replication.instanceFile

        // ********************************
        // populate the header
        // ********************************
        const tblReplInstanceFileHeader = $('#tblReplInstanceFileHeader > tbody')
        const tblReplInstanceFileSlots = $('#tblReplInstanceFileSlots > tbody')
        const tblReplInstanceFileHistory = $('#tblReplInstanceFileHistory > tbody')
        const target = {
            instance: [],
            lmsGroup: [],
            journalPool: [],
            receiver: [],
            flags: [],
            other: []
        }
        let rows = ''

        tblReplInstanceFileHeader.empty()

        this.populateTarget(target, instanceData)

        // instance
        rows += '<tr class="repl-instance-group"><td colspan="2">Instance</td></tr>'

        target.instance.forEach(entry => {
            rows += '<tr>'
            rows += '<td>' + entry.name + '</td>'
            rows += '<td>' + entry.value + '</td>'
            rows += '</tr>'
        })

        // LMS Group
        rows += '<tr class="repl-instance-group"><td colspan="2">LMS Group</td></tr>'

        target.lmsGroup.forEach(entry => {
            rows += '<tr>'
            rows += '<td>' + entry.name + '</td>'
            rows += '<td>' + entry.value + '</td>'
            rows += '</tr>'
        })

        // Journal pool
        rows += '<tr class="repl-instance-group"><td colspan="2">Journal Pool</td></tr>'

        target.journalPool.forEach(entry => {
            rows += '<tr>'
            rows += '<td>' + entry.name + '</td>'
            rows += '<td>' + entry.value + '</td>'
            rows += '</tr>'
        })

        // Receiver
        rows += '<tr class="repl-instance-group"><td colspan="2">Receiver</td></tr>'

        target.receiver.forEach(entry => {
            rows += '<tr>'
            rows += '<td>' + entry.name + '</td>'
            rows += '<td>' + entry.value + '</td>'
            rows += '</tr>'
        })

        // Flags
        rows += '<tr class="repl-instance-group"><td colspan="2">Flags</td></tr>'

        target.flags.forEach(entry => {
            rows += '<tr>'
            rows += '<td>' + entry.name + '</td>'
            rows += '<td>' + entry.value + '</td>'
            rows += '</tr>'
        })

        // Other
        rows += '<tr class="repl-instance-group"><td colspan="2">Other</td></tr>'

        target.other.forEach(entry => {
            rows += '<tr>'
            rows += '<td>' + entry.name + '</td>'
            rows += '<td>' + entry.value + '</td>'
            rows += '</tr>'
        })

        tblReplInstanceFileHeader.append(rows)

        // ********************************
        // populate the slots
        // ********************************
        rows = ''

        tblReplInstanceFileSlots.empty()

        instanceData.slots.forEach((slot, ix) => {
            rows += '<tr>'

            rows += '<td style="text-align: center;">' + ix + '</td>'
            rows += '<td style="text-align: right;">' + slot['Connect Sequence Number'] + '</td>'
            rows += '<td style="text-align: right;">' + slot['Resync Sequence Number'] + '</td>'
            rows += '<td style="text-align: center;">' + slot['Secondary Instance Name'] + '</td>'

            rows += '</tr>'
        })

        tblReplInstanceFileSlots.append(rows)

        // ********************************
        // populate the history
        // ********************************
        rows = ''

        tblReplInstanceFileHistory.empty()

        if (instanceData.history) {
            instanceData.history.forEach((line, ix) => {
                rows += '<tr>'

                rows += '<td style="text-align: center; vertical-align: middle; background: var(--ydb-lightgray); border: 1px solid black;" rowspan="' + Object.keys(line).length + '">' + ix + '</td>'
                for (let node in line) {
                    rows += '<td style="text-align: left;">' + line[node].slice(3) + '</td>'

                    rows += '</tr>'
                }
            })

            tblReplInstanceFileHistory.append(rows)
        }

        // set the first tab as default
        $('#tabStatsInstanceFileHeader').tab('show')

        $('#lblReplInstanceFileTitle').text(instanceData.flags.instanceName)

        // show the dialog
        $('#modalReplInstanceFile')
            .modal({show: true, backdrop: 'static'})
            .draggable({handle: '.modal-header'})
    },

    populateTarget: function (target, instanceData) {
        instanceData.header.forEach(entry => {
            if (entry.name.indexOf('Instance') === 0) {
                target.instance.push({
                    name: entry.name.slice(8),
                    value: entry.value
                })

                return
            }

            if (entry.name.indexOf('JournalPool') === 0) {
                target.journalPool.push({
                    name: entry.name.slice(11),
                    value: entry.value
                })

                return
            }

            if (entry.name.indexOf('LMSGroup') === 0) {
                target.lmsGroup.push({
                    name: entry.name.slice(8),
                    value: entry.value
                })

                return
            }

            if (entry.name.indexOf('Receive') === 0) {
                entry.name = entry.name.slice(7)
                target.receiver.push({
                    name: entry.name.slice(7),
                    value: entry.value
                })

                return
            }

            if (
                entry.name === 'Label(containsMajorVersion)' ||
                entry.name === 'MinorVersion' ||
                entry.name === 'EndianFormat' ||
                entry.name === '64-bitFormat' ||
                entry.name === 'EndianFormat'
            ) {
                target.other.push(entry)

                return
            }

            target.flags.push(entry)
        })
    },

    help: function () {
        const page = 'replication/dynamic/dialogs/instance-file-dialog'
        const path = window.location.origin + '/help/index.html?'

        window.open(path + page, 'ydbgui_help')
    },
}
