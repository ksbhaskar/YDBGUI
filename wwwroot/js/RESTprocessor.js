/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.REST._token = ''

// ********************************
// _debugMode
// ********************************
app.REST._debugMode = () => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'dashboard/debug-mode', {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        }, true)
    })
};

// ********************************
// _replGetBacklog
// ********************************
app.REST._replGetBacklog = () => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'replication/backlog', {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        }, true)
    })
};

// ********************************
// _replGetAll
// ********************************
app.REST._replGetAll = () => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'replication/full-info', {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        }, true)
    })
};

// ********************************
// _replTopology
// ********************************
app.REST._replTopology = (params) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'replication/topology', params, data => {
            resolve(data)

        }, err => {
            reject(err)
        }, true)
    })
};

// ********************************
// _ygblEnumPids
// ********************************
app.REST._ygblEnumPids = (regions) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'ygbl/get-pids?regions=' + regions.join(','), {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _wsStart
// ********************************
app.REST._wsStart = () => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'ws/start?logging=' + (window.location.search.indexOf('?ws-logging') > -1 ? 'true' : 'false'), {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _validateGld
// ********************************
app.REST._validateGld = (gldPath, envVars = [], cwd = '') => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/gld/validate', {path: gldPath, envVars: envVars, cwd: cwd}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _getViewInfo
// ********************************
app.REST._getViewInfo = view => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'octo/views/' + view + '/get-struct', {}, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, false)
    })
};

// ********************************
// _getTableInfo
// ********************************
app.REST._getTableInfo = table => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'octo/tables/' + table + '/get-struct', {}, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, false)
    })
};

// ********************************
// _fetchOctoTree
// ********************************
app.REST._fetchOctoTree = () => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'octo/get-objects', {}, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// _executeOctoQuery
// ********************************
app.REST._executeOctoQuery = (query, timeout = 5000) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'octo/execute', {query: query, timeout: timeout}, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// _getAuth
// ********************************
app.REST._getAuth = () => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'auth-mode', {}, data => {
            resolve(data)

        }, err => {
            reject(err)

        })
    })
};

// ********************************
// _getGlobals
// ********************************
app.REST._getGlobals = (regionName) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'globals/by-region/' + regionName, {}, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// _getGlobalSize
// ********************************
app.REST._getGlobalSize = (type, param, region) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'globals/size?type=' + type + '&param=' + param + '&region=' + region, {}, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// _getZroutines
// ********************************
app.REST._getZroutines = () => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'server/zroutines', {}, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// journal file switch
// ********************************
app.REST._JournalFilesSwitch = region => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/' + region + '/journal-file-switch', {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _getJournalInfo
// ********************************
app.REST._getJournalInfo = (region) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'regions/' + region + '/get-journal-info', {}, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// _backup
// ********************************
app.REST._backup = (payload) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/maintenance/backup', payload, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// _pathExpand
// ********************************
app.REST._pathExpand = (path, dirsOnly = true) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'os/fs/path-expand', {path: path, dirsOnly: dirsOnly}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _ls
// ********************************
app.REST._ls = (path, type = 'D', mask = '*') => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'os/fs/ls', {
            path: path,
            type: type,
            mask: mask
        }, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _integ
// ********************************
app.REST._integ = (params) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/maintenance/integ', params, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// _defrag
// ********************************
app.REST._defrag = (params) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/maintenance/defrag', params, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// _getRoutine
// ********************************
app.REST._getRoutine = (routine) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'routines/', {
            routine: routine.replace(/%/g, '_'),
            zroutines: app.ui.rViewer.searchPath.zroutines
        }, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _findRoutines
// ********************************
app.REST._findRoutines = (mask) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'routines/find', {
            mask: mask.replace(/%/g, '_'),
            zroutines: app.ui.rViewer.searchPath.zroutines
        }, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _getData
// ********************************
app.REST._getData = (path) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'globals/get-data', {path: path}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _dollarData
// ********************************
app.REST._dollarData = (globalName) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'globals/dollar-data?globalName=' + globalName, {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _globalsFind
// ********************************
app.REST._globalsFind = (globalMask, size = 0) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'globals/find?mask=' + globalMask + '&size=' + size, {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _globalsTraverse
// ********************************
app.REST._globalsTraverse = (payload) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'globals/traverse', payload, data => {
            resolve(data)

        }, err => {
            reject(err)

        }, true)
    })
};

// ********************************
// terminates a process
// ********************************
app.REST._terminateProcess = (PID) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'os/processes/' + PID + '/terminate', {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// clears a lock
// ********************************
app.REST._clearLock = (lockNamespace) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/locks/clear?namespace=' + lockNamespace, {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// Get locks info
// ********************************
app.REST._getAllLocks = () => {
    return new Promise(function (resolve, reject) {

        app.REST.execute('get', 'regions/locks/get-all', {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// Add region
// ********************************
app.REST._addRegion = (payload) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/add', payload, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// Edit region
// ********************************
app.REST._editRegion = (payload) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/' + payload.regionName + '/edit', payload, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// Validate path
// ********************************
app.REST._validatePath = (path) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/validate-path', {path: path}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// Parse namespace
// ********************************
app.REST._parseNamespace = (namespace) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/parse-namespace', {namespace: namespace}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// Get templates
// ********************************
app.REST._getTemplates = () => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'dashboard/get-templates', {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// Get dashboard info
// ********************************
app.REST._dashboardGetAll = () => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('get', 'dashboard/get-all', {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// Get region info
// ********************************
app.REST._regionGet = (region) => {
    return new Promise(function (resolve, reject) {

        app.REST.execute('get', 'regions/' + region, {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// delete region
// ********************************
app.REST._regionDelete = (region, withDelete) => {
    return new Promise(function (resolve, reject) {

        app.REST.execute('delete', 'regions/' + region + '?' + (withDelete === true ? 'deleteFiles=true' : ''), {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// create region Db file
// ********************************
app.REST._regionCreateDbFile = (region) => {
    return new Promise(function (resolve, reject) {

        app.REST.execute('post', 'regions/' + region + '/create-db', {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// extend region(blocks)
// ********************************
app.REST._regionExtend = (region, blocks) => {
    return new Promise(function (resolve, reject) {
        app.REST.execute('post', 'regions/' + region + '/extend?blocks=' + blocks, {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// switch region(mode)
// state: 'on' || 'off'
// ********************************
app.REST._JournalSwitch = (region, mode) => {
    return new Promise(function (resolve, reject) {
        if (mode !== 'on' && mode !== 'off') {
            reject({
                err: {
                    description: 'The mode parameter is invalid: ' + mode
                }
            });

            return
        }

        app.REST.execute('post', 'regions/' + region + '/journal-switch?turn=' + mode, {}, data => {
            resolve(data)

        }, err => {
            reject(err)
        })
    })
};

// ********************************
// _login
// ********************************
app.REST._login = (DATA = {}, okCallback, errCallback) => {
    //debug info
    if (app.userSettings.rest.logOnConsole === 'yes') console.log('REST command: LOGIN');
    return new Promise(function (resolve, reject) {

        $.ajax({
            url: '/api/login',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(DATA),
            type: 'POST',
            crossDomain: true,
            timeout: app.userSettings.rest.timeout.short * 1000,
            success: (data) => {
                //debug info
                if (app.userSettings.rest.logOnConsole === 'yes') {
                    console.log('Login OK');
                    console.log(data)
                }
                app.REST._token = data.token
                resolve(data);
            },
            error: (err) => {
                //debug info
                if (app.userSettings.rest.logOnConsole === 'yes') {
                    console.log('Login error: ');
                    console.dir(err);
                }
                reject(err);
            }
        });
    })
};

// ********************************
// _logout
// ********************************
app.REST._logout = (DATA = {}, okCallback, errCallback) => {
    //debug info
    if (app.userSettings.rest.logOnConsole === 'yes') console.log('REST command: LOGOUT');

    return new Promise(function (resolve, reject) {

        $.ajax({
            url: '/api/logout',
            contentType: 'application/json',
            dataType: 'json',
            data: {},
            type: 'GET',
            crossDomain: true,
            headers: {'Authorization': 'Bearer ' + app.REST._token},
            timeout: app.userSettings.rest.timeout.short * 1000,
            success: (data) => {
                //debug info
                if (app.userSettings.rest.logOnConsole === 'yes') console.log('Logout OK');
                resolve(data);
            },
            error: (err) => {
                if (err.status !== 403) {
                    //debug info
                    if (app.userSettings.rest.logOnConsole === 'yes') {
                        console.log('Logout error: ');
                        console.dir(err);
                    }
                    reject(err);
                }
            }
        });
    })
};

// ********************************
// Global Ajax handlers
// ********************************
// this is used in the tests
app.REST.testExecuted = false

app.REST.execute = (type, command, DATA = {}, okCallback, errCallback, longTimeout = false) => {
    // set base path
    command = '/api/' + command

    // process headers
    const headers = {'Authorization': 'Bearer ' + app.REST._token}
    if (app.ui.envSettings.gld !== '') {
        Object.assign(headers, {'x-ydb-global-directory': app.ui.envSettings.gld})
        console.log('Using alternate Global Directory: ' + app.ui.envSettings.gld)
    }

    if (app.ui.envSettings.cwd !== '') {
        Object.assign(headers, {'x-ydb-working-directory': app.ui.envSettings.cwd})
        console.log('Using alternate Working Directory: ' + app.ui.envSettings.cwd)
    }

    if (app.ui.envSettings.envVars !== '') {
        Object.assign(headers, {'x-ydb-env-vars': app.ui.envSettings.envVars})
        console.log('Using alternate Environment Variables: ' + app.ui.envSettings.envVars)
    }

    //debug info
    if (app.userSettings.rest.logOnConsole === 'yes') {
        console.log('REST command: ' + command);
        if (Object.values(DATA).length > 0) console.log(DATA)
    }

    app.REST.testExecuted = true

    $.ajax({
        url: command,
        contentType: 'application/json',
        dataType: 'json',
        data: type === 'post' ? JSON.stringify(DATA) : '',
        type: type,
        crossDomain: true,
        headers: headers,
        timeout: longTimeout === true ? (app.userSettings.rest.timeout.long * 1000) : (app.userSettings.rest.timeout.short * 1000),
        success: (data) => {
            //debug info
            if (app.userSettings.rest.logOnConsole === 'yes') {
                console.log('REST success: ');
                console.log(JSON.parse(JSON.stringify(data)))
            }
            okCallback(data);
        },
        error: async (err) => {
            if (await app.REST.processTimeout(err) === true) return

            //debug info
            if (app.userSettings.rest.logOnConsole === 'yes') {
                console.log('REST error: ');
                console.dir(err);
            }
            errCallback(err);
        }
    });
};

app.REST.parseError = err => {
    let message = 'The following error occurred:&nbsp;';

    if (err.status !== undefined && err.status === 500) {
        if (typeof err.responseJSON === 'object') {
            const error = err.responseJSON.error.errors[0];

            message += '<br>';
            message += '<br>Location: ' + error.message.place;
            message += '<br>Mcode: ' + error.message.mcode;
            message += '<br>Error: ' + error.message.message;

        } else {
            if (err.error !== undefined) {
                message += err.error.description

                if (err.error.dump !== undefined) {
                    message += '<br>'

                    err.error.dump.forEach(line => {
                        message += line + '<br>'
                    })
                }

            } else {
                message += 'Internal error ' + err.responseText
            }
        }
    } else if (err.error !== undefined) {
        message += err.error.description

        if (err.error.dump !== undefined) {
            message += '<br><br>'

            err.error.dump.forEach(line => {
                message += line + '<br>'
            })
        }
    } else {
        let error

        if (err.statusText === 'timeout' && err.status === 0) {
            error = 'Can not connect to the REST server...<br><br>Timeout'

        } else {
            error = err.toString()
        }

        message += error
    }

    return message
};

app.REST.processTimeout = async err => {
    if (err.status === 408) {

        console.log('Token timeout detected...')

        $('#modalWait').modal('hide')
        await app.ui.msgbox.show('You session has timed out. You will need to login again...', 'WARNING', true)

        window.location.replace(window.location.origin)

        return true

    } else return false
}
