/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.gViewer.data = {};

// **********************************************
// DATA FETCHING
// **********************************************

app.ui.gViewer.data.initialFetch = async instance => {
    let payload = {
        namespace: {
            global: instance.viewer.buffer.original.global,
            subscripts: instance.viewer.buffer.original.subscripts,
        },
        mode: 'head',
        size: instance.viewer.buffer.defaults.buffering.initialFetchSize,
        dataSize: instance.viewer.buffer.defaults.maxDataLength,
    };

    try {
        const buffering = instance.viewer.buffer.defaults.buffering;

        $('body').addClass('wait');

        const res = await app.REST._globalsTraverse(payload);

        if (res.result === 'ERROR') {
            $('body').removeClass('wait');

            app.ui.msgbox.show('The following error occurred while fetching the data:\n' + res.error.description, 'ERROR');

            return -1
        }

        // return if no records found
        if (res.data.recordCount === 0) {
            $('body').removeClass('wait');

            $('#altGviewerNoRecordsFound' + instance.tabId).css('display', 'block');

            return 0;
        }
        // populate the array holding the data
        instance.viewer.buffer.data = res.data.list;
        // init the bookmarks array
        instance.viewer.buffer.bookmarks = [];
        for (let ix = 0; ix < res.data.list.length; ix++) {
            instance.viewer.buffer.bookmarks[ix] = ''
        }
        // update the total count
        instance.viewer.buffer.dataCount = res.data.recordCount;
        instance.viewer.buffer.baseOffset = 0;
        // set the tail
        instance.viewer.buffer.tail = instance.viewer.buffer.data[instance.viewer.buffer.data.length - 1].path;
        // check if there are more records to fetch or not
        instance.viewer.buffer.tailNoMoreRecords = instance.viewer.buffer.data.length !== buffering.initialFetchSize;
        // reset the head
        instance.viewer.buffer.headNoMoreRecords = true;
        instance.viewer.buffer.head = 0;
        // and the scrolling params
        instance.viewer.buffer.scrollLast = 0;
        instance.viewer.buffer.scrolling = false;

        $('body').removeClass('wait');

    } catch (err) {
        console.log(err);

        $('body').removeClass('wait');

        app.ui.msgbox.show('The following error occurred while fetching the data:\n' + err.message, 'ERROR');

        return -1
    }

    return instance.viewer.buffer.dataCount
};

app.ui.gViewer.data.scroll = async tabId => {
    const instance = app.ui.gViewer.instance[tabId];
    const buffering = instance.viewer.buffer.defaults.buffering;

    const divGviewerTableScroll = $('#divGviewerTableScroll' + tabId);
    const tblGviewerTable = $('#tblGviewerTable' + tabId);

    const triggerValueDown = tblGviewerTable.height() - (divGviewerTableScroll.height() * buffering.startFetchingPages);
    const triggerValueUp = divGviewerTableScroll.height() * buffering.startFetchingPages;
    const currentTop = divGviewerTableScroll.scrollTop();

    if (instance.viewer.table.scrollLocked === true) return

    if (instance.viewer.buffer.scrollLast > currentTop) {
        // ******************************************
        // scrolling up
        // ******************************************
        if (currentTop < triggerValueUp && instance.viewer.buffer.headNoMoreRecords === false && instance.viewer.buffer.headScrolling === false) {
            if (instance.viewer.buffer.scrollMode === 'head') {
                // ***********************************
                // head up
                // ***********************************
                let abortClicked = false;

                let payload = {
                    namespace: {
                        global: instance.viewer.buffer.original.global,
                        subscripts: instance.viewer.buffer.original.subscripts,
                    },
                    mode: 'head',
                    size: instance.viewer.buffer.defaults.buffering.lazyFetchSize,
                    upOffset: Math.abs(instance.viewer.buffer.baseOffset) - instance.viewer.buffer.defaults.buffering.lazyFetchSize,
                    direction: 'up',
                    dataSize: instance.viewer.buffer.defaults.maxDataLength,
                };

                try {
                    if (Math.abs(instance.viewer.buffer.baseOffset) < 5000) {
                        $('body').addClass('wait');

                    } else {
                        app.ui.wait.show("Scrolling up...", () => {
                            abortClicked = true
                        });
                    }

                    instance.viewer.buffer.headScrolling = true;
                    const res = await app.REST._globalsTraverse(payload);

                    if (res.data.list === undefined) res.data.list = [];

                    instance.viewer.buffer.headScrolling = false;

                    if (res.result === 'ERROR') {
                        $('body').removeClass('wait');

                        app.ui.wait.hide()

                        app.ui.msgbox.show('The following error occurred while fetching the data:\n' + data.error.description, 'ERROR');

                        return
                    }
                    if (res.data.list.length === 0) {
                        // and flag the tail as "fetch no more, we reached the end"
                        instance.viewer.buffer.headNoMoreRecords = true;

                    } else {
                        // trim the existing data array
                        instance.viewer.buffer.data.splice(instance.viewer.buffer.data.length - res.data.list.length, res.data.list.length);
                        // insert the data into array
                        instance.viewer.buffer.data = res.data.list.concat(instance.viewer.buffer.data);
                        // betail the table
                        app.ui.gViewer.table.betail(tabId, res.data.list.length);
                        // set new range
                        range = '0-' + (res.data.list.length).toString();
                        // reset the previous scroll value
                        instance.viewer.buffer.scrollLast = 0;
                        // and flag it for tail fetching
                        instance.viewer.buffer.tailNoMoreRecords = false;
                        // update the baseOffset
                        instance.viewer.buffer.baseOffset += res.data.list.length;
                        if (instance.viewer.buffer.baseOffset > 0) instance.viewer.buffer.baseOffset = 0;
                        // and flag the tail as "can fetch more"if this is the case
                        instance.viewer.buffer.headNoMoreRecords = instance.viewer.buffer.baseOffset === 0;

                        // populate the table
                        app.ui.gViewer.table.populate(tabId, 'insert', range, payload.upOffset);

                        $('body').removeClass('wait');

                        app.ui.wait.hide()

                        app.ui.gViewer.table.setFocus(tabId)
                    }

                } catch (err) {
                    instance.viewer.buffer.scrolling = false;

                    $('body').removeClass('wait');

                    app.ui.wait.hide()

                    console.log(err);

                    app.ui.msgbox.show('The following error occurred while fetching the data:\n' + err.message, 'ERROR');
                }

            } else {
                // ***********************************
                // tail up
                // ***********************************
                let abortClicked = false;

                let payload = {
                    namespace: {
                        global: instance.viewer.buffer.original.global,
                        subscripts: instance.viewer.buffer.original.subscripts,
                    },
                    mode: 'tail',
                    direction: 'up',
                    upOffset: instance.viewer.buffer.head,
                    size: instance.viewer.buffer.defaults.buffering.lazyFetchSize,
                    dataSize: instance.viewer.buffer.defaults.maxDataLength,
                };

                try {
                    if (Math.abs(instance.viewer.buffer.baseOffset) < 5000) {
                        $('body').addClass('wait');

                    } else {
                        app.ui.wait.show("Scrolling up...", () => {
                            abortClicked = true
                        });
                    }

                    instance.viewer.buffer.headScrolling = true;
                    const res = await app.REST._globalsTraverse(payload);

                    if (res.data.list === undefined) res.data.list = [];

                    instance.viewer.buffer.headScrolling = false;

                    if (res.result === 'ERROR') {
                        $('body').removeClass('wait');

                        app.ui.wait.hide()

                        app.ui.msgbox.show('The following error occurred while fetching the data:\n' + data.error.description, 'ERROR');

                        return
                    }
                    if (res.data.list.length === 0) {
                        // and flag the head as "fetch no more, we reached the end"
                        instance.viewer.buffer.headNoMoreRecords = true;

                    } else {
                        // trim the existing data array
                        instance.viewer.buffer.data.splice(instance.viewer.buffer.data.length - res.data.list.length, res.data.list.length);
                        // insert the data into array
                        instance.viewer.buffer.data = res.data.list.concat(instance.viewer.buffer.data);
                        // betail the table
                        app.ui.gViewer.table.betail(tabId, res.data.list.length);
                        // set new range
                        range = '0-' + (res.data.list.length).toString();
                        // reset the previous scroll value
                        instance.viewer.buffer.scrollLast = 0;
                        instance.viewer.buffer.head = res.data.list[0].path
                        // and flag it for tail fetching
                        instance.viewer.buffer.tailNoMoreRecords = false;
                        // update the baseOffset
                        instance.viewer.buffer.baseOffset += res.data.list.length;
                        if (instance.viewer.buffer.baseOffset > 0) instance.viewer.buffer.baseOffset = 0;
                        // and flag the head as "can fetch more"if this is the case
                        instance.viewer.buffer.headNoMoreRecords = res.data.list.length !== buffering.lazyFetchSize
                        // set the tail
                        instance.viewer.buffer.tail = instance.viewer.buffer.data[instance.viewer.buffer.data.length - 1].path;

                        instance.viewer.buffer.tailTracker.push({
                            head: instance.viewer.buffer.head,
                            tail: instance.viewer.buffer.tail
                        })
                        // populate the table
                        app.ui.gViewer.table.populate(tabId, 'insert', range, payload.upOffset);

                        $('body').removeClass('wait');

                        app.ui.wait.hide()

                        app.ui.gViewer.table.setFocus(tabId)
                    }

                } catch (err) {
                    instance.viewer.buffer.scrolling = false;

                    $('body').removeClass('wait');

                    app.ui.wait.hide()

                    console.log(err);

                    app.ui.msgbox.show('The following error occurred while fetching the data:\n' + err.message, 'ERROR');
                }
            }
        }

    } else {
        instance.viewer.buffer.scrollLast = currentTop;

        // ******************************************
        // scrolling down
        // ******************************************

        if (currentTop > triggerValueDown && instance.viewer.buffer.tailNoMoreRecords === false && instance.viewer.buffer.scrolling === false) {
            if (instance.viewer.buffer.original.global === undefined) return;

            if (instance.viewer.buffer.scrollMode === 'head') {
                // ***********************************
                // head down
                // ***********************************
                let payload = {
                    namespace: {
                        global: instance.viewer.buffer.original.global,
                        subscripts: instance.viewer.buffer.original.subscripts,
                    },
                    mode: 'head',
                    size: instance.viewer.buffer.defaults.buffering.lazyFetchSize,
                    offset: instance.viewer.buffer.tail,
                    dataSize: instance.viewer.buffer.defaults.maxDataLength,
                };

                try {
                    instance.viewer.buffer.scrolling = true;
                    let abortClicked = false;


                    if (Math.abs(instance.viewer.buffer.baseOffset) < 5000) {
                        $('body').addClass('wait');

                    } else {
                        app.ui.wait.show("Scrolling down...", () => {
                            abortClicked = true
                        });
                    }

                    const res = await app.REST._globalsTraverse(payload);

                    if (abortClicked === true) {
                        $('body').removeClass('wait');

                        return
                    }

                    if (res.data.list === undefined) res.data.list = [];

                    instance.viewer.buffer.scrolling = false;

                    if (res.result === 'ERROR') {
                        $('body').removeClass('wait');

                        app.ui.wait.hide()

                        app.ui.msgbox.show('The following error occurred while fetching the data:\n' + data.error.description, 'ERROR');

                        return
                    }
                    if (res.data.list.length === 0) {
                        // and flag the tail as "fetch no more, we reached the end"
                        instance.viewer.buffer.tailNoMoreRecords = true;

                    } else {
                        // at first, we compute the new range before we overwrite vars
                        let range = (instance.viewer.buffer.dataCount).toString() + '-' + (instance.viewer.buffer.dataCount + res.data.list.length).toString();

                        // append the data into array (spread operator is no prob, as res.data..list never exceed 100,000 records)
                        instance.viewer.buffer.data.push(...res.data.list);
                        // init the bookmarks array
                        for (let ix = 0; ix < res.data.list.length; ix++) {
                            instance.viewer.buffer.bookmarks.push('');
                        }
                        // update the total count
                        instance.viewer.buffer.dataCount = res.data.recordCount + instance.viewer.buffer.dataCount;
                        // set the new tail
                        instance.viewer.buffer.tail = instance.viewer.buffer.data[instance.viewer.buffer.data.length - 1].path;
                        // and flag the tail as "can fetch more"if this is the case
                        instance.viewer.buffer.tailNoMoreRecords = res.data.list.length !== buffering.lazyFetchSize;

                        // check if we need to remove records in the head
                        if (instance.viewer.buffer.dataCount > buffering.maxSize) {
                            // behead the table
                            app.ui.gViewer.table.behead(tabId, res.data.list.length);
                            // trim the data array
                            instance.viewer.buffer.data.splice(0, res.data.list.length);
                            // update counters
                            instance.viewer.buffer.dataCount -= res.data.list.length;
                            // set new range
                            range = (instance.viewer.buffer.data.length - res.data.list.length).toString() + '-' + (instance.viewer.buffer.data.length).toString();
                            // reset the previous scroll value
                            instance.viewer.buffer.scrollLast = 0;
                            // and flag it for head fetching
                            instance.viewer.buffer.headNoMoreRecords = false;
                            // update the baseOffset
                            instance.viewer.buffer.baseOffset -= res.data.list.length;
                        }
                        // populate the table
                        app.ui.gViewer.table.populate(tabId, 'append', range, instance.viewer.buffer.baseOffset);

                        $('body').removeClass('wait');

                        app.ui.wait.hide()

                        app.ui.gViewer.table.setFocus(tabId)
                    }

                } catch (err) {
                    instance.viewer.buffer.scrolling = false;

                    $('body').removeClass('wait');
                    app.ui.wait.hide()

                    console.log(err);

                    app.ui.msgbox.show('The following error occurred while fetching the data:\n' + err.message, 'ERROR');
                }

            } else {
                // ***********************************
                // tail down
                // ***********************************
                if (instance.viewer.buffer.tailTracker.length === 1) {
                    instance.viewer.buffer.tailTracker = []

                    const res = await app.ui.gViewer.data.endFetch(instance)

                    // populate table using default values if recordCount > 0
                    if (res > 0) {
                        app.ui.gViewer.table.populate(tabId);

                        // set the scroll value at the end
                        await $('#divGviewerTableScroll' + tabId).scrollTop(100000);
                        setTimeout(() => {
                            instance.viewer.table.scrollLocked = false
                        }, 250)
                    }

                    return
                }

                if (instance.viewer.buffer.tailTracker.pop() === undefined) return

                let payload = {
                    namespace: {
                        global: instance.viewer.buffer.original.global,
                        subscripts: instance.viewer.buffer.original.subscripts,
                    },
                    mode: 'tail',
                    size: instance.viewer.buffer.defaults.buffering.lazyFetchSize,
                    upOffset: instance.viewer.buffer.tailTracker.pop().tail,
                    dataSize: instance.viewer.buffer.defaults.maxDataLength,
                };

                try {
                    instance.viewer.buffer.scrolling = true;
                    let abortClicked = false;


                    if (Math.abs(instance.viewer.buffer.baseOffset) < 5000) {
                        $('body').addClass('wait');

                    } else {
                        app.ui.wait.show("Scrolling down...", () => {
                            abortClicked = true
                        });
                    }

                    const res = await app.REST._globalsTraverse(payload);

                    if (abortClicked === true) {
                        $('body').removeClass('wait');

                        return
                    }

                    if (res.data.list === undefined) res.data.list = [];

                    instance.viewer.buffer.scrolling = false;

                    if (res.result === 'ERROR') {
                        $('body').removeClass('wait');

                        app.ui.wait.hide()

                        app.ui.msgbox.show('The following error occurred while fetching the data:\n' + data.error.description, 'ERROR');

                        return
                    }
                    if (res.data.list.length === 0) {
                        // and flag the tail as "fetch no more, we reached the end"
                        instance.viewer.buffer.tailNoMoreRecords = true;

                    } else {
                        // at first, we compute the new range before we overwrite vars
                        let range = (instance.viewer.buffer.dataCount).toString() + '-' + (instance.viewer.buffer.dataCount + res.data.list.length - 1).toString();

                        // append the data into array (spread operator is no prob, as res.data..list never exceed 100,000 records)
                        instance.viewer.buffer.data.push(...res.data.list);
                        // init the bookmarks array
                        for (let ix = 0; ix < res.data.list.length; ix++) {
                            instance.viewer.buffer.bookmarks.push('');
                        }
                        // update the total count
                        instance.viewer.buffer.dataCount = res.data.recordCount + instance.viewer.buffer.dataCount;
                        // set the new tail
                        instance.viewer.buffer.tail = instance.viewer.buffer.data[instance.viewer.buffer.data.length - 1].path;
                        // and flag the tail as "can fetch more"if this is the case
                        instance.viewer.buffer.tailNoMoreRecords = res.data.list.length !== buffering.lazyFetchSize

                        instance.viewer.buffer.head = res.data.list[0].path;

                        // check if we need to remove records in the head
                        if (instance.viewer.buffer.dataCount > buffering.maxSize) {
                            // behead the table
                            app.ui.gViewer.table.behead(tabId, res.data.list.length);
                            // trim the data array
                            instance.viewer.buffer.data.splice(0, res.data.list.length);
                            // update counters
                            instance.viewer.buffer.dataCount -= res.data.list.length;
                            // set new range
                            range = (instance.viewer.buffer.data.length - res.data.list.length).toString() + '-' + (instance.viewer.buffer.data.length).toString();
                            // reset the previous scroll value
                            instance.viewer.buffer.scrollLast = 0;
                            // and flag it for head fetching
                            instance.viewer.buffer.headNoMoreRecords = false;
                            // update the baseOffset
                            instance.viewer.buffer.baseOffset -= res.data.list.length;
                        }
                        // populate the table
                        app.ui.gViewer.table.populate(tabId, 'append', range, instance.viewer.buffer.baseOffset);

                        $('body').removeClass('wait');

                        app.ui.wait.hide()

                        app.ui.gViewer.table.setFocus(tabId)
                    }

                } catch (err) {
                    instance.viewer.buffer.scrolling = false;

                    $('body').removeClass('wait');
                    app.ui.wait.hide()

                    console.log(err);

                    app.ui.msgbox.show('The following error occurred while fetching the data:\n' + err.message, 'ERROR');
                }
            }
        }
    }
};

app.ui.gViewer.data.endFetch = async instance => {
    let upOffset
    const sub0 = instance.viewer.buffer.original.subscripts[0]

    if (sub0.type === '*') {
        upOffset = instance.viewer.buffer.original.global + '("~")'

    } else {
        if (sub0.type === 'V') {
            if (parseFloat(sub0.value) !== 0) {
                upOffset = instance.viewer.buffer.original.global + '("' + (parseFloat(sub0.value) + .00001) + '")'

            } else {
                upOffset = instance.viewer.buffer.original.global + '("' + (sub0.value + '~') + '")'
            }

        } else if (sub0.type === ':') {
            upOffset = instance.viewer.buffer.original.global + '("' + sub0.valueEnd + '","~")'

        } else {
            upOffset = instance.viewer.buffer.original.global + '("~")'
        }
    }

    let payload = {
        namespace: {
            global: instance.viewer.buffer.original.global,
            subscripts: instance.viewer.buffer.original.subscripts,
        },
        mode: 'tail',
        direction: 'up',
        upOffset: upOffset,
        size: instance.viewer.buffer.defaults.buffering.initialFetchSize,
        dataSize: instance.viewer.buffer.defaults.maxDataLength,
    };

    try {
        const buffering = instance.viewer.buffer.defaults.buffering;

        $('body').addClass('wait');

        const res = await app.REST._globalsTraverse(payload);

        if (res.result === 'ERROR') {
            $('body').removeClass('wait');

            app.ui.msgbox.show('The following error occurred while fetching the data:\n' + res.error.description, 'ERROR');

            return -1
        }

        // return if no records found
        if (res.data.list === undefined) {
            $('body').removeClass('wait');

            $('#altGviewerNoRecordsFound' + instance.tabId).css('display', 'block');

            return 0;
        }
        // populate the array holding the data
        instance.viewer.buffer.data = res.data.list;
        // init the bookmarks array
        instance.viewer.buffer.bookmarks = [];
        for (let ix = 0; ix < res.data.list.length; ix++) {
            instance.viewer.buffer.bookmarks[ix] = ''
        }
        // update the total count
        instance.viewer.buffer.dataCount = res.data.recordCount;
        instance.viewer.buffer.baseOffset = 0;
        // set the tail
        instance.viewer.buffer.tail = instance.viewer.buffer.data[instance.viewer.buffer.data.length - 1].path;
        // check if there are more records to fetch or not
        instance.viewer.buffer.tailNoMoreRecords = true
        // reset the head
        instance.viewer.buffer.headNoMoreRecords = instance.viewer.buffer.data.length !== buffering.initialFetchSize
        instance.viewer.buffer.head = instance.viewer.buffer.data[0].path;
        // and the scrolling params
        instance.viewer.buffer.scrollLast = 0;
        instance.viewer.buffer.scrolling = false;

        instance.viewer.buffer.tailTracker = []
        instance.viewer.buffer.tailTracker.push({
            head: instance.viewer.buffer.head,
            tail: instance.viewer.buffer.tail
        })

        $('body').removeClass('wait');

    } catch (err) {
        console.log(err);

        $('body').removeClass('wait');

        app.ui.msgbox.show('The following error occurred while fetching the data:\n' + err.message, 'ERROR');

        return -1
    }

    return instance.viewer.buffer.dataCount
};

