/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.gViewer.settings.updateSettings = () => {
    // here all fields listed

    app.ui.gViewer.settings.params.forEach(item => {
        if (item.oldValue !== item.newValue) {
            switch (item.id) {
                case 'inpGviewerSettingsTotalBufferSize': {
                    app.userSettings.globalViewer.options.table.buffering.maxSize = parseInt(item.newValue);

                    break;
                }
                case 'inpGviewerSettingsInitialFetchSize': {
                    app.userSettings.globalViewer.options.table.buffering.initialFetchSize = parseInt(item.newValue);

                    break;
                }
                case 'inpGviewerSettingsMaxDataLength': {
                    app.userSettings.globalViewer.options.table.maxDataLength = parseInt(item.newValue);

                    break;
                }
                case 'inpGviewerSettingsLazyFetchSize': {
                    app.userSettings.globalViewer.options.table.buffering.lazyFetchSize = parseInt(item.newValue);

                    break;
                }
                case 'inpGviewerSettingsLazyFetchTriggerPages': {
                    app.userSettings.globalViewer.options.table.buffering.startFetchingPages = parseInt(item.newValue);

                    break;
                }
                //
                case 'inpGviewerSettingsLastUsedCount': {
                    app.userSettings.globalViewer.options.menuLastUsedItemsCount = parseInt(item.newValue);

                    break;
                }
                case 'inpGviewerSettingsLastUsedMaxStore': {
                    app.userSettings.globalViewer.options.lastUsedMaxItems = parseInt(item.newValue);

                    break;
                }
                //
                case 'inpGviewerSettingsDefaultBookmarkColor': {
                    app.userSettings.globalViewer.themes.light.table.bookmarkColor = item.newValue;
                    app.userSettings.globalViewer.themes.dark.table.bookmarkColor = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDefaultTheme': {
                    app.userSettings.globalViewer.defaultTheme = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDefaultPieceChar': {
                    app.userSettings.globalViewer.themes.light.table.content.values.pieceChar.char = item.newValue;
                    app.userSettings.globalViewer.themes.dark.table.content.values.pieceChar.char = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDefaultPieceView': {
                    app.userSettings.globalViewer.themes.light.table.content.values.showPieces = item.newValue;
                    app.userSettings.globalViewer.themes.dark.table.content.values.showPieces = item.newValue;

                    break;
                }
                //
                case 'inpGviewerSettingsPathBarGlobal': {
                    app.userSettings.globalViewer.themes.light.titleBar.globalName = item.newValue;
                    app.userSettings.globalViewer.themes.dark.titleBar.globalName = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsPathBarParens': {
                    app.userSettings.globalViewer.themes.light.titleBar.parens = item.newValue;
                    app.userSettings.globalViewer.themes.dark.titleBar.parens = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsPathBarNumbers': {
                    app.userSettings.globalViewer.themes.light.titleBar.numbers = item.newValue;
                    app.userSettings.globalViewer.themes.dark.titleBar.numbers = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsPathBarStrings': {
                    app.userSettings.globalViewer.themes.light.titleBar.strings = item.newValue;
                    app.userSettings.globalViewer.themes.dark.titleBar.strings = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsPathBarCommas': {
                    app.userSettings.globalViewer.themes.light.titleBar.comma = item.newValue;
                    app.userSettings.globalViewer.themes.dark.titleBar.comma = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsPathBarColons': {
                    app.userSettings.globalViewer.themes.light.titleBar.colon = item.newValue;
                    app.userSettings.globalViewer.themes.dark.titleBar.colon = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsPathBarStar': {
                    app.userSettings.globalViewer.themes.light.titleBar.star = item.newValue;
                    app.userSettings.globalViewer.themes.dark.titleBar.star = item.newValue;

                    break;
                }
                // light
                case 'inpGviewerSettingsBookmarkHeaderBack': {
                    app.userSettings.globalViewer.themes.light.table.headers.bookmark.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsHeadersFore': {
                    app.userSettings.globalViewer.themes.light.table.headers.fore = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsHeadersBack': {
                    app.userSettings.globalViewer.themes.light.table.headers.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsBookmarksBack': {
                    app.userSettings.globalViewer.themes.light.table.content.bookmarks.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsBookmarksFore': {
                    app.userSettings.globalViewer.themes.light.table.content.bookmarks.fore = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesGlobal': {
                    app.userSettings.globalViewer.themes.light.table.content.globalName = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesParens': {
                    app.userSettings.globalViewer.themes.light.table.content.parens = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesCommas': {
                    app.userSettings.globalViewer.themes.light.table.content.comma = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesNumbers': {
                    app.userSettings.globalViewer.themes.light.table.content.numbers = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesStrings': {
                    app.userSettings.globalViewer.themes.light.table.content.strings = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataFore': {
                    app.userSettings.globalViewer.themes.light.table.content.values.data.fore = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataPieceFore': {
                    app.userSettings.globalViewer.themes.light.table.content.values.pieceChar.fore = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataPieceBack': {
                    app.userSettings.globalViewer.themes.light.table.content.values.pieceChar.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataPillBack': {
                    app.userSettings.globalViewer.themes.light.table.content.values.piecePills.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataPillFore': {
                    app.userSettings.globalViewer.themes.light.table.content.values.piecePills.fore = item.newValue;

                    break;
                }
                // dark
                case 'inpGviewerSettingsBookmarkHeaderBackDark': {
                    app.userSettings.globalViewer.themes.dark.table.headers.bookmark.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsHeadersForeDark': {
                    app.userSettings.globalViewer.themes.dark.table.headers.fore = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsHeadersBackDark': {
                    app.userSettings.globalViewer.themes.dark.table.headers.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsBookmarksBackDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.bookmarks.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsBookmarksForeDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.bookmarks.fore = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesGlobalDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.globalName = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesParensDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.parens = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesCommasDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.comma = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesNumbersDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.numbers = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsNodesStringsDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.strings = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataForeDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.values.data.fore = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataPieceForeDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.values.pieceChar.fore = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataPieceBackDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.values.pieceChar.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataPillBackDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.values.piecePills.back = item.newValue;

                    break;
                }
                case 'inpGviewerSettingsDataPillForeDark': {
                    app.userSettings.globalViewer.themes.dark.table.content.values.piecePills.fore = item.newValue;

                    break;
                }
            }
        }
    });

    // rebuild menus if needed
    let item = app.ui.gViewer.settings.getField('inpGviewerSettingsLastUsedCount');
    if (item.oldValue !== item.newValue) {
        app.ui.menu.rebuildGlobalViewerLastUsed();
    }

    // adjust storage menu size if needed
    item = app.ui.gViewer.settings.getField('inpGviewerSettingsLastUsedMaxStore');
    if (item.oldValue !== item.newValue) {
        if (app.userSettings.globalViewer.lastUsed.length > app.userSettings.globalViewer.options.lastUsedMaxItems) {
            // trim the array
            app.userSettings.globalViewer.lastUsed.splice(app.userSettings.globalViewer.options.lastUsedMaxItems, app.userSettings.globalViewer.lastUsed.length - app.userSettings.globalViewer.options.lastUsedMaxItems);
        }
    }

    // save the data in the storage
    app.ui.storage.save('gViewer', app.userSettings.globalViewer);

};

app.ui.gViewer.settings.updateTabs = (tabId = null, isChanged) => {
    for (const tabIdIx in app.ui.gViewer.instance) {
        if (tabId === null || (typeof (tabId) === 'string' && tabId === tabIdIx)) {
            const instance = app.ui.gViewer.instance[tabIdIx].viewer;

            app.ui.gViewer.settings.params.forEach(item => {
                if (item.oldValue !== item.newValue) {
                    switch (item.id) {
                        case 'inpGviewerSettingsPathBarGlobal': {
                            instance.themes.light.titleBar.globalName = item.newValue;
                            instance.themes.light.titleBar.globalName = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsPathBarParens': {
                            instance.themes.light.titleBar.parens = item.newValue;
                            instance.themes.dark.titleBar.parens = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsPathBarNumbers': {
                            instance.themes.light.titleBar.numbers = item.newValue;
                            instance.themes.dark.titleBar.numbers = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsPathBarStrings': {
                            instance.themes.light.titleBar.strings = item.newValue;
                            instance.themes.dark.titleBar.strings = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsPathBarCommas': {
                            instance.themes.light.titleBar.comma = item.newValue;
                            instance.themes.dark.titleBar.comma = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsPathBarColons': {
                            instance.themes.light.titleBar.colon = item.newValue;
                            instance.themes.dark.titleBar.colon = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsPathBarStar': {
                            instance.themes.light.titleBar.star = item.newValue;
                            instance.themes.dark.titleBar.star = item.newValue;

                            break;
                        }
                        // light
                        case 'inpGviewerSettingsBookmarkHeaderBack': {
                            instance.themes.light.table.headers.bookmark.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsHeadersFore': {
                            instance.themes.light.table.headers.fore = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsHeadersBack': {
                            instance.themes.light.table.headers.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsBookmarksBack': {
                            instance.themes.light.table.content.bookmarks.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsBookmarksFore': {
                            instance.themes.light.table.content.bookmarks.fore = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesGlobal': {
                            instance.themes.light.table.content.globalName = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesParens': {
                            instance.themes.light.table.content.parens = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesCommas': {
                            instance.themes.light.table.content.comma = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesNumbers': {
                            instance.themes.light.table.content.numbers = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesStrings': {
                            instance.themes.light.table.content.strings = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataFore': {
                            instance.themes.light.table.content.values.data.fore = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataPieceFore': {
                            instance.themes.light.table.content.values.pieceChar.fore = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataPieceBack': {
                            instance.themes.light.table.content.values.pieceChar.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataPillBack': {
                            instance.themes.light.table.content.values.piecePills.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataPillFore': {
                            instance.themes.light.table.content.values.piecePills.fore = item.newValue;

                            break;
                        }
                        // dark
                        case 'inpGviewerSettingsBookmarkHeaderBackDark': {
                            instance.themes.dark.table.headers.bookmark.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsHeadersForeDark': {
                            instance.themes.dark.table.headers.fore = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsHeadersBackDark': {
                            instance.themes.dark.table.headers.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsBookmarksBackDark': {
                            instance.themes.dark.table.content.bookmarks.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsBookmarksForeDark': {
                            instance.themes.dark.table.content.bookmarks.fore = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesGlobalDark': {
                            instance.themes.dark.table.content.globalName = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesParensDark': {
                            instance.themes.dark.table.content.parens = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesCommasDark': {
                            instance.themes.dark.table.content.comma = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesNumbersDark': {
                            instance.themes.dark.table.content.numbers = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsNodesStringsDark': {
                            instance.themes.dark.table.content.strings = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataForeDark': {
                            instance.themes.dark.table.content.values.data.fore = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataPieceForeDark': {
                            instance.themes.dark.table.content.values.pieceChar.fore = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataPieceBackDark': {
                            instance.themes.dark.table.content.values.pieceChar.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataPillBackDark': {
                            instance.themes.dark.table.content.values.piecePills.back = item.newValue;

                            break;
                        }
                        case 'inpGviewerSettingsDataPillForeDark': {
                            instance.themes.dark.table.content.values.piecePills.fore = item.newValue;

                            break;
                        }
                    }
                }
            });

            // refresh path bar
            if (isChanged.pathBar === true) {
                if (instance.parserPayload.global === undefined) {
                    instance.parserPayload.global = ''
                }
                app.ui.gViewer.parser.createPreview(tabIdIx, instance.parserPayload);
            }

            // refresh table header
            if (isChanged.pathBar === true) {
                app.ui.gViewer.table.initHeader(tabIdIx);
            }

            // refresh table body
            if (isChanged.themeLight === true || isChanged.themeDark === true) {
                app.ui.gViewer.table.refresh(tabIdIx);
            }

            // requery
            if (isChanged.buffer === true) {
                const instance = app.ui.gViewer.instance[tabIdIx];

                if (instance.viewer.buffer.original.global !== undefined) {
                    app.ui.gViewer.table.processPath(tabIdIx);
                }
            }
        }
    }
};

app.ui.gViewer.settings.params = [
    {
        id: 'inpGviewerSettingsTotalBufferSize',
        type: 'number',
        min: 500,
        max: 4000,
        caption: 'Total buffer size',
        description: 'The maximum number of rows of the rotating buffer. <br>Data will be removed from the head or the tail automatically when performing lazy-fetching and buffer is full.' +
            '<br><br>Min. value: 500<br>Max. value: 4,000' +
            '<br><br></span><i class="hand bi-info-circle" style="padding-left: 6px; font-size: 11px; color:var(--ydb-orange)!important;"></i> WARNING<br>Changes to the data buffer may result in a slower browser response.',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'buffer',
    },
    {
        id: 'inpGviewerSettingsInitialFetchSize',
        type: 'number',
        min: 100,
        max: 1000,
        caption: 'Initial fetch size',
        description: 'The number of records that will be initially fetched.' +
            '<br><br>Min. value: 100<br>Max. value: 1,000' +
            '<br><br><i class="hand bi-info-circle" style="padding-left: 6px; font-size: 11px; color:var(--ydb-orange)!important;"></i> WARNING<br>Changes to the data buffer may result in a slower browser response.',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'buffer',
    },
    {
        id: 'inpGviewerSettingsMaxDataLength',
        type: 'number',
        min: 16,
        max: 4096,
        caption: 'Max data length',
        description: 'The maximum length of the data. <br>If the length of the data exceed this value, a button will appear and the entire data will be displayed on a separate dialog.' +
            '<br><br>Min. value: 16<br>Max. value: 4,096',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'buffer',
    },
    {
        id: 'inpGviewerSettingsLazyFetchSize',
        type: 'number',
        min: 50,
        max: 300,
        caption: 'Lazy fetch size',
        description: 'The number of records that will be fetched when scrolling up or down' +
            '<br><br>Min. value: 50<br>Max. value: 300' +
            '<br><br><i class="hand bi-info-circle" style="padding-left: 6px; font-size: 11px; color:var(--ydb-orange)!important;"></i> WARNING<br>Changes to the data buffer may result in a slower browser response.',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'buffer',
    },
    {
        id: 'inpGviewerSettingsLazyFetchTriggerPages',
        type: 'number',
        min: 1,
        max: 5,
        caption: 'Lazy fetch trigger pages',
        description: 'The number of table pages that will trigger a fetch in the database. <BR>A table pages contains the number of rows on each page, so the lazy-fetch will trigger when scroll reaches the number of rows on each page * this setting.' +
            '<br><br>Min. value: 1 <br>Max. value: 5',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'buffer',
    },
    //
    //
    {
        id: 'inpGviewerSettingsLastUsedCount',
        type: 'number',
        min: 2,
        max: 15,
        caption: 'Last Used count',
        description: 'The maximum number of entries displayed in the pull down menu: Development / Globals / Recent. <BR>If more items are present, the "more..." menu will be displayed.' +
            '<br><br>Min. value: 2 <br>Max. value: 15',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'menus',
    },
    {
        id: 'inpGviewerSettingsLastUsedMaxStore',
        type: 'number',
        min: 20,
        max: 300,
        caption: 'Last Used Max Store',
        description: 'The maximum number of entries stored in the browser "local storage"' +
            '<br><br>Min. value: 20 <br>Max. value: 300',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'menus',
    },
    //
    //
    {
        id: 'inpGviewerSettingsDefaultBookmarkColor',
        type: 'select',
        caption: 'Default bookmark color',
        description: 'The default color used as bookmark when creating a new viewer',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'settings',
    },
    {
        id: 'inpGviewerSettingsDefaultTheme',
        type: 'select',
        caption: 'Default theme',
        description: 'The default theme that will be used when creating a new viewer<br>You can choose between a light and a dark theme.',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'settings',
    },
    {
        id: 'inpGviewerSettingsDefaultPieceChar',
        type: 'select',
        caption: 'Default piece char',
        description: 'The default character used as piece separator when creating a new viewer',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'settings',
    },
    {
        id: 'inpGviewerSettingsDefaultPieceView',
        type: 'select',
        caption: 'Default piece view',
        description: 'The default view of the data column when creating a new viewer<BR>REGULAR means that the data is displayed as it is.<BR>HIGHLIGHT means it will highlight the piece character.<BR>PILLS means it will display a pill with the piece number instead of the piece character.',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'settings',
    },
    //
    //
    {
        id: 'inpGviewerSettingsPathBarGlobal',
        type: 'color',
        caption: 'Global color',
        description: 'The color of the global',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'pathbar',
    },
    {
        id: 'inpGviewerSettingsPathBarParens',
        type: 'color',
        caption: 'Parens color',
        description: 'The color of the parens',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'pathbar',
    },
    {
        id: 'inpGviewerSettingsPathBarNumbers',
        type: 'color',
        caption: 'Numbers color',
        description: 'The color of the numbers',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'pathbar',
    },
    {
        id: 'inpGviewerSettingsPathBarStrings',
        type: 'color',
        caption: 'Strings color',
        description: 'The color of the strings',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'pathbar',
    },
    {
        id: 'inpGviewerSettingsPathBarCommas',
        type: 'color',
        caption: 'Commas color',
        description: 'The color of the commas',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'pathbar',
    },
    {
        id: 'inpGviewerSettingsPathBarColons',
        type: 'color',
        caption: 'Colons color',
        description: 'The color of the colons',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'pathbar',
    },
    {
        id: 'inpGviewerSettingsPathBarStar',
        type: 'color',
        caption: 'Star color',
        description: 'The color of the star',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'pathbar',
    },
    //
    // light
    //
    {
        id: 'inpGviewerSettingsBookmarkHeaderBack',
        type: 'color',
        caption: 'Headers: bookmark background',
        description: 'The background color of the header bookmark',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsHeadersFore',
        type: 'color',
        caption: 'Headers: font color',
        description: 'The font color of the header',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsHeadersBack',
        type: 'color',
        caption: 'Headers: background',
        description: 'The background color of the headers',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsBookmarksBack',
        type: 'color',
        caption: 'Bookmarks: background',
        description: 'The background color of the bookmarks column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsBookmarksFore',
        type: 'color',
        caption: 'Bookmarks: font color',
        description: 'The font color of the bookmarks column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsNodesGlobal',
        type: 'color',
        caption: 'Nodes cells: global',
        description: 'The global color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsNodesParens',
        type: 'color',
        caption: 'Nodes cells: parens',
        description: 'The paren color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsNodesCommas',
        type: 'color',
        caption: 'Nodes cells: commas',
        description: 'The commas color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsNodesNumbers',
        type: 'color',
        caption: 'Nodes cells: numbers',
        description: 'The number color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsNodesStrings',
        type: 'color',
        caption: 'Nodes cells: strings',
        description: 'The string color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsDataFore',
        type: 'color',
        caption: 'Data cells: font colors',
        description: 'The font color of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsDataPieceFore',
        type: 'color',
        caption: 'Data cells: piece font color',
        description: 'The font color of the piece of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsDataPieceBack',
        type: 'color',
        caption: 'Data cells: piece background',
        description: 'The background of the piece of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsDataPillBack',
        type: 'color',
        caption: 'Data cells: pills background',
        description: 'The background of the pills of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    {
        id: 'inpGviewerSettingsDataPillFore',
        type: 'color',
        caption: 'Data cells: pills font color',
        description: 'The font color of the pills of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewLight',
    },
    //
    // dark
    //
    {
        id: 'inpGviewerSettingsBookmarkHeaderBackDark',
        type: 'color',
        caption: 'Headers: bookmark background',
        description: 'The background color of the header bookmark',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsHeadersForeDark',
        type: 'color',
        caption: 'Headers: font color',
        description: 'The font color of the header',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsHeadersBackDark',
        type: 'color',
        caption: 'Headers: background',
        description: 'The background color of the headers',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsBookmarksBackDark',
        type: 'color',
        caption: 'Bookmarks: background',
        description: 'The background color of the bookmarks column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsBookmarksForeDark',
        type: 'color',
        caption: 'Bookmarks: font color',
        description: 'The font color of the bookmarks column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsNodesGlobalDark',
        type: 'color',
        caption: 'Nodes cells: global',
        description: 'The global color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsNodesParensDark',
        type: 'color',
        caption: 'Nodes cells: parens',
        description: 'The paren color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsNodesCommasDark',
        type: 'color',
        caption: 'Nodes cells: commas',
        description: 'The commas color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsNodesNumbersDark',
        type: 'color',
        caption: 'Nodes cells: numbers',
        description: 'The number color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsNodesStringsDark',
        type: 'color',
        caption: 'Nodes cells: strings',
        description: 'The string color of the node column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsDataForeDark',
        type: 'color',
        caption: 'Data cells: font colors',
        description: 'The font color of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsDataPieceForeDark',
        type: 'color',
        caption: 'Data cells: piece font color',
        description: 'The font color of the piece of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsDataPieceBackDark',
        type: 'color',
        caption: 'Data cells: piece background',
        description: 'The background of the piece of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsDataPillBackDark',
        type: 'color',
        caption: 'Data cells: pills background',
        description: 'The background of the pills of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
    {
        id: 'inpGviewerSettingsDataPillForeDark',
        type: 'color',
        caption: 'Data cells: pills font color',
        description: 'The font color of the pills of the data column',
        oldValue: 0,
        newValue: 0,
        refreshTarget: 'previewDark',
    },
];

app.ui.gViewer.settings.getField = id => {
    return app.ui.gViewer.settings.params.find(el => {
        return el.id === id
    })

};
