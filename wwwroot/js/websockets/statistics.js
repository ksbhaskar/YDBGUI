/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

// This is the statistics module for the web sockets.
// You can add as many module as you want to the web sockets, as long as you follow this signature:
// app[module].init = see below
// app[module].exec =   {
//                          yourFunction: same body as below
//                      }
// Don't forget to run the init() BEFORE to init() the web sockets in order to register your functions

app.statistics = {
    init: function () {
        // push the events in the socket listener array
        for (let event in this.exec) {
            app.websockets.opCodes.push(event)
        }
    },

    exec: {
        start: function (data) {
            app.statistics.initData()
            this.firstSample = false
            app.ui.stats.sparkChart.renderer.render($('#divStatsSparkchart'))

            app.statistics.lastFrameCount = [0, 0]

            data = app.ui.stats.entry.data

            // initialize data object
            const eventName = (arguments.callee.name)
            return new Promise(function (resolve, reject) {

                const hTimeout = setTimeout(() => reject({msg: app.statistics.MSG_TIMEOUT}), app.userSettings.stats.webSockets.defaultResponseTimeout)
                const __callback = e => {
                    clearTimeout(hTimeout)

                    app.websockets.socket.removeEventListener(eventName, __callback)

                    const msg = e.detail

                    if (msg.status === 'ACK') resolve()
                    else reject(msg)
                }

                // de-reference to send only part of the object
                const newData = JSON.parse(JSON.stringify(data))
                const newData2 = []

                newData.forEach((entry, ix) => {
                    if (entry.data.enabled === true) {
                        newData2.push({
                            sampleRate: entry.sampleRate,
                            type: entry.type,
                            ix: entry.ix,
                            id: entry.id,
                            data: {
                                processes: entry.data.processes,
                                regions: entry.data.regions,
                                samples: entry.data.samples,
                                file: entry.data.file ? entry.data.file : ''
                            }
                        })
                    }
                })
                app.websockets.socket.addEventListener(eventName, __callback)
                if (app.websockets.socket.readyState === 1) {
                    app.websockets.socket.send(JSON.stringify({
                        opCode: eventName,
                        data: newData2
                    }))
                } else {
                    reject({msg: 'Socket is disconnected'})
                }
            })
        },

        stop: function () {
            const eventName = (arguments.callee.name)
            return new Promise(function (resolve, reject) {

                const hTimeout = setTimeout(() => reject({msg: app.statistics.MSG_TIMEOUT}), app.userSettings.stats.webSockets.defaultResponseTimeout)
                const __callback = e => {
                    clearTimeout(hTimeout)

                    app.websockets.socket.removeEventListener(eventName, __callback)

                    const msg = e.detail

                    if (msg.status === 'ACK') resolve()
                    else reject(msg)
                }

                app.websockets.socket.addEventListener(eventName, __callback)

                if (app.websockets.socket.readyState === 1) {
                    app.websockets.socket.send(JSON.stringify({
                        opCode: eventName
                    }))
                } else {
                    reject({msg: 'Socket is disconnected'})
                }
            })
        },

        pause: function () {
            const eventName = (arguments.callee.name)
            return new Promise(function (resolve, reject) {

                const hTimeout = setTimeout(() => reject({msg: app.statistics.MSG_TIMEOUT}), app.userSettings.stats.webSockets.defaultResponseTimeout)
                const __callback = e => {
                    clearTimeout(hTimeout)

                    app.websockets.socket.removeEventListener(eventName, __callback)

                    const msg = e.detail

                    if (msg.status === 'ACK') resolve()
                    else reject(msg)
                }

                app.websockets.socket.addEventListener(eventName, __callback)

                if (app.websockets.socket.readyState === 1) {
                    app.websockets.socket.send(JSON.stringify({
                        opCode: eventName
                    }))

                } else {
                    reject({msg: 'Socket is disconnected'})
                }
            })
        },

        resume: function () {
            const eventName = (arguments.callee.name)
            return new Promise(function (resolve, reject) {

                const hTimeout = setTimeout(() => reject({msg: app.statistics.MSG_TIMEOUT}), app.userSettings.stats.webSockets.defaultResponseTimeout)
                const __callback = e => {
                    clearTimeout(hTimeout)

                    app.websockets.socket.removeEventListener(eventName, __callback)

                    const msg = e.detail
                    console.log(msg)

                    if (msg.status === 'ACK') resolve()
                    else reject(msg)
                }

                app.websockets.socket.addEventListener(eventName, __callback)

                if (app.websockets.socket.readyState === 1) {
                    app.websockets.socket.send(JSON.stringify({
                        opCode: eventName
                    }))
                } else {
                    reject({msg: 'Socket is disconnected'})
                }
            })
        },

        ping: function () {
            const eventName = (arguments.callee.name)
            return new Promise(function (resolve, reject) {

                const hTimeout = setTimeout(() => reject({msg: app.statistics.MSG_TIMEOUT}), app.userSettings.stats.webSockets.defaultResponseTimeout)
                const __callback = e => {
                    clearTimeout(hTimeout)

                    app.websockets.socket.removeEventListener(eventName, __callback)

                    const msg = e.detail
                    console.log(msg)

                    if (msg.status === 'ACK') resolve()
                    else reject(msg)
                }

                app.websockets.socket.addEventListener(eventName, __callback)

                if (app.websockets.socket.readyState === 1) {
                    app.websockets.socket.send(JSON.stringify({
                        opCode: eventName
                    }))
                } else {
                    reject({msg: 'Socket is disconnected'})
                }
            })
        },

        mping: function () {
            const eventName = (arguments.callee.name)
            return new Promise(function (resolve, reject) {

                const hTimeout = setTimeout(() => reject({msg: app.statistics.MSG_TIMEOUT}), app.userSettings.stats.webSockets.defaultResponseTimeout)
                const __callback = e => {
                    clearTimeout(hTimeout)

                    app.websockets.socket.removeEventListener(eventName, __callback)

                    const msg = e.detail
                    app.statistics.exec.mpingStatus = true
                    console.log(msg)

                    resolve(msg)
                }

                app.websockets.socket.addEventListener(eventName, __callback)

                if (app.websockets.socket.readyState === 1) {
                    app.websockets.socket.send(JSON.stringify({
                        opCode: eventName
                    }))
                } else {
                    reject({msg: 'Socket is disconnected'})
                }
            })
        },

        mpingStatus: false
    },

    keepAlive: function (msg) {
        if (app.websockets.socket.readyState === 1) {
            app.websockets.socket.send(JSON.stringify({
                opCode: 'keepAlive',
                data: 'keepAliveOk',
                status: 'ACK'
            }))

        } else {
            console.log('Socket is disconnected while trying to pong the server')
        }
    },

    statsData: function (msg) {
        if (msg.status === 'ACK') this.processIncoming(msg.data, msg.timestamp)

        // timestamp
        let date = new Date(msg.timestamp)
        if (isNaN(date) === false) {
            $('#lblStatsControllerTimestamp').text(date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes() + ':' + (date.getSeconds() < 10 ? '0' : '') + date.getSeconds() + '.' + date.getMilliseconds())
        }

        // sample count
        this.sampleCount++
        $('#lblStatsControllerSamples').text(app.ui.formatThousands(this.sampleCount))

        // led activity
        $('#statsLed').addClass(msg.status === 'ACK' ? 'led-green' : 'led-red')
        setTimeout(() => {
            $('#statsLed').removeClass(msg.status === 'ACK' ? 'led-green' : 'led-red')

        }, 50)
    },

    processIncoming: async function (data, timestamp) {
        //console.log(data)
        //console.log(this.data)
        if (this.firstSample === false) {
            this.firstSample = true

            return
        }

        data.forEach(source => {
            if (source.data) {
                if (source.type === 'ygbl' || source.type === 'fhead' || source.type === 'peekByName') {
                    const target = this.data.find(repSource => source.id === repSource.id)

                    for (let region in source.data.regions) {
                        let processIx = -1
                        let stats = []

                        for (let pid in source.data.regions[region].pids) {
                            processIx++

                            source.data.regions[region].pids[pid].forEach(sample => {
                                sample.pid = pid

                                const targetStat = target.regions[region][processIx].find(entry => entry.sample === (Object.keys(sample)[0] === 'pid' ? Object.keys(sample)[1] : Object.keys(sample)[0]))

                                stats.push(this.computeStats(targetStat, sample, timestamp))
                            })

                            //updates, if needed, y axis max
                            app.ui.stats.sparkChart.renderer.sections.graphs.graphData.forEach((chart, ix) => {
                                if (app.ui.stats.sparkChart.renderer.sections.graphs.graphData[ix].source && app.ui.stats.sparkChart.renderer.sections.graphs.graphData[ix].source.id === source.id) {
                                    const chart = 'chart' + (ix + 1).toString()

                                    if (app.ui.stats.sparkChart.renderer.sections.graphs[chart] !== null) {
                                        if (!app.ui.stats.sparkChart.renderer.sections.graphs[chart].config['_config'].options.scales['y']) {
                                            // individual y axis
                                            const maxA = []

                                            app.ui.stats.sparkChart.renderer.sections.graphs[chart].config['_config'].data.datasets.forEach(dataset => {
                                                const statObj = stats.find(stat => {
                                                    if (dataset.originalLabel.indexOf('.') > -1) {
                                                        return stat.sample === dataset.originalLabel.split(' ')[0]

                                                    } else if (dataset.label.substring(0, 2) === 'P ') {
                                                        const labelA = dataset.label.split(' ')

                                                        return stat.sample.substring(0, 3) === labelA[2]

                                                    } else if (dataset.label.charAt(0) === 'P') {
                                                        const labelA = dataset.label.split(' ')

                                                        return stat.sample.substring(0, 3) === labelA[1]

                                                    } else {
                                                        return stat.sample.substring(0, 3) === dataset.label.substring(0, 3)
                                                    }
                                                })

                                                let sampleType = ''

                                                if (source.type === 'ygbl') {
                                                    sampleType = dataset.label.substring(4)

                                                } else {
                                                    sampleType = dataset.label.split(' ')[1]
                                                }

                                                if (sampleType.charAt(0) === 'Δ') {
                                                    if (sampleType.charAt(2) === '') sampleType = 'delta'
                                                    else sampleType = sampleType.substring(2)
                                                }

                                                app.ui.stats.sparkChart.renderer.sections.graphs[chart].options.scales[dataset.label].max = statObj.yAxis[sampleType]

                                                // push in array to select the grid yaxis
                                                maxA.push({
                                                    label: dataset.label,
                                                    max: statObj.max
                                                })
                                            })

                                            // process yaxis grid
                                            let found = false

                                            maxA.forEach(entry => {
                                                if (found === true || entry.max === 0) {
                                                    app.ui.stats.sparkChart.renderer.sections.graphs[chart].options.scales[entry.label].grid.drawOnChartArea = false

                                                    return
                                                }

                                                if (entry.max > 0) {
                                                    app.ui.stats.sparkChart.renderer.sections.graphs[chart].options.scales[entry.label].grid.drawOnChartArea = found = true
                                                }
                                            })

                                        } else {
                                            // common axis
                                            const maxValues = []

                                            app.ui.stats.sparkChart.renderer.sections.graphs[chart].config['_config'].data.datasets.forEach(dataset => {
                                                const statObj = stats.find(stat => {
                                                    if (dataset.label.substring(0, 2) === 'P ') {
                                                        const labelA = dataset.label.split(' ')

                                                        return stat.sample.substring(0, 3) === labelA[2]

                                                    } else if (dataset.label.charAt(0) === 'P') {
                                                        const labelA = dataset.label.split(' ')

                                                        return stat.sample.substring(0, 3) === labelA[1]

                                                    } else {
                                                        const sampleA = stat.sample.split('.')

                                                        if (sampleA.length > 0) {
                                                            return sampleA[sampleA.length - 1] === dataset.label.split(' ')[0]

                                                        } else {
                                                            return stat.sample.substring(0, 3) === dataset.label.substring(0, 3)
                                                        }
                                                    }
                                                })

                                                let sampleType = dataset.label.substring(4)

                                                if (sampleType.charAt(0) === ']') {
                                                    const sampleTypeA = sampleType.split(' ')

                                                    sampleType = sampleTypeA[2]
                                                }

                                                if (sampleType.charAt(0) === 'Δ') {
                                                    if (sampleType.charAt(2) === '') sampleType = 'delta'
                                                    else sampleType = sampleType.substring(2)
                                                }
                                                maxValues.push(statObj[sampleType])
                                            })

                                            let max = app.ui.stats.sparkChart.renderer.sections.graphs[chart].options.scales.y.max
                                            maxValues.forEach(maxValue => {
                                                if (maxValue > max) {
                                                    max = app.statistics.roundUpMax(maxValue)

                                                }
                                            })

                                            app.ui.stats.sparkChart.renderer.sections.graphs[chart].options.scales.y.max = max
                                        }
                                    }
                                }
                            })
                        }
                    }
                }

            } else {
                const currentSource = app.ui.stats.entry.data.find(lSource => source.id === lSource.id)
                const target = this.data.find(repSource => source.id === repSource.id)

                currentSource.data.regions.forEach(region => {
                    let processes = currentSource.data.processes.type === 'agg' ? 1 : (currentSource.data.processes.type === 'top' ? currentSource.data.processes.count : currentSource.data.processes.pids.length)

                    for (let processIx = 0; processIx < processes; processIx++) {
                        currentSource.data.samples.forEach(sampleName => {
                            let sample = {}
                            sample[sampleName] = 0
                            sample.pid = processIx //processes === 1 ? '*' : 'Process n/a'

                            const targetStat = target.regions[region][processIx].find(entry => entry.sample === Object.keys(sample)[0])
                            this.computeStats(targetStat, sample, timestamp)
                        })
                    }
                })
            }
        })

        app.ui.stats.sparkChart.renderer.update()
        app.ui.stats.sparkChart.renderer.sections.graphs.timestamp = timestamp
        if (app.ui.stats.sparkChart.renderer.sections.graphs.chart1 !== null) {
            const graphData = app.ui.stats.sparkChart.renderer.sections.graphs.graphData[0]
            app.ui.stats.sparkChart.renderer.sections.graphs.chart1.options.scales.x.min = timestamp - (graphData.secondsInView * 1000)
            app.ui.stats.sparkChart.renderer.sections.graphs.chart1.options.scales.x.max = timestamp

            if (this.lastFrameCount[0] !== 0) app.ui.stats.sparkChart.renderer.sections.graphs.chart1.options.animations.x.duration = Date.now() - this.lastFrameCount[0] + 50
            else app.ui.stats.sparkChart.renderer.sections.graphs.chart1.options.animations.x.duration = app.ui.stats.sparkChart.renderer.sections.graphs.graphData[0].source.sampleRate * 1000

            await app.ui.stats.sparkChart.renderer.sections.graphs.chart1.update()

            this.lastFrameCount[0] = Date.now()
        }

        if (app.ui.stats.sparkChart.renderer.sections.graphs.chart2 !== null) {
            const graphData = app.ui.stats.sparkChart.renderer.sections.graphs.graphData[1]
            app.ui.stats.sparkChart.renderer.sections.graphs.chart2.options.scales.x.min = timestamp - (graphData.secondsInView * 1000)
            app.ui.stats.sparkChart.renderer.sections.graphs.chart2.options.scales.x.max = timestamp

            if (this.lastFrameCount[1] !== 0) app.ui.stats.sparkChart.renderer.sections.graphs.chart2.options.animations.x.duration = Date.now() - this.lastFrameCount[1] + 50
            else app.ui.stats.sparkChart.renderer.sections.graphs.chart2.options.animations.x.duration = app.ui.stats.sparkChart.renderer.sections.graphs.graphData[1].source.sampleRate * 1000

            await app.ui.stats.sparkChart.renderer.sections.graphs.chart2.update()

            this.lastFrameCount[1] = Date.now()
        }
    },

    lastFrameCount: [0, 0],

    roundUpMax: function (val) {
        const asStr = val.toString()
        const strLength = asStr.length
        const firstCharVal = parseInt(asStr.charAt(0))
        const decimal = '0'
        let result

        switch (true) {
            case  (firstCharVal < 2): {
                result = parseInt('2' + decimal.repeat(strLength - 1))

                break
            }
            case  (firstCharVal < 4): {
                result = parseInt('4' + decimal.repeat(strLength - 1))

                break
            }
            case  (firstCharVal < 6): {
                result = parseInt('6' + decimal.repeat(strLength - 1))

                break
            }
            case  (firstCharVal < 8): {
                result = parseInt('8' + decimal.repeat(strLength - 1))

                break
            }
            default: {
                result = parseInt('1' + decimal.repeat(strLength))
            }
        }

        return result
    },

    computeStats: function (stat, sample, timestamp, processIx) {
        const sampleName = Object.keys(sample)[0] === 'pid' ? Object.keys(sample)[1] : Object.keys(sample)[0]
        const sampleValue = sample[sampleName]

        // abs
        stat.abs = sampleValue

        // delta
        if (stat.lastAbs > 0) stat.lastDelta = stat.delta = sampleValue - stat.lastAbs
        else stat.lastDelta = stat.delta = 0
        if (stat.delta < 0) stat.delta = 0

        stat.lastAbs = sampleValue

        // min
        if (stat.lastDelta < stat.min || stat.min === 0) stat.min = stat.lastDelta
        if (stat.min < 0) stat.min = 0

        // max
        if (stat.lastDelta > stat.max) stat.max = stat.lastDelta

        // avg
        stat.avgTot += stat.lastDelta
        if (stat.avgTot < 0) stat.avgTot = 0
        stat.avg = parseInt((stat.avgTot / stat.data.length).toString())
        if (isNaN(stat.avg) || stat.avg < 0) stat.avg = 0

        // timestamp
        stat.timestamp = timestamp = parseInt(timestamp / 100) * 100

        // app.userSettings.stats.sparkLines.sections.graph.buffering
        if (stat.data.length > app.userSettings.stats.sparkLines.sections.graph.buffering) {
            // flag it
            //app.ui.stats.sparkChart.renderer.sections.graphs.bufferIsRotating = true

            stat.gData.abs.shift()
            stat.gData.delta.shift()
            stat.gData.min.shift()
            stat.gData.max.shift()
            stat.gData.avg.shift()
        }

        stat.data.push({
            abs: sampleValue,
            delta: stat.delta,
            min: stat.min,
            max: stat.max,
            avg: isNaN(stat.avg) ? 0 : stat.avg,
            timestamp: timestamp
        })

        stat.gData.abs.push({x: timestamp, y: stat.abs})
        stat.gData.min.push({x: timestamp, y: stat.min})
        stat.gData.max.push({x: timestamp, y: stat.max})
        stat.gData.avg.push({x: timestamp, y: stat.avg})
        stat.gData.delta.push({x: timestamp, y: stat.delta})

        // max value for y axis
        if (stat.abs > stat.yAxis.abs) {
            stat.yAxis.abs = this.roundUpMax(stat.abs)
        }
        if (stat.delta > stat.yAxis.delta) {
            stat.yAxis.delta = this.roundUpMax(stat.delta)
        }
        if (stat.min > stat.yAxis.min) {
            stat.yAxis.min = this.roundUpMax(stat.min)
        }
        if (stat.max > stat.yAxis.max) {
            stat.yAxis.max = this.roundUpMax(stat.max)
        }
        if (stat.avg > stat.yAxis.avg) {
            stat.yAxis.avg = this.roundUpMax(stat.avg)
        }

        // pid
        stat.pid = sample.pid

        return stat
    },

    initData: function () {
        this.data = []
        this.sampleCount = 0

        app.ui.stats.entry.data.forEach(source => {
            let node = {}
            if (source.data.enabled === true) {
                node.id = source.id
                node.regions = {}

                source.data.regions.forEach(region => {
                    node.regions[region] = []
                    const processCount = source.data.processes.type === 'agg' ? 1 : (source.data.processes.type === 'top' ? source.data.processes.count : source.data.processes.pids.length)

                    for (let ix = 0; ix < processCount; ix++) {
                        const process = []

                        source.data.samples.forEach(sample => {
                            let counters = {
                                sample: sample,
                                min: 0,
                                max: 0,
                                avg: 0,
                                avgTot: 0,
                                lastDelta: 0,
                                lastAbs: 0,
                                prevAbs: 0,
                                yAxis: {
                                    abs: 0,
                                    delta: 0,
                                    min: 0,
                                    max: 0,
                                    avg: 0,
                                },
                                pid: source.data.processes.type === 'pids' ? 'P[' + source.data.processes.pids[ix] + ']' : '',
                                data: [],
                                gData: {
                                    abs: [],
                                    delta: [],
                                    min: [],
                                    max: [],
                                    avg: [],
                                },
                            }
                            process.push(counters)
                        })

                        node.regions[region].push(process)
                    }
                })

                this.data.push(node)
            }
        })
    },

    data: [],
    sourceMapping: [],
    sampleCount: 0,
    firstSample: false,

    MSG_TIMEOUT: 'Sockets execution error: Timeout waiting for a response',

    samplesDescription: {
        abs: 'It is the absolute value, which mirrors the value returned by the Web sockets',
        delta: 'Represents the change between the current and the previous absolute value',
        min: 'It is the lowest of all the collected delta values',
        max: 'It is the highest of all the collected delta values',
        avg: 'It is the average of all the collected delta values'
    }
}
