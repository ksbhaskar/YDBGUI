/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.octoViewer.init = () => {
    $('#btnOctoHelperSelect').on('click', () => app.ui.octoViewer.processDropHelperStatement('select'))
    $('#btnOctoHelperInsert').on('click', () => app.ui.octoViewer.processDropHelperStatement('insert'))
    $('#btnOctoHelperUpdate').on('click', () => app.ui.octoViewer.processDropHelperStatement('update'))
    $('#btnOctoHelperDelete').on('click', () => app.ui.octoViewer.processDropHelperStatement('delete'))

    $('#btnOctoHelperSubmitFields').on('click', () => app.ui.octoViewer.processDropHelperColumns())

    $('#divOctoHelperColumns').on('keyup', e => app.ui.octoViewer.processColumnsEnter('column', e))
    $('#divOctoHelperStatement').on('keyup', e => app.ui.octoViewer.processColumnsEnter('statement', e))

    $('#selOctoHelperColumns').on('dblclick', e => app.ui.octoViewer.processDropHelperColumns())

    $('#btnOctoTabEditOk').on('click', () => app.ui.octoViewer.renameTabOkPressed())
    $('#inpOctoTabEdit').on('keyup', e => app.ui.octoViewer.renameTabKeyUp(e))

    app.ui.setupDialogForTest('modalOctoTabEdit')
}

app.ui.octoViewer.lastId = 0
app.ui.octoViewer.cm = []
app.ui.octoViewer.fontSize = []
app.ui.octoViewer.theme = []
app.ui.octoViewer.queries = []
app.ui.octoViewer.bullet = '<i class="bi-circle-fill octo-queried-tab"></i>'
app.ui.octoViewer.dropContext = {}

app.ui.octoViewer.addNew = () => {
    // we need to look, at first, for fatal errors in the database
    if (app.ui.checkSystemErrors() === true) return

    app.ui.octoViewer.lastId++
    const type = '-O-' + app.ui.octoViewer.lastId

    app.ui.tabs.select(type);
}

app.ui.octoViewer.createInstance = async type => {
    const newViewer = $('#tabOctoViewer').clone();
    const newTabId = 'tab' + type + 'Div';

    $('#tabsDivsContainer').append(newViewer);

    $('#tabsDivsContainer > #tabOctoViewer').prop('id', newTabId);
    const newTabDiv = $('#' + newTabId);

    newTabDiv
        .css('display', 'block');

    // regenerate id's
    newTabDiv.find('#btnOctoRun').attr('id', 'btnOctoRun' + type);

    newTabDiv.find('#btnOctoUndo').attr('id', 'btnOctoUndo' + type);
    newTabDiv.find('#btnOctoRedo').attr('id', 'btnOctoRedo' + type);

    newTabDiv.find('#btnOctoZoomOut').attr('id', 'btnOctoZoomOut' + type);
    newTabDiv.find('#btnOctoZoomIn').attr('id', 'btnOctoZoomIn' + type);

    newTabDiv.find('#btnOctoClear').attr('id', 'btnOctoClear' + type);
    newTabDiv.find('#btnOctoClipboard').attr('id', 'btnOctoClipboard' + type);

    newTabDiv.find('#btnOctoTheme').attr('id', 'btnOctoTheme' + type);

    newTabDiv.find('#divTreeButton').attr('id', 'divTreeButton' + type);
    newTabDiv.find('#btnOctoTree').attr('id', 'btnOctoTree' + type);
    newTabDiv.find('#btnOctoTreeRefresh').attr('id', 'btnOctoTreeRefresh' + type);
    newTabDiv.find('#divOctoTableScroller').attr('id', 'divOctoTableScroller' + type);

    newTabDiv.find('#divOctoTables').attr('id', 'divOctoTables' + type);
    newTabDiv.find('#treeOctoTables').attr('id', 'treeOctoTables' + type);
    newTabDiv.find('#divOctoEditor').attr('id', 'divOctoEditor' + type);
    newTabDiv.find('#divOctoSqlArea').attr('id', 'divOctoSqlArea' + type);
    newTabDiv.find('#divOctoSplitter').attr('id', 'divOctoSplitter' + type);

    newTabDiv.find('#divOctoResult').attr('id', 'divOctoResult' + type);
    newTabDiv.find('#divOctoText').attr('id', 'divOctoText' + type);
    newTabDiv.find('#divOctoCli').attr('id', 'divOctoCli' + type);
    newTabDiv.find('#btnOctoCliClear').attr('id', 'btnOctoCliClear' + type);

    newTabDiv.find('#divOctoTable').attr('id', 'divOctoTable' + type);
    //newTabDiv.find('#tblOctoResult').attr('id', 'tblOctoResult' + type);

    newTabDiv.find('#tabOctoText').attr('id', 'tabOctoText' + type);

    newTabDiv.find('#tabOctoResultTabs').attr('id', 'tabOctoResultTabs' + type);
    newTabDiv.find('#divOctoResultDivs').attr('id', 'divOctoResultDivs' + type);


    newTabDiv.find('#divOctoStatusbar').attr('id', 'divOctoStatusbar' + type);
    newTabDiv.find('#inpOctoTimeout').attr('id', 'inpOctoTimeout' + type);
    newTabDiv.find('#inpOctoLimit').attr('id', 'inpOctoLimit' + type);

    // update tabs pointers
    $('#tabOctoText' + type).attr('data-target', '#divOctoText' + type)
    $('#tabOctoTable' + type).attr('data-target', '#divOctoTable' + type)

    // init tree refresh
    app.ui.octoTree.shown[type] = false
    app.ui.octoTree.loaded[type] = false
    app.ui.octoTree.width[type] = 300


    // fetch the table list
    const tables = {}
    try {
        const res = await app.REST._fetchOctoTree(true)

        res.data.tables.forEach(table => {
            tables[table.name] = []
        })

        // populate the tree
        app.ui.octoViewer.treePressed(type, res)

    } catch (err) {
        console.log(err)
    }

    // init code editor
    app.ui.octoViewer.cm[type] = CodeMirror.fromTextArea(document.getElementById('divOctoSqlArea' + type), {
        mode: "text/x-ydboctosql",
        lineNumbers: true,
        lineWrapping: false,
        readOnly: app.serverMode === 'RO',
        viewportMargin: Infinity,
        theme: 'default',
        extraKeys: {"Ctrl-Space": "autocomplete"}, // To invoke the auto complete
        hint: CodeMirror.hint.sql,
        hintOptions: {
            tables: tables
        },
        dragDrop: true
    });

    app.ui.octoViewer.cm[type].on("cursorActivity", doc => {
        const pos = doc.getCursor()

        $('#divOctoStatusbar' + app.ui.tabs.currentTab).html('Line: ' + (pos.line + 1) + ' Col: ' + (pos.ch + 1))
    })

    app.ui.octoViewer.cm[type].on('drop', (cm, e) => app.ui.octoViewer.processDrop(cm, e))

    // assign an id to the CodeMirror DOM
    $('#divOctoEditor' + type).children()[1].id = 'cmOcto' + type

    // initialize splitters
    $('#divOctoEditor' + type)
        .resizable({
            handleSelector: "#divOctoSplitter" + type,
            resizeWidth: false,
            handles: 's',
            // manual track to update table and CLI
            resize: (event, ui) => app.ui.octoViewer.onResizeSplitter(event, ui, type),
        })

    $('#divOctoTables' + type)
        .resizable({
            handleSelector: "#divOctoSplitter" + type,
            resizeHeight: false,
            handles: 'e',
            // manual track to update table and CLI
            resize: (event, ui) => app.ui.octoViewer.onResizeSplitterVertical(event, ui, type),
        })

    app.ui.octoViewer.cm[type].focus()

    // init tab parameters
    app.ui.octoViewer.fontSize[type] = 16
    app.ui.octoViewer.theme[type] = 'light'

    // mount handlers
    $('#btnOctoRun' + type).on('click', () => app.ui.octoViewer.runPressed(type))

    $('#btnOctoUndo' + type).on('click', () => app.ui.octoViewer.undoPressed(type))
    $('#btnOctoRedo' + type).on('click', () => app.ui.octoViewer.redoPressed(type))

    $('#btnOctoZoomOut' + type).on('click', () => app.ui.octoViewer.zoomPressed(type, '-'))
    $('#btnOctoZoomIn' + type).on('click', () => app.ui.octoViewer.zoomPressed(type, '+'))

    $('#btnOctoClear' + type).on('click', () => app.ui.octoViewer.clearText(type))
    $('#btnOctoClipboard' + type).on('click', () => app.ui.octoViewer.copyText(type))

    $('#btnOctoTheme' + type).on('click', () => app.ui.octoViewer.themePressed(type))

    $('#btnOctoTreeRefresh' + type).on('click', () => app.ui.octoTree.refresh(type))

    $('#btnOctoTree' + type).on('click', () => app.ui.octoViewer.treePressed(type))

    $('#btnOctoCliClear' + type).on('click', () => app.ui.octoViewer.cliClear(type))

    $('#cmOcto' + type)
        .on('keypress', e => app.ui.octoViewer.cmKeyPress(e, type))

    // initial resize (emulate jquery-ui call)
    app.ui.octoViewer.onResizeSplitter(null, {
        element: $('#divOctoEditor' + type),
        size: {height: $('#cmOcto' + type).height() + 10}  // add the padding as offset
    })

    // initialize defaults
    $('#inpOctoTimeout' + type).val(app.userSettings.defaults.octo.defaultTimeout)
    $('#inpOctoLimit' + type).val(app.userSettings.defaults.octo.defaultLimit)

    // TEST closure
    app.ui.setTestClosure('tab' + type + 'Div')

    // viewer tabs handlers
    $('#tabOctoText' + type).on('shown.bs.tab', function (e) {
        app.ui.octoViewer.highlightActiveTab(e.target, type)
    })

    if (app.serverMode === 'RO') {
        app.ui.octoViewer.cm[type].setValue('Writing Queries is not available for Read-Only mode.')
    }
}
