/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.octoViewer.tabCounter = []
app.ui.octoViewer.table = (res, sql, type) => {
    // do we need a new tab ?
    let targetTab = null
    $('#tabOctoResultTabs' + type).children().each((ix, el) => {
        if (el.title === sql) targetTab = $(el)
    })

    let tabType

    if (targetTab === null) {
        // *******************************
        // create a new tab
        // *******************************

        // generate unique id
        if (app.ui.octoViewer.tabCounter[type] === undefined) {
            app.ui.octoViewer.tabCounter[type] = 1

        } else app.ui.octoViewer.tabCounter[type]++

        tabType = type + '-Q' + app.ui.octoViewer.tabCounter[type]

        // create the tab
        $('#tabOctoResultTabs' + type).append($('<button/>', {
            class: 'nav-link',
            'data-target': "#divOctoTable" + tabType,
            'data-toggle': "tab",
            id: 'tabOctoTable' + tabType,
            role: "tab",
            html: app.ui.octoViewer.bullet + '&nbsp;&nbsp;<span name="octo-sub-tab">' + app.ui.octoViewer.extractSqlTable(sql) + '</span>&nbsp;&nbsp;&nbsp;<span onclick="app.ui.octoViewer.closeTab(\'tabOctoTable' + tabType + '\')" class="tab-close-button close" style="margin-top: 1px;" title="">x</span>',
            title: sql
        }));

        $('#tabOctoTable' + tabType).on('shown.bs.tab', function (e) {
            app.ui.octoViewer.highlightActiveTab(e.target, type)
        })

        // and div
        const newDiv = $('#divOctoTable').clone()
        const offset = $('#divOctoText' + type).height() - 225

        newDiv
            .attr('id', 'divOctoTable' + tabType)
            .css('height', 'calc(100vh - ' + offset + 'px)')
            .find('#tblOctoResult').attr('id', 'tblOctoResult' + tabType);

        $('#divOctoResultDivs' + type).append(newDiv)

        app.ui.octoViewer.onResizeSplitter(null, {
            element: $('#divOctoEditor' + type),
            size: {height: $('#cmOcto' + type).height() + 10}  // add the padding as offset
        })

    } else {
        // *******************************
        // reuse tab
        // *******************************
        // get id
        tabType = '-' + targetTab.attr('data-target').split('-').splice(1, 99).join('-')

        $('#tabOctoTable' + tabType).html(app.ui.octoViewer.bullet + $('#tabOctoTable' + tabType).html())
    }

    // *******************************
    // populate table
    // *******************************

    // fill header
    const fields = res.data[0].split('|')
    let header = '<tr>'

    fields.forEach((field, ix) => {
        header += '<th class="octo-table-header" scope="col" name="resizable">' + field + '</th>'
    })

    header += '</tr>'

    // fill body
    let body = ''

    res.data.forEach(record => {
        if (record === res.data[0]) return

        // force to string to avoid single field - numbers
        record = record.toString()
        body += '<tr>'

        record.split('|').forEach(field => {
            body += '<td class="octo-table-col">' + field + '</td>'
        })

        body += '</tr>'
    })

    // insert into table
    $('#tblOctoResult' + tabType + '> tbody')
        .empty()

    $('#tblOctoResult' + tabType + '> thead')
        .empty()
        .append(header)


    // resizable columns
    const table = document.getElementById('divOctoTable' + tabType);
    const cols = table.querySelectorAll('th');

    // Mount the column resizer
    [].forEach.call(cols, function (col) {
        if (col.getAttribute('name') === 'resizable') {
            // Create a resizer element
            const resizer = document.createElement('div');
            resizer.classList.add('resizer')

            // Add a resizer element to the column
            col.appendChild(resizer);

            // Set the height
            resizer.style.height = '38px';

            // Will be implemented in the next section
            createResizableColumn(col, resizer);
        }
    });

    $('#tblOctoResult' + tabType + '> tbody')
        .append(body)


    // select it
    $('#tabOctoTable' + tabType).tab('show')
}

// determine the table of the query
app.ui.octoViewer.extractSqlTable = sql => {
    let table = ''

    sql = sql.split('\n')

    sql.forEach(statement => {
        if (statement.indexOf('--') > -1 || statement.indexOf('#') > -1) return
        const splitted = statement.split(' ')

        if (splitted.length === 1 && (splitted[0] === '\\d' || splitted[0] === '\\dv')) {
            table = splitted[0]

            return
        }

        switch (splitted[0].toLowerCase()) {
            case 'update':
            case 'drop':
            case '\\d': {
                table = splitted[1]

                return
            }
            case 'select':
                if (splitted.length === 4 && splitted[2] === 'limit') {
                    table = '???'

                    return
                }
        }

        splitted.forEach((el, ix) => {
            el = el.toLowerCase()
            if (el === 'from' || el === 'to' || el === 'into' || el === 'exists' || el === 'table') table = splitted[ix + 1] || ''
        })

    })

    return table
}

