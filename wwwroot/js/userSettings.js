/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.userSettings = {
    rest: {
        timeout: {
            short: 10,     // 10 seconds
            long: 3600     // 1 hour
        },
        logOnConsole: 'no'
    },
    dashboard: {
        flashInterval: 500,
        storageRanges: [
            {
                min: 0,
                max: 70,
                class: 'ydb-status-green',
                flash: false
            },
            {
                min: 71,
                max: 90,
                class: 'ydb-status-amber',
                flash: false
            },
            {
                min: 91,
                max: 97,
                class: 'ydb-status-red',
                flash: false
            },
            {
                min: 98,
                max: 100,
                class: 'ydb-status-red',
                flash: true
            },
        ],
        regionRanges: [
            {
                min: 0,
                max: 70,
                class: 'ydb-status-green-readable',
                flash: false
            },
            {
                min: 71,
                max: 90,
                class: 'ydb-status-amber',
                flash: false
            },
            {
                min: 91,
                max: 97,
                class: 'ydb-status-red',
                flash: false
            },
            {
                min: 98,
                max: 100,
                class: 'ydb-status-red',
                flash: false
            },
        ],
        regions: {},
        autoExtend: [
            {
                min: 0,
                max: 2,
                class: 'ydb-status-red',
                flash: true
            },
            {
                min: 3,
                max: 5,
                class: 'ydb-status-red',
                flash: false
            },
            {
                min: 6,
                max: 10,
                class: 'ydb-status-amber',
                flash: false
            },
            {
                min: 11,
                max: 999999,
                class: 'ydb-status-green',
                flash: false
            },
        ],
        manualExtend: [
            {
                min: 0,
                max: 70,
                class: 'ydb-status-green',
                flash: false
            },
            {
                min: 71,
                max: 80,
                class: 'ydb-status-amber',
                flash: false
            },
            {
                min: 81,
                max: 90,
                class: 'ydb-status-red',
                flash: false
            },
            {
                min: 91,
                max: 100,
                class: 'ydb-status-red',
                flash: true
            },
        ],
        activeJournalRanges: [
            {
                min: 0,
                max: 70,
                class: 'range-bg-green'
            },
            {
                min: 71,
                max: 90,
                class: 'range-bg-amber'
            },
            {
                min: 91,
                max: 97,
                class: 'range-bg-red'
            },
            {
                min: 98,
                max: 100,
                class: 'range-bg-red'
            },

        ]
    },
    globalViewer: {
        options: {
            table: {
                buffering: {
                    initialFetchSize: 600,
                    maxSize: 1000,
                    lazyFetchSize: 100,
                    startFetchingPages: 3
                },
                maxDataLength: 512,            // The maximum length of the $GET string. If longer, we will fetch it with a button
            },
            pathBar: {
                fetchAt: 2,
                fetchAllSize: 10
            },
            tab: {
                maxLengthCaption: 25,
            },
            menuLastUsedItemsCount: 10,
            lastUsedMaxItems: 100,
        },
        themes: {
            light: {
                background: 'white',
                border: 'solid gray 1px;',
                titleBar: {
                    background: 'white',
                    globalName: '#0000ff',
                    parens: '#ff00ff',
                    numbers: '#ff0000',
                    strings: '#00aa00',
                    comma: '#600000',
                    colon: '#ff0000',
                    star: '#ff0000'
                },
                toolbar: {
                    background: 'black',
                    icons: 'orange'
                },
                table: {
                    bookmarkColor: 'red',
                    headers: {
                        bookmark: {
                            fontSize: '11px',
                            back: '#808080',
                        },
                        fontSize: '14px',
                        fore: '#0000ff',
                        back: '#d3d3d3',
                    },
                    content: {
                        background: '#ffffff',
                        globalName: '#0000ff',
                        parens: '#ff00ff',
                        numbers: '#ff0000',
                        strings: '#00aa00',
                        comma: '#600000',
                        fontSize: 16,
                        values: {
                            showPieces: 'pills',            // yes, no, pills
                            data: {
                                fore: '#00a000',
                            },
                            pieceChar: {
                                fore: '#ff0000',
                                back: '#a8ffca',
                                char: '^',
                            },
                            piecePills: {
                                paddingLr: '12px',
                                borderRadius: '5px',
                                back: '#a8ffca',
                                fore: '#004cff'
                            },
                        },
                        bookmarks: {
                            fore: '#0000ff',
                            back: '#c0c0c0',
                        }
                    },
                }
            },
            dark: {
                background: 'white',
                border: 'solid gray 1px;',
                titleBar: {
                    background: 'white',
                    globalName: '#0000ff',
                    parens: '#ff00ff',
                    numbers: '#ff0000',
                    strings: '#00aa00',
                    comma: '#600000',
                    colon: '#ff0000',
                    star: '#ff0000'
                },
                toolbar: {
                    background: 'black',
                    icons: 'orange'
                },
                table: {
                    bookmarkColor: 'red',
                    headers: {
                        bookmark: {
                            fontSize: '11px',
                            back: '#808080',
                        },
                        fontSize: '14px',
                        fore: '#0000ff',
                        back: '#d3d3d3',
                    },
                    content: {
                        background: '#ffffff',
                        globalName: '#fbf074',
                        parens: '#ff0033',
                        numbers: '#00ffff',
                        strings: '#ee0000',
                        comma: '#d0d0d0',
                        fontSize: 16,
                        values: {
                            showPieces: 'pills',            // yes, no, pills
                            data: {
                                fore: '#00e600',
                            },
                            pieceChar: {
                                fore: '#7affa7',
                                back: '#6666ff',
                                char: '^',
                            },
                            piecePills: {
                                paddingLr: '12px',
                                borderRadius: '5px',
                                back: '#bebef9',
                                fore: '#0033ff'
                            },
                        },
                        bookmarks: {
                            fore: '#0000ff',
                            back: '#c0c0c0',
                        }
                    },
                }
            }
        },
        defaultTheme: 'light',
        lastUsed: []
    },
    stats: {
        webSockets: {
            connectionTimeout: 3000,
            defaultResponseTimeout: 1000
        },
        displayHelpAtTabOpen: true,
        sparkLines: {
            sections: {
                reportLine: {
                    header: {                                                           // ok
                        border: {
                            color: 'gray',
                            thickness: '1'
                        },
                    },
                    region: {                                                           // ok
                        background: '#ff7f27',
                        border: {
                            color: 'lightgray',
                            thickness: '0'
                        },
                        font: {
                            size: 24,
                            color: '#3b1a68',
                            fontFamily: 'Inconsolata',
                            weight: 'bold',
                            style: 'normal',
                            decoration: {
                                type: 'normal',
                                color: 'red',
                                style: 'wavy',
                                thickness: 1
                            }
                        },
                        align: 'center',
                    },
                    process: {                                                          // ok
                        background: '#ff7f27',
                        border: {
                            color: 'silver',
                            thickness: '1'
                        },
                        font: {
                            size: 15,
                            color: '#3b1a68',
                            fontFamily: 'Inconsolata',
                            weight: 'normal',
                            style: 'normal',
                            decoration: {
                                type: 'normal',
                                color: 'red',
                                style: 'wavy',
                                thickness: 1
                            }
                        },
                        align: 'left',
                    },
                    sample: {                                                           // ok
                        background: '#dfdfdf',
                        border: {
                            color: 'silver',
                            thickness: '1'
                        },
                        font: {
                            size: 17,
                            color: '#ff7f27',
                            fontFamily: 'Inconsolata',
                            weight: 'bold',
                            style: 'normal',
                            decoration: {
                                type: 'normal',
                                color: 'red',
                                style: 'wavy',
                                thickness: 1
                            }
                        },
                        align: 'center',
                    },
                    sampleMap: {                                                        // ok
                        background: '#b3b3b3',
                        border: {
                            color: 'silver',
                            thickness: '1'
                        },
                        font: {
                            size: 17,
                            color: '#ffffff',
                            fontFamily: 'Inconsolata',
                            weight: 'normal',
                            style: 'normal',
                            decoration: {
                                type: 'normal',
                                color: 'red',
                                style: 'wavy',
                                thickness: 1
                            }
                        },
                        align: 'center',
                    },
                    value: {                                                            //ok
                        background: '#f8f8f8',
                        border: {
                            color: 'gray',
                            thickness: '1'
                        },
                        font: {
                            size: 15,
                            color: 'indigo',
                            fontFamily: 'Inconsolata',
                            weight: 'bold',
                            style: 'normal',
                            decoration: {
                                type: 'normal',
                                color: 'red',
                                style: 'dotted',
                                thickness: 2
                            }
                        },
                        align: 'center',
                        formatting: {
                            thousands: true
                        },
                    },
                    noValue: {                                                            //ok
                        background: 'white',
                        border: {
                            color: 'gray',
                            thickness: '1'
                        },
                    },
                    highlighters: {
                        low: '#64a555',
                        mid: '#eab83b',
                        high: '#ce3a3a',
                        topProcess: '#64a555'
                    }
                },
                reportLineDark: {
                    header: {                                                           // ok
                        border: {
                            color: 'gray',
                        },
                    },
                    region: {                                                           // ok
                        background: '#5b3333',
                        border: {
                            color: 'lightgray',
                        },
                        font: {
                            color: '#ff7f27',
                            decoration: {
                                color: 'red',
                            }
                        },
                    },
                    process: {                                                          // ok
                        background: '#000000',
                        border: {
                            color: 'silver',
                        },
                        font: {
                            color: '#eab83b',
                            decoration: {
                                color: 'red',
                            }
                        },
                    },
                    sample: {                                                           // ok
                        background: '#000000',
                        border: {
                            color: 'silver',
                        },
                        font: {
                            color: '#ff7f27',
                            decoration: {
                                color: 'red',
                            }
                        },
                    },
                    sampleMap: {                                                        // ok
                        background: '#000000',
                        border: {
                            color: 'silver',
                        },
                        font: {
                            color: '#ffffff',
                            decoration: {
                                color: 'red',
                            }
                        },
                    },
                    value: {                                                            //ok
                        background: '#000000',
                        border: {
                            color: 'gray',
                        },
                        font: {
                            color: '#64a555',
                            decoration: {
                                color: 'red',
                            }
                        },
                    },
                    noValue: {                                                            //ok
                        background: '#000000',
                        border: {
                            color: 'gray',
                        },
                    },
                },
                graph: {                                                                // to do here and below
                    buffering: 200,
                    series: {
                        default: 'line',                                                // line, area
                        colors: [
                            '#ff822e',
                            '#2effc0',
                            '#ff2e93',
                            '#962eff',
                            '#2ed5ff',
                            '#8cff2e',
                            '#ff0000',
                            '#00ff00',
                            '#0000ff',
                            '#ffff00',
                            '#ff00ff',
                            '#00ffff',
                        ],
                        line: {
                            stroke: {
                                type: 'smooth',                                         // smooth, straight, stepline
                                smoothTension: 0.2,
                                color: '#a03423',
                                width: 2,
                                dashArray: 0,
                            },
                            marking: {
                                type: 'none',                                               // none, markers
                                markers: {
                                    size: 3,
                                    color: '#ff0000',
                                    shape: 'square'                                     // circle, cross, rect, rectRounded, star, triangle
                                },
                            }
                        },
                        area: {
                            stroke: {
                                type: 'smooth',                                         // smooth, straight, stepline
                                width: 2,
                                dashArray: 0,
                                color: ''
                            },
                            fill: {
                                type: 'gradient',                                   // solid, gradient
                                gradient: {
                                    opacityFrom: 0.9,
                                    opacityTo: 0.2,
                                    type: 'vertical',                                // vertical, horizontal, diagonal1, diagonal2
                                    shade: 'dark',                                 // light, dark
                                }
                            },
                            marking: {
                                type: 'none',                                           // none, markers, dataLabels
                                markers: {
                                    size: 0,
                                    color: '#ff0000',
                                    shape: 'circle'                                     // circle, square
                                },
                                dataLabels: {
                                    font: {
                                        size: 9,
                                        family: 'Inconsolata',
                                        weight: 'normal',
                                        color: '#2ed5ff',
                                    },
                                }
                            }
                        },
                    },
                    chart: {
                        globals: {
                            background: '#ffffff',
                            color: '#373d3f',
                            fontFamily: 'FiraGO',
                            theme: 'light'
                        },
                        globalsDark: {
                            background: '#000000',
                            color: '#ffffff',
                            gridRowOpacity: 0.1
                        },
                        title: {
                            display: true,
                            text: '',
                            align: 'center',                                     // start, center, end
                            position: 'top',                                    // top, left, bottom, right
                            style: {
                                size: 15,
                                fontFamily: 'Inconsolata',
                                weight: 'bold',
                                color: '#ff7f27',
                            }
                        },
                        subTitle: {
                            display: false,
                            text: '',
                            align: 'center',                                     // start, center, end
                            position: 'top',                                    // top, left, bottom, right
                            style: {
                                size: 13,
                                fontFamily: 'Inconsolata',
                                weight: 'normal',
                                color: '#ff7f27',
                            }
                        },
                        legend: {
                            display: true,
                            position: 'bottom',                                    // top, bottom, left, right
                            hAlign: 'center',                                   // start, center, end
                            font: {
                                size: 11,
                                fontFamily: 'Inconsolata',
                                weight: 'normal',
                                color: '#ff7f27',
                            },
                        },
                        grid: {
                            row: {
                                color: '#f3f3f3',
                            },
                            column: {
                                color: '#f3f3f3'
                            },
                            xAxisLines: true,
                            yAxisLines: true,
                        },
                        xaxis: {
                            labels: {
                                show: true,
                                colors: '#808080',
                                fontSize: 11,
                                fontFamily: 'Inconsolata',
                                fontWeight: 'bold',
                            },
                        },
                        tooltips: {
                            enabled: true,
                        },
                        yAxis: {
                            common: true,
                            defaultsCommon: {
                                show: true,
                                type: 'linear',                             // linear, logarithmic
                                min: 1,
                                max: 100,
                                color: '#ff7f27',
                                useManualSetting: false,
                                labels: {
                                    show: true,
                                    style: {
                                        fontSize: 12,
                                        fontFamily: 'Inconsolata',
                                        fontWeight: 'normal'
                                    },
                                    rotate: 0,
                                },
                                axisBorder: {
                                    show: true,
                                },
                                title: {
                                    align: 'center',
                                    text: '',
                                    style: {
                                        colors: '#ff7f27',
                                        fontSize: 12,
                                        fontFamily: 'Inconsolata',
                                        fontWeight: 'normal'
                                    }
                                }
                            },
                            defaultsSeries: {
                                show: true,
                                type: 'linear',                             // linear, logarithmic
                                min: 1,
                                max: 100,
                                useManualSetting: false,
                                labels: {
                                    show: true,
                                    style: {
                                        fontSize: 12,
                                        fontFamily: 'Inconsolata',
                                        fontWeight: 'normal'
                                    },
                                },
                                axisBorder: {
                                    show: true,
                                },
                                title: {
                                    align: 'center',
                                    text: '',
                                    style: {
                                        fontSize: 12,
                                        fontFamily: 'Inconsolata',
                                        fontWeight: 'normal'
                                    }
                                }
                            }
                        }
                    },
                },
            },
            theme: 'light'
        }
    },
    replicationFiles: [],
    current: {version: 5.65}
}

app.userSettings.defaults = {
    backup: {
        target: '/',
        replTarget: '/'
    },
    octo: {
        defaultTimeout: 3600,
        defaultLimit: 1000,
        displayHelpOnDrop: true,
        treeViewsDefMaxLength: 20
    },
    replication: {
        discoveryService: {
            responseType: 'F',
            servers: [
                {
                    instance: '',
                    host: '',
                    port: 0,
                    statsPort: 0,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 0,
                    statsPort: 0,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 0,
                    statsPort: 0,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 0,
                    statsPort: 0,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 0,
                    statsPort: 0,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 0,
                    statsPort: 0,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 0,
                    statsPort: 0,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 0,
                    statsPort: 0,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 80,
                    statsPort: 81,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 80,
                    statsPort: 81,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 80,
                    statsPort: 81,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 80,
                    statsPort: 81,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 80,
                    statsPort: 81,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 80,
                    statsPort: 81,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 80,
                    statsPort: 81,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
                {
                    instance: '',
                    host: '',
                    port: 80,
                    statsPort: 81,
                    protocol: 'http',
                    tlsFileLocation: '',
                    verifyCertificates: true,
                },
            ],
            timeouts: {
                onConnect: 500,
                onResponse: 10000,
            }
        },
        topology: {
            refresh: "2",
            backlog: {
                processRange: true
            },
            links: {
                connector: 'straight',
                router: 'normal'
            },
            layout: 'TB',
            undoBufferSize: 200,

        },
        dashboard: {
            refreshBacklog: '2',
            processRange: true
        }
    },
    mainScreenRefresh: '5',
}


// we make a difference between settings and lists of recently used items...
// Therefore, we use one item for settings, which comprehend the whole settings
// and separate items for the recently used items, which can be big
//
// current valid keys are: 'dashboard', 'gViewer', 'current', 'defaults', 'stats'
//
// a separate key is used for Stats (statsSources), as we don't want to overwrite them on new userSettings version

app.ui.storage.init = async () => {
    if (typeof (Storage) !== "undefined") {
        // localStorage is supported, check if already initialized
        const current = app.ui.storage.get('current');

        if (current === null || storageReset === true || app.userSettings.current.version !== current.version) {

            // perform hives initialization
            app.ui.storage.save('dashboard', app.userSettings.dashboard);
            app.ui.storage.save('gViewer', app.userSettings.globalViewer);
            app.ui.storage.save('current', app.userSettings.current);
            app.ui.storage.save('defaults', app.userSettings.defaults);
            app.ui.storage.save('rest', app.userSettings.rest);
            app.ui.storage.save('stats', app.userSettings.stats);
            app.ui.storage.save('replicationFiles', app.userSettings.replicationFiles);

            // initialize the default statistics report if needed
            console.log('Loading default stats reports...')
            await loadTestScript('js/prefs/defaultReports.js')
            app.ui.storage.save('statsSources', demoStatsReports)

        } else {
            //fetch settings from the HIVES
            app.userSettings.dashboard = app.ui.storage.get('dashboard');
            app.userSettings.globalViewer = app.ui.storage.get('gViewer')
            app.userSettings.defaults = app.ui.storage.get('defaults')
            app.userSettings.rest = app.ui.storage.get('rest')
            app.userSettings.stats = app.ui.storage.get('stats')
            app.userSettings.current = app.ui.storage.get('current')
            app.userSettings.replicationFiles = app.ui.storage.get('replicationFiles')

            // override demo report if needed
            const reports = app.ui.storage.get('statsSources')
            let found = false

            // checking stats demo report
            for (let report in reports) {
                if (reports[report].name === 'demo') {
                    // report found
                    found = true

                    // overwrite it
                    await loadTestScript('js/prefs/defaultReports.js')
                    reports[report] = demoStatsReports[0]
                    app.ui.storage.save('statsSources', reports)
                }
            }

            if (found === false) {
                // append demo report
                await loadTestScript('js/prefs/defaultReports.js')
                reports.push(demoStatsReports[0])

                app.ui.storage.save('statsSources', reports)
            }
        }

    } else {
        // No web storage Support.
        console.log('Your browser does NOT support local storage. You can not store / change settings.')
    }
};

app.ui.storage.save = (key, object) => {
    window.localStorage.setItem(key, JSON.stringify(object))
};

app.ui.storage.get = key => {
    return JSON.parse(window.localStorage.getItem(key))
};
