/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.defrag.init = () => {
    const fillFactor = $('#rngCompactFillFactor');
    const indexFillFactor = $('#rngCompactIndexFillFactor');
    const rngCompactReclamation = $('#rngCompactReclamation');

    fillFactor.on('input', () => app.ui.defrag.fillFactorChange());
    fillFactor.on('change', () => app.ui.defrag.fillFactorChange());
    indexFillFactor.on('input', () => app.ui.defrag.indexFillFactorChange());
    indexFillFactor.on('change', () => app.ui.defrag.indexFillFactorChange());
    rngCompactReclamation.on('input', () => app.ui.defrag.reclamationFactorChange());
    rngCompactReclamation.on('change', () => app.ui.defrag.indexFillFactorChange());

    $('#swCompactRecover').on('change', () => app.ui.defrag.recoverClicked())

    $('#btnCompactOk').on('click', () => app.ui.compact.okPressed());

    app.ui.defrag.resumeMode = false;
    4
    $('#btnCompactResultCopyToClipboard').on('click', () => app.ui.defrag.copyToClipboardResult())
    $('#btnCompactResultDetailsCopyToClipboard').on('click', () => app.ui.defrag.copyToClipboardResultDetails())

    app.ui.setupDialogForTest('modalDefrag')
    app.ui.setupDialogForTest('modalDefragResult')
    app.ui.setupDialogForTest('modalDefragDetailsResult')
};

app.ui.defrag.show = () => {
    let regions = [];
    app.ui.defrag.resumeMode = false;

    // determine access type for each region that has a pending reorg
    Object.keys(app.system.regions).forEach((region) => {
        if (app.system.regions[region].dbFile !== undefined && app.system.regions[region].dbFile.flags !== undefined && app.system.regions[region].dbFile.flags.pendingReorg !== undefined && app.system.regions[region].dbFile.flags.pendingReorg === true) {
            const regionData = {};
            regionData.id = region;
            regionData.data = app.ui.getKeyValue(app.system.regions[region].dbFile.data, "ACCESS_METHOD")
            regionData.text = region + ' (' + regionData.data + ")";
            regionData.icon = false;
            regionData.state = {selected: true};

            regions.push(regionData)
        }
    });

    // check pending reorg
    if (regions.length > 0) {
        // pending reorg in effect, display msgbox
        app.ui.defrag.resumeMode = true;

        let regionList = '';
        regions.forEach(region => {
            regionList += region.id + '<br>'
        });
        app.ui.inputbox.show('The following region(s) have an interrupted REORG status:<br>' + regionList + '<br>Do you want to complete the REORG procedure ?', 'WARNING', async (opt) => {
            if (opt === 'YES') {

                // populate the region list
                const tree = $("#treeCompactRegions");

                tree.jstree("destroy");
                tree.jstree({
                    "checkbox": {
                        "keep_selected_style": false
                    }, "plugins": ["checkbox"], 'core': {
                        'data': regions, 'themes': {
                            'name': 'proton',
                        }
                    },
                });
                tree.on("changed.jstree", (e, data) => {
                    app.ui.defrag.validateOk();
                });

                // reset all values
                $('#divDefragFillFactor').css('display', 'none')
                $('#divDefragIndexFillFactor').css('display', 'none')
                $('#divDefragReclaim').css('display', 'none')
                $('#swCompactRunBackground').prop('checked', false)
                $('#collapseReclaim').collapse('hide');

                $('#lblReorgTitle').text('Reorg (Defrag / Compact) RESUME MODE')
                $('#modalDefrag').modal({show: true, backdrop: 'static'})

            } else {
                app.ui.compact.initRegular()
            }
        });
    } else {
        app.ui.compact.initRegular()
    }
};

app.ui.defrag.fillFactorChange = () => {
    $('#lblCompactFillFactor').text($('#rngCompactFillFactor').val())
};

app.ui.defrag.indexFillFactorChange = () => {
    $('#lblCompactIndexFillFactor').text($('#rngCompactIndexFillFactor').val())
};

app.ui.defrag.reclamationFactorChange = () => {
    $('#lblCompactReclamation').text($('#rngCompactReclamation').val())
};

app.ui.defrag.validateOk = () => {
    const tree = $("#treeCompactRegions");
    const btn = $('#btnCompactOk');

    if (tree.jstree("get_checked").length !== 0) {
        btn.removeClass('disabled');
        btn.prop("disabled", false)
    } else {
        btn.addClass('disabled');
        btn.prop("disabled", true)
    }
};

app.ui.defrag.recoverClicked = () => {
    let mmFound = false
    const selElem = $("#treeCompactRegions").jstree("get_checked", true);
    selElem.forEach(el => {
        const region = {};
        region.name = el.id;
        region.segment = {
            accessType: el.data
        };

        if (el.data === 'MM') mmFound = true
    })

    if (mmFound === true) {
        $('#swCompactRecover').prop('checked', false);
        $('#collapseReclaim').collapse('hide')
        app.ui.msgbox.show('One or more regions have an MM segment.<br>' + 'If you wish to recover space on an MM segment, convert it to BG before to proceed.', 'Information')

    } else {
        $('#collapseReclaim').collapse($('#swCompactRecover').prop('checked') === true ? 'show' : 'hide')
    }
};

app.ui.compact.initRegular = () => {
    let regions = [];
    Object.keys(app.system.regions).forEach((region) => {
        const regionData = {};
        regionData.id = region;
        regionData.data = app.ui.getKeyValue(app.system.regions[region].dbFile.data, "ACCESS_METHOD")
        regionData.text = region + ' (' + regionData.data + ")";
        regionData.icon = false;
        regionData.state = {selected: false};

        regions.push(regionData)
    })

    // populate the region list
    const tree = $("#treeCompactRegions");

    tree.jstree("destroy");
    tree.jstree({
        "checkbox": {
            "keep_selected_style": false
        }, "plugins": ["checkbox"], 'core': {
            'data': regions, 'themes': {
                'name': 'proton',
            }
        },
    });
    tree.on("changed.jstree", (e, data) => {
        if ($('#swCompactRecover').prop('checked') === true && data.node.data === 'MM') {
            $('#swCompactRecover').prop('checked', false);
            $('#collapseReclaim').collapse('hide');

            app.ui.msgbox.show('This region has an MM segment AND the option \'Reclaim deleted space\' is checked.<br>' + 'If you wish to recover space on an MM segment, convert it to BG before to proceed.', 'Information')
        }

        app.ui.defrag.validateOk();
    });

    // reset all values
    $('#divDefragFillFactor').css('display', 'flex')
    $('#divDefragIndexFillFactor').css('display', 'flex')
    $('#divDefragReclaim').css('display', 'flex')

    $('#rngCompactFillFactor').val(100);
    $('#rngCompactIndexFillFactor').val(100);
    $('#rngCompactReclamation').val(0);
    $('#lblCompactFillFactor').text('100');
    $('#lblCompactIndexFillFactor').text('100');
    $('#lblCompactReclamation').text('0');
    $('#swCompactRecover').prop('checked', false)
    $('#swCompactRunBackground').prop('checked', false)
    $('#collapseReclaim').collapse('hide');

    $('#lblReorgTitle').text('Reorg (Defrag / Compact)')
    $('#modalDefrag').modal({show: true, backdrop: 'static'})

    // disable ok button
    $('#btnCompactOk').attr('disabled', true)
}

app.ui.compact.okPressed = () => {
    const runBackground = $('#swCompactRunBackground').is(':checked');
    const bgndText = runBackground ? 'You have chosen to run the defragment / compact in the background. When it will complete it will alert you with the result.<br>' : '';

    app.ui.inputbox.show(bgndText + 'Do you want to proceed with the Reorg (Defrag / Compact) operation ?', 'Defrag / Compact', async (opt) => {
        if (opt === 'YES') {
            const compactData = {};
            const selRegions = [];

            const selElem = $("#treeCompactRegions").jstree("get_checked", true);
            selElem.forEach(el => {
                selRegions.push(el.id)
            });

            compactData.resume = app.ui.defrag.resumeMode;
            compactData.fillFactor = parseInt($('#rngCompactFillFactor').val());
            compactData.indexFillFactor = parseInt($('#rngCompactIndexFillFactor').val());
            if ($('#swCompactRecover').prop('checked') === true) {
                compactData.recover = parseInt($('#rngCompactReclamation').val());
            } else {
                compactData.recover = false
            }
            compactData.regions = selRegions;

            $('#modalDefrag').modal('hide');

            if (runBackground === false) {
                app.ui.wait.show("Defragmenting the selected regions...");

            } else {
                app.ui.toaster.startInterval('Reorg')
            }

            try {
                const res = await app.REST._defrag(compactData)
                if (app.ui.defrag.resumeMode === true) app.ui.dashboard.refresh()

                if (runBackground) {
                    if (res.result !== undefined && res.result === 'OK') {
                        app.ui.toaster.createToast('defrag', 'Reorg', 'Click to display the report...', 'success', res);

                    } else {
                        app.ui.toaster.createToast('defrag', 'Reorg', 'Click to display the report...', 'fatal', res);
                    }

                } else {
                    app.ui.wait.hide();

                    // display the report
                    app.ui.defrag.displayResult(res)
                }
            } catch (err) {
                app.ui.wait.hide();

                app.ui.msgbox.show(app.REST.parseError(err), 'ERROR')
            }
        }
    });
};

app.ui.defrag.displayResult = res => {
    let result = res.data
    let regions = res.payloadRegions
    const tableBody = $("#tblCompactResult > tbody");

    tableBody.empty();

    // if result = "noGlobals" then all globals in list had no globals
    // if one or more regions are missing in the reports, means that they had no globals

    // if truncate then totals.truncate = 'noSpace'

    // adjust the regions by detecting which one is missing
    if (result === 'noGlobals') {
        result = {};

    } else {
        Object.keys(result).forEach(region => {
            regions = regions.filter(el => el !== region)
        });
    }
    regions.forEach(region => {
        result[region] = "noGlobals"


    });

    Object.keys(result).forEach(regionName => {
        if (result[regionName].incompleteGlobalList === true) {
            app.ui.msgbox.show('Due to the previously interrupted REORG, the global list in region: ' + regionName + ' is incomplete...', 'WARNING')
        }
        tableBody.append('<tr><td colspan="2" class="compact-result-region-header">Region: ' + regionName + '</td></tr>');
        const region = result[regionName];

        if (region === 'noGlobals') {
            // no globals
            tableBody.append('<tr><td colspan="2" style="text-align: center;">No globals found</td></tr>');

        } else {
            // reorg data region summary
            tableBody.append('<tr><td>Globals in region:</td><td class="defrag-report-td">' + app.ui.formatThousands(region.totals.globals) + '</td></tr>');
            tableBody.append('<tr><td style="width: 300px;">Blocks processed:</td><td class="defrag-report-td">' + app.ui.formatThousands(region.totals.processed) + '</td></tr>');
            tableBody.append('<tr><td>Blocks coalesced:</td><td class="defrag-report-td">' + app.ui.formatThousands(region.totals.coalesced) + '</td></tr>');
            tableBody.append('<tr><td>Blocks split:</td><td class="defrag-report-td">' + app.ui.formatThousands(region.totals.split) + '</td></tr>');
            tableBody.append('<tr><td>Blocks swapped:</td><td class="defrag-report-td">' + app.ui.formatThousands(region.totals.swapped) + '</td></tr>');
            tableBody.append('<tr><td>Blocks freed:</td><td class="defrag-report-td">' + app.ui.formatThousands(region.totals.freed) + '</td></tr>');
            tableBody.append('<tr><td>Blocks reused:</td><td class="defrag-report-td">' + app.ui.formatThousands(region.totals.reused) + '</td></tr>');
            tableBody.append('<tr><td>Blocks extended:</td><td class="defrag-report-td">' + app.ui.formatThousands(region.totals.extended) + '</td></tr>');

            if (region.totals.truncate !== undefined) {
                if (region.totals.truncate.freeBlocks !== undefined) {
                    const totalBytes = region.totals.truncate.totalBlocks;
                    const freeBytes = region.totals.truncate.freeBlocks;
                    tableBody.append('<tr><td>Reduced total blocks:</td><td class="defrag-report-td">from ' + app.ui.formatThousands(totalBytes.from) + ' to ' + app.ui.formatThousands(totalBytes.to) + '</td></tr>');
                    tableBody.append('<tr><td>Reduced free blocks:</td><td class="defrag-report-td">from ' + app.ui.formatThousands(freeBytes.from) + ' to ' + app.ui.formatThousands(freeBytes.to) + '</td></tr>');

                } else {
                    tableBody.append('<tr><td>Reduced total blocks:</td><td >Insufficient space to meet truncate target percentage</td></tr>');
                }
            }

            // append button for details
            tableBody.append('<tr><td colspan="2" style="padding: 0">' +
                '<button class="btn btn-outline-info btn-block btn-sm" id="btnDefragDetails' + regionName + '" tabindex="10" type="button">View breakdown by global</button>' +
                '</td></tr>')
            region.name = regionName
            $('#btnDefragDetails' + regionName).on('click', () => app.ui.defrag.displayDetailsResult(region))
        }

        tableBody.append('<tr><td colspan="2" style="height:30px;"></td></tr')
    });

    $('#modalDefragResult').modal({show: true, backdrop: 'static'});
};

app.ui.defrag.displayDetailsResult = region => {
    $('#defragDetailsReportHeader').text(region.name);

    const tableBody = $("#tblCompactDetailResult > tbody");

    tableBody.empty();

    Object.keys(region.byGlobal).forEach(globalName => {
        const global = region.byGlobal[globalName];

        tableBody.append('<tr>' + '<td>' + (globalName.slice(0, 1) === '#' ? '' : '^') +
            globalName + '</td>' + '<td class="defrag-detail-report-td">' +
            app.ui.formatThousands(global.processed) + '</td>' +
            '<td class="defrag-detail-report-td">' +
            app.ui.formatThousands(global.coalesced) + '</td>' +
            '<td class="defrag-detail-report-td">' +
            app.ui.formatThousands(global.split) +
            '</td>' + '<td class="defrag-detail-report-td">' +
            app.ui.formatThousands(global.swapped) +
            '</td>' + '<td class="defrag-detail-report-td">' +
            app.ui.formatThousands(global.freed) + '</td>' +
            '<td class="defrag-detail-report-td">' +
            app.ui.formatThousands(global.reused) + '</td>' +
            '<td class="defrag-detail-report-td">' +
            app.ui.formatThousands(global.extended) + '</td>' + '</tr>')
    });

    $('#modalDefragDetailsResult').modal({show: true, backdrop: 'static'});
};

app.ui.defrag.copyToClipboardResult = () => {
    const trs = $("#tblCompactResult > tbody > tr");
    let clipboard = ''

    $(trs).each((ix, line) => {
        const td1 = $(line).children()[0]
        const td2 = $(line).children()[1]

        if (td1.colSpan === 2) {
            const text = $(td1)[0].textContent

            if (text.indexOf('View breakdown') === -1) clipboard += text + '\n'

        } else {
            clipboard += $(td1)[0].textContent + '\t' + ($(td1)[0].textContent.length < 18 ? '\t' : '')
            clipboard += $(td2)[0].textContent + '\n'
        }
    })

    app.ui.copyToClipboard(clipboard, $('#btnCompactResultCopyToClipboard'))
}

app.ui.defrag.copyToClipboardResultDetails = () => {
    const headers = $("#tblCompactDetailResult > thead > tr > th");
    const trs = $("#tblCompactDetailResult > tbody > tr");
    let clipboard = ''

    $(headers).each((ix, th) => {
        clipboard += $(th)[0].textContent + '\t'
    })

    clipboard += '\n\n'

    $(trs).each((ix, line) => {
        $(line).children().each((ix, td) => {
            clipboard += $(td)[0].textContent + '\t' + ($(td)[0].textContent.length < 6 ? '\t' : '')
        })

        clipboard += '\n'
    })

    app.ui.copyToClipboard(clipboard, $('#btnCompactResultDetailsCopyToClipboard'))
}
