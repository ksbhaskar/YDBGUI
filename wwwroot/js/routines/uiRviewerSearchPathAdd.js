/****************************************************************
 *                                                              *
 * Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

// *********************************************
// Add path dialog
// *********************************************
app.ui.rViewer.searchPath.initAdd = () => {
    // display path
    $('#inpRoutinesSearchPathAdd')
        .on('keyup', () => app.ui.rViewer.searchPath.targetPathChanged())
        .on('change', () => app.ui.rViewer.searchPath.targetPathChanged())
    $('#btnRoutinesSearchPathAddSelect').on('click', () => app.ui.rViewer.searchPath.targetPathSelect())
    $('#btnRoutinesSearchPathAddValidate').on('click', () => app.ui.rViewer.searchPath.targetPathValidate())

    $('#btnRoutinesSearchPathAddOk').on('click', () => app.ui.rViewer.searchPath.AddDirOkPressed())

    // display file
    $('#inpRoutinesSearchFileAdd')
        .on('keyup', () => app.ui.rViewer.searchPath.targetFileChanged())
        .on('change', () => app.ui.rViewer.searchPath.targetFileChanged())
    $('#btnRoutinesSearchFileAddSelect').on('click', () => app.ui.rViewer.searchPath.targetFileSelect())
    $('#btnRoutinesSearchFileAddValidate').on('click', () => app.ui.rViewer.searchPath.targetFileValidate())

    $('#btnRoutinesSearchFileAddOk').on('click', () => app.ui.rViewer.searchPath.AddFileOkPressed())

    app.ui.setupDialogForTest('modalRoutinesSearchPathAdd')
    app.ui.setupDialogForTest('modalRoutinesSearchFileAdd')
}

app.ui.rViewer.searchPath.selectPathType = null
app.ui.rViewer.searchPath.selectPathCallback = null

app.ui.rViewer.searchPath.displayPathSelection = (type, callback) => {
    app.ui.rViewer.searchPath.selectPathType = type
    app.ui.rViewer.searchPath.selectPathCallback = callback
    $('#inpRoutinesSearchPathAdd')
        .val('')
        .removeClass('is-valid')
        .removeClass('is-invalid')
    $('#btnRoutinesSearchPathAddValidate')
        .addClass('disabled')
        .prop('disabled', true)
    $('#btnRoutinesSearchPathAddOk')
        .addClass('disabled')
        .prop('disabled', true)

    $('#lblRoutinesSearchPathAdd').text('Add ' + type + ' path')

    $('#divRoutineSearchPathAddAutoRelink').css('display', type === 'object' ? 'block' : 'none')
    $('#chkRoutineSearchPathAddAutoRelink').prop('checked', false)

    $('#modalRoutinesSearchPathAdd')
        .modal({show: true, backdrop: 'static'})
        .on('shown.bs.modal', () => {
            $('#inpRoutinesSearchPathAdd').focus()
        })
}

app.ui.rViewer.searchPath.targetPathSelect = async () => {
    const inpRoutinesSearchPathAdd = $('#inpRoutinesSearchPathAdd')
    let path = inpRoutinesSearchPathAdd.val() === '' ? '/' : inpRoutinesSearchPathAdd.val()

    // validate the path first
    if (path !== '') {
        try {
            const res = await app.REST._pathExpand(path, true)

            if (res.result === 'OK') {
                path = res.data

            } else {
                if (res.error.code === -1) {
                    app.ui.msgbox.show('The path: ' + path + ' is not a valid path.', 'ERROR')

                } else {
                    app.ui.msgbox.show('The path: ' + path + ' is a file and not a directory.', 'ERROR')
                }

                return
            }
        } catch (err) {
            app.ui.msgbox.show(app.REST.parseError(err), 'ERROR')

        }
    }

    app.ui.dirSelect.show(path, path => {
        $('#inpRoutinesSearchPathAdd').val(path)

        $('#inpRoutinesSearchPathAdd')
            .removeClass('is-invalid')
            .addClass('is-valid')

        $('#btnRoutinesSearchPathAddValidate')
            .addClass('disabled')
            .prop('disabled', true)

        app.ui.rViewer.searchPath.validateDirOk()
    })
}

app.ui.rViewer.searchPath.targetPathValidate = async () => {
    const inpRoutinesSearchPathAdd = $('#inpRoutinesSearchPathAdd')
    const btnRoutinesSearchPathAddValidate = $('#btnRoutinesSearchPathAddValidate')

    try {
        const res = await app.REST._pathExpand(inpRoutinesSearchPathAdd.val(), true)

        if (res.result === 'OK') {
            inpRoutinesSearchPathAdd
                .removeClass('is-invalid')
                .addClass('is-valid')

            btnRoutinesSearchPathAddValidate
                .addClass('disabled')
                .prop('disabled', true)

        } else {
            if (res.error.code === -1) {
                app.ui.msgbox.show('The path: ' + inpRoutinesSearchPathAdd.val() + ' is not a valid path.', 'ERROR')

            } else {
                app.ui.msgbox.show('The path: ' + inpRoutinesSearchPathAdd.val() + ' is a file and not a directory.', 'ERROR')
            }

            inpRoutinesSearchPathAdd
                .removeClass('is-valid')
                .addClass('is-invalid')

            return
        }

    } catch (err) {
        app.ui.msgbox.show(app.REST.parseError(err), 'ERROR')
    }

    app.ui.rViewer.searchPath.validateDirOk()
}

app.ui.rViewer.searchPath.targetPathChanged = () => {
    const btnRoutinesSearchPathAddValidate = $('#btnRoutinesSearchPathAddValidate')
    const inpRoutinesSearchPathAdd = $('#inpRoutinesSearchPathAdd')

    inpRoutinesSearchPathAdd
        .removeClass('is-valid')
        .removeClass('is-invalid')

    if (inpRoutinesSearchPathAdd.val() !== '') {
        btnRoutinesSearchPathAddValidate.removeClass('disabled');
        btnRoutinesSearchPathAddValidate.prop("disabled", false)

    } else {
        btnRoutinesSearchPathAddValidate.addClass('disabled');
        btnRoutinesSearchPathAddValidate.prop("disabled", true)
    }

    app.ui.rViewer.searchPath.validateDirOk()
}

app.ui.rViewer.searchPath.validateDirOk = () => {
    const btn = $('#btnRoutinesSearchPathAddOk')

    if ($('#inpRoutinesSearchPathAdd').hasClass('is-valid')) {
        btn.removeClass('disabled');
        btn.prop("disabled", false)
    } else {
        btn.addClass('disabled');
        btn.prop("disabled", true)
    }
}

app.ui.rViewer.searchPath.AddDirOkPressed = () => {
    const inpRoutinesSearchPathAdd = $('#inpRoutinesSearchPathAdd')
    const selected = $('#treeRoutinesSearch').jstree(true).get_selected(true)[0]
    const selectedId = selected.id.split('-')
    let selectedParent = selected.parent
    if (selectedParent.slice(-1).toString() === '*') selectedParent = selectedParent.slice(0, -1)

    // check for duplicates
    let found = false

    $('#treeRoutinesSearch').jstree(true).get_json('#').forEach(node => {
        if (found === true) return

        // remove the * if present
        if (node.id.slice(-1).toString() === '*') node.id = node.id.slice(0, -1)

        if (inpRoutinesSearchPathAdd.val() === node.id.split('-')[1]) {
            // found
            if (app.ui.rViewer.searchPath.selectPathType === 'source') {
                // if selected = obj and path is the same, it is ok, to have a duplicate
                switch (selectedId[0]) {
                    case 'obj': {
                        break
                    }
                    case 'child': {
                        if (selectedParent.split('-')[1] !== inpRoutinesSearchPathAdd.val()) found = true

                        break
                    }
                    default:
                        found = true
                }

            } else if (app.ui.rViewer.searchPath.selectPathType === 'source') {

            } else {
                found = true
            }
        }

        // check if there are children
        if (node.children && node.children.length > 0) {
            node.children.forEach(node => {
                if (inpRoutinesSearchPathAdd.val() === node.id.split('-')[1]) {
                    // found
                    found = true
                }
            })
        }
    })

    if (found === true) {
        app.ui.msgbox.show('The path: ' + inpRoutinesSearchPathAdd.val() + ' is already in use.', 'Warning')

        return
    }

    $('#modalRoutinesSearchPathAdd').modal('hide')

    let target = inpRoutinesSearchPathAdd.val()

    if (app.ui.rViewer.searchPath.selectPathType === 'object' && $('#chkRoutineSearchPathAddAutoRelink').prop('checked') === true) {
        target += '*'
    }

    app.ui.rViewer.searchPath.selectPathCallback(target)
}

// *********************************************
// Add file dialog
// *********************************************
app.ui.rViewer.searchPath.selectFileCallback = null

app.ui.rViewer.searchPath.displayFileSelection = callback => {
    app.ui.rViewer.searchPath.selectFileCallback = callback
    $('#inpRoutinesSearchFileAdd')
        .val('')
        .removeClass('is-valid')
        .removeClass('is-invalid')
    $('#btnRoutinesSearchFilehAddValidate')
        .addClass('disabled')
        .prop('disabled', true)
    $('#btnRoutinesSearchFileAddOk')
        .addClass('disabled')
        .prop('disabled', true)

    $('#modalRoutinesSearchFileAdd')
        .modal({show: true, backdrop: 'static'})
        .on('shown.bs.modal', () => {
            $('#inpRoutinesSearchFileAdd').focus()
        })
}

app.ui.rViewer.searchPath.targetFileSelect = async () => {
    const inpRoutinesSearchFileAdd = $('#inpRoutinesSearchFileAdd')
    let path = inpRoutinesSearchFileAdd.val() === '' ? '/' : inpRoutinesSearchFileAdd.val()
    // validate the path first
    if (path !== '') {
        try {
            const res = await app.REST._pathExpand(path.substring(0, path.lastIndexOf("/") + 1), true)

            if (res.result === 'OK') {
                path = res.data

            } else {
                if (res.error.code === -1) {
                    app.ui.msgbox.show('The path: ' + path + ' is not a valid path to an .so file.', 'ERROR')

                } else {
                    app.ui.msgbox.show('The path: ' + path + ' is a directory.', 'ERROR')
                }

                return
            }

        } catch (err) {
            app.ui.msgbox.show('The following error occurred:<br>' + app.REST.parseError(err), 'ERROR')

        }
    }

    app.ui.fileSelect.show(path, '.so', path => {
        $('#inpRoutinesSearchFileAdd').val(path)

        $('#inpRoutinesSearchFileAdd')
            .removeClass('is-invalid')
            .addClass('is-valid')

        $('#btnRoutinesSearchFileAddValidate')
            .addClass('disabled')
            .prop('disabled', true)

        app.ui.rViewer.searchPath.validateFileOk()
    })
}

app.ui.rViewer.searchPath.targetFileValidate = async () => {
    const inpRoutinesSearchFileAdd = $('#inpRoutinesSearchFileAdd')
    const btnRoutinesSearchFileAddValidate = $('#btnRoutinesSearchFileAddValidate')

    try {
        const res = await app.REST._pathExpand(inpRoutinesSearchFileAdd.val(), false)

        if (res.result === 'OK') {
            if (res.data.slice(-3) === '.so') {
                inpRoutinesSearchFileAdd
                    .removeClass('is-invalid')
                    .addClass('is-valid')

                btnRoutinesSearchFileAddValidate
                    .addClass('disabled')
                    .prop('disabled', true)
            } else {
                app.ui.msgbox.show('The path: ' + inpRoutinesSearchFileAdd.val() + ' is not a valid .so file.', 'ERROR')

                inpRoutinesSearchFileAdd
                    .removeClass('is-valid')
                    .addClass('is-invalid')

                return
            }
        } else {
            if (res.error.code === -1) {
                app.ui.msgbox.show('The path: ' + inpRoutinesSearchFileAdd.val() + ' is not a valid .so file.', 'ERROR')

            } else {
                app.ui.msgbox.show('The path: ' + inpRoutinesSearchFileAdd.val() + ' is a directory.', 'ERROR')
            }

            inpRoutinesSearchFileAdd
                .removeClass('is-valid')
                .addClass('is-invalid')

            return
        }

    } catch (err) {
        app.ui.msgbox.show('The following error occurred:<br>' + app.REST.parseError(err), 'ERROR')
    }

    app.ui.rViewer.searchPath.validateFileOk()
}

app.ui.rViewer.searchPath.targetFileChanged = () => {
    const btnRoutinesSearchFileAddValidate = $('#btnRoutinesSearchFileAddValidate')
    const inpRoutinesSearchFileAdd = $('#inpRoutinesSearchFileAdd')

    inpRoutinesSearchFileAdd
        .removeClass('is-valid')
        .removeClass('is-invalid')

    if (inpRoutinesSearchFileAdd.val() !== '') {
        btnRoutinesSearchFileAddValidate.removeClass('disabled');
        btnRoutinesSearchFileAddValidate.prop("disabled", false)

    } else {
        btnRoutinesSearchFileAddValidate.addClass('disabled');
        btnRoutinesSearchFileAddValidate.prop("disabled", true)
    }

    app.ui.rViewer.searchPath.validateFileOk()
}


app.ui.rViewer.searchPath.validateFileOk = () => {
    const btn = $('#btnRoutinesSearchFileAddOk')

    if ($('#inpRoutinesSearchFileAdd').hasClass('is-valid')) {
        btn.removeClass('disabled');
        btn.prop("disabled", false)
    } else {
        btn.addClass('disabled');
        btn.prop("disabled", true)
    }
}

app.ui.rViewer.searchPath.AddFileOkPressed = () => {
    const inpRoutinesSearchFileAdd = $('#inpRoutinesSearchFileAdd')
    const selected = $('#treeRoutinesSearch').jstree(true).get_selected(true)[0]
    const selectedId = selected.id.split('-')
    let selectedParent = selected.parent
    if (selectedParent.slice(-1).toString() === '*') selectedParent = selectedParent.slice(0, -1)

    // check for duplicates
    let found = false

    $('#treeRoutinesSearch').jstree(true).get_json('#').forEach(node => {
        if (found === true) return

        if (inpRoutinesSearchFileAdd.val() === node.id.split('-')[1]) {
            // found
            found = true
        }
    })

    if (found === true) {
        app.ui.msgbox.show('The file: ' + inpRoutinesSearchFileAdd.val() + ' is already in use.', 'Warning')

        return
    }

    $('#modalRoutinesSearchPathAdd').modal('hide')

    let target = inpRoutinesSearchFileAdd.val()

    if (app.ui.rViewer.searchPath.selectPathType === 'object' && $('#chkRoutineSearchPathAddAutoRelink').prop('checked') === true) {
        target += '*'
    }

    $('#modalRoutinesSearchFileAdd').modal('hide')

    app.ui.rViewer.searchPath.selectFileCallback($('#inpRoutinesSearchFileAdd').val())
}
