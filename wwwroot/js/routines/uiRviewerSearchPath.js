/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

app.ui.rViewer.searchPath = {}

app.ui.rViewer.searchPath.initPaths = async () => {
    // copy zroutines from app.system
    app.ui.rViewer.searchPath.zroutines = app.system.zroutines

    // TEST
    app.ui.setupDialogForTest('modalRoutinesSearchPath')

    $('#modalRoutinesSearchPathAdd').on('hide.bs.modal', () => {
        $('.modal-backdrop').css('z-index', 1040)
    });
}

app.ui.rViewer.searchPath.init = () => {
    $('#btnRoutinesSearchMoveUp').on('click', () => app.ui.rViewer.searchPath.moveUpClicked())
    $('#btnRoutinesSearchMoveDown').on('click', () => app.ui.rViewer.searchPath.moveDownClicked())
    $('#btnRoutinesSearchRemove').on('click', () => app.ui.rViewer.searchPath.deleteClicked())
    $('#btnRoutinesSearchAddSo').on('click', () => app.ui.rViewer.searchPath.addLibraryClicked())
    $('#btnRoutinesSearchAddObject').on('click', () => app.ui.rViewer.searchPath.addObjectClicked())
    $('#btnRoutinesSearchAddSource').on('click', () => app.ui.rViewer.searchPath.addSourceClicked())
    $('#btnRoutinesSearchAddObjectSource').on('click', () => app.ui.rViewer.searchPath.addSourceObjectClicked())

    $('#btnRoutinesSearchDirOk').on('click', () => app.ui.rViewer.searchPath.okPressed())
}

app.ui.rViewer.searchPath.show = () => {
    app.ui.rViewer.searchPath.initTree()

    app.ui.rViewer.searchPath.dirty = false

    $('#modalRoutinesSearchPath').modal({show: true, backdrop: 'static'})
}

app.ui.rViewer.searchPath.initTree = () => {
    let treeData = [];

    let objectSectionOpen = false
    let tempObject;
    app.ui.rViewer.searchPath.zroutines.split(' ').forEach(line => {
        let node;

        if (objectSectionOpen === false && line.indexOf('(') === -1 && line.indexOf('.so') === -1) {
            node = {
                id: 'om-' + line,
                text: '<b>OBJ+M:</b> ' + line,
                icon: 'bi-file-earmark-binary',
                data: {
                    systemItem: false
                }
            }

            treeData.push(node)
        }

        if (objectSectionOpen === true) {
            if (line.indexOf(')') > -1) {
                // last entry in sources
                const src = line.substring(0, line.indexOf(')'))
                tempObject.children.push({
                    id: 'child-' + src,
                    text: '<b>M:</b> ' + src,
                    icon: 'bi-body-text',
                    data: {}
                })

                objectSectionOpen = false
                treeData.push(tempObject)

            } else {
                // there are more sources
                const src = line.substring(line.indexOf('(') + 1, line.indexOf(')'))
                tempObject.children.push({
                    id: 'child-' + line,
                    text: '<b>M:</b> ' + line,
                    icon: 'bi-body-text',
                    data: {}
                })
            }

            return
        }

        if (line.indexOf('(') > -1) {
            // this is an object
            objectSectionOpen = true
            const obj = line.substring(0, line.indexOf('('))
            tempObject = {
                id: 'obj-' + obj,
                text: '<b>OBJ:</b> ' + obj,
                icon: 'bi-file-binary',
                data: {
                    object: true,
                    autoRelink: obj.slice(-1) === '*'
                },
                state: {opened: true},
                children: []
            }

            // now we find the children
            if (line.indexOf(')') > -1) {
                // single lines, just append the src path
                const src = line.substring(line.indexOf('(') + 1, line.indexOf(')'))
                if (src !== '') {
                    tempObject.children.push({
                        id: 'child-' + src,
                        text: '<b>M:</b> ' + src,
                        icon: 'bi-body-text',
                        data: {}
                    })
                }

                objectSectionOpen = false
                treeData.push(tempObject)
            } else {
                // append path and fetch more src
                const src = line.substring(line.indexOf('(') + 1)
                tempObject.children.push({
                    id: 'child-' + src,
                    text: '<b>M:</b> ' + src,
                    icon: 'bi-body-text',
                    data: {}
                })
            }

            return
        }

        if (line.indexOf('.so') > -1) {
            // this is an .so

            // is it a reserved item ?
            if (
                line.indexOf('_ydbgui.so') > -1 ||
                line.indexOf('_ydbmwebserver.so') > -1 ||
                line.indexOf('_ydbposix.so') > -1 ||
                line.indexOf('libyottadbutil.so') > -1 ||
                line.indexOf('libgtmutil.so') > -1
            ) {
                node = {
                    id: 'sso-' + line,
                    text: '<span style="color: var(--ydb-darkgray); background: var(--ydb-lightgray)"><b>System SO:</b><i> ' + line + '</i></span>',
                    icon: 'bi-file-earmark-binary-fill',
                    data: {
                        systemItem: true
                    }
                }

            } else {
                node = {
                    id: 'so-' + line,
                    text: '<b>SO:</b> ' + line,
                    icon: 'bi-file-earmark-binary',
                    data: {
                        systemItem: false
                    }
                }
            }

            treeData.push(node)
        }
    })

    // ensure libyottadbutil.so OR libgtmutil.so are LAST
    let libIx = -1

    treeData.forEach((node, ix) => {
        if (node.id.indexOf('libyottadbutil.so') > -1 || node.id.indexOf('libgtmutil.so') > -1) {
            libIx = ix
        }
    })

    // and make it last if not
    if (libIx + 1 !== treeData.length) {
        const libNode = treeData[libIx]
        treeData.splice(libIx, 1)
        treeData.push(libNode)
    }

    // populate the tree
    treeData[0].state = {selected: true, opened: true}

    $('#treeRoutinesSearch')
        .jstree("destroy")
        .jstree({
            'core': {
                'check_callback': true,
                //'dblclick_toggle': false,
                'multiple': false,
                'data': treeData,
                'themes': {
                    'name': 'proton',
                }
            },
        })
        .on("select_node.jstree", function (node, selected) {
            app.ui.rViewer.searchPath.processClick(selected.selected[0])
        })

    app.ui.rViewer.searchPath.processClick(treeData[0].id)
};

app.ui.rViewer.searchPath.processClick = node => {
    const btnRoutinesSearchMoveUp = $('#btnRoutinesSearchMoveUp')
    const btnRoutinesSearchMoveDown = $('#btnRoutinesSearchMoveDown')
    const btnRoutinesSearchRemove = $('#btnRoutinesSearchRemove')
    const btnRoutinesSearchAddSo = $('#btnRoutinesSearchAddSo')
    const btnRoutinesSearchAddObject = $('#btnRoutinesSearchAddObject')
    const btnRoutinesSearchAddSource = $('#btnRoutinesSearchAddSource')
    const btnRoutinesSearchAddObjectSource = $('#btnRoutinesSearchAddObjectSource')

    switch (node.split('-')[0]) {
        // ************************
        // SSO >>> system so. These files can not be removed, neither moved up and down.
        // om >>> object and M  code
        // ************************
        case 'sso':
        case 'om': {
            if (node.split('-')[0] === 'sso') {
                btnRoutinesSearchRemove
                    .addClass('disabled')
                    .prop('disabled', true)

            } else {
                btnRoutinesSearchRemove
                    .removeClass('disabled')
                    .prop('disabled', false)
            }

            if (node.indexOf('libyottadbutil.so') > -1 || node.indexOf('libgtmutil.so') > -1) {
                btnRoutinesSearchMoveUp
                    .addClass('disabled')
                    .prop('disabled', true)

                btnRoutinesSearchMoveDown
                    .addClass('disabled')
                    .prop('disabled', true)

                btnRoutinesSearchAddSource
                    .addClass('disabled')
                    .prop('disabled', true)

                btnRoutinesSearchAddSo
                    .addClass('disabled')
                    .prop('disabled', true)

                btnRoutinesSearchAddObject
                    .addClass('disabled')
                    .prop('disabled', true)

                btnRoutinesSearchAddObjectSource
                    .addClass('disabled')
                    .prop('disabled', true)

            } else {
                const next = $('#treeRoutinesSearch').jstree(true).get_next_dom(node, true)

                if (next !== false) {
                    if (next[0].id.indexOf('libyottadbutil.so') > -1 || next[0].id.indexOf('libgtmutil.so') > -1) {
                        btnRoutinesSearchMoveDown
                            .addClass('disabled')
                            .prop('disabled', true)

                    } else {
                        btnRoutinesSearchMoveDown
                            .removeClass('disabled')
                            .prop('disabled', false)
                    }
                } else {
                    btnRoutinesSearchMoveDown
                        .addClass('disabled')
                        .prop('disabled', true)
                }

                const objPrev = $('#treeRoutinesSearch').jstree(true).get_prev_dom(node, true)
                if (objPrev === false) {
                    btnRoutinesSearchMoveUp
                        .addClass('disabled')
                        .prop('disabled', true)

                } else {
                    btnRoutinesSearchMoveUp
                        .removeClass('disabled')
                        .prop('disabled', false)
                }

                btnRoutinesSearchAddSource
                    .addClass('disabled')
                    .prop('disabled', true)

                btnRoutinesSearchAddSo
                    .removeClass('disabled')
                    .prop('disabled', false)

                btnRoutinesSearchAddObject
                    .removeClass('disabled')
                    .prop('disabled', false)

                btnRoutinesSearchAddObjectSource
                    .removeClass('disabled')
                    .prop('disabled', false)
            }

            break
        }

        // ************************
        // SO
        // ************************
        case 'so': {
            // remove
            btnRoutinesSearchRemove
                .removeClass('disabled')
                .prop('disabled', false)

            // move up
            const prev = $('#treeRoutinesSearch').jstree(true).get_prev_dom(node, true)
            if (prev === false) {
                btnRoutinesSearchMoveUp
                    .addClass('disabled')
                    .prop('disabled', true)

            } else {
                btnRoutinesSearchMoveUp
                    .removeClass('disabled')
                    .prop('disabled', false)
            }

            // move down
            const next = $('#treeRoutinesSearch').jstree(true).get_next_dom(node, true)
            if (next !== false) {
                if ($(next)[0].id.split('-')[0] === 'sso' && ($(next)[0].id.indexOf('libyottadbutil.so') > -1 || $(next)[0].id.indexOf('libgtmutil.so') > -1)) {
                    btnRoutinesSearchMoveDown
                        .addClass('disabled')
                        .prop('disabled', true)

                } else {
                    btnRoutinesSearchMoveDown
                        .removeClass('disabled')
                        .prop('disabled', false)
                }
            } else {
                btnRoutinesSearchMoveDown
                    .addClass('disabled')
                    .prop('disabled', true)
            }

            // add library / object
            const libPrev = $('#treeRoutinesSearch').jstree(true).get_prev_dom(node, true)
            if (libPrev === false) {
                btnRoutinesSearchAddSo
                    .removeClass('disabled')
                    .prop('disabled', false)

                btnRoutinesSearchAddObject
                    .removeClass('disabled')
                    .prop('disabled', false)

                btnRoutinesSearchAddObjectSource
                    .removeClass('disabled')
                    .prop('disabled', false)

            } else {
                if ($(libPrev)[0].id.split('-')[0] === 'sso') {
                    btnRoutinesSearchAddSo
                        .addClass('disabled')
                        .prop('disabled', true)

                    btnRoutinesSearchAddObject
                        .addClass('disabled')
                        .prop('disabled', true)

                    btnRoutinesSearchAddObjectSource
                        .addClass('disabled')
                        .prop('disabled', true)
                } else {
                    btnRoutinesSearchAddSo
                        .removeClass('disabled')
                        .prop('disabled', false)

                    btnRoutinesSearchAddObject
                        .removeClass('disabled')
                        .prop('disabled', false)

                    btnRoutinesSearchAddObjectSource
                        .removeClass('disabled')
                        .prop('disabled', false)
                }
            }

            // add source
            btnRoutinesSearchAddSource
                .addClass('disabled')
                .prop('disabled', true)

            break
        }

        // ************************
        // OBJ
        // ************************
        case
        'obj'
        : {
            // remove
            btnRoutinesSearchRemove
                .removeClass('disabled')
                .prop('disabled', false)

            // move up
            const objPrev = $('#treeRoutinesSearch').jstree(true).get_prev_dom(node, true)
            if (objPrev === false) {
                btnRoutinesSearchMoveUp
                    .addClass('disabled')
                    .prop('disabled', true)

            } else {
                btnRoutinesSearchMoveUp
                    .removeClass('disabled')
                    .prop('disabled', false)
            }

            // move down
            const next = $('#treeRoutinesSearch').jstree(true).get_next_dom(node, true)
            if (next !== false) {
                if ($(next)[0].id.split('-')[0] === 'sso' && ($(next)[0].id.indexOf('libyottadbutil.so') > -1 || $(next)[0].id.indexOf('libgtmutil.so') > -1)) {
                    btnRoutinesSearchMoveDown
                        .addClass('disabled')
                        .prop('disabled', true)

                } else {
                    btnRoutinesSearchMoveDown
                        .removeClass('disabled')
                        .prop('disabled', false)
                }
            }

            // add library / object
            const prev = $('#treeRoutinesSearch').jstree(true).get_prev_dom(node, true)
            if (prev === false) {
                btnRoutinesSearchAddSo
                    .removeClass('disabled')
                    .prop('disabled', false)

                btnRoutinesSearchAddObject
                    .removeClass('disabled')
                    .prop('disabled', false)

                btnRoutinesSearchAddObjectSource
                    .removeClass('disabled')
                    .prop('disabled', false)

            } else {
                if ($(prev)[0].id.split('-')[0] === 'sso' && ($(next)[0].id.indexOf('libyottadbutil.so') > -1 || $(next)[0].id.indexOf('libgtmutil.so') > -1)) {
                    btnRoutinesSearchAddSo
                        .addClass('disabled')
                        .prop('disabled', true)

                    btnRoutinesSearchAddObject
                        .addClass('disabled')
                        .prop('disabled', true)

                    btnRoutinesSearchAddObjectSource
                        .addClass('disabled')
                        .prop('disabled', true)
                } else {
                    btnRoutinesSearchAddSo
                        .removeClass('disabled')
                        .prop('disabled', false)

                    btnRoutinesSearchAddObject
                        .removeClass('disabled')
                        .prop('disabled', false)

                    btnRoutinesSearchAddObjectSource
                        .removeClass('disabled')
                        .prop('disabled', false)
                }
            }

            // add source
            btnRoutinesSearchAddSource
                .removeClass('disabled')
                .prop('disabled', false)

            break
        }

        // ************************
        // CHILD
        // ************************
        case
        'child'
        : {
            // remove
            btnRoutinesSearchRemove
                .removeClass('disabled')
                .prop('disabled', false)

            // move up
            if ($('#treeRoutinesSearch').jstree(true).get_prev_dom(node, true) === false) {
                btnRoutinesSearchMoveUp
                    .addClass('disabled')
                    .prop('disabled', true)

            } else {
                btnRoutinesSearchMoveUp
                    .removeClass('disabled')
                    .prop('disabled', false)
            }

            // move down
            if ($('#treeRoutinesSearch').jstree(true).get_next_dom(node, true) === false) {
                btnRoutinesSearchMoveDown
                    .addClass('disabled')
                    .prop('disabled', true)

            } else {
                btnRoutinesSearchMoveDown
                    .removeClass('disabled')
                    .prop('disabled', false)
            }

            // add source
            btnRoutinesSearchAddSource
                .removeClass('disabled')
                .prop('disabled', false)

            // add library
            btnRoutinesSearchAddSo
                .addClass('disabled')
                .prop('disabled', true)

            // add object
            btnRoutinesSearchAddObject
                .addClass('disabled')
                .prop('disabled', true)

            // add object and source
            btnRoutinesSearchAddObjectSource
                .addClass('disabled')
                .prop('disabled', true)

            break
        }

        default: {
        }
    }
}

// *********************************************
// Buttons handlers
// *********************************************

app.ui.rViewer.searchPath.moveUpClicked = () => {
    const treeRoutinesSearch = $('#treeRoutinesSearch')
    const selected = treeRoutinesSearch.jstree(true).get_selected(true)[0]

    const children = $('#treeRoutinesSearch').jstree(true).get_children_dom(selected.parent)
    let prevId = 0;
    $(children).each((id, child) => {
        if (child.id === selected.id) prevId = id - 1
    })

    treeRoutinesSearch.jstree(true).move_node(selected.id, selected.parent, prevId)

    app.ui.rViewer.searchPath.dirty = true

    app.ui.rViewer.searchPath.processClick(selected.id)
}

app.ui.rViewer.searchPath.moveDownClicked = () => {
    const treeRoutinesSearch = $('#treeRoutinesSearch')
    const selected = treeRoutinesSearch.jstree(true).get_selected(true)[0]

    const children = $('#treeRoutinesSearch').jstree(true).get_children_dom(selected.parent)
    let nextId = 0;
    $(children).each((id, child) => {
        if (child.id === selected.id) nextId = id + 2
    })

    treeRoutinesSearch.jstree(true).move_node(selected.id, selected.parent, nextId)

    app.ui.rViewer.searchPath.dirty = true

    app.ui.rViewer.searchPath.processClick(selected.id)
}

app.ui.rViewer.searchPath.deleteClicked = () => {
    const treeRoutinesSearch = $('#treeRoutinesSearch')
    const selected = treeRoutinesSearch.jstree(true).get_selected()[0].split('-')
    const filename = selected[1].slice(-1) === '*' ? selected[1].slice(0, -1) : selected[1]
    let message = 'You are going to remove the '
    let type;

    switch (selected[0]) {
        case 'obj': {
            type = 'object path'
            break
        }

        case 'child': {
            type = 'source path'

            break
        }

        case 'so': {
            type = 'library file'

            break
        }

        case 'om': {
            type = 'object and source'

            break
        }
    }

    message += type + ': ' + filename + '<br>Are you sure ?'

    app.ui.inputbox.show(message, 'Remove ' + type, async (opt) => {
        if (opt === 'YES') {
            //re -select the first entry
            treeRoutinesSearch.jstree(true).select_node($('#treeRoutinesSearch').jstree(true).get_json('#')[0].id)

            // delete the node
            treeRoutinesSearch.jstree(true).delete_node(selected.join('-'))
        }
    })

    app.ui.rViewer.searchPath.dirty = true
}

app.ui.rViewer.searchPath.addLibraryClicked = () => {
    app.ui.rViewer.searchPath.displayFileSelection(path => {
        const selected = $('#treeRoutinesSearch').jstree(true).get_selected(true)[0]

        // get current index
        const children = $('#treeRoutinesSearch').jstree(true).get_children_dom(selected.parent)
        let nextId = 0;
        $(children).each((id, child) => {
            if (child.id === selected.id) nextId = id + 1
        })

        // insert object as next
        const node = {
            id: 'so-' + path,
            text: '<b>SO:</b> ' + path,
            icon: 'bi-file-earmark-binary',
            state: {selected: true},
            children: []
        }
        $('#treeRoutinesSearch').jstree(true).create_node(selected.parent, node, nextId)
        $('#treeRoutinesSearch').jstree(true).deselect_all()
        $('#treeRoutinesSearch').jstree(true).select_node(node.id)

        app.ui.rViewer.searchPath.dirty = true
    })
}

app.ui.rViewer.searchPath.addObjectClicked = () => {
    app.ui.rViewer.searchPath.displayPathSelection('object', path => {
        const selected = $('#treeRoutinesSearch').jstree(true).get_selected(true)[0]

        // get current index
        const children = $('#treeRoutinesSearch').jstree(true).get_children_dom(selected.parent)
        let nextId = 0;
        $(children).each((id, child) => {
            if (child.id === selected.id) nextId = id + 1
        })

        // insert object as next
        const node = {
            id: 'obj-' + path,
            text: '<b>OBJ:</b> ' + path,
            icon: 'bi-file-binary',
            data: {
                object: true,
                autoRelink: true
            },
            state: {opened: true},
            children: []
        }
        $('#treeRoutinesSearch').jstree(true).create_node(selected.parent, node, nextId)
        $('#treeRoutinesSearch').jstree(true).deselect_all()
        $('#treeRoutinesSearch').jstree(true).select_node(node.id)

        app.ui.rViewer.searchPath.dirty = true
    })
}

app.ui.rViewer.searchPath.addSourceClicked = () => {
    app.ui.rViewer.searchPath.displayPathSelection('source', path => {
        const selected = $('#treeRoutinesSearch').jstree(true).get_selected(true)[0]

        let nextId = 0;
        let parent = null

        if (selected.id.indexOf('child-') > -1) {
            // get current index
            const children = $('#treeRoutinesSearch').jstree(true).get_children_dom(selected.parent)
            $(children).each((id, child) => {
                if (child.id === selected.id) nextId = id + 2
            })

            parent = selected.parent
        } else {
            parent = selected.id
            nextId = 'first'
        }

        // insert object as next
        const node = {
            id: 'child-' + path,
            text: '<b>M:</b> ' + path,
            icon: 'bi-body-text',
            data: {}
        }
        $('#treeRoutinesSearch').jstree(true).create_node(parent, node, nextId)
        $('#treeRoutinesSearch').jstree(true).deselect_all()
        $('#treeRoutinesSearch').jstree(true).select_node(node.id)

        app.ui.rViewer.searchPath.dirty = true
    })
}

app.ui.rViewer.searchPath.addSourceObjectClicked = () => {
    app.ui.rViewer.searchPath.displayPathSelection('object and source', path => {
        const selected = $('#treeRoutinesSearch').jstree(true).get_selected(true)[0]

        // get current index
        const children = $('#treeRoutinesSearch').jstree(true).get_children_dom(selected.parent)
        let nextId = 0;
        $(children).each((id, child) => {
            if (child.id === selected.id) nextId = id + 1
        })

        // insert object as next
        const node = {
            id: 'om-' + path,
            text: '<b>OBJ+M:</b> ' + path,
            icon: 'bi-file-earmark-binary',
            data: {
                object: true,
                autoRelink: true
            },
            state: {opened: true},
            children: []
        }
        $('#treeRoutinesSearch').jstree(true).create_node(selected.parent, node, nextId)
        $('#treeRoutinesSearch').jstree(true).deselect_all()
        $('#treeRoutinesSearch').jstree(true).select_node(node.id)

        app.ui.rViewer.searchPath.dirty = true
    })

}

app.ui.rViewer.searchPath.okPressed = () => {
    // build the new zroutines
    let zroutines = ''

    $('#treeRoutinesSearch').jstree(true).get_json('#').forEach(node => {
        switch (node.id.split('-')[0]) {
            case 'obj': {
                zroutines += ' ' + node.id.split('-')[1] + '('
                if (node.children && node.children.length > 0) {
                    node.children.forEach(node => {
                        zroutines += node.id.split('-')[1] + ' '
                    })
                } else {
                    zroutines += ' '
                }
                zroutines = zroutines.slice(0, -1) + ')'

                break
            }

            case 'om':
            case 'so':
            case 'sso': {
                zroutines += ' ' + node.id.split('-')[1]
            }
        }
    })

    zroutines = zroutines.slice(1)

    if (app.ui.rViewer.searchPath.dirty === false ||
        zroutines === app.ui.rViewer.searchPath.zroutines) {
        app.ui.msgbox.show('Nothing has changed...', 'Alert')

    } else {
        app.ui.inputbox.show('Do you want to update the search path with the following $zroutines ?<br><br><span class="inconsolata">' + zroutines + '</span>', 'Routines search path', async (opt) => {
            if (opt === 'YES') {
                app.ui.rViewer.searchPath.zroutines = zroutines

                $('#modalRoutinesSearchPath').modal('hide')
            }
        })
    }
}
