/****************************************************************
 *                                                              *
 * Copyright (c) 2022-2023 YottaDB LLC and/or its subsidiaries. *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

let rViewer = class {
    constructor() {
        this.fontSize = 14;
        this.cm = null;
        this.routine = {};
        this.theme = 'light';
    }
};

app.ui.rViewer.init = () => {

};

app.ui.rViewer.instance = {};

app.ui.rViewer.lastIdGviewer = 0;

app.ui.rViewer.addNew = async metadata => {
    return new Promise(async function (resolve) {
        let tabId = null;
        if (metadata.tabId === null) {

            app.ui.rViewer.lastIdGviewer++;
            tabId = '-R-' + app.ui.rViewer.lastIdGviewer;

            metadata.tabId = tabId
        }
        await app.ui.tabs.select(tabId, metadata, true);
        resolve();
    })
};

app.ui.rViewer.createInstance = async (type, metadata) => {
    return new Promise(async function (resolve, reject) {
        const newViewer = $('#tabRviewer').clone();
        const newTabId = 'tab' + type + 'Div';

        $('#tabsDivsContainer').append(newViewer);

        $('#tabsDivsContainer > #tabRviewer').prop('id', newTabId);

        const newTabDiv = $('#' + newTabId);

        newTabDiv
            .attr('aria-labelledby', 'tab' + type)
            .css('display', 'block');

        // regenerate id's
        newTabDiv.find('#divRviewerCode').attr('id', 'divRviewerCode' + type);

        $('#btnRviewerZoomOut').prop('id', 'btnRviewerZoomOut' + type);
        $('#btnRviewerZoomIn').prop('id', 'btnRviewerZoomIn' + type);

        $('#btnRviewerTheme').prop('id', 'btnRviewerTheme' + type);

        $('#selRviewerLabels').prop('id', 'selRviewerLabels' + type);

        $('#btnRviewerProperties').prop('id', 'btnRviewerProperties' + type);

        $('#btnRviewerPermalink').prop('id', 'btnRviewerPermalink' + type);

        $('#divRviewerDiv').prop('id', 'divRviewerDiv' + type);

        // mount handlers
        $('#btnRviewerZoomOut' + type).on('click', () => app.ui.rViewer.zoom(type, '-'));
        $('#btnRviewerZoomIn' + type).on('click', () => app.ui.rViewer.zoom(type, '+'));

        $('#btnRviewerTheme' + type).on('click', () => app.ui.rViewer.toggleTheme(type));

        $('#inpRviewerFind' + type).on('click', () => app.ui.rViewer.findNext(type));

        $('#btnRviewerProperties' + type).on('click', () => app.ui.rViewer.props.show(type));

        $('#selRviewerLabels' + type).on('change', () => app.ui.rViewer.jumpToLabel(type));

        $('#btnRviewerPermalink' + type).on('click', () => app.ui.permalinks.show(type));

        // instance the class
        app.ui.rViewer.instance[type] = new rViewer;
        const instance = app.ui.rViewer.instance[type];

        // append the popup on the tab
        $('#tab' + type).attr('title', metadata.routineName)

        // pull code
        let res;
        try {
            res = await app.REST._getRoutine(metadata.routineName);

            if (res.result === 'ERROR') {
                app.ui.msgbox.show('The following error occurred while fetching the routine:<BR>' + res.error.description, 'ERROR');

                app.ui.tabs.close('tab' + type)

                resolve()

                return
            }

            instance.routine = res.data;
            instance.routine.name = metadata.routineName;

            const file = res.data.file.join('\n');
            $('#divRviewerCode' + type).val(file)

        } catch (err) {
            app.ui.msgbox.show(app.REST.parseError(err), 'ERROR');

            app.ui.tabs.close('tab' + type)

            resolve()

            return
        }

        app.ui.rViewer.formatRoutine(type);

        // extract the labels
        let options = '<option name="1"></option>';
        let lineNumber = 0;
        res.data.file.forEach(line => {
            lineNumber++;
            const char1 = line.charAt(0);

            if (char1 !== '\t' && char1 !== ' ' && char1 !== ';' && char1 !== '\r' && char1 !== '') {
                options += '<option name="' + lineNumber + '">' +
                    line.split(' ')[0].split('\r')[0].split('(')[0].split(':')[0] +
                    '</option>>'
            }
        });

        $('#selRviewerLabels' + type)
            .empty()
            .append(options);

        resolve('done');
    })
};

app.ui.rViewer.toggleTheme = tabId => {
    const instance = app.ui.rViewer.instance[tabId];

    const lightFlag = $('#btnRviewerTheme' + tabId).attr('aria-pressed');
    instance.theme = lightFlag === 'true' ? 'light' : 'dark';

    app.ui.rViewer.formatRoutine(tabId)
};

app.ui.rViewer.formatRoutine = tabId => {
    const instance = app.ui.rViewer.instance[tabId];

    // format code
    const cm = CodeMirror.fromTextArea(document.getElementById('divRviewerCode' + tabId), {
        mode: "mumps",
        lineNumbers: true,
        lineWrapping: false,
        readOnly: true,
        extraKeys: {"Alt-F": "findPersistent"},
        theme: instance.theme === 'light' ? 'default' : 'night'
    });

    // store codeMirror handle in instance
    instance.cm = cm;

    // and set viewer height
    const el = $('#divRviewerDiv' + tabId).find('div')[0];
    el.style.height = 'calc( 100vh - 155px )';
    el.style.scrollTo = 0;
    el.style.border = '1px solid black';
    el.style.fontSize = instance.fontSize + 'px';
    el.style.fontFamily = 'Inconsolata';


    // refresh the viewer
    cm.refresh();
};

app.ui.rViewer.zoom = (tabId, direction) => {
    const instance = app.ui.rViewer.instance[tabId];
    let fontSize = instance.fontSize;

    $('body').addClass('wait');

    if (direction === '-') {
        if (fontSize > 6) {
            fontSize--

        } else {
            $('body').removeClass('wait');

            return
        }

    } else {
        if (fontSize < 35) {
            fontSize++

        } else {
            $('body').removeClass('wait');

            return
        }
    }

    instance.fontSize = fontSize;

    const el = $('#divRviewerDiv' + tabId).find('div')[0];
    el.style.fontSize = fontSize + 'px';
    instance.cm.refresh();

    $('body').removeClass('wait');
};

app.ui.rViewer.jumpToLabel = tabId => {
    const instance = app.ui.rViewer.instance[tabId];

    const line = $('#selRviewerLabels' + tabId).find('option:selected').attr("name");
    instance.cm.setCursor(parseInt(line) - 1)
};

app.ui.rViewer.props = {};

app.ui.rViewer.props.show = tabId => {
    const instance = app.ui.rViewer.instance[tabId];

    // source file
    if (instance.routine.props !== undefined) {
        $('#rViewerPropsFileFile').text(instance.routine.props.filename);
        $('#rViewerPropsFilePath').text(instance.routine.props.path);

        $('#rViewerPropsFileSize').text(app.ui.formatThousands(instance.routine.props.size));
        $('#rViewerPropsFileAccess').text(instance.routine.props.accessRightsOctal);

        $('#rViewerPropsFileOwnerGroup').text(instance.routine.props.ownerGroup);
        $('#rViewerPropsFileOwnerUser').text(instance.routine.props.ownerUser);

        let birthValue;
        if (instance.routine.props.lastChanged === 0) {
            birthValue = '---'
        } else {
            const birth = new Date(instance.routine.props.lastChanged * 1000);
            birthValue = birth.toISOString().split('.')[0].replace(/T/g, " ");
        }
        $('#rViewerPropsFileBirth').text(birthValue);

        let lastModified = new Date(instance.routine.props.lastModified * 1000);
        let lastModifiedValue = lastModified.toISOString().split('.')[0].replace(/T/g, " ");
        $('#rViewerPropsFileLastModified').text(lastModifiedValue);

        let lastAccess = new Date(instance.routine.props.lastAccess * 1000);
        let lastAccessValue = lastAccess.toISOString().split('.')[0].replace(/T/g, " ");
        $('#rViewerPropsFileLastAccess').text(lastAccessValue);

    } else {
        $('#rViewerPropsFileFile').text('N/A');
        $('#rViewerPropsFilePath').text('N/A');

        $('#rViewerPropsFileSize').text('0');
        $('#rViewerPropsFileAccess').text('N/A');

        $('#rViewerPropsFileOwnerGroup').text('N/A');
        $('#rViewerPropsFileOwnerUser').text('N/A');

        $('#rViewerPropsFileBirth').text('N/A');

        $('#rViewerPropsFileLastAccess').text('N/A');

        $('#rViewerPropsFileLastModified').text('N/A');

        $('#rViewerPropsFileLastStatusChange').text('N/A');
    }
    // obj file
    if (instance.routine.object !== undefined && instance.routine.object.props !== -1) {
        $('#rViewerPropsObjFile').text(instance.routine.object.props.filename);
        $('#rViewerPropsObjPath').text(instance.routine.object.props.path);

        $('#rViewerPropsObjSize').text(app.ui.formatThousands(instance.routine.object.props.size));
        $('#rViewerPropsObjAccess').text(instance.routine.object.props.accessRightsOctal);

        $('#rViewerPropsObjOwnerGroup').text(instance.routine.object.props.ownerGroup);
        $('#rViewerPropsObjOwnerUser').text(instance.routine.object.props.ownerUser);

        if (instance.routine.object.props.lastChanged === 0) {
            birthValue = '---'
        } else {
            const birth = new Date(instance.routine.object.props.lastChanged * 1000);
            birthValue = birth.toISOString().split('.')[0].replace(/T/g, " ");
        }
        $('#rViewerPropsObjBirth').text(birthValue);

        let lastAccess = new Date(instance.routine.object.props.lastAccess * 1000);
        let lastAccessValue = lastAccess.toISOString().split('.')[0].replace(/T/g, " ");
        $('#rViewerPropsObjLastAccess').text(lastAccessValue);

        let lastModified = new Date(instance.routine.object.props.lastModified * 1000);
        let lastModifiedValue = lastModified.toISOString().split('.')[0].replace(/T/g, " ");
        $('#rViewerPropsObjLastModified').text(lastModifiedValue);
        //

    } else {
        $('#rViewerPropsObjFile').text('N/A');
        $('#rViewerPropsObjPath').text('N/A');

        $('#rViewerPropsObjSize').text('0');
        $('#rViewerPropsObjAccess').text('N/A');

        $('#rViewerPropsObjOwnerGroup').text('N/A');
        $('#rViewerPropsObjOwnerUser').text('N/A');

        $('#rViewerPropsObjBirth').text('N/A');

        $('#rViewerPropsObjLastAccess').text('N/A');

        $('#rViewerPropsObjLastModified').text('N/A');

        $('#rViewerPropsObjLastStatusChange').text('N/A');
    }

    $('#modalRviewerProps').modal({show: true, backdrop: 'static'})
};
