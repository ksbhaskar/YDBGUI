/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

Object.assign(app.ui.prefs, {
    // ************************************
    // populate table
    // ************************************
    populateTable: function (items) {
        const $tbody = $('#tblPrefsItems > tbody')
        let rows = ''

        $tbody.empty()

        if (items && items.length > 0) {
            items.forEach(item => {
                let row = '<tr class="region-view-detail-table-row">'

                // *************
                // Param cell
                // *************
                row += '<td>' + item.display + ':'

                if (item.description !== undefined) {
                    row += '<a tabindex="-1" data-toggle="popover" data-placement="bottom" data-trigger="hover" title="' + item.display + '" id="puRegionViewDbUsage" data-content="' + item.description + '">'
                    row += '<i class="hand bi-info-circle' + '" style="padding-left: 14px; color: var(--ydb-status-red)' + '!important;"></i></a>';
                }

                row += '</td><td></td>'

                // *************
                // Value cell
                // *************
                switch (item.type) {
                    case 'numeric': {
                        const inpId = 'inp-' + item.field
                        const popoverId = 'pop-' + item.field
                        const popText = 'Min: ' + app.ui.formatThousands(item.min) + '<br>Max: ' + app.ui.formatThousands(item.max);

                        row += '<td onclick="app.ui.prefs.tableUtils.cellClicked(\'' + inpId + '\')" class="hand" style="display: flex;">'
                        row += '<a id="' + popoverId + '" style="width: 100%;" tabindex="-1" data-toggle="popover" data-placement="bottom" data-trigger="hover" title="' + item.display + '" id="" data-content="' + popText + '">'
                        row += '<input type="number" min="' + item.min + '" max="' + item.max + '" step="' + item.step + '" value="' + item.newValue + '" id="' + inpId + '" style="display: block;" class="form-control form-control-sm table-controls-prefs-cells" onclick="app.ui.prefs.tableUtils.numChange(\'' + inpId + '\')" onchange="app.ui.prefs.tableUtils.numChange(\'' + inpId + '\')" >'
                        row += '</a>'
                        row += '</td>'

                        break
                    }
                    case 'text': {
                        const textId = 'inp-' + item.field
                        const popoverId = 'pop-' + item.field

                        row += '<td onclick="app.ui.prefs.tableUtils.cellClicked(\'' + textId + '\')" class="hand" style="display: flex;">'
                        row += '<input style="width: 100%;" type="text" value="' + item.newValue + '" id="' + textId + '" style="display: block;" class="form-control form-control-sm table-controls-prefs-cells" onclick="app.ui.prefs.tableUtils.numChange(\'' + textId + '\')" onchange="app.ui.prefs.tableUtils.textChange(\'' + textId + '\')" >'
                        row += '</td>'

                        break
                    }
                    case 'boolean': {
                        const boolId = 'bool-' + item.field

                        row += '<td class="hand" style="display: flex;">';
                        row += '<select id="' + boolId + '" class="form-control form-control-sm  table-controls-prefs-cells"  onchange="app.ui.prefs.tableUtils.boolChange(\'' + boolId + '\')">'
                        row += '<option value="true" ' + (item.newValue === true ? 'selected' : '') + '>Yes</option>'
                        row += '<option value="false" ' + (item.newValue === false ? 'selected' : '') + '>No</option>'
                        row += '</td>'

                        break
                    }
                    case 'path': {
                        const pathId = 'path-' + item.field;

                        row += '<td  style="display: flex;">' +
                            '<span style="width: 100%;;" id="' + pathId + '">' + item.newValue + '</span>'
                            + '<button class="btn btn-outline-info btn-sm region-filename-button" id="btn-' + +item.field + '" type="button" onclick="app.ui.prefs.tableUtils.pathClicked(\'' + pathId + '\')">...</button></td>'
                            + '</td>'

                        break
                    }
                    case 'color': {
                        const colorId = 'color-' + item.field

                        row += '<td class="hand" style="display: flex;">';
                        row += '<input id="' + colorId + '" class="form-control form-control-sm table-controls-prefs-color" type="color" value="' + this.standardize_color(item.newValue) + '" onchange="app.ui.prefs.tableUtils.colorChange(\'' + colorId + '\')">'
                        row += '</td>'

                        break
                    }
                    case 'select': {
                        const selId = 'sel-' + item.field

                        row += '<td class="hand" style="display: flex;">';
                        row += '<select id="' + selId + '" class="form-control form-control-sm  table-controls-prefs-cells"  onchange="app.ui.prefs.tableUtils.selChange(\'' + selId + '\')">'
                        item.options.forEach(option => {
                            row += '<option value="' + option.value + '" ' + (item.newValue === option.value ? 'selected' : '') + '>' + option.text + '</option>'
                        })
                        row += '</td>'

                        break
                    }
                }

                row += '</td>'

                // *************
                // Unit cell
                // *************
                row += '<td style="vertical-align: middle;">'

                if (item.unit && item.unit !== '') {
                    row += item.unit
                }

                row += '</td>'

                // *************
                // append row
                // *************
                rows += row
            })

            // append rows
            $tbody.append(rows)
        }

        // display table
        $('#divPrefsItemsContainer').css('display', 'block')
        $('#divPrefsRangePercentContainer').css('display', 'none')
        $('#divPrefsRangeExtensionsContainer').css('display', 'none')

        app.ui.regeneratePopups()
    },

    tableUtils: {
        cellClicked: function (id) {
        },

        numChange: function (inpId) {
            const value = parseFloat($('#' + inpId).val())
            const field = inpId.split('-')[1]
            const item = app.ui.prefs.currentNode.data.find(el => el.field === field)

            if (item.oldValue !== value) {
                item.newValue = value
                item.dirty = true
            }
        },

        pathClicked: function (pathId) {
            const field = pathId.split('-')[1]
            const item = app.ui.prefs.currentNode.data.find(el => el.field === field)

            app.ui.dirSelect.show(item.newValue, newPath => {
                if (item.oldValue !== newPath) {
                    item.newValue = newPath
                    item.dirty = true
                    $('#' + pathId).text(newPath)
                }
            })
        },

        boolChange: function (boolId) {
            const field = boolId.split('-')[1]
            const item = app.ui.prefs.currentNode.data.find(el => el.field === field)
            const value = $('#' + boolId).find(":selected").val() === 'true'

            if (item.oldValue !== value) {
                item.newValue = value
                item.dirty = true
            }
        },

        colorChange: function (colorId) {
            const field = colorId.split('-')[1]
            const item = app.ui.prefs.currentNode.data.find(el => el.field === field)
            const value = $('#' + colorId).val()

            if (item.oldValue !== value) {
                item.newValue = value
                item.dirty = true
            }
        },

        selChange: function (selId) {
            const field = selId.split('-')[1]
            const item = app.ui.prefs.currentNode.data.find(el => el.field === field)
            const value = $('#' + selId).find(":selected").val()

            if (item.oldValue !== value) {
                item.newValue = value
                item.dirty = true
            }
        },

        textChange: function (textId) {
            const field = textId.split('-')[1]
            const item = app.ui.prefs.currentNode.data.find(el => el.field === field)
            const value = $('#' + textId).val()

            if (item.oldValue !== value) {
                item.newValue = value
                item.dirty = true
            }

        },
    },

    standardize_color: function (str) {
        if (str.indexOf('var(') > -1) {
            switch (str.split('(')[1]) {
                case '--ydb-orange)': {
                    return '#ff7f27'
                }
                case '--ydb-purple)': {
                    return '#3b1a68'
                }
                case '--ydb-lightgray)': {
                    return '#f3f3f3'
                }
                case '--ydb-darkgray)': {
                    return '#231f20'
                }
                case '--ydb-status-red)': {
                    return '#ce3a3a'
                }
                case '--ydb-status-amber)': {
                    return '#eab83b'
                }
                case '--ydb-status-green)': {
                    return '#64a555'
                }
            }

        } else {
            let ctx = document.createElement('canvas').getContext('2d');
            ctx.fillStyle = str;
            return ctx.fillStyle;
        }
    }
})
