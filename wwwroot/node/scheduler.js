/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const mumpsClient = require('./mumpsClient')
const LOGGER = require('./logger')

const start = (data, ws) => {
    if (hInterval !== null) {
        LOGGER.log('Start command error: Scheduler is already running', LOGGER.ERROR)

        return {
            opCode: 'start',
            status: 'NAK',
            error: {description: 'Scheduler is already running'}
        }
    }

    scheduling = data

    schedule(ws)

    status = 'started'

    LOGGER.log('Start command executed', LOGGER.INFO)

    return {
        opCode: 'start',
        status: 'ACK'
    }
}

const stop = async () => {
    if (hInterval !== null || status === 'paused') {
        clearInterval(hInterval)

        hInterval = null
        scheduling = null

        status = 'stopped'

        LOGGER.log('Stop command executed', LOGGER.INFO)

        try {
            await mumpsClient.execute({opCode: 'eoa'})
        } catch (err) {
        }

        return {
            opCode: 'stop',
            status: 'ACK'
        }

    } else {
        LOGGER.log('Stop command error: Scheduler not running', LOGGER.ERROR)

        return {
            opCode: 'stop',
            status: 'NAK',
            error: {description: 'Scheduler not running'}
        }
    }
}

const pause = () => {
    if (hInterval !== null) {
        clearInterval(hInterval)

        hInterval = null

        status = 'paused'

        LOGGER.log('Pause command executed', LOGGER.INFO)

        return {
            opCode: 'pause',
            status: 'ACK',
        }

    } else {
        LOGGER.log('Pause command error: Scheduler not running', LOGGER.ERROR)

        return {
            opCode: 'pause',
            status: 'NAK',
            error: {description: 'Scheduler not running'}
        }
    }
}

const resume = (ws) => {
    if (hInterval !== null) {
        LOGGER.log('Resume command error: Scheduler is already running', LOGGER.ERROR)

        return {
            opCode: 'resume',
            status: 'NAK',
            error: {description: 'Scheduler is already running'}
        }

    } else if (scheduling === null) {
        LOGGER.log('Resume command error: Scheduler was not started or got stopped', LOGGER.ERROR)

        return {
            opCode: 'resume',
            status: 'NAK',
            error: {description: 'Scheduler was not started or got stopped'}
        }

    } else {
        status = 'started'

        schedule(ws)

        LOGGER.log('Resume command executed', LOGGER.INFO)

        return {
            opCode: 'resume',
            status: 'ACK'
        }
    }
}

const schedule = ws => {
    console.dir(scheduling, {depth: 20})

    // start the timers
    hInterval = setInterval(async () => {
        const mumpsList = []

        tickCounter++

        LOGGER.log('Timer tick', LOGGER.INFO)

        // populate the (current) scheduling array
        scheduling.forEach(entry => {
            if (entry.data.enabled === false) return

            const triggerRate = entry.sampleRate * 10

            // checking if tick matches one of the entries
            let quotientDecimal = (tickCounter / triggerRate).toString().split('.')

            if (quotientDecimal.length === 1) {
                //trigger, push entry in the MUMPS to do array...
                mumpsList.push(entry)
            }
        })

        // execute the Sample collection...
        if (mumpsList.length > 0) {
            LOGGER.log('Calling M server', LOGGER.INFO)

            // submit request to mumps
            try {
                const res = JSON.parse(await mumpsClient.execute({stats: mumpsList}))

                LOGGER.log('Response from M server', LOGGER.INFO)

                // alter the response and send it to the client
                let wsRes = Object.assign(res, {
                    opCode: 'timer',
                    timestamp: Date.now()
                })

                ws.send(JSON.stringify(wsRes))

                LOGGER.log('Response from M server flushed to web socket', LOGGER.INFO)

            } catch (err) {
                LOGGER.log('Internal error while executing mumpsClient.execute', LOGGER.ERROR)

                ws.send(JSON.stringify({
                    opCode: 'timer',
                    status: 'NAK',
                    error: {
                        description: 'Internal error while executing mumpsClient.execute. See  dump for details...',
                        dump: err
                    },
                }))
            }
        }
    }, 100)
}

let hInterval = null
let tickCounter = 0
let scheduling = null
let status = 'stopped'

module.exports.start = start;
module.exports.stop = stop;
module.exports.pause = pause;
module.exports.resume = resume;
module.exports.status = status;
