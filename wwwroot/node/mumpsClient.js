/*
#################################################################
#                                                               #
# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const {spawn} = require('child_process');
const LOGGER = require('./logger')

let child
const connect = (ydb_dist = '') => {
    return new Promise(function (resolve, reject) {
        try {
            LOGGER.log('Starting MUMPS server using base path: ' + ydb_dist, LOGGER.INFO)

            if (ydb_dist === '') {
                // interactive mode
                console.log('Interactive mode...')
                LOGGER.log('Interactive mode', LOGGER.INFO)

                child = spawn('mumps', ['-run', 'start^%ydbguiStatsServer'])
                child.stdout.pipe(process.stdout)

            } else {
                // non-interactive mode (regular mode)
                child = spawn(ydb_dist + 'mumps', ['-run', 'start^%ydbguiStatsServer'], {stdio: ['pipe', 'pipe', 'pipe']})
            }

            resolve()

            LOGGER.log('MUMPS server started', LOGGER.SUCCESS)

        } catch (err) {
            LOGGER.log('Error occurred while stdarting the MUMPS server: ' + err.message, LOGGER.SUCCESS)
            reject(err)
        }
    })
}

// currently not used
const terminate = () => {
    LOGGER.log('Terminating MUMPS server...', LOGGER.INFO)

    try {
        child.kill()

    } catch (err) {
        LOGGER.log('Error occurred while killing the MUMPS process', LOGGER.ERROR)
    }

    LOGGER.log('MUMPS server terminated', LOGGER.SUCCESS)
}

const execute = req => {
    return new Promise(function (resolve, reject) {
        try {
            const __callback = (data) => {
                // convert buffer to string
                let dataAsString = data.toString()
                resolve(dataAsString)

                child.stdout.removeListener('data', __callback)
            }

            child.stdout.on('data', __callback)

            child.stdin.cork()
            child.stdin.write(JSON.stringify(req) + '\n')
            child.stdin.uncork()

        } catch (err) {
            LOGGER.log('Internal error while executing mumpsClient.execute', LOGGER.ERROR)

            reject(JSON.stringify({
                    status: 'NAK',
                    error: {
                        description: 'Internal error while executing mumpsClient.execute. See  dump for details...',
                        dump: err
                    },
                }
            ))
        }
    })
}

module.exports.connect = connect;
module.exports.execute = execute;
module.exports.terminate = terminate;
