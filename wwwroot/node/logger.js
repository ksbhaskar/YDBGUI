/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/
const {appendFile} = require('fs')

const log = (message = '', level = 1, consoleLog = false) => {
    if (consoleLog === true || level === 3 || level === 4) console.log(message)

    if (logging === false) return

    let logStr = new Date().toJSON()
    if (logFile === '') logFile = logDir + 'ydbgui-ws-server-' + Date.now() + '.log'

    switch (level) {
        case 1: {
            logStr += ' INFO   '

            break
        }
        case 2: {
            logStr += ' WARNING'

            break
        }
        case 3: {
            logStr += ' ERROR  '

            break
        }
        case 4: {
            logStr += ' FATAL  '

            break
        }
        case 5: {
            logStr += ' SUCCESS'

        }
    }
    logStr += ' ' + message

    appendFile(logFile, logStr + '\n', () => {})
}

let logFile = ''

const INFO = 1
const WARNING = 2
const ERROR = 3
const FATAL = 4
const SUCCESS = 5

module.exports.log = log;
module.exports.INFO = INFO;
module.exports.WARNING = WARNING;
module.exports.ERROR = ERROR;
module.exports.FATAL = FATAL;
module.exports.SUCCESS = SUCCESS;

