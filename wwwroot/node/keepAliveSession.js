/*
#################################################################
#                                                               #
# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const poolTime = 5000
const waitForResponseTimeout = 5000
const ackStatus = false

const start = function (ws, process, LOGGER) {
    const hInterval = setInterval(() => {
        this.ackStatus = false

        ws.send(JSON.stringify({
            opCode: 'keepAlive'
        }))

        setTimeout(() => {
            if (this.ackStatus === false) {
                LOGGER.log('Didn\'t get a timely response from keepAlive: Terminating server', LOGGER.FATAL)

                setInterval(() => process.exit(), 100)
            }
        }, waitForResponseTimeout)
    }, poolTime)
}

module.exports.start = start
module.exports.ackStatus = ackStatus
