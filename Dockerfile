#################################################################
#                                                               #
# Copyright (c) 2022-2024 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#	This source code contains the intellectual property	        #
#	of its copyright holder(s), and is made available	        #
#	under a license.  If you do not know the terms of	        #
#	the license, please stop and do not read further.	        #
#                                                               #
#################################################################


FROM yottadb/octo

# Extra's to run non-interactive Chrome
RUN apt-get update && apt-get install -y curl unzip wget cmake git gcc make \
			npm libasound2 libnss3-dev libgdk-pixbuf2.0-dev \
			libgtk-3-dev libxss-dev libgconf-2-4 libatk1.0-0 \
			libatk-bridge2.0-0 libgdk-pixbuf2.0-0 libgtk-3-0 \
			libssl-dev libconfig-dev libgcrypt-dev libgpgme-dev libicu-dev libsodium-dev \
			curl libcurl4-openssl-dev

# Initialize files for working directory
WORKDIR /YDBGUI

# Install npm testing packages
COPY package.json /YDBGUI/package.json
RUN npm install

# Install Encryption Plugin
ENV ydb_dist "/opt/yottadb/current"
ENV ydb_icu_version "70"
RUN git clone https://gitlab.com/YottaDB/Util/YDBEncrypt
RUN cd YDBEncrypt && make install

ENV ydb_xc_libcurl "/opt/yottadb/current/plugin/libcurl.xc"

# Create Certificates
RUN mkdir -p /YDBGUI/certs
RUN openssl genrsa -aes128 -passout pass:ydbgui -out /YDBGUI/certs/ydbgui.key 2048
RUN openssl req -new -key /YDBGUI/certs/ydbgui.key -passin pass:ydbgui -subj '/C=US/ST=Pennsylvania/L=Malvern/CN=localhost' -out /YDBGUI/certs/ydbgui.csr
RUN openssl req -x509 -days 365 -sha256 -in /YDBGUI/certs/ydbgui.csr -key /YDBGUI/certs/ydbgui.key -passin pass:ydbgui -out /YDBGUI/certs/ydbgui.pem

# Set-up YottaDB Certificate Config
COPY docker-configuration/ydbgui.ydbcrypt /YDBGUI/certs/
ENV ydb_crypt_config /YDBGUI/certs/ydbgui.ydbcrypt

ENV gtm_lct_stdnull=1
ENV gtm_lvnullsubs=2

# Tell Node to ignore the self-signed certificate
ENV NODE_TLS_REJECT_UNAUTHORIZED 0

# Install GUI
COPY CMakeLists.txt /build/CMakeLists.txt
COPY _ydbgui.manifest.json /build/_ydbgui.manifest.json
COPY routines /build/routines/
COPY wwwroot  /build/wwwroot/
RUN cd /build/ && mkdir build && cd build && cmake .. && make && make install

COPY docker-configuration/docker-startup.sh /YDBGUI/docker-startup.sh
COPY docker-configuration/dev /YDBGUI/dev
# Default environment
RUN echo ". /YDBGUI/dev" >> $HOME/.bashrc
# Mount point directories. Empty by default.
RUN mkdir /YDBGUI/routines /YDBGUI/mwebserver /YDBGUI/objects /YDBGUI/wwwroot
# used by replication to open new browser tabs in the right location
ENV ydbgui_docker "true"

EXPOSE 8089
ENTRYPOINT ["/YDBGUI/docker-startup.sh"]

# to build the image
# docker image build --progress=plain -t ydbgui .

# to run the machine
# docker run --rm --name=ydbgui -p 8089:8089 ydbgui

# to enter development mode
# (passing volumes is optional: but it lets you change the code and see the changes immediately applied on the fly)
# in Linux:   docker run -d --init --name=ydbguidev -p 8089:8089 -p 8090:8090 -p 1337:1337 -v $PWD/wwwroot:/YDBGUI/wwwroot:rw -v $PWD/routines:/YDBGUI/routines:rw -v $HOME/work/gitlab/M-Web-Server/src:/YDBGUI/mwebserver:rw ydbgui server --tlsconfig ydbgui --readwrite --log 1
# in windows: docker run -d --init --name=ydbguidev -p 8089:8089 -p 8090:8090 -p 1337:1337 -v C:\Users\stefa\WebstormProjects\YDBGUI2/wwwroot:/YDBGUI/wwwroot:rw -v C:\Users\stefa\WebstormProjects\YDBGUI2/routines:/YDBGUI/routines:rw ydbgui server --tlsconfig ydbgui --log 1 --readwrite
# to get the user authentication to work, append this: --auth-file /YDBGUI/wwwroot/test/users.json
# to start in utf8, add the following: --env ydb_chset='UTF-8' to the docker run command
# Then, docker exec -it ydbguidev bash

# to not start web server, but just to enter a shell
# docker run --rm -it --name=ydbgui -p 8089:8089 ydbgui shell

# to run the tests (Ctrl-C [maybe twice] to stop)
# In Linux: docker run --rm -v $PWD/routines:/YDBGUI/routines:rw -v $PWD/wwwroot:/YDBGUI/wwwroot:rw ydbgui test
# In Windows: docker run --rm -v C:\Users\stefa\WebstormProjects\YDBGUI2/wwwroot:/YDBGUI/wwwroot:rw -v C:\Users\stefa\WebstormProjects\YDBGUI2/routines:/YDBGUI/routines:rw ydbgui test
