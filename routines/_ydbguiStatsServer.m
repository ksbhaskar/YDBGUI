%ydbguiStatsServer ; Stats Server code
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2023-2024 YottaDB LLC and/or its subsidiaries.  #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	;
	quit
	;
start(test)
	new cmd,jdom,jerr,res,resJson,ix,job,exitWithError,jobRes,level
	set level=$zlevel
	new $etrap
	set $etrap="goto etrap"
	;
	; interactive execution for manual testing
	if $get(test)'="" set cmd=test goto preProcess
	;
readLoop
	set cmd=""
	read cmd:100
	goto:$test=0 readLoop
	;
preProcess
	use $principal
	; new command received, parse the json
	kill jdom,jerr,res
	;
	do decode^%ydbwebjson("cmd","jdom","jerr")
	;
	if $get(jerr(1))'="" do  goto submit
	. set res("status")="NAK"
	. set res("error","description")="Error encountered while parsing the request. See 'dump' for additional information"
	. merge res("error","dump")=jerr
	;
	; detect mping
	if $get(jdom("opCode"))="mping" do  goto submit
	. set res("status")="ACK"
	. set res("data")="mpong"
	. set res("opCode")="mping"
	;
	; detect eoa
	if $get(jdom("opCode"))="eoa" do  halt
	. set res("status")="ACK"
	. set res("data")="eoa"
	. set res("opCode")="eoa"
	. kill resJson,jerr
	. do encode^%ydbwebjson("res","resJson","jerr")
	. set ix="" for  set ix=$order(resJson(ix)) quit:ix=""  write resJson(ix)
	. write !
	. hang .500
	. halt
	;
	; check if we have an array
	if $data(jdom("stats"))<10 do  goto submit
	. set res("status")="NAK"
	. set res("error","description")="The request doesn't have 'stats' as array"
	;
	; start processing the array
	set exitWithError=0
	set ix="" for  set ix=$order(jdom("stats",ix)) quit:ix=""  do  if exitWithError goto jobError
	. kill job,jobRes
	. merge job=jdom("stats",ix)
	. ;
	. ; if no type, error
	. if $get(job("type"))="" set exitWithError=1 quit
	. ;
	. if job("type")="ygbl" do  quit
	. . set *jobRes=$$handlerYgbl(.job)
	. . merge res("data",ix)=jobRes
	. ;
	. if job("type")="fhead" do  quit
	. . set *jobRes=$$handlerFhead(.job)
	. . merge res("data",ix)=jobRes
	. ;
	. if job("type")="peekByName" do  quit
	. . set *jobRes=$$handlerPeekByName(.job)
	. . merge res("data",ix)=jobRes
	. ;
	. ; if no handler found,error
	. set exitWithError=2
	;
	set res("status")="ACK"
	;
submit
	kill resJson,jerr
	do encode^%ydbwebjson("res","resJson","jerr")
	;
	set ix="" for  set ix=$order(resJson(ix)) quit:ix=""  write resJson(ix)
	write !
	;
	goto readLoop
	;
etrap
	set $ecode=""
	kill res
	set res("status")="NAK"
	set res("error","description")="Internal MUMPS error: "_$zstatus
	;
	zgoto level:submit
	;
jobError
	set res("status")="NAK"
	if exitWithError=1 set res("error","description")="Error processing job: "_ix_". Type not found"
	if exitWithError=2 set res("error","description")="Error processing job: "_ix_". Type not supported"
	;
	goto submit
	;
	;
;****************************************************************
; handlerYgbl
;
; This is the handler to execute %ygbl operations
;****************************************************************
handlerYgbl(job)
	new jobRes,samples,regions,processes,ix,exit,sample,region,cnt,pid
	new result,process,iy,array,interResult,interSampleValue,iz
	;
	; extract the samples
	set exit=0,samples=""
	set ix="" for  set ix=$order(job("data","samples",ix)) quit:ix=""!(exit)  do
	. set sample=job("data","samples",ix)
	. if sample="*" set samples="*",exit=1 quit
	. set samples=samples_sample_","
	set samples=$extract(samples,1,$zlength(samples)-1)
	;
	; extract the regions
	set (cnt,exit)=0
	set ix="" for  set ix=$order(job("data","regions",ix)) quit:ix=""!(exit)  do
	. set region=job("data","regions",ix)
	. if region="*" set regions="*",exit=1 quit
	. set regions(region)=""
	;
	; get currently registered processes
	if job("data","processes","type")="agg" do	; this section is for AGG
	. if $get(regions)="*" set processes(0)="*"
	. else  do
	. . set region="" for  set region=$order(regions(region)) quit:region=""  do
	. . . set regions(region,"processes","*")=""
	else  do	; this section is for TOP or PIDS
	. if job("data","processes","type")="top" do	; TOP
	. . if $get(regions)="*" do
	. . . set pid="" for  set pid=$$ORDERPID^%YGBLSTAT(pid) quit:pid=""  set processes(pid)=""
	. . else  do
	. . . set region="" for  set region=$order(regions(region)) quit:region=""  do
	. . . . set pid="" for  set pid=$$ORDERPID^%YGBLSTAT(pid,,region) quit:pid=""  set regions(region,"processes",pid)=""
	. else  do	; PIDS
	. . if $get(regions)="*" do
	. . . set ix="" for  set ix=$order(job("data","processes","pids",ix)) quit:ix=""  set processes(job("data","processes","pids",ix))=""
	. . else  do
	. . . set region="" for  set region=$order(regions(region)) quit:region=""  do
	. . . . set ix="" for  set ix=$order(job("data","processes","pids",ix)) quit:ix=""  set regions(region,"processes",job("data","processes","pids",ix))=""
	;
	set jobRes("type")="ygbl"
	set jobRes("ix")=$get(job("ix"))
	set jobRes("id")=$get(job("id"))
	;
	; get samples data
	if $get(regions)="*" do
	. ; *******************
	. ; process ALL regions
	. ; *******************
	. if $get(processes(0))="*" do
	. . ; aggregate
	. . set processes(0,"result")=$$STAT^%YGBLSTAT("*",samples)
	. . set *array=$$samplesToArray(processes(0,"result"),samples)
	. . merge:$data(array) jobRes("data","regions","*","pids","*")=array
	. . ;
	. else  do
	. . kill top
	. . set process="" for  set process=$order(processes(process)) quit:process=""  do
	. . . set processes(process,"result")=$$STAT^%YGBLSTAT(process,samples)
	. . ; here we need to fork between sep and top
	. . if job("data","processes","type")="top" do
	. . . ; top
	. . . ; sort first the values
	. . . set process="" for  set process=$order(processes(process)) quit:process=""  do:$data(processes(process,"result"))
	. . . . ; ******************** extract the samples
	. . . . kill interResult
	. . . . set *interResult=$$samplesToArray(processes(process,"result"),samples)
	. . . . set iy="" for  set iy=$order(interResult(iy)) quit:iy=""  do
	. . . . . set iz=$order(interResult(iy,""))
	. . . . . if iz=job("data","processes","sampleCount") set interSampleValue=interResult(iy,iz)
	. . . . merge top(interSampleValue,process)=interResult
	. . . ; then get the top values
	. . . set iy=""
	. . . for ix=1:1:job("data","processes","count") do
	. . . . set iy=$order(top(iy),-1)
	. . . . set:iy>0 process=$order(top(iy,""))
	. . . . if process'=""&(+iy>0) merge jobRes("data","regions","*","pids","P"_process)=top(iy,process)
	. . . ;
	. . else  do
	. . . ; individual
	. . . set process="" for  set process=$order(processes(process)) quit:process=""  do:$data(processes(process,"result"))
	. . . . kill array
	. . . . set *array=$$samplesToArray(processes(process,"result"),samples)
	. . . . merge:$data(array) jobRes("data","regions","*","pids","P"_process)=array
	. ;
	else  do
	. ; **************************
	. ; process individual regions
	. ; **************************
	. set region="" for  set region=$order(regions(region)) quit:region=""  do
	. . if $data(regions(region,"processes","*")) do
	. . . ; aggregate
	. . . set *array=$$samplesToArray($$STAT^%YGBLSTAT("*",samples,,region),samples)
	. . . merge:$data(array) jobRes("data","regions",region,"pids","*")=array
	. . . ;
	. . else  do
	. . . ;
	. . . set process="" for  set process=$order(regions(region,"processes",process)) quit:process=""  do
	. . . . set regions(region,"processes",process,"result")=$$STAT^%YGBLSTAT(process,samples,,region)
	. . . ;
	. . . ; ******************** extract the samples
	. . . kill top
	. . . if job("data","processes","type")="top" do
	. . . . ; top
	. . . . set process="" for  set process=$order(regions(region,"processes",process)) quit:process=""  do:$data(regions(region,"processes",process,"result"))
	. . . . . kill interResult
	. . . . . set *interResult=$$samplesToArray(regions(region,"processes",process,"result"),samples)
	. . . . . set iy="" for  set iy=$order(interResult(iy)) quit:iy=""  do
	. . . . . . set iz=$order(interResult(iy,""))
	. . . . . . if iz=job("data","processes","sampleCount") set interSampleValue=interResult(iy,iz)
	. . . . . merge top(interSampleValue,process)=interResult
	. . . . ; then get the top values
	. . . . set iy=""
	. . . . for ix=1:1:job("data","processes","count") do
	. . . . . set iy=$order(top(iy),-1)
	. . . . . set:iy>0 process=$order(top(iy,""))
	. . . . . if process'=""&(+iy>0) merge jobRes("data","regions",region,"pids","P"_process)=top(iy,process)
	. . . ;
	. . . ; individual
	. . . else  do
	. . . . set process="" for  set process=$order(regions(region,"processes",process)) quit:process=""  do:$data(regions(region,"processes",process,"result"))
	. . . . . set *array=$$samplesToArray(regions(region,"processes",process,"result"),samples)
	. . . . . merge:$data(array) jobRes("data","regions",region,"pids","P"_process)=array
	;
	set jobRes("status")="ACK"
	;
	quit *jobRes
	;
samplesToArray(result,samples)
	new array,ix
	;
	if $find(samples,",")=0,samples'="*" do  goto samplesToArrayQuit
	. set array(1,samples)=+result
	;
	if result="" goto samplesToArrayQuit
	;
	set *array=$$SPLIT^%MPIECE(result,",")
	set ix="" for  set ix=$order(array(ix)) quit:ix=""  do
	. set array(ix,$zpiece(array(ix),":"))=$zpiece(array(ix),":",2)
	. zkill array(ix)
	;
samplesToArrayQuit
	quit *array
	;
	;
;****************************************************************
; handlerFhead
;
; This is the handler to execute fhead operations
;****************************************************************
handlerFhead(job)
	new level,ix,fields,jobRes
	;
	new $etrap
	set $etrap="goto handlerFheadError"
	;
	set level=$zlevel
	;
	set jobRes("type")="fhead"
	set jobRes("ix")=$get(job("ix"))
	set jobRes("id")=$get(job("id"))
	;
	; Perform the fetch
	do getfields^%DUMPFHEAD(.fields,job("data","file"))
	;
	set ix="" for  set ix=$order(job("data","samples",ix)) quit:ix=""  do
	. set jobRes("data","regions",job("data","regions",1),"pids","*",ix,job("data","samples",ix))=$$FUNC^%HD(fields(job("data","samples",ix)))
	. set jobRes("data","regions",job("data","regions",1),"pids","*",ix,"pid")="*"
	;
	set jobRes("status")="ACK"
	;
handlerFheadQuit
	quit *jobRes
	;
handlerFheadError
	set $ecode=""
	;
	set jobRes("status")="NAK"
	set jobRes("error","description")="Internal MUMPS error: "_$zstatus
	;
	zgoto level:handlerFheadQuit
	;
	;
;****************************************************************
; handlerPeekByName
;
; This is the handler to execute peekByName operations
;****************************************************************
handlerPeekByName(job)	
	new ix,fields,jobRes
	;
	new $etrap
	set $etrap="goto handlerPeekByNameError"
	;
	set jobRes("type")="peekByName"
	set jobRes("ix")=$get(job("ix"))
	set jobRes("id")=$get(job("id"))
	;
	set job("data","regions",1)=$$FUNC^%UCASE(job("data","regions",1))
	set ix="" for  set ix=$order(job("data","samples",ix)) quit:ix=""  do
	. set jobRes("data","regions",job("data","regions",1),"pids","*",ix,job("data","samples",ix))=$$FUNC^%HD($$^%PEEKBYNAME(job("data","samples",ix),job("data","regions",1)))
	. ; adjust total if field is # of users to remove this instance
	. set:job("data","samples",ix)="node_local.ref_cnt" jobRes("data","regions",job("data","regions",1),"pids","*",ix,job("data","samples",ix))=jobRes("data","regions",job("data","regions",1),"pids","*",ix,job("data","samples",ix))-1
	. set jobRes("data","regions",job("data","regions",1),"pids","*",ix,"pid")="*"
	; 
	set jobRes("status")="ACK"
	;
handlerPeekByNameQuit
	quit *jobRes
	;
handlerPeekByNameError	
	set $ecode=""
	;
	set jobRes("status")="NAK"
	set jobRes("error","description")="Internal MUMPS error: "_$zstatus
	;
	zgoto level:handlerPeekByNameQuit
	;
	;
;****************************************************************
; ygblGetPids
;
; Returns the PIDs for the specified regions in the format:
; data.[region].processes.[pid1, pid2, ...]
;****************************************************************
ygblGetPids(regions)
	new res,processes,pid,ix
	;
	if regions(1)="*" do
	. set pid="" for  set pid=$$ORDERPID^%YGBLSTAT(pid) quit:pid=""  set res("data","processes",pid)=pid
	else  do
	. set ix="" for  set ix=$order(regions(ix)) quit:ix=""  do
	. . set pid="" for  set pid=$$ORDERPID^%YGBLSTAT(pid,,regions(ix)) quit:pid=""  set res("data","processes",pid)=pid
	;
	set res("result")="OK"
	;
	quit *res
	;
	;
