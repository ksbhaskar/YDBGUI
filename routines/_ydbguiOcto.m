%ydbguiOcto ; Octo code
	;#################################################################
	;#                                                               #
	;# Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.       #
	;# All rights reserved.                                          #
	;#                                                               #
	;#   This source code contains the intellectual property         #
	;#   of its copyright holder(s), and is made available           #
	;#   under a license.  If you do not know the terms of           #
	;#   the license, please stop and do not read further.           #
	;#                                                               #
	;#################################################################
	;
	;
	quit
	;
; ****************************************************************
; detect
; ****************************************************************
detect()
	new basePath,ret,res,fileList,file,notFound,data,parsedData
	new confPath,lineCnt,strippedConf,line,tmp,%ZR,shellData,lineIx,hSocket
	new shellRet,roctoPid,file,fileData,fileCnt,buffer,foundFlag,net,ip,port
	;
	; init
	set basePath=$ztrnlnm("ydb_dist")_"/"
	set fileList("plugin/o/_ydbocto.so")=""
	set fileList("plugin/octo/bin/octo")=""
	set fileList("plugin/octo/bin/rocto")=""
	set fileList("plugin/bin/octo")=""
	set fileList("plugin/bin/rocto")=""
	set fileList("plugin/octo/octo.conf")=""
	;
	; search for files and validate
	set file="",notFound=0
	for  set file=$order(fileList(file)) quit:file=""  do
	. set ret=$zsearch(basePath_file,-1)'=""
	. set res("files",file)=ret
	. if ret=0 set notFound=1
	if notFound set res("error")="Missing files",res("status")="error" goto detectQuit
	;
	; merge raw configuration files
	; the result is not used in the client
	for confPath=basePath_"plugin/octo/octo.conf","~/octo.conf","./octo.conf" do
	. kill data,parsedData
	. if $$readConfig(confPath,.data) do
	. . merge res("configuragtion",confPath,"raw")=data
	. . do extractConfig(.data,.parsedData)
	. . merge res("configuragtion",confPath,"parsed")=parsedData
	. else  set res("configuragtion",confPath)=""
	;
	; ensure that the %ydbocto.so is linked
	do SILENT^%RSEL("%ydboctoInit","OBJ")
	if +%ZR=0 set res("error")="%ydbocto library not linked...",res("status")="error" goto detectQuit
	;
	; perform full test by running octo
	set *tmp=$$execute("select 1;",5000)
	if $find($get(tmp("data",1)),"[ERROR]") set res("error")="Internal error",res("status")="error" goto detectQuit
	;
	set res("status")="ok"
	;
	; find the PID
	kill shellRet
	set ret=$$runShell^%ydbguiUtils("pgrep rocto",.shellRet)
	;
	if ret=1 do  goto detectQuit
	. set res("rocto","status")="stopped"
	;
	if ret'=0 do  goto detectQuit
	. set res("rocto","status")="internal error"
	. set res("rocto","error")="Got: "_ret_" while fetching the PID"
	;
	set roctoPid=$get(shellRet(1))
	;
	; double check the PID returned
	if +roctoPid=0 do  goto detectQuit
	. set res("rocto","status")="stopped"
	;
	; validate the process
	new $etrap
	set $etrap="goto roctoError"
	;
	set file="/proc/"_roctoPid_"/maps"
	open file:readonly
	use file
	for  read buffer quit:$zeof  set fileData($increment(fileCnt))=buffer
	close file
	set fileCnt="",foundFlag=0
	for  set fileCnt=$order(fileData(fileCnt)) quit:fileCnt=""!(foundFlag)  if $find(fileData(fileCnt),"_ydbocto.so") set foundFlag=1
	if foundFlag=0 do  goto detectQuit
	. set res("rocto","status")="error"
	. set res("rocto","error")="Process in not a rocto process"
	;
	; ensure it is listening
	;
	; first find the socket handle
	set ret=$$runShell^%ydbguiUtils("ls /proc/"_roctoPid_"/fd -l",.shellData)
	if ret'=0 goto roctoError
	set (lineIx,hSocket)="" for  set lineIx=$order(shellData(lineIx)) quit:lineIx=""  do
	. if $find(shellData(lineIx),"socket") set hSocket=$piece(shellData(lineIx),"[",2),hSocket=$extract(hSocket,1,$length(hSocket)-1)
	if +hSocket=0 goto roctoError
	;
	; then look up the handle in the tcp file
	set file="/proc/"_roctoPid_"/net/tcp"
	kill fileData
	open file:readonly
	use file
	for  quit:$zeof  read fileData if $find(fileData,hSocket) set fileData("found")=fileData 
	close file
	if $data(fileData)<10 goto roctoError
	;
	; and parse out IP and port
	set fileData=$$strRemoveExtraSpaces^%ydbguiUtils(fileData("found"))
	set net=$zpiece(fileData," ",3)
	set ip=$zpiece(net,":")
	set ip=$$FUNC^%HD($zextract(ip,7,8))_"."_$$FUNC^%HD($zextract(ip,5,6))_"."_$$FUNC^%HD($zextract(ip,3,4))_"."_$$FUNC^%HD($zextract(ip,1,2))
	set port=$$FUNC^%HD($zpiece(net,":",2))
	set res("rocto","port")=port
	set res("rocto","ip")=ip
	;
	; extract the parameters
	set file="/proc/"_roctoPid_"/cmdline"
	open file:readonly
	use file
	read fileData
	close file
	set fileData=$ztranslate(fileData,$char(0),"")
	set fileData=$zextract(fileData,$find(fileData,"rocto"),99)
	;
	set res("rocto","params")=fileData
	;
	; Rocto is running
	set res("rocto","status")="running"
	;
detectQuit
	quit *res
	;
roctoError	
	set res("rocto","params")="N/A"
	set res("rocto","port")="N/A"
	set res("rocto","ip")="N/A"
	set $ecode=""
	;
	goto detectQuit
	;
	; the result of this routine is not used in the client, but the code kept for future use
readConfig(file,data)
	if $zsearch(file,-1)="" quit 0
	;
	new line,lineCnt,res
	open file:readonly
	use file
	for  read line quit:$zeof  set data($increment(lineCnt))=line
	close file
	;
	quit 1
	;
	;
	; the result of this routine is not used in the client, but the code kept for future use
extractConfig(data,parsed)
		new lineCnt,commentOpen,line,lineL,parCnt
		;
	set lineCnt="",commentOpen=0
	for  set lineCnt=$order(data(lineCnt)) quit:lineCnt=""  do
	. set line=data(lineCnt)
	. set lineL=$$L^%TRIM(line)
	. if line="" quit
	. if $extract(lineL,1,2)="//" quit
	. if $extract(lineL,1,2)="/*" set commentOpen=1 quit
	. if $find(lineL,"*/") set commentOpen=0 quit
	. if commentOpen quit
	. set parsed($increment(parCnt))=line
	;
	quit
	;
	;
; ****************************************************************
; execute
; ****************************************************************
execute(sqlCommand,timeout)
	new res,device,currentdevice,counter,shellResult,timeoutElapsed,octoCommand,file,tmpDir
	;
	set counter=0
	set currentdevice=$io
	set device="runshellcommmandpipe"_$job
	set timeout=timeout/1000
	set timeoutElapsed=0
	if $extract(sqlCommand,$length(sqlCommand))'=";" set sqlCommand=sqlCommand_";"
	;
	; create tmp file
	set tmpDir=$ztrnlnm("ydb_tmp")
	if tmpDir="" set tmpDir="/tmp/"
	set file=tmpDir_"/"_$job_".sql"
	open file:newversion
	use file write sqlCommand
	close file
	;
	open device:(shell="/bin/sh":command="$ydb_dist/plugin/bin/octo -f "_file)::"pipe"
	use device
	read string:timeout
	if '$test set timeoutElapsed=1 goto terminateShell
	set shellResult($increment(counter))=string
	;
	for  quit:$zeof  read string:.050 set shellResult($increment(counter))=string quit:'$test
terminateShell
	close device
	if $get(shellResult(counter))="" kill shellResult(counter)
	if $get(shellResult(1))="" kill shellResult(1)
	use currentdevice
	set ret=$zclose
	;
	if timeoutElapsed do  goto executeQuit
	. set res("result")="ERROR"
	. set res("error","description")="Timeout while executing the query"
	;
	set res("parseError")="false"
	if ret=1 set res("parseError")="true"
	;
	if ret<0 do  goto executeQuit
	. set res("result")="ERROR"
	. set res("error","description")="Got: "_ret_" while executing 'octo'"
	. merge res("error","dump")=shellResult
	;
	merge res("data")=shellResult
	set res("result")="OK"
	;
executeQuit
	open file
	close file:delete
	;
	quit *res
	;
	;
; ****************************************************************
; getObjects
; ****************************************************************
getObjects()
	new res,tablesTmp,tableTmp,ix,table,resIx,tableObj,timeout
	new fields,fieldIx,field,index,indexesFound,indexIx,indexes,indexMore
	new functionDefs,functionsData,qryFunctionDefs,qryFunctionsData
	new recordIx,record,args,argsIx,newArgs
	new view,viewObj,viewsIx
	;
	set timeout=5000
	set *tablesTmp=$$execute("\d;",timeout)
	;
	for ix=2:1:$order(tablesTmp("data",""),-1)-1 do
	. if $piece(tablesTmp("data",ix),"|",3)="table" do
	. . set table=$piece(tablesTmp("data",ix),"|",2)
	. . kill tableObj
	. . set tableObj("name")=table
	. . set res("data","tables",$increment(resIx),"name")=table
	. ;
	. else  if $piece(tablesTmp("data",ix),"|",3)="view" do
	. . set view=$piece(tablesTmp("data",ix),"|",2)
	. . kill viewObj
	. . set viewObj("name")=view
	. . set res("data","views",$increment(viewsIx),"name")=view
	;
	; get functions information
	set cmd="select oid,typname from pg_catalog.pg_type;"
	set *qryFunctionDefs=$$execute(cmd,timeout)
	for ix=2:1:$order(qryFunctionDefs("data",""),-1)-1 do  
	. set field=qryFunctionDefs("data",ix)
	. set functionDefs($piece(field,"|"))=$piece(field,"|",2)
	;
	set cmd="select proname,pronargs,typname,proargtypes,prosrc from pg_catalog.pg_proc inner join pg_catalog.pg_type on pg_catalog.pg_proc.prorettype = pg_catalog.pg_type.oid order by PRONAME;"
	set *qryFunctionsData=$$execute(cmd,timeout)
	for ix=2:1:$order(qryFunctionsData("data",""),-1)-1 do  
	. set field=qryFunctionsData("data",ix)
	. ;
	. set record("name")=$piece(field,"|")
	. set record("numArgs")=$piece(field,"|",2)
	. set record("returnType")=$piece(field,"|",3)
	. set *args=$$SPLIT^%MPIECE($piece(field,"|",4)," ")
	. kill newArgs
	. ; looking up the argument data type and replace it with the value found in pg_catalog.pg_type
	. for argsIx=1:1:record("numArgs") set newArgs(argsIx)=functionDefs(args(argsIx))
	. merge record("args")=newArgs
	. set record("source")=$piece(field,"|",5)
	. merge functionData($increment(recordIx))=record
	;
	merge res("data","functions")=functionData
	;
	set res("result")="OK"
	;
	quit *res
	;
	;
; ****************************************************************
; getTable
; ****************************************************************
getTable(table)	
	new res,cmd,timeout,tablesTmp,tableTmp,ix,resIx,tableObj
	new fields,fieldIx,field,index,indexesFound,indexIx,indexes,indexMore
	new recordIx,record,args,argsIx,newArgs
	;
	set timeout=500
	set indexesFound=0
	set tableObj("name")=table
	;
	set cmd="\d "_table_";"
	set *tableTmp=$$execute(cmd,timeout)
	;
	if $data(tableTmp)=0!($find($get(tableTmp(1)),"UNKNOWN_TABLE")) do  goto getTableQuit
	. set res("result")="ERROR"
	. set res("error","description")="Table does not exist"
	;
	set tableObj("global")=$$FUNC^%TRIM($piece(tableTmp("data",1),":",2))
	set tableObj("type")=$$FUNC^%TRIM($piece($piece(tableTmp("data",1),":",3),"=",2))
	kill fields,indexes
	for fieldIx=3:1:$order(tableTmp("data",""),-1) do
	. set field=tableTmp("data",fieldIx)
	. if field="Indexes:" set indexesFound=1,indexIx=0 quit
	. ;
	. if indexesFound=0 do
	. . set fields(fieldIx,"name")=$piece(field,"|",1)
	. . set fields(fieldIx,"type")=$piece(field,"|",2)
	. . set fields(fieldIx,"collation")=$piece(field,"|",3)
	. . set fields(fieldIx,"nullable")=$piece(field,"|",4)
	. . set fields(fieldIx,"default")=$piece(field,"|",5)
	. else  do
	. . set indexIx=indexIx+1
	. . set indexes(indexIx,"name")=$piece(field,"""",2)
	. . set indexMore=$piece(field,"""",3)
	. . set indexes(indexIx,"type")=$$FUNC^%TRIM($piece(indexMore,","))
	. . set indexes(indexIx,"global")=$$FUNC^%TRIM($piece(indexMore,":",2))
	. . set indexes(indexIx,"columns")=$piece($piece($piece(indexMore,"Column(s)",2),"(",2),")")
	;
	merge tableObj("columns")=fields
	merge tableObj("indexes")=indexes
	;
	merge res("data")=tableObj
	;
	set res("result")="OK"
	;
getTableQuit	
	quit *res
	;
	;
; ****************************************************************
; getView
; ****************************************************************
getView(view)	
	new res,cmd,timeout,viewsTmp,viewTmp,ix,resIx,viewObj
	new fields,fieldIx,field,def,defFound
	new recordIx,record,args,argsIx,newArgs
	;
	set timeout=500
	set defFound=0
	set viewObj("name")=view
	;
	set cmd="\d "_view_";"
	set *tableTmp=$$execute(cmd,timeout)
	;
	if $data(tableTmp)=0!($find($get(tableTmp(1)),"UNKNOWN_TABLE")) do  goto getTableQuit
	. set res("result")="ERROR"
	. set res("error","description")="View does not exist"
	;
	m ^stef($zut)=tableTmp
	kill fields,def
	set viewObj("def")=""
	for fieldIx=3:1:$order(tableTmp("data",""),-1) do
	. set field=tableTmp("data",fieldIx)
	. if field="View definition:" set defFound=1,indexIx=0 quit
	. ;
	. if defFound=0 do
	. . set fields(fieldIx,"name")=$piece(field,"|",1)
	. . set fields(fieldIx,"type")=$piece(field,"|",2)
	. . set fields(fieldIx,"collation")=$piece(field,"|",3)
	. . set fields(fieldIx,"nullable")=$piece(field,"|",4)
	. . set fields(fieldIx,"default")=$piece(field,"|",5)
	. else  set viewObj("def")=viewObj("def")_field_"<br>"
	;
	merge viewObj("columns")=fields
	;
	merge res("data")=viewObj
	;
	set res("result")="OK"
	;
getViewQuit	
	quit *res
