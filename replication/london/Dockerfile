#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

ARG profile
FROM ydbgui AS base

ENV ydb_repl_instance=$ydb_dir/london.repl
ENV ydb_repl_instname=london

# Create Certificates
RUN mkdir -p /YDBGUI/certs
RUN openssl genrsa -aes128 -passout pass:ydbgui -out /YDBGUI/certs/ydbgui.key 2048
RUN openssl req -new -key /YDBGUI/certs/ydbgui.key -passin pass:ydbgui -subj '/C=US/ST=Pennsylvania/L=Malvern/CN=localhost' -out /YDBGUI/certs/ydbgui.csr
RUN openssl req -x509 -days 365 -sha256 -in /YDBGUI/certs/ydbgui.csr -key /YDBGUI/certs/ydbgui.key -passin pass:ydbgui -out /YDBGUI/certs/ydbgui.pem

# change host name
RUN echo "london" /etc/hostname
ENV HOSTNAME="london"

# create instance
RUN $ydb_dist/mupip replicate -instance_create -name=london

##################
# bc7
##################
FROM base as layer-bc7
COPY replication/london/repl-startup.sh /repl/repl-startup.sh


##################
# bc2bc4
##################
FROM base as layer-bc2bc4
COPY replication/london/repl-startup.sh /repl/repl-startup.sh

##################
# bc2si4
##################
FROM base as layer-bc2si4
COPY replication/london/repl-startup-bc2si4.sh /repl/repl-startup.sh
COPY replication/london/repl-startup-bc2si4-after.sh /repl/repl-startup.sh.after

# create instance
RUN $ydb_dist/mupip replicate -instance_create -name=london -supplementary

#############################
#############################
#############################
FROM layer-$profile AS final

COPY replication/common/repl-shutdown.sh /repl/shutdown.sh

# startup comes from the ydbgui machine
# ENTRYPOINT ["/YDBGUI/docker-startup.sh"]
