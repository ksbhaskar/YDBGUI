#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

FROM ydbgui

ENV ydb_repl_instance=$ydb_dir/madrid.repl
ENV ydb_repl_instname=madrid

# Create Certificates
RUN mkdir -p /YDBGUI/certs
RUN openssl genrsa -aes128 -passout pass:ydbgui -out /YDBGUI/certs/ydbgui.key 2048
RUN openssl req -new -key /YDBGUI/certs/ydbgui.key -passin pass:ydbgui -subj '/C=US/ST=Pennsylvania/L=Malvern/CN=localhost' -out /YDBGUI/certs/ydbgui.csr
RUN openssl req -x509 -days 365 -sha256 -in /YDBGUI/certs/ydbgui.csr -key /YDBGUI/certs/ydbgui.key -passin pass:ydbgui -out /YDBGUI/certs/ydbgui.pem

RUN mkdir /repl
COPY replication/madrid/repl-startup.sh /repl/repl-startup.sh

# change host name
RUN echo "madrid" /etc/hostname
ENV HOSTNAME="madrid"

# create instance
RUN $ydb_dist/mupip replicate -instance_create -name=madrid

COPY replication/common/repl-shutdown.sh /repl/shutdown.sh

# startup comes from the ydbgui machine
# ENTRYPOINT ["/YDBGUI/docker-startup.sh"]
