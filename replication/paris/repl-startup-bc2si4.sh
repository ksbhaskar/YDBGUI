#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

cp /YDBGUI/certs/ydbgui.pem /repl/backups/paris.ydbgui.pem

$ydb_dist/mupip set -replication=on -region "*"
sleep 1

rm /repl/backups/*.repl

date=`date +%Y%m%d:%H:%M:%S`
$ydb_dist/mupip replicate -source -start -passive -instsecondary=dummy -log=/tmp/dummy"$date".log

$ydb_dist/mupip replicate -receive -start -listenport=3001 -log=/tmp/paris"$date".log
sleep 1
echo '*********************'
echo 'Receive checkhealth'
echo '*********************'
$ydb_dist/mupip replicate -receive -checkhealth

echo '*********************'
echo 'paris log'
echo '*********************'
tail -30 /tmp/paris"$date".log

# sender to Rome
mupip replicate -source -start -propagateprimary -instsecondary=rome -secondary=rome:3003 -log=/tmp/rome"$date".log
$ydb_dist/mupip replicate -receive -checkhealth

# sender to amsterdam
mupip replicate -source -start -propagateprimary -instsecondary=amsterdam -secondary=amsterdam:3004 -log=/tmp/amsterdam"$date".log
$ydb_dist/mupip replicate -receive -checkhealth

# sender to london
mupip replicate -source -start -propagateprimary -instsecondary=london -secondary=london:3005 -log=/tmp/london"$date".log
$ydb_dist/mupip replicate -receive -checkhealth

# sender to tokio
mupip replicate -source -start -propagateprimary -instsecondary=tokio -secondary=tokio:3006 -log=/tmp/tokio"$date".log
$ydb_dist/mupip replicate -receive -checkhealth
sleep 2
echo '*********************'
echo 'Source checkhealth'
echo '*********************'
$ydb_dist/mupip replicate -source -checkhealth

echo '*********************'
echo 'Dummy log'
echo '*********************'
tail -30 /tmp/dummy"$date".log

echo '*********************'
echo 'rome log'
echo '*********************'
tail -30 /tmp/rome"$date".log

echo '*********************'
echo 'amsterdam log'
echo '*********************'
tail -30 /tmp/amsterdam"$date".log

echo '*********************'
echo 'london log'
echo '*********************'
tail -30 /tmp/london"$date".log

echo '*********************'
echo 'tokio log'
echo '*********************'
tail -30 /tmp/tokio"$date".log

rm /repl/backups/*.repl
$ydb_dist/mupip backup -replinst=/repl/backups/paris.repl
