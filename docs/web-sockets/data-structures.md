<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2023 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

# Statistics

### Commands and data structures

#### Web Sockets

In the Web Sockets, communication is typically initiated by the client, beside when data is being collected, which is
triggered by a timer in the server.

This is initiated by the client

````js
const req: {
    opCode: 'start',                        // start | stop | pause | resume | ping (for testing)
    // (only start requires the sources node)
    data:   [
        {
            type: 'ygbl',                   // ygbl 
            sampleRate: 1.5,                // the sample rate, in seconds (FP)
            ix: 0,                          // the index in the original array in the client, to be routed back
                                            // in the response to match the original request node
            data: {
                enabled: true,              // only items with this property set to true will be sent to the MUMPS server
                dataType: 'delta',          // delta ! abs
                processes: {
                    type: 'agg',            // agg > All processes (aggregate)
                                            // sep > All processes (separate)
                                            // top (if top is selected, only one sample can be used)
                    count: 0                // only if type = top
                },
                regions: [
                    'DEFAULT',              // can be * (all others will be ignored)
                    'TEMP'
                ],
                samples: [
                    'LKF',                  // can be * (all others will be ignored)
                    'SET',
                    'KIL'
                ]
            },
        },
    ]
}

````

**OP CODES** and related responses

`start`

It will start the stats collection.

This opCode is the only requiring a `data` array, which contains a list of statistics tasks to be scheduled.

This operation will:

- Schedule all the entries inside the `data` array
- Start the timers (to collect data at the provided sample rate(s))
- ACKnowledge the request

If another instance of Statistics is already running, it will return NAK and fill the error description as appropriate.

---

`stop`

It stops the current stats collection.

If there is no stats collection in progress, it will return NAK and error description

If there is a stats collection in progress, it will immediately

- stop the timer
- clear the timer
- clean up all the scheduling
- return ACK

````js
res = {
    opCode: 'stop',                         // the originator opCode
    status: 'ACK',                          // can be ACK or NAK
    error: {                                // set if NAK status has been sent
        description: ''
    },
    data: [                                 // populated when status is ACK
    ]
}

````

---

`pause`

It will pause the current data collection.

If there is no stats collection in progress, it will return NAK and error description

If there is a stats collection in progress, it will simply stop the timers, while keeping the schedule in memory, and
return ACK.

````js
res = {
    opCode: 'pause',                        // the originator opCode
    status: 'ACK',                          // can be ACK or NAK
    error: {                                // set if NAK status has been sent
        description: ''
    },
}
````

---

`resume`

It resumes a paused operation

If there is no stats collection in progress, it will return NAK and error description

If there is no stats collection in pause, it will return NAK and error description

Otherwise, it will restart the timers (keeping the previous schedule) and return ACK.

> NOTE: the `timestamp` field will still represent the correct date and time

````js
res = {
    opCode: 'resume',                       // the originator opCode
    status: 'ACK',                          // can be ACK or NAK
    error: {                                // set if NAK status has been sent
        description: ''
    },
}
````

---

`ping`

Used for testing, it will return `pong` in the `data` section

````js
res = {
    opCode: 'ping',                         // the originator opCode
    status: 'ACK',                          // can be ACK or NAK
    data: 'pong'
}

````

---


**Collected data samples** responses

````js
res = {
    opCode: 'timer',                        // opCode generated by the scheduler
    result: 'ACK',                          // can be ACK or NAK
    error: {                                // set if NAK status has been sent
        description: ''
    },
    data: []                                // array of samples
}
````

The `data` node is always an array and may have the following structures:


<!--TODO-->

#### MUMPS Stats Server

The MUMPS Stats Server requests is a subset of the Web Socket Server entries, filtered by enabled (only enabled = true
will be sent to the socket server).

````js
const jobList = {
    stats: [
        {
            type: 'ygbl',                   // ygbl 
            sampleRate: 1.5,                // the sample rate, in seconds (FP)
            ix: 0,                          // the index in the original array in the client, to be routed back
                                            // in the response to match the original request node
            data: {
                enabled: true,              // this is always true here
                dataType: 'delta',          // delta ! abs
                processes: {
                    type: 'agg',            // agg > All processes (aggregate)
                                            // sep > All processes (separate)
                                            // top (if top is selected, only one sample can be used)
                    count: 0                // only if type = top
                },
                regions: [
                    'DEFAULT',              // can be * (all others will be ignored)
                    'TEMP'
                ],
                samples: [
                    'LKF',                  // can be * (all others will be ignored)
                    'SET',
                    'KIL'
                ]
            },
        },
        {},
        {}
    ]
}
````

The responses body is:

````js
const res = [
    {
        status: 'ACK',                          // ACK or NAK (then error is populated)
        error: {
            description: ''                     // It supports also M errors ($zstatus)
        },
        data: [
            {}                                  // see ygbl section for details of the returned package
        ]
    }
]
````
